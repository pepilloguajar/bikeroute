/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.talentum.sightracksteam.io.file.exporter;



import java.io.OutputStream;
import java.util.List;

import com.talentum.sightracksteam.model.Segment;
import com.talentum.sightracksteam.model.UserWarning;

public class FileSegmentExporter {


	private final List<Segment> segments;
	private final List<UserWarning> warnings;
	private final SegmentWriter segmentWriter;
	private final String name;

	public FileSegmentExporter(List<Segment> segments, SegmentWriter segmentWriter, String name, List<UserWarning> warnings) {
		this.segments = segments;
		this.warnings = warnings;
		this.segmentWriter = segmentWriter;
		this.name = name;
	}

	//Segundo constructor, a emplear si la ruta contiene avisos
	public FileSegmentExporter(List<Segment> segments,List<UserWarning> warnings, String name,SegmentWriter segmentWriter) {
		this.segments = segments;
		this.warnings = warnings;
		this.segmentWriter = segmentWriter;
		this.name = name;
	}

	public boolean writeSegment(OutputStream outputStream) {
		segmentWriter.prepare(outputStream);
		segmentWriter.writeHeader(segments);
		
		//Escribe los avisos, en caso de que la ruta contenga alguno
		if(warnings != null){
//			String warningBaseFile = Resources.getSystem().getString(R.string.warning_file_name);
			for (int i = 0; i < warnings.size(); i++) {
					segmentWriter.writeWayPoint(warnings.get(i).getLocation(), String.valueOf(warnings.get(i).getWarningId()), String.valueOf(warnings.get(i).getPlayDistance()),warnings.get(i).getWarningDescription());
			}
		}
		segmentWriter.writeBeginSegment(name);
		for (int i = 0; i < segments.size(); i++) {
			segmentWriter.writeOpenSegment();
			for (int j=0; j < segments.get(i).getLocations().size(); j++){
				segmentWriter.writeLocation(segments.get(i).getLocations().get(j));
			}
			segmentWriter.writeCloseSegment();
		}
		segmentWriter.writeEndSegment(null, null);
		segmentWriter.writeFooter();
		segmentWriter.close();
		return true;
	}
}
