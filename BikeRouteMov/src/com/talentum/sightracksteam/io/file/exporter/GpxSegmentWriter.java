

package com.talentum.sightracksteam.io.file.exporter;


import android.location.Location;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import com.talentum.sightracksteam.model.Segment;


public class GpxSegmentWriter implements SegmentWriter {

	private static final NumberFormat ELEVATION_FORMAT = NumberFormat.getInstance(Locale.US);
	private static final NumberFormat COORDINATE_FORMAT = NumberFormat.getInstance(Locale.US);
	static {
		/*
		 * GPX readers expect to see fractional numbers with US-style punctuation.
		 * That is, they want periods for decimal points, rather than commas.
		 */
		ELEVATION_FORMAT.setMaximumFractionDigits(1);
		ELEVATION_FORMAT.setGroupingUsed(false);

		COORDINATE_FORMAT.setMaximumFractionDigits(6);
		COORDINATE_FORMAT.setMaximumIntegerDigits(3);
		COORDINATE_FORMAT.setGroupingUsed(false);
	}

	private PrintWriter printWriter;

	public GpxSegmentWriter() {
	}

	@Override
	public String getExtension() {
		return ".gpx";
	}

	@Override
	public void prepare(OutputStream outputStream) {
		this.printWriter = new PrintWriter(outputStream);
	}

	@Override
	public void close() {
		if (printWriter != null) {
			printWriter.flush();
			printWriter = null;
		}
	}

	@Override
	public void writeHeader(List<Segment> segments){
		if (printWriter != null) {
			printWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			printWriter.println("<gpx");
			printWriter.println("  version=\"1.1\"");
			printWriter.println(
					"  creator=\"" + "Talentum BikeLab" + "\"");
			printWriter.println("  xmlns=\"http://www.topografix.com/GPX/1/1\"");
			printWriter.println(
					"  xmlns:topografix=\"http://www.topografix.com/GPX/Private/TopoGrafix/0/1\"");
			printWriter.println("  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
			printWriter.println("  xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1"
					+ " http://www.topografix.com/GPX/1/1/gpx.xsd"
					+ " http://www.topografix.com/GPX/Private/TopoGrafix/0/1"
					+ " http://www.topografix.com/GPX/Private/TopoGrafix/0/1/topografix.xsd\">");
			// printWriter.println("<metadata>");
			// printWriter.println("</metadata>");
		}
	}

	@Override
	public void writeFooter() {
		if (printWriter != null) {
			printWriter.println("</gpx>");
		}
	}

	@Override
	public void writeBeginWaypoints(Segment track) {
		// Do nothing
	}

	@Override
	public void writeEndWaypoints() {
		// Do nothing
	}

//	@Override
//	public void writeTrackPoint(Location location) {
//		if (printWriter != null) {
//			if (location != null) {
//				printWriter.println("<trkpt " + formatLocation(location) + ">");
//				if (location.hasAltitude()) {
//					printWriter.println("  <ele>" + location.getAltitude() + "</ele>");
//				}
//				printWriter.println(
//						"  <time>" + location.getTime() + "</time>");
//				printWriter.println("</trkpt>");
//			}
//		}
//	}


	@Override
	public void writeBeginSegment(String name) {
		if (printWriter != null) {
			printWriter.println("<trk>");
			printWriter.println("<name>" + name + "</name>");
			printWriter.println("<extensions><topografix:color>c0c0c0</topografix:color></extensions>");
		}
	}

	@Override
	public void writeEndSegment(Segment segment, Location endLocation) {
		if (printWriter != null) {
			printWriter.println("</trk>");
		}
	}

	@Override
	public void writeOpenSegment() {
		printWriter.println("<trkseg>");
	}

	@Override
	public void writeCloseSegment() {
		printWriter.println("</trkseg>");
	}

	@Override
	public void writeLocation(Location location) {
		if (printWriter != null) {
			printWriter.println("<trkpt " + formatLocation(location) + ">");
			if (location.hasAltitude()) {
				printWriter.println("  <ele>" + ELEVATION_FORMAT.format(location.getAltitude()) + "</ele>");
			}
			printWriter.println(
					"  <time>" + location.getTime() + "</time>");
			printWriter.println("</trkpt>");
		}
	}

	/**
	 * Formats a location with latitude and longitude coordinates.
	 * 
	 * @param location the location
	 */
	private String formatLocation(Location location) {
		return "lat=\"" + COORDINATE_FORMAT.format(location.getLatitude()) + "\" lon=\""
				+ COORDINATE_FORMAT.format(location.getLongitude()) + "\"";
	}

	@Override
	public void writeBeginSegments() {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeEndSegments() {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeWayPoint(Location location, String name, String comment, String desc) {
		if (printWriter != null) {
			if (location != null) {
				printWriter.println("<wpt " + formatLocation(location) + ">");
				printWriter.println("<name>"+name+"</name>");
				printWriter.println("<cmt>"+comment+"</cmt>");
				printWriter.println("<desc>"+desc+"</desc>");
				printWriter.println("</wpt>");
			}
		}
	}
}
