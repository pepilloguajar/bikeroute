/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.talentum.sightracksteam.io.file.exporter;


import android.location.Location;

import java.io.OutputStream;
import java.util.List;

import com.talentum.sightracksteam.model.Segment;

/**
 * Interface for writing tracks to a file. The expected sequence of calls is:
 * 
 * <pre>
 * {@link #prepare(OutputStream)}
 * {@link #writeHeader(Segment[])}
 * For each track:
 *     {@link #writeBeginWaypoints(Track)}
 *     For each waypoint:
 *         {@link #writeTrackPoint(Location)}
 *     {@link #writeEndWaypoints()}
 * {@link #writeBeginTracks()}
 * For each track:
 *     {@link #writeBeginTrack(Track, Location)}
 *     For each segment:
 *         {@link #writeOpenSegment()}
 *         For each location in the segment:
 *             {@link #writeLocation(Location)}
 *         {@link #writeCloseSegment()}
 *     {@link #writeEndTrack(Segment, Location)}
 * {@link #writeEndTracks()}
 * {@link #writeFooter()}
 * {@link #close()}
 * </pre>
 * 
 * @author Rodrigo Damazio
 */
public interface SegmentWriter {

  /**
   * Gets the file extension (e.g, gpx, kml, ...).
   */
  public String getExtension();

  /**
   * Prepares the output stream.
   * 
   * @param outputStream the output stream
   */
  public void prepare(OutputStream outputStream);

  /**
   * Closes the output stream.
   */
  public void close();

  /**
   * Writes the header
   * 
   * @param tracks the tracks
   */
  public void writeHeader(List<Segment> segments);

  /**
   * Writes the footer.
   */
  public void writeFooter();

  /**
   * Writes the beginning of the waypoints.
   * 
   * @param segment the segment
   */
  public void writeBeginWaypoints(Segment segment);

  /**
   * Writes the end of the waypoints.
   */
  public void writeEndWaypoints();

  /**
   * Writes a trackponint.
   * 
   * @param location the location
   */
//  public void writeTrackPoint(Location location);
//
//  /**
//   * Writes a waypoint
//   * 
//   * @param location the location
//   * @param string the name
//   */
  
  public void writeWayPoint(Location location, String name, String comment, String desc);

  /**
   * Writes the beginning of the segments.
   */
  
  public void writeBeginSegments();

  /**
   * Writes the end of the segments,
   */
  public void writeEndSegments();

  /**
   * Writes the beginning of a segment.
   * 
   * @param segment the segment
   * @param startLocation the start location
   */
  public void writeBeginSegment(String name);

  /**
   * Writes the end of a segment.
   * 
   * @param segment the segment
   * @param endLocation the end location
   */
  public void writeEndSegment(Segment track, Location endLocation);

  /**
   * Writes open segment.
   */
  public void writeOpenSegment();

  /**
   * Writes close segment.
   */
  public void writeCloseSegment();

  /**
   * Writes a location.
   * 
   * @param location the location
   */
  public void writeLocation(Location location);
}