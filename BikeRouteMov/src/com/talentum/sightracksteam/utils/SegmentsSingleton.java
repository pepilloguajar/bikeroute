package com.talentum.sightracksteam.utils;

import java.util.ArrayList;
import java.util.List;

import com.talentum.sightracksteam.model.Segment;

public class SegmentsSingleton {
	private static SegmentsSingleton INSTANCE = null;
	
	
	private List<Segment> Segments;
	
	private SegmentsSingleton(){
		Segments = new ArrayList<Segment>();
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new SegmentsSingleton();
        }
    }

    public static SegmentsSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    
    public List<Segment> getSegments() {
		return Segments;
	}

	public void setSegments(List<Segment> Segments) {
		this.Segments = Segments;
	}

	public void clear(){
		Segments = null;
    }
    
}
