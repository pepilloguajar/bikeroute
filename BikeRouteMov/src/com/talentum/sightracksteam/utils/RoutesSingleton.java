package com.talentum.sightracksteam.utils;

import java.util.ArrayList;
import java.util.List;

import com.talentum.sightracksteam.model.Route;


/** 
 * M??todo para disponer de las Rutas en una ??nica instancia
 * @author escabia
 *
 */
public class RoutesSingleton {
	private static RoutesSingleton INSTANCE = null;
	
	
	private List<Route> listRoutes;
	
	private RoutesSingleton(){
		listRoutes = new ArrayList<Route>();
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new RoutesSingleton();
        }
    }

    public static RoutesSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    
    public List<Route> getListRoutes() {
		return listRoutes;
	}

	public void setListRoutes(List<Route> listRoutes) {
		this.listRoutes = listRoutes;
	}
	
	public void addRoute(Route route){
		this.listRoutes.add(0,route);
	}
	
	public void setNameandPublic(Route route){		
		for(int i=0; i<this.listRoutes.size(); i++){
			if(this.listRoutes.get(i).getId().equals(route.getId())){
				this.listRoutes.get(i).setName(route.getName());
				this.listRoutes.get(i).setPublicroute(route.isPublicroute());
			}
		}
	}

	public void logout(){
    	listRoutes = null;
    }
    
}
