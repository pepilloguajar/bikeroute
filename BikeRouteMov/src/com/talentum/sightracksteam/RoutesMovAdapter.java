package com.talentum.sightracksteam;



import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import com.talentum.sightracksteam.model.Route;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class RoutesMovAdapter extends BaseAdapter {
    private final Activity actividad;
    private final List<Route> lista;

    @SuppressWarnings("deprecation")
	private String timeAgo(View view, Date dateroute){
    	String timeago = null;
    	Date today = new Date();
    	if(dateroute.getYear() == today.getYear()){
    		if(dateroute.getMonth() == today.getMonth()){
    			if((today.getDate() == dateroute.getDate()))
    				return view.getResources().getString(R.string.today);
    			else if((today.getDate() - dateroute.getDate()) > 1){
    				timeago = today.getDate() - dateroute.getDate() + view.getResources().getString(R.string.daysago);
    			}
    			else
    				timeago = today.getDate() - dateroute.getDate() + view.getResources().getString(R.string.dayago);
    		}
    		else{
    			if((today.getMonth() - dateroute.getMonth()) > 1)
	        		timeago = today.getMonth() - dateroute.getMonth() + view.getResources().getString(R.string.monthsago);
    			else
	        		timeago = today.getMonth() - dateroute.getMonth() + view.getResources().getString(R.string.monthago);
    		}
    	}
    	else{
    		if((today.getYear() - dateroute.getYear()) > 1)
    			timeago = today.getYear() - dateroute.getYear() + view.getResources().getString(R.string.yearssago);
    		else
    			timeago = today.getYear() - dateroute.getYear() + view.getResources().getString(R.string.yearago);
    	}
    	return view.getResources().getString(R.string.trackdone) + view.getResources().getString(R.string.before) + timeago;
    }
    
	@SuppressWarnings("deprecation")
	private String timetogo(View view, Date dateroute){
    	
    	String timeago = null;
    	Date today = new Date();
    	if(dateroute.getTime() > today.getTime()){
        	if(dateroute.getYear() == today.getYear()){
    			Long onemonth = new Long("2419200000");//1 mes en milisegundos
        		if(dateroute.getMonth() == today.getMonth() || (today.getTime() - dateroute.getTime()) < onemonth){
        			if(dateroute.getDate() == today.getDate())
        				return view.getResources().getString(R.string.today);
        			else if((dateroute.getDate() - today.getDate()) > 1){
        				timeago = dateroute.getDate() - today.getDate() + view.getResources().getString(R.string.daysago);
        			}
        			else
        				timeago = Math.abs(dateroute.getDate() - today.getDate()) + view.getResources().getString(R.string.daysago);
        		}
        		else{
        			if((dateroute.getMonth() - today.getMonth()) > 1)
    	        		timeago = dateroute.getMonth() - today.getMonth() + view.getResources().getString(R.string.monthsago);
        			else
    	        		timeago = dateroute.getMonth() - today.getMonth() + view.getResources().getString(R.string.monthago);
        		}
        	}
        	else{
        		if((dateroute.getYear() - today.getYear()) > 1)
        			timeago = dateroute.getYear() - today.getYear() + view.getResources().getString(R.string.yearssago);
        		else
        			timeago = dateroute.getYear() - today.getYear() + view.getResources().getString(R.string.yearago);
        	}
        	return view.getResources().getString(R.string.in) + timeago;
    	}
    	else
    		return timeAgo(view, dateroute);
    }
    
    
    public RoutesMovAdapter(Activity actividad, List<Route> lista) {
          super();
          this.actividad = actividad;
          this.lista = lista;
    }

    public View getView(int position, View convertView, 
                                     ViewGroup parent) {
    	//Redondeo a dos decimales
    	DecimalFormat df = new DecimalFormat("##.#");
    	df.setRoundingMode(RoundingMode.UP);
    	
    	LayoutInflater inflater = actividad.getLayoutInflater();
    	View view = inflater.inflate(R.layout.elemento_lista, parent, false);
                                                                                                                        
    	TextView nameRoute =(TextView)view.findViewById(R.id.titulo);
    	nameRoute.setText((lista.get(position).getName()));
    	ImageView ico =(ImageView)view.findViewById(R.id.type);
    	if(lista.get(position).getCategory().compareTo("LL")==0){
    		ico.setImageResource(R.drawable.llana_movistar);
    	}else if(lista.get(position).getCategory().compareTo("MC")==0){
    		ico.setImageResource(R.drawable.mc_movistar);
    	}else{
    		ico.setImageResource(R.drawable.hc_movistar);
    	}
    	
    	TextView km =(TextView)view.findViewById(R.id.km);
    	km.setText(df.format(lista.get(position).getDistance())+" km");

    	
    	TextView date =(TextView)view.findViewById(R.id.dateDetails);
    	if(lista.get(position).getDatetodo() == null || lista.get(position).getDatetodo() == ""){
        	date.setText(view.getResources().getString(R.string.determined));
    	}
    	else{
    		
    		Date dtodo = new Date(lista.get(position).calculateDatetodo().getTime());

        	date.setText(timetogo(view, dtodo));
    	}
    	
    	return view;
    }

    public int getCount() {
          return lista.size();
    }

    public Object getItem(int arg0) {
          return lista.size();
    }

    public long getItemId(int position) {
          return position;
    }
}
