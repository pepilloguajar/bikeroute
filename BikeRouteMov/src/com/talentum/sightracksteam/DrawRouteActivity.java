package com.talentum.sightracksteam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.amazonaws.tvmclient.AmazonClientManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talentum.sightracksteam.RecorderManager;
import com.talentum.sightracksteam.model.Route;
import com.talentum.sightracksteam.model.Segment;
import com.talentum.sightracksteam.model.UserWarning;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.SegmentsSingleton;
import com.talentum.sightracksteam.utils.WarningsSingleton;


public class DrawRouteActivity extends FragmentActivity implements ConnectionCallbacks,
OnConnectionFailedListener,
LocationListener,
OnMyLocationButtonClickListener
{

	private GoogleMap mMap;

	SegmentsSingleton listSegments;
	//private List<Segment> segments;
	private List<UserWarning> warnings;
	private List<Segment> currentSegment;
	private LocationClient mLocationClient;
	private LocationManager locationManager;
	private String provider;

	private Route route;
	private Location loc = null;
	private RecorderManager recorderManager;
	private boolean recording;
	private ArrayList<String> names3gp;
	private File file;


	public static AmazonClientManager clientManager = null;



	// These settings are the same as the settings for the map. They will in fact give you updates
	// at the maximal rates currently possible.
	private static final LocationRequest REQUEST = LocationRequest.create()
			.setInterval(Integer.valueOf(PropertyLoader.getInstance().getIntervalGPS()))         // 5 seconds
			.setFastestInterval(16)    // 16ms = 60fps
			.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


	@SuppressWarnings({ "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		file = (File) getIntent().getSerializableExtra("filegpx");
		route = (Route) getIntent().getExtras().get("route");
		names3gp = getIntent().getStringArrayListExtra("files3gp");

		recorderManager = new RecorderManager();

		//Inicializa la lista de segmentos
		currentSegment = new ArrayList<Segment>();
		currentSegment.add(new Segment());

		// Get the location manager
		locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);

		// Define the criteria how to select the locatioin provider -> use
		// default
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);

		Location location = locationManager.getLastKnownLocation(provider);

		// Initialize the location fields
		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
		} 		

		/** Empieza reconocimiento **/
		setContentView(R.layout.activity_draw_route);
		setTitle(route.getName());
	}


	//Menu actionBar
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.action_draw, menu);
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			Intent in = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(in);
			finish();

			return true;
		case R.id.details:
			// details action
			Intent i = new Intent(getApplicationContext(),RouteDetails.class);
			i.putExtra("route", route);
			startActivity(i);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	protected void onDestroy(){
		SegmentsSingleton.getInstance().clear();
		WarningsSingleton.getInstance().clear();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		//Instancia el singletron de segments y warnings
		warnings = WarningsSingleton.getInstance().getWarnings();
		listSegments = SegmentsSingleton.getInstance();
		super.onResume();
		try {
			setUpMapIfNeeded();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setUpLocationClientIfNeeded();
		//mLocationClient.connect();
	}

	public void onSaveInstanceState(Bundle icicle) {
		super.onSaveInstanceState(icicle);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mLocationClient != null) {
			/** ------>>> Cierra intervalo **/
			loc = null;
			mLocationClient.disconnect();
		}
	}

	private void setUpMapIfNeeded() throws ParserConfigurationException, SAXException {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				drawPath();
				mMap.setMyLocationEnabled(true);
				mMap.setOnMyLocationButtonClickListener(this);

				mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
					@Override
					public boolean onMarkerClick(Marker marker) {
						//Log.i("MyMap","markerLocation. Lon: "+marker.getPosition().longitude+" / Lat: "+marker.getPosition().latitude);
						marker.showInfoWindow();
						//Log.i("MyMap","isShown "+marker.isInfoWindowShown()+". Title: "+marker.getTitle());
						for(int i=0; i<warnings.size(); i++){
							Location l = new Location("");
							l.setLatitude(marker.getPosition().latitude);
							l.setLongitude(marker.getPosition().longitude);
							
							//double difference = Math.abs((float)marker.getPosition().longitude - (float)warnings.get(i).getLocation().getLongitude());
							if( l.distanceTo(warnings.get(i).getLocation()) < 1 ){
								if(warnings.get(i).hasVoice()){
									Log.i("MyMap","hasVoice");
									recorderManager.startPlaying(
											Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/files3gp/"+route.getId()+"/"+warnings.get(i).getWarningId()+".3gp");
								} 
							}

						}
						return false;
					}
				});
			}
		}
	}

	private void setUpLocationClientIfNeeded() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(
					getApplicationContext(),
					this,  // ConnectionCallbacks
					this); // OnConnectionFailedListener
		}
	}

	@Override
	public boolean onMyLocationButtonClick() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onLocationChanged(Location location) {
		loc = new Location(location);
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mLocationClient.requestLocationUpdates(
				REQUEST,
				this);  // LocationListener

		Location location = mLocationClient.getLastLocation();


		/* Se centra la camara */
		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
		mMap.animateCamera(cameraUpdate);
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
	}

	public Location getLocation(){
		return loc;
	}

	public boolean isRecording(){
		return recording;
	}

	//Metodo para parsear rutas formato GPX
	public Vector<LatLng> parseGPX() throws ParserConfigurationException, SAXException{
		Vector<Vector<LatLng>> pathFragment = null;
		Vector<LatLng> temp = new Vector<LatLng>();

		try {

			InputStream inputStream = new FileInputStream(file);
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = docBuilder.parse(inputStream);
			if (document == null) {
				return null;
			}
			//Primero parsea los trkpt para pintar la ruta
			NodeList listTrckptTag = document.getElementsByTagName("trkpt");
			for (int i = 0; i < listTrckptTag.getLength(); i++) {
				String latText = ((Element) listTrckptTag.item(i)).getAttribute("lat");
				String lonText = ((Element) listTrckptTag.item(i)).getAttribute("lon");
				LatLng punto = new LatLng(Double.parseDouble(latText),Double.parseDouble(lonText));
				Location pointLocation = new Location("parser");
				pointLocation.setLatitude(Double.parseDouble(latText));
				pointLocation.setLongitude(Double.parseDouble(lonText));
				currentSegment.get(0).addLocation(pointLocation);
				temp.add(punto);
				if(pathFragment != null){
					pathFragment.add(temp);
				}
			}
			listSegments.setSegments(currentSegment);

			//Busca el nodo hijo que se llama wpt            
			NodeList children = document.getFirstChild().getChildNodes();
			boolean predef = true;
			boolean hasVoice = false;
			Location warningLocation = null;
			int playDistance = 0;
			int warningId = 0;
			String warningDescription = "";
			for (int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);
				if (child.getNodeName().equalsIgnoreCase("wpt")) { 
					String latText = ((Element) child).getAttribute("lat");
					String lonText = ((Element) child).getAttribute("lon");
					NodeList wptChildren = child.getChildNodes();
					for (int i = 0; i < wptChildren.getLength(); i++) {
						if(wptChildren.item(i).getNodeName().equals("name")){
							warningId = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("cmt")){
							playDistance = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("desc")){
							warningDescription = wptChildren.item(i).getFirstChild().getNodeValue();
						}
						warningLocation = new Location("parser");
						warningLocation.setLatitude(Double.parseDouble(latText));
						warningLocation.setLongitude(Double.parseDouble(lonText));
					}
					if(warningDescription.equals("ap"))
						predef = false;
					else
						predef = true;
					hasVoice = false;
					for(int k=0; k<names3gp.size();k++){
						if(names3gp.get(k).equals("files3gp/"+route.getId()+"/"+warnings.size()+".3gp")){
							hasVoice = true;
						}
					}
					//Log.i("MyMap","AvisoEncontrado: "+j);
					UserWarning newWarning = new UserWarning(warningLocation, predef, warningId, warningDescription,hasVoice, playDistance);	  
					warnings.add(newWarning);						
				}
			}
			for(int i=0; i < warnings.size(); i++){
				//mMap.addMarker(new MarkerOptions()
				//.position(new LatLng(warnings.get(i).getLocation().getLatitude(),warnings.get(i).getLocation().getLongitude()))
				//.title(Integer.toString(warnings.get(i).getWarningId())));
				BitmapDescriptor icon_market = null;
				Log.i("DrawRouteActivity",warnings.get(i).getWarningDescription());
				String title = getResources().getString(getResources().getIdentifier(warnings.get(i).getWarningDescription(),"string",this.getPackageName()));
				if(warnings.get(i).hasVoice()){
					//title = warnings.get(i).getWarningDescription();
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_micro);
				}
				else{
					//title = getResources().getString(getResources().getIdentifier(warnings.get(i).getWarningDescription(),"string",this.getPackageName()));
					if(warnings.get(i).getWarningDescription().contains("gr")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_unevenroad);
					}
					else if(warnings.get(i).getWarningDescription().contains("dc")){
						if(warnings.get(i).getWarningDescription().contains("le")){
							if(warnings.get(i).getWarningDescription().contains("ff")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left45);
							}
							else if(warnings.get(i).getWarningDescription().contains("ni")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left90);
							}
							else{
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left180);
							}
						}
						else if(warnings.get(i).getWarningDescription().contains("ri")){
							if(warnings.get(i).getWarningDescription().contains("ff")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right45);
							}
							else if(warnings.get(i).getWarningDescription().contains("ni")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right90);
							}
							else{
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right135);
							}
						}				
					}
					else if(warnings.get(i).getWarningDescription().contains("wi")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_wind);
					}
					else if(warnings.get(i).getWarningDescription().contains("sb")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_baden);
					}
					else if(warnings.get(i).getWarningDescription().contains("sc")){
						if(warnings.get(i).getWarningDescription().contains("as")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopeup);
						}
						else if(warnings.get(i).getWarningDescription().contains("de")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopedown);
						}
					}
					else if(warnings.get(i).getWarningDescription().contains("rb")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_roundabout);
					}
					else if(warnings.get(i).getWarningDescription().contains("dczi")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_zigzag);
					}			
					else{
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.ic_help);//esta linea hay que quitarla
					}
				}
				
				mMap.addMarker(new MarkerOptions()
				.position(new LatLng(warnings.get(i).getLocation().getLatitude(),warnings.get(i).getLocation().getLongitude()))
				.title(title)
				.icon(icon_market));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return temp;
	}


	public void onNewMarket(BitmapDescriptor icon, LatLng l, String title){
		mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(title)
		.icon(icon));		
	}

	//Metodo para dibujar rutas sobre el mapa con Polylines. Se le pasa como argumento la ruta al archivo .GPX o .KML y el mapa
	//sobre el que se va a pintar la ruta
	public void drawPath() throws ParserConfigurationException, SAXException {
		try {
			final Vector<LatLng> list = parseGPX();
			BitmapDescriptor icon_start = null;
			icon_start = BitmapDescriptorFactory.fromResource(R.drawable.market_start);
			onNewMarket(icon_start,list.get(0), getResources().getString(R.string.ini_sight));
			
			BitmapDescriptor icon_finish = null;
			icon_finish = BitmapDescriptorFactory.fromResource(R.drawable.market_finish);
			onNewMarket(icon_finish,list.get(list.size()-1), getResources().getString(R.string.fin_sight));
			
//			mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(list.get(list.size()/2),11));
//			for(int z = 0; z<list.size()-1;z++){
//				LatLng src= list.get(z);
//				LatLng dest= list.get(z+1);
			mMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {
				
				@Override
				public void onMapLoaded() {
					LatLngBounds.Builder builder = new LatLngBounds.Builder();
					for (LatLng marker : list) {
					    builder.include(marker);
					}
					LatLngBounds bounds = builder.build();
					int padding = 80; // offset from edges of the map in pixels
					CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
					
					//mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(list.get(list.size()/2),11));
					mMap.animateCamera(cu);		
				}
			});
			PolylineOptions polylineOptions = new PolylineOptions()
			.width(Integer.parseInt(PropertyLoader.getInstance().getLineRoute()))
			.color(0xff062344);
			mMap.addPolyline(polylineOptions.addAll(list));
//			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
