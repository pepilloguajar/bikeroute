package com.talentum.sightracksteam;


import java.util.List;

import com.talentum.sightracksteam.PredefinedWarningDialog;
import com.talentum.sightracksteam.model.UserWarning;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.SegmentsSingleton;
import com.talentum.sightracksteam.utils.WarningsSingleton;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.location.Location;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class RightControlFragment extends Fragment implements OnTouchListener, android.view.View.OnClickListener{
	
	// Container Activity must implement this interface
	public interface OnNewWarningListener {
		public void onNewWarning(UserWarning warning);

		//void onNewWarning(UserWarning warning);
	}
	
	OnNewWarningListener mCallBack;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (OnNewWarningListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnMarkerAdder");
        }
    }
    
    private int mStackLevel = 0;
    SegmentsSingleton listSegments;
    private RecorderManager recorderManager;
    private List<UserWarning> warnings = WarningsSingleton.getInstance().getWarnings();
    private Location storedLocation;

    public RightControlFragment() {
    }

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.rightcontrol, container,
				false);
		//warnings = new ArrayList<UserWarning>();
		recorderManager = new RecorderManager();
		listSegments = SegmentsSingleton.getInstance();
		ImageButton warningButton = (ImageButton) rootView.findViewById(R.id.noticeButton);
		ImageButton recordButton = (ImageButton) rootView.findViewById(R.id.recordButton);
        recordButton.setOnTouchListener(this);
        warningButton.setOnClickListener(this);
		return rootView;
	}
    
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(((NewRouteMovActivity) getActivity()).isRecording()){
			int warningId = warnings.size();
			if(event.getAction() == MotionEvent.ACTION_DOWN ){
				Toast.makeText(getActivity(),getResources().getString(R.string.recording_voice), Toast.LENGTH_LONG).show();
				newVoiceWarning(true,String.valueOf(warningId));
				storedLocation = ((NewRouteMovActivity) getActivity()).getLocation();
				return true;
			}
			if(event.getAction() == MotionEvent.ACTION_UP ){
				newVoiceWarning(false,String.valueOf(warningId));
				//Location currentLocation = listSegments.getSegments().get(0).getLocations().get(0);
				int playDistance = Integer.parseInt(PropertyLoader.getInstance().getDefaultPlayDistance());
				UserWarning newWarning = new UserWarning(storedLocation, false, warningId, "ap", true, playDistance);
				setNewMarker(newWarning);
				WarningsSingleton.getInstance().addWarning(newWarning);
				Toast.makeText(getActivity(),getResources().getString(R.string.finalize_recording_voice), Toast.LENGTH_LONG).show();
			}
		}
		else{
			Toast.makeText(getActivity(),R.string.inpause, Toast.LENGTH_LONG).show();
		}

		return false;
	}
	
	@Override
	public void onClick(View v) {
		if(((NewRouteMovActivity) getActivity()).isRecording()){
		    // DialogFragment.show() will take care of adding the fragment
		    // in a transaction.  We also want to remove any currently showing
		    // dialog, so make our own transaction and take care of that here.
		    FragmentTransaction ft = getFragmentManager().beginTransaction();
		    Fragment prev = getFragmentManager().findFragmentByTag("dialog");
		    if (prev != null) {
		        ft.remove(prev);
		    }
		    ft.addToBackStack(null);

		    // Create and show the dialog.
		    DialogFragment newFragment = PredefinedWarningDialog.newInstance(mStackLevel);
		    newFragment.show(ft, "dialog");
		}
		else{
			Toast.makeText(getActivity(),R.string.inpause, Toast.LENGTH_LONG).show();
		}
	}
	
	private void newVoiceWarning(boolean start, String name){
		recorderManager.onRecord(start, name);
	}
	
	private void setNewMarker(UserWarning warning){
		mCallBack.onNewWarning(warning);
	}


}
