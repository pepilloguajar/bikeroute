package com.talentum.sightracksteam;

import java.util.Arrays;
import java.util.List;

import com.talentum.sightracksteam.RightControlFragment.OnNewWarningListener;
import com.talentum.sightracksteam.model.UserWarning;
import com.talentum.sightracksteam.utils.WarningsSingleton;

import android.app.Activity;
import android.app.DialogFragment;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PredefinedWarningDialog extends DialogFragment implements OnItemClickListener, OnTouchListener, OnClickListener{

	//Variable que controla en que nivel del menu se encuentra el usuario
	int menuLevel;
	//Array que controla las opciones marcadas en los menus anteriores
	int [] prevMenuPosition;
	Button backButton;
	Button skipButton;
	Button finalizeButton;
	Button cancelButton;
	ImageButton recordButton;
	TextView sideText;
	TextView summary;
	
	List <String> dialogElements;
	
	ListView vlist;
	
	OnNewWarningListener mCallBack;
	RecorderManager recorderManager;
	
	String warningId;
	boolean isVoiceRecorded;
	String warningDescription;
	int playDistance;
	Location currentLocation;
	private List<UserWarning> warnings;

	/**
	 * Create a new instance of MyDialogFragment, providing "num"
	 * as an argument.
	 */
	static PredefinedWarningDialog newInstance(int num) {
		PredefinedWarningDialog f = new PredefinedWarningDialog();

		// Supply num input as an argument.
		Bundle args = new Bundle();
		args.putInt("num", num);
		f.setArguments(args);

		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		menuLevel = 0;
		prevMenuPosition = new int [7];
		isVoiceRecorded = false;
		int style = DialogFragment.STYLE_NORMAL, theme = 0;
		setStyle(style, theme);
		recorderManager = new RecorderManager();
		warnings = WarningsSingleton.getInstance().getWarnings();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);        
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			mCallBack = (OnNewWarningListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnMarkerAdder");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.predefined_warning, container, false);
		
		recordButton= (ImageButton) v.findViewById(R.id.imageButton1);
		recordButton.setVisibility(View.GONE);
		recordButton.setOnTouchListener(this);
		
		sideText= (TextView) v.findViewById(R.id.textView1);
		sideText.setVisibility(View.GONE);
		
		summary= (TextView) v.findViewById(R.id.summary);
		summary.setVisibility(View.GONE);
		
		
		vlist=(ListView)v.findViewById(R.id.listView1);
		dialogElements = Arrays.asList(getMenuElements());

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice, dialogElements);
		vlist.setAdapter(adapter);
		vlist.setOnItemClickListener(this);
		vlist.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


		backButton = (Button) v.findViewById(R.id.backButton);
		skipButton = (Button) v.findViewById(R.id.skipButton);
		finalizeButton = (Button) v.findViewById(R.id.finalizeButton);
		cancelButton = (Button) v.findViewById(R.id.cancelButton);
		
		backButton.setOnClickListener(this);
		skipButton.setOnClickListener(this);
		finalizeButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
		
		backButton.setText(R.string.back);
		skipButton.setText(R.string.skip);
		finalizeButton.setText(R.string.finalize);
		cancelButton.setText(R.string.cancel);
		
		updateButtons();
		currentLocation = ((NewRouteMovActivity) getActivity()).getLocation();
	
		return v;
	}

	//Devuelve el array con los elemenos (opciones) de cada dialogo
	private String[] getMenuElements(){
		switch(menuLevel){
			case 1: switch(prevMenuPosition[0]){
				case 0: 
					return getResources().getStringArray(R.array.dialog_ground);
				case 1: 
					return getResources().getStringArray(R.array.dialog_side_curve);
				case 2: 
					return getResources().getStringArray(R.array.dialog_wind);
				case 3: 
					return getResources().getStringArray(R.array.dialog_bump);
				case 4: 
					return getResources().getStringArray(R.array.dialog_slope);
				case 5:
					return getResources().getStringArray(R.array.dialog_side);
			}
			case 2: switch(prevMenuPosition[0]){
				case 1: 
					return getResources().getStringArray(R.array.dialog_degrees);
				case 2: 
					//Si el viento es de frente o de cola, anade el campo de viento directo
					if(prevMenuPosition[1] == 3)
						return getResources().getStringArray(R.array.dialog_side);
					else
						return getResources().getStringArray(R.array.dialog_side_wind);
				case 5: 
					return getResources().getStringArray(R.array.dialog_roundabout_entry);
			}
			case 3: 
				return getResources().getStringArray(R.array.dialog_roundabout_exit);
			case 5: 
				return getResources().getStringArray(R.array.dialog_distance);
			
		default:
			getDialog().setTitle(R.string.dialog_main_title); 
			return getResources().getStringArray(R.array.dialog_main);
		}
	}

	//Determina que item (id) se ha pulsado
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		//Si se trata del menu de firme en mal estado, no cambiar el menu directamente al pulsar una opcion (es multiopcion)
		prevMenuPosition[menuLevel]=(int)id;
		if(menuLevel==1){
			//Si no se trata de los menus Side o Wind (excepto viento aleatorio), aumenta el nivel en 2 (pasa a recording)
			if((prevMenuPosition[0] != 1) && prevMenuPosition[0] != 2 && prevMenuPosition[0] != 5){
				menuLevel=+2;
			}
			if((prevMenuPosition[0]==2 && id == 0) || prevMenuPosition[0]==1 && id==2){
				menuLevel=+2;
			}
		}
		//Si se trata de los menus Degrees o Side, aumenta un nivel extra para saltar el menu de salida de rotonda
		if(menuLevel==2 && prevMenuPosition[0] != 5){
			menuLevel++;
		}
		menuLevel++;
		nextMenu();
		updateButtons();		
	}
	
	//Para el recordButton, detecta cuando se pulsa (MotionEvent.ACTION_DOWN) para empezar a grabar y cuando se levanta
	// (MotionEvent.ACTION_DOWN) para dejar de grabar.
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int warningId = warnings.size();
		if(event.getAction() == MotionEvent.ACTION_DOWN ){
			//mCallBack.onNewWarning(true, true, true);
			recorderManager.onRecord(true,String.valueOf(warningId));
			Toast.makeText(getActivity(),getResources().getString(R.string.recording_voice), Toast.LENGTH_LONG).show();
		}
		if(event.getAction() == MotionEvent.ACTION_UP ){
			//mCallBack.onNewWarning(false, true, true);
			recorderManager.onRecord(false,String.valueOf(warningId));
			prevMenuPosition[menuLevel]=0;
			menuLevel++;
			nextMenu();
			updateButtons();
			isVoiceRecorded = true;
			Toast.makeText(getActivity(),getResources().getString(R.string.finalize_recording_voice), Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	//Determina que elementos del layout son visibles en funcion del nivel del menu y los activa o desactiva
	//Tambien cambia el titulo en funcion del menu
	public void updateButtons(){
		switch(menuLevel){
			case 0:
				vlist.setVisibility(View.VISIBLE);
				recordButton.setVisibility(View.GONE);
				sideText.setVisibility(View.GONE);
				backButton.setVisibility(View.GONE);
				skipButton.setVisibility(View.GONE);
				cancelButton.setVisibility(View.VISIBLE);
				finalizeButton.setVisibility(View.GONE);
				break;
			case 1:
				vlist.setVisibility(View.VISIBLE);
				backButton.setVisibility(View.VISIBLE);
				cancelButton.setVisibility(View.GONE);
				recordButton.setVisibility(View.GONE);
				sideText.setVisibility(View.GONE);
				skipButton.setVisibility(View.GONE);
				switch(prevMenuPosition[0]){
				case 0: 
					getDialog().setTitle(R.string.dialog_ground_title);
					break;
				case 1: 
					getDialog().setTitle(R.string.dialog_side_title);
					break;
				case 2: 
					getDialog().setTitle(R.string.dialog_wind_title);
					break;
				case 3: 
					getDialog().setTitle(R.string.dialog_bump_title);
					break;
				case 4: 
					getDialog().setTitle(R.string.dialog_slope_title);
					break;
				case 5: 
					getDialog().setTitle(R.string.dialog_roundabout_entry_title);
					break;
				}
				break;
			case 2:
				vlist.setVisibility(View.VISIBLE);
				recordButton.setVisibility(View.GONE);
				sideText.setVisibility(View.GONE);
				skipButton.setVisibility(View.GONE);
				switch(prevMenuPosition[0]){
				case 1: 
					getDialog().setTitle(R.string.dialog_degrees_title);
					break;
				case 2: 
					getDialog().setTitle(R.string.dialog_wind_title);
					break;
				case 5: 
					getDialog().setTitle(R.string.dialog_roundabout_type_title);
					break;
				}
				break;
			case 3:
				vlist.setVisibility(View.VISIBLE);
				recordButton.setVisibility(View.GONE);
				sideText.setVisibility(View.GONE);
				skipButton.setVisibility(View.GONE);
				getDialog().setTitle(R.string.dialog_roundabout_exit_title);
				break;
			case 4:
				vlist.setVisibility(View.GONE);
				recordButton.setVisibility(View.VISIBLE);
				sideText.setVisibility(View.VISIBLE);
				skipButton.setVisibility(View.VISIBLE);
				getDialog().setTitle(R.string.dialog_record_title);
				break;
			case 5:
				vlist.setVisibility(View.VISIBLE);
				recordButton.setVisibility(View.GONE);
				sideText.setVisibility(View.GONE);
				skipButton.setVisibility(View.GONE);
				finalizeButton.setVisibility(View.GONE);
				summary.setVisibility(View.GONE);
				getDialog().setTitle(R.string.dialog_distance_title);
				break;
			case 6:
				vlist.setVisibility(View.GONE);
				finalizeButton.setVisibility(View.VISIBLE);
				warningDescription = getWarningDescription();
				summary.setVisibility(View.VISIBLE);
				getDialog().setTitle(R.string.dialog_summary_title);
				break;
		}
	}
	
	private void saveAndClose(){
		
		int warningId = WarningsSingleton.getInstance().getSize();
		UserWarning newWarning = new UserWarning(currentLocation, true,
				warningId,warningDescription,isVoiceRecorded, playDistance);
		setNewMarker(newWarning);
		WarningsSingleton.getInstance().addWarning(newWarning);
		dismiss();
	}
	
	private void nextMenu(){
		if(menuLevel != 4 && menuLevel != 6){
		dialogElements = Arrays.asList(getMenuElements());
		ArrayAdapter<String> adapter;
		//Si es el menu ground, el layout cambia por un multichoice
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice, dialogElements);
		vlist.setAdapter(adapter);
		}
	}

	//Metodo que captura los clicks sobre los botones del final.
	@Override
	public void onClick(View v) {
		//Si se ha pulsado el boton de atras, restar uno a la variable de control del nivel y regargar los menus
		if(v.getId() == R.id.backButton){
			if(menuLevel==4){
				if(prevMenuPosition[0] != 5){
					menuLevel--;
				}
				if((prevMenuPosition[0] != 1) && (prevMenuPosition[0] != 2) && (prevMenuPosition[0] != 5)){
					menuLevel--;
				}
				if((prevMenuPosition[0]==2 && prevMenuPosition[1] == 0) || (prevMenuPosition[0]==1 && prevMenuPosition[1] == 2)){
					menuLevel--;
				}
			}
			menuLevel--;
			nextMenu();
			updateButtons();
		}
		
		if(v.getId() == R.id.cancelButton)
		{
			//Cierra el Dialog sin hacer nada mas
			dismiss();
		}
		if(v.getId() == R.id.finalizeButton){
			saveAndClose();
		}
		
		//Saltar la grabacion de audio. Aumenta el nivel en uno
		if(v.getId() == R.id.skipButton){
			menuLevel++;
			nextMenu();
			updateButtons();
		}
	}
	
	private String getSummary(){
		String out = "";
		out+="\n"+getResources().getString(R.string.is_voice_recorded)+": ";
		if(isVoiceRecorded){
			out+=getResources().getString(R.string.yes);
		}
		else{
			out+=getResources().getString(R.string.no);
		}
		out+="\n"+getResources().getString(R.string.dialog_distance_title)+": ";
		switch(prevMenuPosition[5]){
			case 0: out+=getResources().getString(R.string.fifty);
					playDistance = 50;
					break;
			case 1: out+=getResources().getString(R.string.hundred);
					playDistance = 100;
					break;
			case 2: out+=getResources().getString(R.string.two_hundred);
			        playDistance = 200;
					break;
			case 3: out+=getResources().getString(R.string.five_hundred);
			        playDistance = 500;
					break;
			case 4: out+=getResources().getString(R.string.thousand);
			        playDistance = 1000;
					break;
		}
		return out;
	}
	
	
	//Metodo que construye un String que contiene el resumen final, en funcion de las opciones seleccionadas.
	//Para determinar cuales son los items elegidos, emplea el array prevMenuPosition que contiene la opcion seleccionadas 
	//en cada nivel
	private String getWarningDescription(){
		String out="";
		switch(prevMenuPosition[0]){
			case 0: 
				out = "gr";
				//Se cuenta el numero de elementos seleccionados para mostrarlos correctamente
						switch(prevMenuPosition[1]){
							case 0: 
								summary.setText(getResources().getString(R.string.grph) + getSummary());
								out += "ph";
								break;
							case 1: 
								summary.setText(getResources().getString(R.string.grde) + getSummary());
								out+= "de"; 
								break;
							case 2: 
								summary.setText(getResources().getString(R.string.grso) + getSummary());
								out+= "so";
								break;
							case 3: 
								summary.setText(getResources().getString(R.string.grpa) + getSummary());
								out+="pa";
								break;
							case 4: 
								summary.setText(getResources().getString(R.string.grsl) + getSummary());
								out+="sl";
								break;
					}
				break;
			case 1: 
				out = "dc";//getResources().getString(R.string.dangerous_cruve)+": ";
				switch(prevMenuPosition[1]){
					case 0: 
						out+="ri";//getResources().getString(R.string.left)+" ";
						break;
					case 1: 
						out+="le";//getResources().getString(R.string.right)+" ";
						break;
					case 2:
						out+="zi";
						summary.setText(getResources().getString(R.string.dczi) + getSummary());
						break;
				}
				if(prevMenuPosition[1] != 2){
					switch(prevMenuPosition[2]){
						case 0: 
							if(prevMenuPosition[1] == 0)
								summary.setText(getResources().getString(R.string.dcleff) + getSummary());
							else
								summary.setText(getResources().getString(R.string.dcriff) + getSummary());
							out+="ff";//getResources().getString(R.string.forty_five);
							break;
						case 1: 
							if(prevMenuPosition[1] == 0)
								summary.setText(getResources().getString(R.string.dcleni) + getSummary());
							else
								summary.setText(getResources().getString(R.string.dcrini) + getSummary());
							out+="ni";
							break;
						case 2: 
							if(prevMenuPosition[1] == 0)
								summary.setText(getResources().getString(R.string.dcleni) + getSummary());
							else
								summary.setText(getResources().getString(R.string.dcrini) + getSummary());
							out+="ht";
							break;
						case 3:
							if(prevMenuPosition[1] == 0)
								summary.setText(getResources().getString(R.string.dclehe) + getSummary());
							else
								summary.setText(getResources().getString(R.string.dcrihe) + getSummary());
							out+="he";
							break;
					}
				}
				break;
			case 2: out = "wi";
				switch(prevMenuPosition[1]){
					case 0: 
						summary.setText(getResources().getString(R.string.wira) + getSummary());
						out+="ra";
						break;
					case 1: 
						out+="fr";
						break;
					case 2:
						out+="bw";
						break;
					case 3: 
						out+="si";
						break;
				}
				if(prevMenuPosition[1] != 0){
					switch(prevMenuPosition[1]){
						case 1: 
							switch(prevMenuPosition[2]){
								case 0: 
									summary.setText(getResources().getString(R.string.wifrri) + getSummary());
									out+="ri";
									break;
								case 1: 
									summary.setText(getResources().getString(R.string.wifrle) + getSummary());
									out+="le";
									break;
								case 2: 
									summary.setText(getResources().getString(R.string.wifrst) + getSummary());
									out+="st";
									break;
							}
							break;
						case 2: 
							switch(prevMenuPosition[2]){
								case 0: 
									summary.setText(getResources().getString(R.string.wibwri) + getSummary());
									out+="ri";
									break;
								case 1: 
									summary.setText(getResources().getString(R.string.wibwle) + getSummary());
									out+="le";
									break;
								case 2: 
									summary.setText(getResources().getString(R.string.wibwst) + getSummary());
									out+="st";
									break;
							}
							break;
						case 3:
							switch(prevMenuPosition[2]){
								case 0: 
									summary.setText(getResources().getString(R.string.wifrri) + getSummary());
									out+="ri";
									break;
								case 1: 
									summary.setText(getResources().getString(R.string.wifrle) + getSummary());
									out+="le";
									break;
							}
							break;
					}
				}
				break;
			case 3: out = "sb";
				switch(prevMenuPosition[1]){
					case 0: 
						summary.setText(getResources().getString(R.string.sbbl) + getSummary());
						out+="bl";
						break;
					case 1: 
						summary.setText(getResources().getString(R.string.sbbm) + getSummary());
						out+="bm";
						break;
					case 2: 
						summary.setText(getResources().getString(R.string.sbbu) + getSummary());
						out+="bu";
						break;
					case 3: 
						summary.setText(getResources().getString(R.string.sbpl) + getSummary());
						out+="pl";
						break;
					case 4: 
						summary.setText(getResources().getString(R.string.sbrc) + getSummary());
						out+="rc";
						break;
				}
				break;
			case 4: out = "sc";
				switch(prevMenuPosition[1]){
					case 0: 
						summary.setText(getResources().getString(R.string.scas) + getSummary());
						out+="as";
						break;
					case 1: 
						summary.setText(getResources().getString(R.string.scde) + getSummary());
						out+="de";
						break;
				}
			break;
			case 5: out = "rb";
				switch(prevMenuPosition[1]){
					case 0: 
						out+="ri";//getResources().getString(R.string.left)+" ";
						break;
					case 1: 
						out+="le";//getResources().getString(R.string.right)+" ";
						break;
				}
				if(prevMenuPosition[1] == 0){
					
					if(prevMenuPosition[2] == 0){
						out+="op";
						if(prevMenuPosition[3] == 0){
							summary.setText(getResources().getString(R.string.rbriopfr) + getSummary());
							out+="fr";
						}
						else if(prevMenuPosition[3] == 1){
							summary.setText(getResources().getString(R.string.rbriople) + getSummary());
							out+="le";
						}
						else{
							summary.setText(getResources().getString(R.string.rbriopri) + getSummary());
							out+="ri";
						}
					}						
					else{
						out+="cl";
						if(prevMenuPosition[3] == 0){
							summary.setText(getResources().getString(R.string.rbriclfr) + getSummary());
							out+="fr";
						}
						else if(prevMenuPosition[3] == 1){
							summary.setText(getResources().getString(R.string.rbriclle) + getSummary());
							out+="le";
						}
						else{
							summary.setText(getResources().getString(R.string.rbriclri) + getSummary());
							out+="ri";
						}						
					}
				}
				else{
					if(prevMenuPosition[2] == 0){
						out+="op";
						if(prevMenuPosition[3] == 0){
							summary.setText(getResources().getString(R.string.rbleopfr) + getSummary());
							out+="fr";
						}
						else if(prevMenuPosition[3] == 1){
							summary.setText(getResources().getString(R.string.rbleople) + getSummary());
							out+="le";
						}
						else{
							summary.setText(getResources().getString(R.string.rbleopri) + getSummary());
							out+="ri";
						}
					}						
					else{
						out+="cl";
						if(prevMenuPosition[3] == 0){
							summary.setText(getResources().getString(R.string.rbleclfr) + getSummary());
							out+="fr";
						}
						else if(prevMenuPosition[3] == 1){
							summary.setText(getResources().getString(R.string.rbleclle) + getSummary());
							out+="le";
						}
						else{
							summary.setText(getResources().getString(R.string.rbleclri) + getSummary());
							out+="ri";
						}
					}
				}
		}
		return out;
	}
	
	private void setNewMarker(UserWarning warning){
		mCallBack.onNewWarning(warning);
	}
}
