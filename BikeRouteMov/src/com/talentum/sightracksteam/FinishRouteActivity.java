package com.talentum.sightracksteam;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.sightracksteam.io.file.exporter.FileSegmentExporter;
import com.talentum.sightracksteam.io.file.exporter.GpxSegmentWriter;
import com.talentum.sightracksteam.model.Route;
import com.talentum.sightracksteam.sdb.RoutesList;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.RoutesSingleton;
import com.talentum.sightracksteam.utils.SegmentsSingleton;
import com.talentum.sightracksteam.utils.WarningsSingleton;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FinishRouteActivity extends ActionBarActivity implements OnClickListener {
	public static AmazonClientManager clientManager = null;
	Route route;
	String idroute;
    File file = new File(Environment.getExternalStorageDirectory(), "Android/data/com.talentum.bikeroute/temp.gpx");
    public EditText et_datetodo;	  
    public static boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          if (files == null) {
              return true;
          }
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return true;
        //return( path.delete() );
      }
    
	/**
	 * Check Test Connection Internet 
	 * @return
	 */
	private boolean checkOnlineState() {
	    ConnectivityManager CManager =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo NInfo = CManager.getActiveNetworkInfo();
	    if (NInfo != null && NInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}    
    
	/** 
	 * @brief Carga los datos estad??sticos de descripci??n de la ruta
	 */
	private void loadViewData(){
		Date fecha = new Date();
		
		idroute = "Route"+String.valueOf(fecha.getTime());
		route = new Route(idroute,"","LL",11,124.0,100,300,Long.toString(fecha.getTime()),12,null,null,false,"www", "");
		//Lineas de c??digo que calculan la distancia de la ruta y el desnivel positivo y negativo
		boolean firstIteration=true, changeSegment=false;
		float distanceTotal=0;
		Double dNeg=0.0, dPos=0.0;
		Location loc_a=null, loc_b=null;
		SegmentsSingleton segments = SegmentsSingleton.getInstance();
		for(int i=0; i<segments.getSegments().size(); i++){
    		for(int j=0; j<segments.getSegments().get(i).getLocations().size() - 1; j++){
    			if(!(i==0 && j==0)){// Este if est?? proque el primer tramo de puntos toma un valor an??malo
	    			if(firstIteration){
	    				if(changeSegment){
	    					loc_a = loc_b;
	    					loc_b = segments.getSegments().get(i).getLocations().get(j);
	    					distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
	    					changeSegment=false;
	    				}else{
	        				loc_a = segments.getSegments().get(i).getLocations().get(j);
	        				loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
	        				distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
	    				}
	        			firstIteration=false;
	   				}else{
	   					loc_a = loc_b;
	   					loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
	   					distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
	   				}
	    			if(loc_a.getAltitude() != 0.0 && loc_b.getAltitude() != 0.0 ){
		    			if((loc_a.getAltitude() - loc_b.getAltitude())>0){
							dNeg = dNeg + (loc_a.getAltitude() - loc_b.getAltitude());
						}else{
							dPos = dPos + ((loc_a.getAltitude() - loc_b.getAltitude())*-1);
						}
	    			}
    			}
	    		
    			if(j==segments.getSegments().get(i).getLocations().size() - 2){
    				changeSegment=true;
    			}
    		}
    	}
		distanceTotal= distanceTotal/1000; 
		//Redondeo a dos decimales
		DecimalFormat df = new DecimalFormat("##.##");
		df.setRoundingMode(RoundingMode.UP);
		
		//Detecta las rutas muy cortas
		if(distanceTotal <  Integer.parseInt(PropertyLoader.getInstance().getDistanceMin())){
			startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
			Toast.makeText(this, getResources().getString(R.string.toleast)+" "+PropertyLoader.getInstance().getDistanceMin()+" "+getResources().getString(R.string.meters), Toast.LENGTH_LONG).show();
		}else{
		
			Button discardroute = (Button) this.findViewById(R.id.discardroute);
			Button saveroute = (Button) this.findViewById(R.id.saveroute);
	   		discardroute.setOnClickListener(this);
	   		saveroute.setOnClickListener(this);
			
			EditText name = (EditText)this.findViewById(R.id.name);
			name.setText(route.getName());
			
			final CheckBox ispublic = (CheckBox)this.findViewById(R.id.ispublic);
			if(route.isPublicroute()){
				ispublic.setChecked(true);
			}else{
				ispublic.setChecked(false);
			}
			
			TextView distance =(TextView)this.findViewById(R.id.distance);
			distance.setText(df.format(distanceTotal)+"km");
			route.setDistance((double) distanceTotal);
			
			route.setStartpointlat(SegmentsSingleton.getInstance().getSegments().get(0).getLocations().get(0).getLatitude());
			route.setStartpointlon(SegmentsSingleton.getInstance().getSegments().get(0).getLocations().get(0).getLongitude());
			
			TextView positiveD =(TextView)this.findViewById(R.id.DPositive);
			positiveD.setText(dPos.intValue()+"m");
			route.setPositiveSlope(dPos.intValue());
			
			TextView negativeD =(TextView)this.findViewById(R.id.DNegative);
			negativeD.setText(dNeg.intValue()+"m");
			route.setNegativeSlope(dNeg.intValue());
			
			//Coeficiente de dificultad de la ruta
			ImageView ico =(ImageView)this.findViewById(R.id.type);
			if((int)distanceTotal == 0){
				ico.setImageResource(R.drawable.llana_movistar);
			}else{
				double coefType = dPos/(int)distanceTotal;
				if(coefType<=Integer.parseInt(PropertyLoader.getInstance().getLlanaTop())){
		      	  ico.setImageResource(R.drawable.llana_movistar);
		        }else if(coefType<=Integer.parseInt(PropertyLoader.getInstance().getMedMonTop())){
		      	  ico.setImageResource(R.drawable.mc_movistar);
		        }else{
		        	//comentado porque es la imagen que se carga por defecto
		      	  	//ico.setImageResource(R.drawable.hc_movistar);
		        }
			}
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE)); //Carga cliente de amaz??n
		
		setContentView(R.layout.activity_finish_route);
		
		/** Recupera los datos grabados (segments) de la anterior actividad **/
		getActionBar().hide(); //desactiva actionBar

		/** Carga componentes de p??gina (botones, textos...) **/
		loadViewData();
		
		et_datetodo = (EditText) findViewById(R.id.datetodo);
		
		setTitle(getResources().getString(R.string.description));
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				switch (which){
    				case DialogInterface.BUTTON_POSITIVE:
    					finish();
    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.route_des), Toast.LENGTH_SHORT).show();
    					startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
    					break;

    				case DialogInterface.BUTTON_NEGATIVE:
    					break;
    				}
    			}
    		};
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setMessage(getResources().getString(R.string.cancelMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
	    }
	    return true;
	}


	@Override
	public void onClick(View v) {
		 switch (v.getId()) {
		    case R.id.discardroute:
				WarningsSingleton.getInstance().clear(); 
				SegmentsSingleton.getInstance().clear(); 
		    	
				DialogInterface.OnClickListener dialogClickListenerDiscard = new DialogInterface.OnClickListener() {
	    			@Override
	    			public void onClick(DialogInterface dialog, int which) {
	    				switch (which){
	    				case DialogInterface.BUTTON_POSITIVE:
	    					finish();
	    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.route_des), Toast.LENGTH_SHORT).show();
	    					startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
	    					break;

	    				case DialogInterface.BUTTON_NEGATIVE:
	    					break;
	    				}
	    			}
	    		};
	    		AlertDialog.Builder builderDiscard = new AlertDialog.Builder(this);
	    		builderDiscard.setMessage(getResources().getString(R.string.cancelMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerDiscard)
	    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerDiscard).show();
	    		
		        break;
		    case R.id.saveroute:
		    	EditText name = (EditText) this.findViewById(R.id.name);
		    	if(!name.getText().toString().isEmpty() && !et_datetodo.getText().toString().isEmpty()){
		    		route.setName(name.getText().toString());
		    		boolean publico = route.isPublicroute();
		    		route.setPublicroute(publico);
			    	if(checkOnlineState()){
			    		new S3PutObjectTask().execute(idroute);
			    	}
			    	else{//Si no esta online
			    		//displayAlert(getResources().getString(R.string.error_conexion), getResources().getString(R.string.error_nointernetsave));
			    		new S3PutLocalObjectTask().execute(idroute);
			    	}
		    	}else if(name.getText().toString().isEmpty() && et_datetodo.getText().toString().isEmpty()){
		    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_nonamenodate), Toast.LENGTH_SHORT).show();
		    	}
		    	else if(name.getText().toString().isEmpty()){
		    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_noname), Toast.LENGTH_SHORT).show();
		    	}
		    	else{
		    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_nodate), Toast.LENGTH_SHORT).show();
		    	}
		        break;
		    default:
		        break;
	    }
	}

	
	private class AddRoute extends AsyncTask<Route, Void, Void> {
		protected Void doInBackground(Route... routes) {
	        /** Recupera url para guardarla en la bd **/
			RoutesList routeList = new RoutesList();			
			routeList.addNewRoute(routes[0]);
			RoutesSingleton listRoutes = RoutesSingleton.getInstance();
			listRoutes.addRoute(routes[0]);
			
			return null;
		}

		protected void onPostExecute(Void result) {
			FinishRouteActivity.this.finish();
			startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
		}
	}
	
	
	
	// Display an Alert message for an error or failure.
	protected void displayAlert(String title, String message) {

		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(
				FinishRouteActivity.this.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}
				});

		confirm.show().show();
	}

	protected void displayErrorAlert(String title, String message) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(
				FinishRouteActivity.this.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						FinishRouteActivity.this.finish();
					}
				});

		confirm.show();
	}
	
	
	
	private class S3PutObjectTask extends AsyncTask<String, Void, S3TaskResult> {
		ProgressDialog dialog;

		protected void onPreExecute() {
	        super.onPreExecute();
	        int currentOrientation = getResources().getConfiguration().orientation; 
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
			
			dialog = new ProgressDialog(FinishRouteActivity.this);
			dialog.setMessage(FinishRouteActivity.this
					.getString(R.string.saving));
			dialog.setCancelable(false);
			dialog.show();
		}

		protected S3TaskResult doInBackground(String... strings) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (strings == null || strings.length != 1) {
				return null;
			}

			
			S3TaskResult result = new S3TaskResult();

			// Put the image data into S3.
			try {
		        /**
		         * Ficheros de audio
		         */
				for (int i = 0; i < WarningsSingleton.getInstance().getWarnings().size(); i++) {
					if(WarningsSingleton.getInstance().getWarnings().get(i).hasVoice()){
						File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/files3gp/", WarningsSingleton.getInstance().getWarnings().get(i).getWarningId()+".3gp");
						PutObjectRequest por = new PutObjectRequest(PropertyLoader.getInstance().getBucket3GP(), strings[0]+"/"+WarningsSingleton.getInstance().getWarnings().get(i).getWarningId()+".3gp", f);
						clientManager.s3().putObject(por);		
					}
				}
				
				
				FileOutputStream fileOutputStream = null;
		        FileSegmentExporter segmentExporter = new FileSegmentExporter(SegmentsSingleton.getInstance().getSegments(), 
		        		new GpxSegmentWriter(), route.getName(), WarningsSingleton.getInstance().getWarnings());

		        try {
					fileOutputStream = new FileOutputStream(file);
					segmentExporter.writeSegment(fileOutputStream);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
		        
		        /** Fichero de la ruta **/
				clientManager.s3().getBucketAcl(PropertyLoader.getInstance().getBucketGPX());
				PutObjectRequest filegpx = new PutObjectRequest(PropertyLoader.getInstance().getBucketGPX(), strings[0]+".gpx", file);
				clientManager.s3().putObject(filegpx);
				
				GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
						PropertyLoader.getInstance().getBucketGPX(), "/"+strings[0]+".gpx");

				URL url = clientManager.s3().generatePresignedUrl(urlRequest);
				route.setUrlfilegpx(url.toString());//Guarda la url que se ha asignado al fichero gpx
				deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/"));
			} catch (Exception exception) {

				result.setErrorMessage(exception.getMessage());
			}
			return result;
		}

		protected void onPostExecute(S3TaskResult result) {
			WarningsSingleton.getInstance().clear();
			Toast.makeText(getApplicationContext(), "Ruta guardada correctamente", Toast.LENGTH_LONG).show();
			dialog.dismiss();
			new AddRoute().execute(route);
			if (result.getErrorMessage() != null) {
				displayErrorAlert(
						FinishRouteActivity.this
								.getString(R.string.loading_),
						result.getErrorMessage());
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
	
	private void saveRoute(){
	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	      Editor ed = prefs.edit();
	      ed.putString("localrouteid", route.getId());
	      ed.putString("localroutename", route.getName());
	      ed.putString("localroutecategory", route.getCategory());
	      ed.putInt("localroutepopularity", route.getPopularity());
	      ed.putFloat("localroutedistance", route.getDistance().floatValue());
	      ed.putInt("localroutepositiveSlope", route.getPositiveSlope());
	      ed.putInt("localroutenegativeSlope", route.getNegativeSlope());
	      ed.putString("localroutecreation", route.getCreation());
	      ed.putInt("localroutedifficulty", route.getDifficulty());
	      ed.putFloat("localroutestartpointlat", route.getStartpointlat().floatValue());
	      ed.putFloat("localroutestartpointlon", route.getStartpointlon().floatValue());
	      ed.putBoolean("localroutepublicroute", route.isPublicroute());
	      ed.putString("localrouteurlfilegpx", route.getUrlfilegpx());
	      ed.putString("localroutedatetodo", route.getDatetodo());
	      ed.commit();
	}

	
	//Salvado Local
	private class S3PutLocalObjectTask extends AsyncTask<String, Void, S3TaskResult> {
		ProgressDialog dialog;

		protected void onPreExecute() {
	        super.onPreExecute();
	        int currentOrientation = getResources().getConfiguration().orientation; 
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
			
			dialog = new ProgressDialog(FinishRouteActivity.this);
			dialog.setMessage(FinishRouteActivity.this
					.getString(R.string.saving));
			dialog.setCancelable(false);
			dialog.show();
		}

		protected S3TaskResult doInBackground(String... strings) {//strings[0] es el id
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (strings == null || strings.length != 1) {
				return null;
			}

			
			S3TaskResult result = new S3TaskResult();

			// Put the image data into S3.
			try {
				saveRoute();
				
				FileOutputStream fileOutputStream = null;
		        FileSegmentExporter segmentExporter = new FileSegmentExporter(SegmentsSingleton.getInstance().getSegments(), 
		        		new GpxSegmentWriter(), route.getName(), WarningsSingleton.getInstance().getWarnings());

		        try {
					fileOutputStream = new FileOutputStream(file);
					segmentExporter.writeSegment(fileOutputStream);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}					
			} catch (Exception exception) {

				result.setErrorMessage(exception.getMessage());
			}
			return result;
		}

		protected void onPostExecute(S3TaskResult result) {
			WarningsSingleton.getInstance().clear();
			displayErrorAlert(getResources().getString(R.string.error_nointernet), getResources().getString(R.string.error_localinternet));
		}
	}
	
	
	
	
	Calendar myCalendar = Calendar.getInstance();
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
	        myCalendar.set(Calendar.YEAR, year);
	        myCalendar.set(Calendar.MONTH, monthOfYear);
	        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
	        updateLabel();			

	        /*if(myCalendar.getTime().before(new Date())){NO BORRAR, ES ??TIL PARA FILTRAR LA FECHA
		    	Log.i("FinishRouteActivity","Es anterior a la fecha de hoy");
		    	Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_datebefore), Toast.LENGTH_LONG).show();
	        }
	        else{
		        updateLabel();			
	        }*/
		}
	};

	public void showDatePickerDialog(View v) {
		DatePickerDialog d;
		d = new DatePickerDialog(FinishRouteActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
		d.show();
	}
	
	@SuppressWarnings("deprecation")
	private void updateLabel() {
	    String myFormat = "dd/MM/yy"; //In which you need put here
	    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
	    
	    et_datetodo.setText(sdf.format(myCalendar.getTime()));	
	    
	    Date d = myCalendar.getTime();
	    route.setDatetodo(String.valueOf(d.getYear()+"/"+d.getMonth()+"/"+d.getDate()));
	}	
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}
}
