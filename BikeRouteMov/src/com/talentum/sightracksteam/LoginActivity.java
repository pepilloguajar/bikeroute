/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.sightracksteam;

import com.amazonaws.tvmclient.Response;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AlertActivity {

	protected Button okButton;
	protected EditText username;
	protected EditText password;
	protected TextView introText;
	protected TextView usernameHeader;
	protected TextView passwordHeader;
	protected ProgressDialog pDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_menu);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
		username = (EditText) findViewById(R.id.login_username_input_field);
		password = (EditText) findViewById(R.id.login_password_input_field);

		okButton = (Button) findViewById(R.id.login_main_ok_button);

		wireButtons();
	}

	public void wireButtons() {
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				int duration = Toast.LENGTH_SHORT;
				if(username.getText().toString().equals("")){
					CharSequence text = getResources().getString(R.string.usernameclear);
					Toast toast = Toast.makeText(context, text, duration);
					InputMethodManager imm = (InputMethodManager)getSystemService(
							Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
					toast.show();
				}
				else if(password.getText().toString().equals("")){
					CharSequence text = getResources().getString(R.string.passclear);
					Toast toast = Toast.makeText(context, text, duration);
					InputMethodManager imm = (InputMethodManager)getSystemService(
							Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
					toast.show();
				}
				else if(username.getText().toString().equals("movistarteam") || username.getText().toString().equals("movistarteam1") || 
						username.getText().toString().equals("movistarteam2") || username.getText().toString().equals("movistarteam3")
						|| username.getText().toString().equals("jose")){
					new TVMLoginTask().execute();
				}
				else{
					displayCredentialsIssueAndExit();
				}
			}
		});

	}

	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(getResources().getString(R.string.error_credentials));
		confirm.setMessage(getResources().getString(R.string.error_descredentials));
		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		confirm.show().show();
	}

	/**
	 * 
	 * @param response
	 */
	protected void displayErrorAndExit(Response response) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		if (response == null) {
			confirm.setTitle("Error Code Unkown");
			confirm.setMessage("Please review the README file.");
		} else {
			confirm.setTitle(getResources().getString(R.string.error_enter));
			confirm.setMessage(getResources().getString(R.string.error_desenter));
		}

		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//finish();
			}
		});
		confirm.show().show();
	}
	
	 public void gohelp(View view) {
     	Intent intent = new Intent(Intent.ACTION_VIEW);
     	intent.setData(Uri.parse("https://s3-us-west-2.amazonaws.com/sightracksteam/help.html"));
     	startActivity(intent);
	 }
	 
	 public void gotalentum(View view) {
     	Intent intent = new Intent(Intent.ACTION_VIEW);
     	intent.setData(Uri.parse("https://talentum.telefonica.com"));
     	startActivity(intent);
	 }	
	 
	 
   /***************** TAREAS AS??NCRONAS *****************/
	private class TVMLoginTask extends AsyncTask<Void, Void, Response> {
	
	    @Override
	    protected void onPreExecute() {
	        // TODO Auto-generated method stub
	        super.onPreExecute();
	        int currentOrientation = getResources().getConfiguration().orientation; 
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
	     // TODO Auto-generated method stub
	        pDialog = new ProgressDialog(LoginActivity.this);
	        pDialog.setCancelable(true);
	        pDialog.show();
	    }
	    
		protected Response doInBackground(Void... voids) {
			try {
				return MainActivity.clientManager.login(username
						.getText().toString(), password.getText().toString());
				
			} catch (Throwable e) {
				setStackAndPost(e);
			}
			return null;
		}

		protected void onPostExecute(Response response) {
		       pDialog.dismiss();
			if (response != null && response.getResponseCode() == 404) {
				LoginActivity.this.displayCredentialsIssueAndExit();
				//LoginActivity.this.displayErrorAndExit(response); Respuesta de Amazon
			} else if (response != null && response.getResponseCode() != 200) {
				LoginActivity.this.displayCredentialsIssueAndExit();
			} else {
			   startActivity(new Intent(LoginActivity.this, MainActivity.class));
				finish();
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
}
