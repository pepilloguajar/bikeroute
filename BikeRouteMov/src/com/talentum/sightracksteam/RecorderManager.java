package com.talentum.sightracksteam;

import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;

public class RecorderManager extends MediaPlayer{
	    private final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();

	    private MediaRecorder mRecorder = null;
	    private MediaPlayer   mPlayer = null;
	    
	    public RecorderManager(){
	    	mRecorder = new MediaRecorder(); 
	    	mPlayer = new MediaPlayer();
	    	mPlayer.setOnCompletionListener(new OnCompletionListener() {
		        @Override
		        public void onCompletion(MediaPlayer mp) {
		        	mPlayer.stop();
		        	mPlayer.reset();
		        }
		        	
		        });
	    }

	    public void onRecord(boolean start, String mFileName) {
	        if (start) {
	            startRecording(mFileName);
	        } else {
	            stopRecording();
	        }
	    }

	    public void startPlaying(String mFileName) {
        	//mPlayer.reset();
            try {
				mPlayer.setDataSource(mFileName);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            try {
				mPlayer.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            mPlayer.start();
	    }


	    private void startRecording(String mFileName) {
	        mRecorder = new MediaRecorder();
	        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	        mRecorder.setOutputFile(PATH+"/Android/data/com.talentum.bikeroute/files3gp/"+mFileName+".3gp");
	        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
				mRecorder.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        mRecorder.start();
	    }

	    private void stopRecording() {
	        mRecorder.stop();
	        mRecorder.release();
	        mRecorder = null;
	    }
}
