package com.talentum.sightracksteam;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talentum.sightracksteam.RecorderManager;
import com.talentum.sightracksteam.model.Segment;
import com.talentum.sightracksteam.model.UserWarning;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.SegmentsSingleton;
import com.talentum.sightracksteam.utils.WarningsSingleton;

import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;

public class NewRouteMovActivity extends ActionBarActivity implements RightControlFragment.OnNewWarningListener,
ConnectionCallbacks,
OnConnectionFailedListener,
LocationListener,
OnMyLocationButtonClickListener
{
	
	private GoogleMap mMap;
	private PolylineOptions polylineOptions;
	
	SegmentsSingleton listSegments = SegmentsSingleton.getInstance();
	private ArrayList<Segment> segments;
	private List<UserWarning> warnings;
	private Segment currentSegment;
	private LocationClient mLocationClient;
	private LocationManager locationManager;
	private String provider;
	private int lineRoute = Integer.parseInt(PropertyLoader.getInstance().getLineRoute());
	  
	private Location loc = null;
	private boolean recording;
    protected PowerManager.WakeLock wakelock;
	
	
    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(Integer.valueOf(PropertyLoader.getInstance().getIntervalGPS()))         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	
	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //evitar que la pantalla se apague
        final PowerManager pm=(PowerManager)getSystemService(this.POWER_SERVICE);
        this.wakelock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "etiqueta");
        wakelock.acquire();
		
		
		getActionBar().hide();
		
		/** ---->>> Crea fichero GPS con cabeceras **/
		segments = new ArrayList<Segment>();
		
		//ArrayList con los avisos
		warnings = WarningsSingleton.getInstance().getWarnings();
		
		recording = true;
	    // Get the location manager
	    locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
	    
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
	    
	    Location location = locationManager.getLastKnownLocation(provider);
	    
	    new RecorderManager();

	    // Initialize the location fields
	    if (location != null) {
	      System.out.println("Provider " + provider + " has been selected.");
	    } 		
		
		/** Empieza reconocimiento **/
	    polylineOptions = new PolylineOptions().width(lineRoute).color(0xff062344);
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.recording), Toast.LENGTH_LONG).show();

	    setContentView(R.layout.activity_new_route_mov);
	}
	
	@SuppressLint("Wakelock")
	protected void onDestroy(){
        super.onDestroy();
        this.wakelock.release();
    }
	
    @Override
    protected void onResume() {
        super.onResume();
        wakelock.acquire();
        setUpMapIfNeeded();
        setUpLocationClientIfNeeded();
        mLocationClient.connect();
    }
    
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (mLocationClient != null) {
        	/** ------>>> Cierra intervalo **/
        	segments.add(currentSegment);
        	currentSegment = new Segment(); 
    	    polylineOptions = new PolylineOptions().width(lineRoute).color(0xff062344);
    	    loc = null;
            mLocationClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
    	// Do a null check to confirm that we have not already instantiated the map.
    	if (mMap == null) {
    		// Try to obtain the map from the SupportMapFragment.
    		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
    				.getMap();
    		// Check if we were successful in obtaining the map.
    		if (mMap != null) {
    			mMap.setMyLocationEnabled(true);
    			mMap.setOnMyLocationButtonClickListener(this);

    			mMap.setOnMarkerClickListener(new OnMarkerClickListener() {			
    				@Override
    				public boolean onMarkerClick(Marker marker) {
    					//String mFileName = marker.getTitle();
    					//LatLng markerLocation= marker.getPosition();
//    					for(int i=0; i<warnings.size(); i++){
//    						warnings.get(i).getLocation().equals(new Location(markerLocation.latitude,markerLocation.longitude));
//    					}
    					Log.i("MyMap","markerLocation. Lon: "+marker.getPosition().longitude+" / Lat: "+marker.getPosition().latitude);
    					for(int i=0; i<warnings.size(); i++){
    						Double lon = warnings.get(0).getLocation().getLongitude();
    						Double lat = warnings.get(0).getLocation().getLatitude();
    						Log.i("MyMap","warningLocation. Lon: "+lon+" / Lat: "+lat);
    						double tolerance = 0.0000001;
    						if(marker.getPosition().longitude - warnings.get(0).getLocation().getLongitude() <= tolerance)
    							Log.i("MyMap","Coinciden!");
    					}
    					//String markerId = marker.getId();
    					//warnings.get(Integer.parseInt(markerId.substring(1)));
    					//recorderManager.startPlaying("/"+mFileName+".3gp");
    					return false;
    				}
    			});
    		}
    	}
    }

    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }
    
    @Override
	public boolean onMyLocationButtonClick() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public void onLocationChanged(Location location) {
        if(loc != null){
            mMap.addPolyline(polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude())).width(lineRoute).color(0xff062344));
        }
        if(segments.isEmpty() && currentSegment.getLocations().size() == 1){
    		BitmapDescriptor icon_start = null;
    		icon_start = BitmapDescriptorFactory.fromResource(R.drawable.market_start);
    	    onNewMarket(icon_start,new LatLng(location.getLatitude(), location.getLongitude()));
        }
    	loc = new Location(location);
    	
    	
    	/** Guarda la posicion loc en el fichero GPS **/
        currentSegment.addLocation(loc);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
		mMap.animateCamera(cameraUpdate);
	}



	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
	}



	@Override
	public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener

        Location location = mLocationClient.getLastLocation();
        loc = location;
        
        
    	/** ------>>> Abre intervalo **/
		currentSegment = new Segment();
        currentSegment.addLocation(location);
        
        
        /* Se centra la camara */
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        mMap.animateCamera(cameraUpdate);  
        
//		BitmapDescriptor icon_start = null;
//		icon_start = BitmapDescriptorFactory.fromResource(R.drawable.market_start);
//	    onNewMarket(icon_start,latLng);
	}



	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
	
	public void pauseresumeRoute(View view){
		ImageButton pauseresume  = (ImageButton)findViewById(R.id.pause_button);
		
		if(recording == true){//Se pausa
			pauseresume.setImageResource(R.drawable.start_button);
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.pause), Toast.LENGTH_LONG).show();
			onPause();
			recording = false;		
		}
		else{//Vuelve a grabar
			pauseresume.setImageResource(R.drawable.pause_button);
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.recording), Toast.LENGTH_LONG).show();
			onResume();
			recording = true;
		}
	}
	
	//Finalizamos la ruta y pasamos a asignarle nombre y guardarla
	public void finishRoute(View view){		
		/** -------> Pone cabeceras finales al fichero y calcula medidas **/
    	listSegments.setSegments(segments);
		
    	
		finish();
		Intent intent = new Intent(this, FinishRouteActivity.class);
		startActivity(intent);
	}
	
	public void onNewMarket(BitmapDescriptor icon, LatLng l){
		mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(getResources().getString(R.string.ini_sight))
		.icon(icon));		
	}
	
	@Override
	public void onNewWarning(UserWarning warning) {
		BitmapDescriptor icon_market = null;
		if(warning.hasVoice()){
			icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_micro);
		}
		else{
			if(warning.getWarningDescription().contains("gr")){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_unevenroad);
			}
			else if(warning.getWarningDescription().contains("dc")){
				if(warning.getWarningDescription().contains("le")){
					if(warning.getWarningDescription().contains("ff")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left45);
					}
					else if(warning.getWarningDescription().contains("ni")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left90);
					}
					else{
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left180);
					}
				}
				else if(warning.getWarningDescription().contains("ri")){
					if(warning.getWarningDescription().contains("ff")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right45);
					}
					else if(warning.getWarningDescription().contains("ni")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right90);
					}
					else{
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right135);
					}
				}		
				else{
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_zigzag);
				}
			}
			else if(warning.getWarningDescription().contains("wi")){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_wind);
			}
			else if(warning.getWarningDescription().contains("sb")){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_baden);
			}
			else if(warning.getWarningDescription().contains("sc")){
				if(warning.getWarningDescription().contains("as")){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopeup);
				}
				else if(warning.getWarningDescription().contains("de")){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopedown);
				}
			}
			else if(warning.getWarningDescription().contains("rb")){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_roundabout);
			}
			else if(warning.getWarningDescription().contains("dczi")){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_zigzag);
			}			
		}
		String title = getResources().getString(getResources().getIdentifier(warning.getWarningDescription(),"string",this.getPackageName()));
		mMap.addMarker(new MarkerOptions()
				.position(new LatLng(warning.getLocation().getLatitude(),warning.getLocation().getLongitude()))
				.title(title)
				.icon(icon_market))
				.setDraggable(true);
		
		//Si comenzar es false significa que se ha levantado el dedo del boton de grabar y hay que detener la grabacion
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
						Intent in = new Intent(getApplicationContext(),MainActivity.class);
						startActivity(in);
						finish();

			            //break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.exit)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
			
		}
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) || (keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            Log.i("NewRouteMovActivity","volumen");
        }
		
	    return true;
	}	
	public Location getLocation(){
		return loc;
	}
	
	public boolean isRecording(){
		return recording;
	}
}



