package com.talentum.sightracksteam;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.talentum.sightracksteam.model.Route;
import com.talentum.sightracksteam.sdb.RoutesList;
import com.talentum.sightracksteam.utils.RoutesSingleton;





public class RouteDetails extends Activity implements OnClickListener {
	
	boolean nameChange = false;
	boolean dateChange = false;
	boolean ischeck;
	boolean checkChange = false;
    public EditText et_datetodo;	  
	public Route route;
	Date newdatetodo;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.route_details);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Bundle extras = getIntent().getExtras();
		Route routes = (Route) extras.get("route");
		
		EditText name = (EditText)this.findViewById(R.id.name);
		name.setText(routes.getName());
		
		et_datetodo = (EditText) findViewById(R.id.datetodo);

		String patron = "dd/MM/yyyy";
	    SimpleDateFormat formato = new SimpleDateFormat(patron);
	    
	    if(routes.getDatetodo() == null || routes.getDatetodo().equals(""))
			et_datetodo.setText(getResources().getString(R.string.determined));
	    else{
		    Date dtodo = new Date(routes.calculateDatetodo().getTime());
			et_datetodo.setText(formato.format(dtodo));
	    }
	    
		final CheckBox ispublic = (CheckBox)this.findViewById(R.id.ispublico);
		if(routes.isPublicroute()){
			ispublic.setChecked(true);
		}else{
			ispublic.setChecked(false);
		}
		
		//Redondeo a dos decimales
		DecimalFormat df = new DecimalFormat("##.##");
		df.setRoundingMode(RoundingMode.UP);
		TextView distance =(TextView)this.findViewById(R.id.distance);
		distance.setText(df.format(routes.getDistance())+"km");
		
		TextView positiveD =(TextView)this.findViewById(R.id.DPositive);
		positiveD.setText((int)routes.getPositiveSlope()+"m");
		
		TextView negativeD =(TextView)this.findViewById(R.id.DNegative);
		negativeD.setText((int)routes.getNegativeSlope()+"m");
		
		TextView dateDetails =(TextView)this.findViewById(R.id.dateDetails);
		Date d = new Date(Long.valueOf(routes.getCreation()));
		dateDetails.setText(formato.format(d));

		
		ImageView ico =(ImageView)this.findViewById(R.id.type);
		if(routes.getCategory()!=null){
			if(routes.getCategory().compareTo("LL")==0){
	      	  ico.setImageResource(R.drawable.llana_movistar);
	        }else if(routes.getCategory().compareTo("MC")==0){
	      	  ico.setImageResource(R.drawable.mc_movistar);
	        }else{
	        	//comentado porque es la imagen que se carga por defecto
	      	  //ico.setImageResource(R.drawable.hc_movistar);
	        }
		}else{
			ico.setImageResource(R.drawable.llana_movistar);
		}
		
		
		name.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
	            nameChange = true;	        	  
	          }

	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	       });
		
		
		ispublic.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			 
			   @Override
			   public void onCheckedChanged(CompoundButton arg0, boolean checked) {
			      if (checked){
			    	  checkChange=true;
			    	  ischeck = true;
			      }
			      else {
			    	  checkChange=true;
			    	  ischeck = false;
			      }
			   }
		});
		  		      
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home: // ID del boton
	    	if (nameChange==true || checkChange==true || dateChange == true) {
				Bundle extras = getIntent().getExtras();
				route = (Route) extras.get("route");
				RoutesSingleton listRoutes = RoutesSingleton.getInstance();
				if(dateChange == true){
				    route.setDatetodo(String.valueOf(newdatetodo.getYear()+"/"+newdatetodo.getMonth()+"/"+newdatetodo.getDate()));
				}
				if(nameChange==true && checkChange==true){
					//guardo el nuevo nombre de la ruta
					EditText name = (EditText)this.findViewById(R.id.name);
					route.setName(name.getText().toString());
					route.setPublicroute(ischeck);
				}else if(nameChange==true && checkChange==false){
					//guardo el nuevo nombre de la ruta
					EditText name = (EditText)this.findViewById(R.id.name);
					route.setName(name.getText().toString());
				}else{
					route.setPublicroute(ischeck);
				}
				new ChangeNameAndPublic().execute(route);
				listRoutes.setNameandPublic(route);
	    	}
	        finish(); 
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
	

	
	
	@SuppressWarnings({ "deprecation" })
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

			if (keyCode == KeyEvent.KEYCODE_BACK && (nameChange==true || checkChange==true || dateChange == true)) {
				Bundle extras = getIntent().getExtras();
				Route routes = (Route) extras.get("route");
				if(dateChange == true){
				    route.setDatetodo(String.valueOf(newdatetodo.getYear()+"/"+newdatetodo.getMonth()+"/"+newdatetodo.getDate()));
				}
				
				if(nameChange==true && checkChange==true){
					//guardo el nuevo nombre de la ruta
					EditText name = (EditText)this.findViewById(R.id.name);
					routes.setName(name.getText().toString());
					routes.setPublicroute(ischeck);
				}else if(nameChange==true && checkChange==false){
					//guardo el nuevo nombre de la ruta
					//EditText name = (EditText)this.findViewById(R.id.name);
				}else{
					routes.setPublicroute(ischeck);
				}
				new ChangeNameAndPublic().execute(routes);			
				finish();
		    }

	    return super.onKeyDown(keyCode, event);
	}
	
	
	Calendar myCalendar = Calendar.getInstance();
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
	        myCalendar.set(Calendar.YEAR, year);
	        myCalendar.set(Calendar.MONTH, monthOfYear);
	        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
	        updateLabel();			
		}
	};

	public void showDatePickerDialog(View v) {
		DatePickerDialog d;
		d = new DatePickerDialog(RouteDetails.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
		d.show();
	}
	
	private void updateLabel() {
	    String myFormat = "dd/MM/yy"; //In which you need put here
	    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
		
	    et_datetodo = (EditText) findViewById(R.id.datetodo);
	    newdatetodo = myCalendar.getTime();
	    et_datetodo.setText(sdf.format(newdatetodo));	
	    
	    dateChange = true;
	}	

	
	
	private class ChangeNameAndPublic extends AsyncTask<Route, Void, Void> {
		protected Void doInBackground(Route... routes) {
			RoutesList routeList = new RoutesList();
			routeList.changeAttrListRoute(routes[0]);

			return null;
		}

		protected void onPostExecute(Void result) {
			RouteDetails.this.finish();
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
}
