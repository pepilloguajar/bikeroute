package com.talentum.sightracksteam.model;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class Segment implements Parcelable{
	
	private int numberOfPoints = 0;
	private ArrayList<Location> locations = new ArrayList<Location>();
	
	public Segment(){
		
	}
	
	public Segment(Parcel in){		
	    numberOfPoints = in.readInt();
	    ClassLoader classLoader = getClass().getClassLoader();
	    for (int i = 0; i < numberOfPoints; ++i) {
	        Location location = in.readParcelable(classLoader);
	        locations.add(location);
	      }
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
	    dest.writeInt(numberOfPoints);
	    for (int i = 0; i < numberOfPoints; ++i) {
	        dest.writeParcelable(locations.get(i), 0);
	      }
	}
	
	public ArrayList<Location> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}
	
	public int getNumberOfPoints() {
		return numberOfPoints;
	}


	public void setNumberOfPoints(int numberOfPoints) {
		this.numberOfPoints = numberOfPoints;
	}


	public void addLocation(Location loc){
		locations.add(loc);
	}
}
