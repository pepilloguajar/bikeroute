package com.talentum.sightracksteam.model;

import android.location.Location;

public class UserWarning {

	private Location location;
	private float distanceToStart;
	private int playDistance;
	private boolean isPredefined;
	private boolean hasVoice;
	private int warningId;
	private String warningDescription;


	public UserWarning(Location location, boolean isAvisoPredefinido, int warningId, 
			String warningDescription, boolean hasVoice, int playDistance){
		this.location = location;
		this.isPredefined = isAvisoPredefinido;
		this.hasVoice = hasVoice;
		this.warningId = warningId;
		this.warningDescription = warningDescription;
		this.playDistance = playDistance;
		//this.distanceToStart = calculateDistanceToStart();
		//Log.i("MyMap","distanceToStart: "+distanceToStart);
	}

	//getters y setters
	public Location getLocation(){
		return location;
	}

	public float getDistanceToStart(){
		return distanceToStart;
	}

	public boolean isPredefinedWarning(){
		return isPredefined;
	}

	public boolean hasVoice(){
		return hasVoice;
	}

	public int getPlayDistance(){
		return playDistance;
	}

	public int getWarningId(){
		return warningId;
	}

	public String getWarningDescription(){
		return warningDescription;
	}

	/*private float calculateDistanceToStart(){
		//Lineas de c??????digo que calculan la distancia de la ruta y el desnivel positivo y negativo
		float totalDistance=0;
		SegmentsSingleton segments = SegmentsSingleton.getInstance();
		//while (location.distanceTo(dest))
		ArrayList<Location> locationList = new ArrayList<Location>();
		for(int i=0; i<segments.getSegments().size(); i++){
			locationList.addAll(segments.getSegments().get(i).getLocations());
		}
		int j=0;
		if(j<locationList.size()){
			while(location.distanceTo(locationList.get(j)) > location.distanceTo(locationList.get(j+1))){
				totalDistance+=locationList.get(j).distanceTo(locationList.get(j+1));
				j++;
			}
			//Falta la distancia entre el ultimo punto y el aviso
			totalDistance+=locationList.get(j).distanceTo(location);
		}
		//			for(int j=0; j<segments.getSegments().get(i).getLocations().size() - 1; j++){
		//				if(!(i==0 && j==0)){// Este if est?????? proque el primer tramo de puntos toma un valor an??????malo
		//					if(firstIteration){
		//						if(changeSegment){
		//							loc_a = loc_b;
		//							loc_b = segments.getSegments().get(i).getLocations().get(j);
		//							//distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
		//							changeSegment=false;
		//						}else{
		//							loc_a = segments.getSegments().get(i).getLocations().get(j);
		//							loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
		//							distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
		//						}
		//						firstIteration=false;
		//					}else{
		//						loc_a = loc_b;
		//						loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
		//						distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
		//					}
		//					Log.i("Distancia cada punto", "distancia entre punto: "+ j+" y "+(j+1)+": "+loc_a.distanceTo(loc_b));
		//				}
		//				if(j==segments.getSegments().get(i).getLocations().size() - 2){
		//					changeSegment=true;
		//				}
		//				//Log.i("FinishRouteActivity", "SEGMENTO: "+ i + " " +segments.getSegments().get(i).getLocations().get(j).getLatitude());
		//			}
		return totalDistance;
	}*/


}
