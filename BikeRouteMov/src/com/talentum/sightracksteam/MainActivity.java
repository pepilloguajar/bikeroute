/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.sightracksteam;

import java.io.File;
import java.net.URL;

import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.amazonaws.tvmclient.Response;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.talentum.sightracksteam.model.Route;
import com.talentum.sightracksteam.sdb.RoutesList;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.RoutesSingleton;
import com.talentum.sightracksteam.utils.WarningsSingleton;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

public class MainActivity extends Activity {
	public String routeid;
	public Route route;
	private ProgressBar spinner;
	public static AmazonClientManager clientManager = null;

	
	
	/**
	 * Check Test Connection Internet 
	 * @return
	 */
	private boolean checkOnlineState() {
	    ConnectivityManager CManager =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo NInfo = CManager.getActiveNetworkInfo();
	    if (NInfo != null && NInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}

    public static boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          if (files == null) {
              return true;
          }
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return true;
        //return( path.delete() );
      }
    
	protected void displayErrorConnection() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(getResources().getString(R.string.error_internet));
		confirm.setMessage(getResources().getString(R.string.error_desinternet));
		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		confirm.show().show();
	}	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		spinner = (ProgressBar)findViewById(R.id.progressBar1);
		spinner.setVisibility(View.GONE);
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
		if(checkGooglePlayServices()){
			if(checkOnlineState()){
				if (!MainActivity.clientManager.hasCredentials()) {
					this.displayCredentialsIssueAndExit();
					finish();
				} else if (!MainActivity.clientManager.isLoggedIn()) {
					startActivity(new Intent(MainActivity.this,LoginActivity.class));
					finish();
				} else {
					//Mira si esta localmente guardada alguna ruta y se sube
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
					routeid = prefs.getString("localrouteid", "");
					new GetRoutes().execute();
				}			
			}
			else{
				displayErrorConnection();
			}
		}
	}

	/**
	 * Al reanudar
	 */
	/*protected void onResume() {
		super.onResume();
		if (!MainActivity.clientManager.isLoggedIn()) {
			Log.i("MainActivity", "NO esta LOGEADO RESUMMEEEEE----");
			startActivity(new Intent(MainActivity.this,LoginActivity.class));
		} else {
			Log.i("MainActivity", "CARGA RUTAS RESUMEEEE----");
			new GetRoutes().execute();
		}
	}*/

	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setMessage(getResources().getString(R.string.error_userpassword));
		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

	/**
	 * Muestra el error por fallo del Login
	 * @param response
	 */
	protected void displayErrorAndExit(Response response) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		if (response == null) {
			confirm.setTitle("Error Code Unkown");
			confirm.setMessage("Please review the README file.");
		} else {
			confirm.setTitle("Error Code [" + response.getResponseCode() + "]");
			confirm.setMessage(response.getResponseMessage()
					+ "\nPlease review the README file.");
		}

		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

	
	private void cleanRoutePref(){
	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	      Editor ed = prefs.edit();
	      ed.putString("localrouteid", "");
	      ed.commit();
	}
	
	/**
	 * Check Google Play Services
	 * @return
	 */
	private boolean checkGooglePlayServices() {
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS){
			return true;
		}
		else{
			try {
				GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), this, 1).show();
			} catch (Exception e) {
			}
			return false;
		}

	}
	
	/***************** TAREAS AS??NCRONAS *****************/
	/**
	 * Obtiene las Rutas y las almacena en RoutesSingleton (para poder estar accessibles desde cualquier sitio)
	 * @author escabia
	 *
	 */
	private class GetRoutes extends AsyncTask<Void, Void, Void> {
       @Override
       protected void onPreExecute() {
           super.onPreExecute();
	        int currentOrientation = getResources().getConfiguration().orientation; 
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
   			spinner.setVisibility(View.VISIBLE);
       }

       @Override
       protected Void doInBackground(Void... arg0) {
    	   RoutesSingleton listRoutes = RoutesSingleton.getInstance();
           RoutesList rlist = new RoutesList();
           listRoutes.setListRoutes(rlist.getRoutes());
           
           
           
           if(!routeid.equals("")){
        	   Log.i("MainActivity","***********Si esta guardada localmente");
        	   //////////////
	           SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        	   route = new Route();
               route.setId(prefs.getString("localrouteid", ""));
               route.setName(prefs.getString("localroutename", ""));
               route.setCategory(prefs.getString("localroutecategory", ""));
               route.setPopularity(prefs.getInt("localroutepopularity", 0));
               route.setDistance((double)prefs.getFloat("localroutedistance", 0));
               route.setPositiveSlope(prefs.getInt("localroutepositiveSlope", 0));
               route.setNegativeSlope(prefs.getInt("localroutenegativeSlope", 0));
               route.setCreation(prefs.getString("localroutecreation", ""));
               route.setDifficulty(prefs.getInt("localroutedifficulty", 0));
               route.setStartpointlat((double)prefs.getFloat("localroutestartpointlat", 0));
               route.setStartpointlon((double)prefs.getFloat("localroutestartpointlon", 0));
               route.setPublicroute(prefs.getBoolean("localroutepublicroute", false));
               route.setUrlfilegpx(prefs.getString("localrouteurlfilegpx", ""));
               route.setDatetodo(prefs.getString("localroutedatetodo", ""));
        	   
        	    File file = new File(Environment.getExternalStorageDirectory(), "Android/data/com.talentum.bikeroute/temp.gpx");
        	   
        	   
		        /**
		         * Ficheros de audio
		         */
				for (int i = 0; i < WarningsSingleton.getInstance().getWarnings().size(); i++) {
					if(WarningsSingleton.getInstance().getWarnings().get(i).hasVoice()){
						File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/files3gp/", WarningsSingleton.getInstance().getWarnings().get(i).getWarningId()+".3gp");
						PutObjectRequest por = new PutObjectRequest(PropertyLoader.getInstance().getBucket3GP(), routeid+"/"+WarningsSingleton.getInstance().getWarnings().get(i).getWarningId()+".3gp", f);
						clientManager.s3().putObject(por);		
					}
				}
		        
		        /** Fichero de la ruta **/
				Log.i("MainActivity", "Se guardan los ficheros de audio");
				clientManager.s3().getBucketAcl(PropertyLoader.getInstance().getBucketGPX());
				
				
				Log.i("MainActivity", "Bucket ACL");
				
				
				PutObjectRequest filegpx = new PutObjectRequest(PropertyLoader.getInstance().getBucketGPX(), routeid+".gpx", file);
				clientManager.s3().putObject(filegpx);
				
				GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
						PropertyLoader.getInstance().getBucketGPX(), "/"+routeid+".gpx");

				URL url = clientManager.s3().generatePresignedUrl(urlRequest);
				route.setUrlfilegpx(url.toString());//Guarda la url que se ha asignado al fichero gpx
				deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/"));        	   
        	   
           }

           return null;
       }

       @Override
       protected void onPostExecute(Void result) {
           super.onPostExecute(result);
           
           File folder = new File(Environment.getExternalStorageDirectory() + PropertyLoader.getInstance().getPathallroutes());
                      
           if(!folder.exists()){
	           if (!folder.exists()) {
	               folder.mkdir();
	           }
           }          
           
           File folder3gp = new File(Environment.getExternalStorageDirectory() + PropertyLoader.getInstance().getPathallroutes() +"files3gp/");
           if(!folder3gp.exists()){
	           if (!folder3gp.exists()) {
	               folder3gp.mkdir();
	           }
           }
           File folderGpx = new File(Environment.getExternalStorageDirectory() + PropertyLoader.getInstance().getPathallroutes() +"filesGPX/");
           if(!folderGpx.exists()){
	           if (!folderGpx.exists()) {
	               folderGpx.mkdir();
	           }
           }
           if(!routeid.equals("")){
				new AddRoute().execute(route);
	    	    RoutesSingleton listRoutes = RoutesSingleton.getInstance();
	    	    listRoutes.addRoute(route);
				cleanRoutePref();
           }
           
		   startActivity(new Intent(MainActivity.this, RoutesListActivity.class));
		   if(spinner.isActivated())
			   spinner.setActivated(false);
		   spinner.destroyDrawingCache();
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	   		finish();
       }
	}	   
	
	private class AddRoute extends AsyncTask<Route, Void, Void> {
		protected Void doInBackground(Route... routes) {
	        /** Recupera url para guardarla en la bd **/
			RoutesList routeList = new RoutesList();			
			routeList.addNewRoute(routes[0]);
			RoutesSingleton listRoutes = RoutesSingleton.getInstance();
			listRoutes.addRoute(routes[0]);
			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
}