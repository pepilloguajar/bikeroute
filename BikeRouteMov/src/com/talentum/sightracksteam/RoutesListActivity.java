package com.talentum.sightracksteam;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.sightracksteam.DrawRouteActivity;
import com.talentum.sightracksteam.model.Route;
import com.talentum.sightracksteam.sdb.RoutesList;
import com.talentum.sightracksteam.utils.PropertyLoader;
import com.talentum.sightracksteam.utils.RoutesSingleton;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;


public class RoutesListActivity extends ListActivity implements OnQueryTextListener, OnActionExpandListener{
		public int positiontodelete;
		public static AmazonClientManager clientManager = null;
		RoutesSingleton listRoutes = RoutesSingleton.getInstance();
		RoutesMovAdapter adapter;
		List<Route> routesReverse;
		ArrayList<String> names3gp;

		private boolean gpsenabled(){
			boolean gpsenabled = false;
		    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		    if(provider.contains("gps")){ //if gps is disabled
		    	gpsenabled = true;
		    }
		    return gpsenabled;
		}
		
	    /**
	     * Escribe un inputstream con nombre name en la carpeta de la aplicaci??n
	     * @param inputStream
	     * @param name
	     */
	    private void writefile(InputStream inputStream, String routeId, String name){
	    	OutputStream outputStream = null;
	     
	    	try {
	    		// write the inputStream to a FileOutputStream
	    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/files3gp/"+routeId);
	            if(!routeFolder.exists()){
     	           if (!routeFolder.exists()) {
     	               routeFolder.mkdir();
     	           }
                }
	    		outputStream = 
	                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/", name));
	     
	    		int read = 0;
	    		byte[] bytes = new byte[1024];
	     
	    		while ((read = inputStream.read(bytes)) != -1) {
	    			outputStream.write(bytes, 0, read);
	    		}
	          
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	} finally {
	    		if (inputStream != null) {
	    			try {
	    				inputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		}
	    		if (outputStream != null) {
	    			try {
	    				// outputStream.flush();
	    				outputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	     
	    		}
	    	}    	
	    }
		protected void displayGPSenabled() {
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
			        	Intent intentDisplaySetting = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			        	startActivity(intentDisplaySetting);
			        	break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.error_gpsdis)).setPositiveButton(getResources().getString(R.string.accept), dialogClickListener)
			    .show();
		}
	    
	    
		
		protected void displayDeleteRoute() {
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
						new DeleteRoute().execute(listRoutes.getListRoutes().get(positiontodelete));
						listRoutes.getListRoutes().remove(positiontodelete);
						adapter.notifyDataSetChanged();
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.deleterec) + listRoutes.getListRoutes().get(positiontodelete).getName() + "?").setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
		}
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			clientManager = new AmazonClientManager(getSharedPreferences(
					"com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
	
			names3gp = new ArrayList<String>();
			setContentView(R.layout.route_list);
			routesReverse = listRoutes.getListRoutes();
			adapter = new RoutesMovAdapter(RoutesListActivity.this, routesReverse);
	   		setListAdapter(adapter); 
	   		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					//view.setAlpha(0);
					positiontodelete = position;
					displayDeleteRoute();
					return true;
			
				}
			});
	   		
	   		//Método comentado de borrar deslizando
	        /*ListView listView = getListView();
	        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                    listView,
                    new SwipeDismissListViewTouchListener.DismissCallbacks() {
                        @Override
                        public boolean canDismiss(int position) {
                            return true;
                        }

                        @Override
                        public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                            for (int position : reverseSortedPositions) {
            					positiontodelete = position;
                            }
    						new DeleteRoute().execute(listRoutes.getListRoutes().get(positiontodelete));
    						listRoutes.getListRoutes().remove(positiontodelete);
    						adapter.notifyDataSetChanged();
                        }
                    });
	        listView.setOnTouchListener(touchListener);*/

		}
		
		@Override
		protected void onResume() {
			if(getListAdapter().getCount() == 0){
				if(routesReverse.isEmpty()){
					routesReverse = listRoutes.getListRoutes();
					adapter = new RoutesMovAdapter(RoutesListActivity.this, routesReverse);
				}
				setListAdapter(adapter); 
			}
			super.onResume();
		}
	
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
		    MenuInflater inflater = getMenuInflater();
		    inflater.inflate(R.menu.action, menu);
			
			MenuItem searchItem = menu.findItem(R.id.buscar);

		    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		    searchView.setMaxWidth(2000);
		    
			SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		    //Cambio el estilo del searchView del actionBar
		    SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
		    searchAutoComplete.setTextColor(Color.WHITE);
		    searchAutoComplete.setHint(R.string.hint_search);
		    
		    ImageView searchIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
		    searchIcon.setImageResource(R.drawable.ic_menu_search);
		    
		    ImageView searchCloseIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
		    searchCloseIcon.setImageResource(R.drawable.discard_button);
		    
			// LISTENER PARA EL EDIT TEXT   
		    searchView.setOnQueryTextListener((OnQueryTextListener) this);
		    // LISTENER PARA LA APERTURA Y CIERRE DEL WIDGET
		    MenuItemCompat.setOnActionExpandListener(searchItem, (OnActionExpandListener) this);
	     
		    return true;
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle item selection
		    switch (item.getItemId()) {
		        case R.id.exit:
					clientManager.clearCredentials();
					displayLogoutSuccess();
		            //showHelp();
		            return true;
		        case R.id.info:
		        	Intent intent = new Intent(Intent.ACTION_VIEW);
		        	intent.setData(Uri.parse("https://s3-us-west-2.amazonaws.com/sightracksteam/help.html"));
		        	startActivity(intent);
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
		
		public boolean onQueryTextChange(String arg0) {
			Vector<Route> listaBusqueda = new Vector<Route>();
			for(int i=0; i<listRoutes.getListRoutes().size();i++){
				String rutaCompara = listRoutes.getListRoutes().get(i).getName().toLowerCase();
				if(rutaCompara.contains(arg0.toLowerCase())){
					listaBusqueda.add(listRoutes.getListRoutes().get(i));
				}
			}
			
			//Cargo las listas que contengan la palabra que busco
			setListAdapter(new RoutesMovAdapter(this,listaBusqueda));
			return false;
		}
		public boolean onQueryTextSubmit(String arg0) {
	//		Toast.makeText(this, "Buscando..."+arg0, Toast.LENGTH_LONG).show();
			Vector<Route> listaBusqueda = new Vector<Route>();
			for(int i=0; i<listRoutes.getListRoutes().size();i++){
				String rutaCompara = listRoutes.getListRoutes().get(i).getName().toLowerCase();
				if(rutaCompara.contains(arg0.toLowerCase())){
					listaBusqueda.add(listRoutes.getListRoutes().get(i));
				}
			}
			
			//Cierro el teclado virtual automaticamente al hacer la b??squeda
			View v = (View) findViewById(R.id.buscar);
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			
			//Cargo las listas que contengan la palabra que busco
			setListAdapter(new RoutesMovAdapter(this,listaBusqueda));
		    return true;
		}
		public boolean onMenuItemActionCollapse(MenuItem arg0) {
		    return true;
		}
		public boolean onMenuItemActionExpand(MenuItem arg0) {
		    return true;
		}

	
		
		protected void displayLogoutSuccess() {
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
						startActivity(new Intent(RoutesListActivity.this, MainActivity.class));
						finish();
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.exit)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
		}
		
		@Override
		protected void onListItemClick(ListView listView, View view, int position, long id){
			super.onListItemClick(listView, view, position, id);
			new S3PutObjectTask().execute(String.valueOf(position));
		}
		
		
		public void startnewRoute(View view){
			/** Comprobar la conexión con GPS **/
			if(gpsenabled()){
				finish();
				Intent intent = new Intent(this, NewRouteMovActivity.class);
				startActivity(intent);
			}
			else{
				displayGPSenabled();
				//Toast.makeText(getApplicationContext(), "Debe habilitar su GPS para realizar el reconocimeinto", Toast.LENGTH_LONG);
			}
		}
		
		private class DeleteRoute extends AsyncTask<Route, Void, Void> {

			protected Void doInBackground(Route... routes) {
				
				RoutesList routeList = new RoutesList();
				routeList.deleteRoute(routes[0]);

				return null;
			}

			protected void onPostExecute(Void result) {
			}
		}
		
		
		private class S3PutObjectTask extends AsyncTask<String, Void, S3TaskResult> {
			ProgressDialog dialog;
			File file;
			Integer posicion;
			
			protected void onPreExecute() {
		        super.onPreExecute();
		        int currentOrientation = getResources().getConfiguration().orientation; 
		        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		        }
		        else {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		        }
		        
		        
		        dialog = new ProgressDialog(RoutesListActivity.this);
				dialog.setMessage(RoutesListActivity.this
						.getString(R.string.loading_));
				dialog.setCancelable(false);
				dialog.show();
			}
			
			protected S3TaskResult doInBackground(String... strings) {
				/** Guarda el archivo GPX (PRUEBAS) **/

				if (strings == null || strings.length != 1) {
					return null;
				}
				posicion = Integer.parseInt(strings[0]);
				
				S3TaskResult result = new S3TaskResult();	
				try{
					/************/
				    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
		            .withBucketName("sightracksteam").withPrefix("files3gp/"+listRoutes.getListRoutes().get(posicion).getId()+"/");	
				   
				    
				    ObjectListing objectListing;

				    do {
				    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
				    	for (S3ObjectSummary objectSummary : 
				    		objectListing.getObjectSummaries()) {
		    		    	InputStream in = clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
		    		    	writefile(in, listRoutes.getListRoutes().get(posicion).getId(), objectSummary.getKey());
		    		    	names3gp.add(objectSummary.getKey());
		    		    	
				    		System.out.println( " - " + objectSummary.getKey() + "  " +
				                    "(size = " + objectSummary.getSize() + 
				    				")");
				    	}
				    	listObjectsRequest.setMarker(objectListing.getNextMarker());
				    } while (objectListing.isTruncated());  					
					/************/
				    
				    
					clientManager.s3().getBucketAcl(PropertyLoader.getInstance().getBucketGPX());
					GetObjectRequest gor = new GetObjectRequest(PropertyLoader.getInstance().getBucketGPX(),listRoutes.getListRoutes().get(posicion).getId()+".gpx");
					S3Object object =clientManager.s3().getObject(gor);
					InputStream reader = new BufferedInputStream(object.getObjectContent());
					
					file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.bikeroute/filesGPX/",listRoutes.getListRoutes().get(posicion).getId());
					OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
					int read = -1;

					while ( ( read = reader.read() ) != -1 ) {
					    writer.write(read);
					}
					writer.flush();
					writer.close();
					reader.close();
				} catch (Exception exception) {
					result.setErrorMessage(exception.getMessage());
				}


				return result;
			}
			
			protected void onPostExecute(S3TaskResult result) {
				dialog.dismiss();
				if (result.getErrorMessage() != null) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
					RoutesListActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),MainActivity.class);
					startActivity(i);
				}
				else {
					RoutesListActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
					i.putExtra("route", listRoutes.getListRoutes().get(posicion));
					i.putExtra("filegpx", file);
					i.putStringArrayListExtra("files3gp", names3gp);
					startActivity(i);
					
				}
			   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			}
		}
		
		private class S3TaskResult {
			String errorMessage = null;

			public String getErrorMessage() {
				return errorMessage;
			}

			public void setErrorMessage(String errorMessage) {
				this.errorMessage = errorMessage;
			}
			
		}

	}
