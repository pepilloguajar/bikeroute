package ian.util;

import java.io.Serializable;

public class IntVector implements Serializable {
   private static final long serialVersionUID = 8232085861855452732L;
   private int m_vec[];
   private int count = 0;
   public IntVector() {
      m_vec = new int[2];
   }
   public IntVector(final int startSize) {
      m_vec = new int[startSize];
   }
   public void add(final int val) {
      if (count >= m_vec.length) {
         final int vec[] = new int[m_vec.length * 2];
         System.arraycopy(m_vec, 0, vec, 0, count);
         m_vec = vec;
      }
      m_vec[count] = val;
      ++count;
   }
   public void add(final int val[], final int offset, final int length) {
      while (count + length >= m_vec.length) {
         final int vec[] = new int[m_vec.length * 2];
         System.arraycopy(m_vec, 0, vec, 0, count);
         m_vec = vec;
      }
      System.arraycopy(val, offset, m_vec, count, length);
      count += length;
   }
   public int get(final int i) {
      return m_vec[i];
   }
   public int[] getArray() {
      final int vec[] = new int[count];
      System.arraycopy(m_vec, 0, vec, 0, count);
      return vec;
   }
   public int[] getArray(final int start) {
      return getArray(start, count - start);
   }
   public int[] getArray(final int start, final int len) {
      final int arr[] = new int[len];
      System.arraycopy(m_vec, start, arr, 0, len);
      return arr;
   }
   public int getLength() {
      return count;
   }
   public void set(final int i, final int val) {
      m_vec[i] = val;
   }
}
