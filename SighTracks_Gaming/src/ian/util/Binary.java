package ian.util;

import java.math.BigInteger;

public class Binary {
   final static char[] HEXTAB = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
         'd', 'e', 'f' };
   public final static int byteArrayToInt(final byte[] buf, final int ofs) {
      return (buf[ofs] << 24) | ((buf[ofs + 1] & 0x0ff) << 16) | ((buf[ofs + 2] & 0x0ff) << 8)
            | (buf[ofs + 3] & 0x0ff);
   }
   public final static long byteArrayToLong(final byte[] buf, final int ofs) {
      return ((long) ((buf[ofs] << 24) | ((buf[ofs + 1] & 0x0ff) << 16)
            | ((buf[ofs + 2] & 0x0ff) << 8) | (buf[ofs + 3] & 0x0ff)) << 32)
            | (((buf[ofs + 4] << 24) | ((buf[ofs + 5] & 0x0ff) << 16)
                  | ((buf[ofs + 6] & 0x0ff) << 8) | (buf[ofs + 7] & 0x0ff)) & 0x0ffffffffL);
   }
   public final static String byteArrayToStr(final byte[] data, int ofs, int len) {
      // we need two bytes for every character
      len &= ~1;
      // enough bytes in the buffer?
      final int availCapacity = data.length - ofs;
      if (availCapacity < len) {
         len = availCapacity;
      }
      final StringBuffer sbuf = new StringBuffer();
      sbuf.setLength(len >> 1);
      int sbufPos = 0;
      while (0 < len) {
         sbuf.setCharAt(sbufPos++, (char) ((data[ofs] << 8) | (data[ofs + 1] & 0x0ff)));
         ofs += 2;
         len -= 2;
      }
      return sbuf.toString();
   }
   public final static String bytesToHexStr(final byte[] data) {
      return bytesToHexStr(data, 0, data.length);
   }
   public final static String bytesToHexStr(final byte[] data, int ofs, final int len) {
      final StringBuffer sbuf = new StringBuffer();
      sbuf.setLength(len << 1);
      int pos = 0;
      final int c = ofs + len;
      while (ofs < c) {
         sbuf.setCharAt(pos++, HEXTAB[(data[ofs] >> 4) & 0x0f]);
         sbuf.setCharAt(pos++, HEXTAB[data[ofs++] & 0x0f]);
      }
      return sbuf.toString();
   }
   public final static long fromLexString(final String str) {
      BigInteger bd = new BigInteger(str, Character.MAX_RADIX);
      bd = bd.shiftRight(64);
      return bd.longValue();
   }
   public final static int hexStrToBytes(final String hex, final byte[] data, int srcofs,
         int dstofs, int len) {
      // check for correct ranges
      final int strlen = hex.length();
      final int availBytes = (strlen - srcofs) >> 1;
      if (availBytes < len) {
         len = availBytes;
      }
      final int outputCapacity = data.length - dstofs;
      if (len > outputCapacity) {
         len = outputCapacity;
      }
      // convert now
      final int dstofsBak = dstofs;
      for (int i = 0; i < len; i++) {
         byte abyte = 0;
         boolean convertOK = true;
         for (int j = 0; j < 2; j++) {
            abyte <<= 4;
            final char cActChar = hex.charAt(srcofs++);
            if ((cActChar >= 'a') && (cActChar <= 'f')) {
               abyte |= (byte) (cActChar - 'a') + 10;
            } else {
               if ((cActChar >= '0') && (cActChar <= '9')) {
                  abyte |= (byte) (cActChar - '0');
               } else {
                  convertOK = false;
               }
            }
         }
         if (convertOK) {
            data[dstofs++] = abyte;
         }
      }
      return (dstofs - dstofsBak);
   }
   public final static byte[] intToByteArray(final int value) {
      return new byte[] { (byte) ((value >> 24) & 0x0ff), (byte) ((value >> 16) & 0x0ff),
            (byte) ((value >> 8) & 0x0ff), (byte) value };
   }
   public final static void intToByteArray(final int value, final byte[] buf, final int ofs) {
      buf[ofs] = (byte) ((value >> 24) & 0x0ff);
      buf[ofs + 1] = (byte) ((value >> 16) & 0x0ff);
      buf[ofs + 2] = (byte) ((value >> 8) & 0x0ff);
      buf[ofs + 3] = (byte) value;
   }
   public final static int longHi32(final long val) {
      return (int) (val >> 32);
   }
   public final static int longLo32(final long val) {
      return (int) val;
   }
   public final static byte[] longToByteArray(final long value) {
      return new byte[] { (byte) ((value >> 56) & 0x0ff), (byte) ((value >> 48) & 0x0ff),
            (byte) ((value >> 40) & 0x0ff), (byte) ((value >> 32) & 0x0ff),
            (byte) ((value >> 24) & 0x0ff), (byte) ((value >> 16) & 0x0ff),
            (byte) ((value >> 8) & 0x0ff), (byte) value };
   }
   public final static void longToByteArray(final long value, final byte[] buf, final int ofs) {
      buf[ofs] = (byte) ((value >> 56) & 0x0ff);
      buf[ofs + 1] = (byte) ((value >> 48) & 0x0ff);
      buf[ofs + 2] = (byte) ((value >> 40) & 0x0ff);
      buf[ofs + 3] = (byte) ((value >> 32) & 0x0ff);
      buf[ofs + 4] = (byte) ((value >> 24) & 0x0ff);
      buf[ofs + 5] = (byte) ((value >> 16) & 0x0ff);
      buf[ofs + 6] = (byte) ((value >> 8) & 0x0ff);
      buf[ofs + 7] = (byte) value;
   }
   public final static void longToIntArray(final long value, final int[] buf, final int ofs) {
      buf[ofs] = (int) (value >> 32);
      buf[ofs + 1] = (int) value;
   }
   public final static long makeLong(final int lo, final int hi) {
      return (((long) hi << 32) | (lo & 0x00000000ffffffffL));
   }
   public final static byte[] toBinary(final long x) {
      final byte arr[] = new byte[64];
      for (int i = 0; i < 64; ++i) {
         arr[i] = (byte) ((x >> i) & 1);
      }
      return arr;
   }
   public final static String toLexString(final long x) {
      BigInteger bd = BigInteger.valueOf(x);
      bd = bd.shiftLeft(64);
      return bd.toString(Character.MAX_RADIX);
   }
   public final static int toUnsigned(final byte b) {
      return b & 0xff;
   }
}
