package ian.util;

import java.io.Serializable;
import java.util.StringTokenizer;

public class StringVector implements Serializable {
   private static final long serialVersionUID = -8075103341689850495L;
   public static StringVector getDelimitedStringVector(final String value,
         final String delimiters) {
      final StringVector vec = new StringVector();
      final StringTokenizer toks = new StringTokenizer(value, delimiters);
      while (toks.hasMoreTokens()) {
         vec.add(toks.nextToken());
      }
      return vec;
   }
   private String m_vec[] = new String[2];
   private int count = 0;
   public void add(final String val) {
      if (count >= m_vec.length) {
         final String vec[] = new String[m_vec.length * 2];
         System.arraycopy(m_vec, 0, vec, 0, count);
         m_vec = vec;
      }
      m_vec[count] = val;
      ++count;
   }
   public void add(final String val[]) {
      add(val, 0, val.length);
   }
   public void add(final String val[], final int offset, final int length) {
      while (count + length >= m_vec.length) {
         final String vec[] = new String[m_vec.length * 2];
         System.arraycopy(m_vec, 0, vec, 0, count);
         m_vec = vec;
      }
      System.arraycopy(val, offset, m_vec, count, length);
      count += length;
   }
   public String get(final int i) {
      return m_vec[i];
   }
   public String[] getArray() {
      final String vec[] = new String[count];
      System.arraycopy(m_vec, 0, vec, 0, count);
      return vec;
   }
   public String[] getArray(final int start) {
      return getArray(start, count - start);
   }
   public String[] getArray(final int start, final int len) {
      final String arr[] = new String[len];
      System.arraycopy(m_vec, start, arr, 0, len);
      return arr;
   }
   public int getLength() {
      return count;
   }
   public void set(final int i, final String str) {
      m_vec[i] = str;
   }
}