/*
 * Author: ian.schumacher Created: Feb 13, 2004
 */
package ian.util;

import java.lang.reflect.Array;

public class Arr {
   public static boolean[] cat(final boolean a[], final boolean b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final boolean r[] = new boolean[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static byte[] cat(final byte a[][]) {
      int length = 0;
      for (int i = 0; i < a.length; ++i) {
         length += a[i].length;
      }
      final byte r[] = new byte[length];
      int pos = 0;
      for (int i = 0; i < a.length; ++i) {
         System.arraycopy(a[i], 0, r, pos, a[i].length);
         pos += a[i].length;
      }
      return r;
   }
   public static byte[] cat(final byte a[], final byte b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final byte r[] = new byte[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   @SuppressWarnings("unchecked")
   public static <T> T[] cat(final Class<T> type, final T a[], final T b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final T r[] = (T[]) Array.newInstance(type, a.length + b.length);
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static double[] cat(final double a[], final double b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final double r[] = new double[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static float[] cat(final float a[], final float b[]) {
      if (a == null) {
         return b;
      }
      if (b == null) {
         return a;
      }
      final float r[] = new float[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static int[] cat(final int a[], final int b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final int r[] = new int[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static long[] cat(final long a[], final long b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final long r[] = new long[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static short[] cat(final short a[], final short b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final short r[] = new short[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static String[] cat(final String a[], final String b[]) {
      if (a == null || a.length == 0) {
         return b;
      }
      if (b == null || b.length == 0) {
         return a;
      }
      final String r[] = new String[a.length + b.length];
      System.arraycopy(a, 0, r, 0, a.length);
      System.arraycopy(b, 0, r, a.length, b.length);
      return r;
   }
   public static boolean[] copy(final boolean a[]) {
      final boolean r[] = new boolean[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static byte[] copy(final byte a[]) {
      final byte r[] = new byte[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static byte[][] copy(final byte a[][]) {
      final byte r[][] = new byte[a.length][];
      for (int i = 0; i < a.length; ++i) {
         r[i] = new byte[a[i].length];
         System.arraycopy(a[i], 0, r[i], 0, a[i].length);
      }
      return r;
   }
   public static double[] copy(final double a[]) {
      final double r[] = new double[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static double[][] copy(final double a[][]) {
      final double r[][] = new double[a.length][];
      for (int i = 0; i < a.length; ++i) {
         r[i] = new double[a[i].length];
         System.arraycopy(a[i], 0, r[i], 0, a[i].length);
      }
      return r;
   }
   public static double[][][] copy(final double a[][][]) {
      final double r[][][] = new double[a.length][][];
      for (int i = 0; i < a.length; ++i) {
         r[i] = new double[a[i].length][];
         for (int j = 0; j < a[0].length; ++j) {
            r[i][j] = new double[a[i][j].length];
            System.arraycopy(a[i][j], 0, r[i][j], 0, a[i][j].length);
         }
      }
      return r;
   }
   public static float[] copy(final float a[]) {
      final float r[] = new float[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static int[][] copy(final int a[][]) {
      final int r[][] = new int[a.length][];
      for (int i = 0; i < a.length; ++i) {
         r[i] = new int[a[i].length];
         System.arraycopy(a[i], 0, r[i], 0, a[i].length);
      }
      return r;
   }
   public static int[] copy(final int a[]) {
      final int r[] = new int[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static long[] copy(final long a[]) {
      final long r[] = new long[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static short[] copy(final short a[]) {
      final short r[] = new short[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static String[] copy(final String a[]) {
      final String r[] = new String[a.length];
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   @SuppressWarnings("unchecked")
   public static <T> T[] copy(final T a[]) {
      final T r[] = (T[]) Array.newInstance(a[0].getClass(), a.length);
      System.arraycopy(a, 0, r, 0, a.length);
      return r;
   }
   public static String toString(final Object arr) {
      final StringBuffer buf = new StringBuffer();
      final int l = Array.getLength(arr);
      buf.append('{');
      int last = 0;
      for (int i = 0; i < l; ++i) {
         Object obj = Array.get(arr, i);
         if (obj.getClass().isArray()) {
            obj = toString(obj);
         }
         buf.append(obj);
         if (i < l - 1) {
            buf.append(',');
         }
         final int length = buf.length();
         if (length - last > 100) {
            buf.append('\n');
            last = length;
         }
      }
      buf.append("}");
      return buf.toString();
   }
}
