package ian.aws.sdb;


import ian.aws.sdb.model.Challenge;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

public class Challenges {
	
	protected String nextToken;
	
	/** Atributos del dominio **/
	private static final String IDROUTE_ATTRIBUTE = "idRoute";
	private static final String IDUSER_ATTRIBUTE = "idUser";
	private static final String PUNCTUATION_ATTRIBUTE = "Punctuation";
	private static final String TIMETODO_ATTRIBUTE = "timeToDo";
	private static final String LASTTIME_ATTRIBUTE = "lastTime";
	
	
	/** Consultas sobre SimpleDB **/
	private static final String CHALLENGE_USER = "Select * from Challenges where idUser = ";
	private static final String ROUTES_DOMAIN = "Challenges";

	
	
	/**
	 * Converts a item list to List of Profiles
	 * @param items
	 * @return
	 */
	protected List<Challenge> convertItemListToActivityList( List<Item> items ) {
		List<Challenge> challenges = new ArrayList<Challenge>( items.size() );
		for ( Item item : items ) {
			challenges.add( this.convertItemToChallenge( item ) );
		}
		return challenges;
	}
	
	/**
	 * Convert Item to Route
	 * @param item
	 * @return
	 */
	protected Challenge convertItemToChallenge( Item item ) {
		return new Challenge( item.getName(), this.getIdUser(item), this.getIdRoute(item), this.gettimeToDo(item), this.getPunctuation(item), this.getlastTime(item) );
	}		

	/************ METHODS TO GET SPECIFIC ATTRIBUTES FOR ITEM ****************/
	protected String getIdUser( Item item ) {
		return this.getStringValueForAttributeFromList( IDUSER_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getIdRoute( Item item ) {
		return this.getStringValueForAttributeFromList( IDROUTE_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String gettimeToDo( Item item ) {
		return this.getStringValueForAttributeFromList( TIMETODO_ATTRIBUTE, item.getAttributes() );
	}
	
	protected int getPunctuation( Item item ) {
		return this.getIntValueForAttributeFromList(PUNCTUATION_ATTRIBUTE, item.getAttributes());
	}
	
	protected String getlastTime( Item item ) {
		return this.getStringValueForAttributeFromList( TIMETODO_ATTRIBUTE, item.getAttributes() );
	}
	
	
	/**
	 * Get String value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected String getStringValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return attribute.getValue();
			}
		}
		
		return "";		
	}
		

	/**
	 * Get int value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected int getIntValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Integer.parseInt( attribute.getValue() );
			}
		}
		
		return 0;		
	}


	/**
	 * Get boolean value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected boolean getBooleanValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				if(Integer.parseInt(attribute.getValue()) == 1)
					return true;
			}
		}
		return false;
	}	
	
	/**
	 * Get all challenge for user
	 * @return
	 */
	public synchronized List<Challenge> getChallenges(String user) {
		String query = CHALLENGE_USER + "'" + user + "'";
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = AmazonClient.sdb.select( selectRequest );
		this.nextToken = response.getNextToken();
		List<Challenge> lchallenges = this.convertItemListToActivityList( response.getItems() );
		return lchallenges;	
	}
	
	public void delChallenge(String id){
		DeleteAttributesRequest par = new DeleteAttributesRequest(ROUTES_DOMAIN, id);		
		try {
			AmazonClient.sdb.deleteAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
}

