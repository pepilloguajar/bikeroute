package ian.aws.sdb;

import ian.aws.sdb.model.Challenge;
import ian.aws.sdb.model.UserAct;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

public class UtilsSDB extends SDB{
	
	/**
	 * Recupera las ultimas actividades del Usuario
	 * @param userid
	 * @param numberOfActivities
	 * @return
	 */
	private ArrayList<UserAct> getLastActivitiesFromUser(String userid){
		//Si el parametro numberOfActivities
		ArrayList<UserAct> activities = new ArrayList<UserAct>();
		String query = "select * from " + ACTIVITIES_DOMAIN + " where Userid = '"+userid+"'";
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		SelectResult response = AmazonClient.sdb.select(selectRequest);

		List<Item> items = response.getItems();
		Set<String> idRoutes = new HashSet<String>();
		for(Item item : items){
			if(idRoutes.add(getRouteidForItem(item))){
					activities.add(convertItemToActivity(item));
			}
		}
//		if(numberOfActivities < activities.size()){
//			activities = (ArrayList<UserAct>) activities.subList(0, numberOfActivities);}

		return activities;
	}	
	
	public UtilsSDB(){
	}
	
	
	/**
	 * Lista de los usuarios (que han rellenado su Perfil)
	 * @return
	 */
	public ArrayList<String> UsersID(){
		ArrayList<String> usersId = new ArrayList<String>();
		String query = QUERY_USERS;
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = AmazonClient.sdb.select(selectRequest);
		this.nextToken = response.getNextToken();

		List<Item> items = response.getItems();
		
		
		for(Item item : items){
			usersId.add(item.getName().toString());
		}		
		return usersId;
	}	
	
	/**
	 * Recupera un reto para un usuario userid de sus max ultimas actividades, 
	 * poniendo un tiempo máximo en la realización de la ruta asignada al reto de percent
	 * @param userid
	 * @param max
	 * @param percent entre 0 y 100
	 * @return
	 */
	public Challenge getChallenge(String userid, int max, float percent){
		// Recupera la lista actividades del usuario
		ArrayList<UserAct> activities = getLastActivitiesFromUser(userid);
		ArrayList<UserAct> lastActivities = new ArrayList<UserAct>();
		int numberOfActivities = activities.size();
		//Recupera una lista con los id de las rutas de los retos lanzados actualmente a un usuario 
		ArrayList<String> currentChallenges = getCurrentChallenges(userid);
		//Iteracion
		int n = 0;
		System.out.println("numberOfActivities: "+numberOfActivities+" currentChallenges: "+currentChallenges.size());
		//Itera mientras el numero de actividades con routeId diferentes sea maypr que el numero actual de retos
		//y no se haya sobrepasado el numero maximo de retos
		while(lastActivities.isEmpty() && (numberOfActivities > currentChallenges.size()) && (max > currentChallenges.size())){
			if(n+max < activities.size()){
				lastActivities.addAll(activities.subList(n, n+max));
				}
			else{
				lastActivities.addAll(activities.subList(n,activities.size()-1));
			}
			//Elimina de la lista de actividades aquellas con id de la ruta que ya se haya lanzado en otro reto, para no lanzarlo de nuevo
			Iterator<UserAct> i = lastActivities.iterator();
			while (i.hasNext()) {
				UserAct s = i.next();
				if(currentChallenges.contains(s.getRouteid())){
					System.out.println("Idruta repetido: "+s.getRouteid());
					i.remove();
				}
			}
			n += max+1;
		}
		
		// Genera un número aleatorio
		
		System.out.println("lastActivities.isEmpty(): "+lastActivities.isEmpty());
		Challenge c = null;
		if(!lastActivities.isEmpty()){
		Random r = new Random();
		int rand = r.nextInt(lastActivities.size());		
		String route = lastActivities.get(rand).getRouteid(); //BORRAR por activities.get(rand).getRoute;
		
		String timetodo = String.valueOf(lastActivities.get(rand).getDuration() * ((100-percent)/100));// Hay que multiplicarlo por 100 - percent
		int punctuation = 5;
		
		Date challenge = new Date();
		String idchallenge = "Challenge" + challenge.getTime();
		
		c = new Challenge(idchallenge, userid, route, timetodo, punctuation, String.valueOf(lastActivities.get(rand).getDuration()));
		}
		return c;
	}
	
	/**
	 * Añade un nuevo reto a la BD
	 * @param challenge
	 */
	public void addNewChallenge( Challenge challenge ) {
		ReplaceableAttribute routeidAttribute = new ReplaceableAttribute( IDROUTE_ATTRIBUTE, challenge.getRouteid(), Boolean.FALSE );
		ReplaceableAttribute useridAttribute = new ReplaceableAttribute( IDUSER_ATTRIBUTE, challenge.getIduser(), Boolean.FALSE );
		ReplaceableAttribute timetodoAttribute = new ReplaceableAttribute( TIMETODO_ATTRIBUTE, challenge.getTimetodo().toString(), Boolean.FALSE );
		ReplaceableAttribute punctuationAttribute = new ReplaceableAttribute( PUNCTUATION_ATTRIBUTE, String.valueOf(challenge.getPunctuation()), Boolean.FALSE );
		ReplaceableAttribute lastTimeAttribute = new ReplaceableAttribute( LASTTIME_ATTRIBUTE, challenge.getLasttime().toString(), Boolean.FALSE );
				
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(5);
		attrs.add(routeidAttribute);
		attrs.add(useridAttribute);
		attrs.add(timetodoAttribute);
		attrs.add( punctuationAttribute );
		attrs.add( lastTimeAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( CHALLENGES_DOMAIN, challenge.getIdchallenge(), attrs);		
		try {
			AmazonClient.sdb.putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
	
	public ArrayList<String> getCurrentChallenges(String userId){
		ArrayList<String> routeIds = new ArrayList<String>();
		String query = QUERY_CHALLENGES+"'"+userId+"'";
//		System.out.println("Query: "+QUERY_CHALLENGES+"'"+userId+"'");
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = AmazonClient.sdb.select(selectRequest);
		this.nextToken = response.getNextToken();

		List<Item> items = response.getItems();
		
		
		for(Item item : items){
			//Coge el attributo de lel item que contiene el id de la ruta asociada al challenge
			routeIds.add(item.getAttributes().get(0).getValue().toString());
		}
		return routeIds;
	}
	
	
}
