package ian.aws.sdb;

import ian.aws.sdb.model.UserAct;

import java.util.List;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;

public class SDB {
	/** Atributos del dominio **/
	protected static final String IDROUTE_ATTRIBUTE = "idRoute";
	protected static final String IDUSER_ATTRIBUTE = "idUser";
	protected static final String TIMETODO_ATTRIBUTE = "timeToDo";
	protected static final String LASTTIME_ATTRIBUTE = "lastTime";
	
	protected static final String QUERY_USERS = "Select itemName() from Profiles";
	protected static final String QUERY_CHALLENGES = "select idRoute from `Challenges` where idUser= ";

	/** Atributos del dominio **/
	protected static final String ROUTEID_ATTRIBUTE = "Routeid";
	protected static final String USERID_ATTRIBUTE = "Userid";
	protected static final String DURATION_ATTRIBUTE = "Duration";
	protected static final String AVGSPEED_ATTRIBUTE = "AVGSpeed";
	protected static final String PUNCTUATION_ATTRIBUTE = "Punctuation";
	protected static final String CREATIONDATE_ATTRIBUTE = "CreationDate";
	
	/** Consultas sobre SimpleDB **/
	protected static final String ACTIVITIESSUSER = "Select * from UsersActivities where Userid =  ";
	protected static final String ACTIVITIES_DOMAIN = "UsersActivities";
	protected static final String PROFILES_DOMAIN = "UsersActivities";

	protected static final String ACTIVITIES = "UsersActivities";
	protected static final String CHALLENGES_DOMAIN = "Challenges";

	protected String nextToken;

	/**
	 * Convert Item to Route
	 * @param item
	 * @return
	 */
	protected UserAct convertItemToActivity( Item item ) {
		return new UserAct( item.getName(), this.getRouteidForItem(item), this.getUseridForItem( item ), this.getDurationForItem(item), this.getAVGSpeedForItem(item), this.getPunctuationForItem(item), this.getCreationDateForItem(item));
	}		

	/************ METHODS TO GET SPECIFIC ATTRIBUTES FOR ITEM ****************/
	protected String getRouteidForItem( Item item ) {
		return this.getStringValueForAttributeFromList( ROUTEID_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getUseridForItem( Item item ) {
		return this.getStringValueForAttributeFromList( USERID_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getDurationForItem( Item item ) {
		return this.getIntValueForAttributeFromList( DURATION_ATTRIBUTE, item.getAttributes() );
	}
		
	protected Double getAVGSpeedForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( AVGSPEED_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getPunctuationForItem( Item item ) {
		return this.getIntValueForAttributeFromList( PUNCTUATION_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getCreationDateForItem( Item item ) {
		return this.getStringValueForAttributeFromList( CREATIONDATE_ATTRIBUTE, item.getAttributes() );
	}
	
	/**
	 * Get String value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected String getStringValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return attribute.getValue();
			}
		}
		
		return "";		
	}
	
	/**
	 * Get int value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected int getIntValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Integer.parseInt( attribute.getValue() );
			}
		}
		
		return 0;		
	}
	
	/**
	 * Get double value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected double getDoubleValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Double.parseDouble( attribute.getValue() );
			}
		}
		
		return 0;		
	}	
}
