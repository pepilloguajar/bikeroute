package ian.xml;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface XMap {
   Class<?> custom() default Object.class;
   String defaultValue() default "-1";
   String path() default "";
   String rePath() default "";
}
