package ian.xml;

import ian.util.IntVector;
import ian.util.StringVector;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.regex.Pattern;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLVector {
   private static class XMLHandler extends DefaultHandler {
      protected StringVector m_path = new StringVector();
      protected StringVector m_value = new StringVector();
      private final String m_ipath[] = new String[1024];
      private int m_level = 0;
      private StringBuffer m_text = null;
      private String getPath() {
         final StringBuffer buf = new StringBuffer();
         buf.append('/');
         for (int i = 0; i < m_level; ++i) {
            buf.append(m_ipath[i]).append('/');
         }
         return buf.toString();
      }
      public void characters(final char[] ch, final int start, final int length)
            throws SAXException {
         if (m_text == null) {
            m_text = new StringBuffer();
         }
         m_text.append(new String(ch, start, length));
      }
      public void endElement(final String namespaceURI, final String localName, final String qName)
            throws SAXException {
         if (m_text != null) {
            final String text = m_text.toString().trim();
            if (text.length() > 0) {
               m_path.add(getPath() + "#text");
               m_value.add(text);
            }
            m_text = null;
         }
         m_path.add(getPath());
         m_value.add(END);
         --m_level;
      }
      public String[] getPathArr() {
         return m_path.getArray();
      }
      public String[] getValueArr() {
         return m_value.getArray();
      }
      public void startElement(final String namespaceURI, final String localName,
            final String qName, final Attributes atts) throws SAXException {
         m_ipath[m_level] = qName;
         ++m_level;
         final int length = atts.getLength();
         m_path.add(getPath());
         m_value.add(START);
         for (int i = 0; i < length; ++i) {
            m_path.add(getPath() + "@" + atts.getQName(i));
            m_value.add(atts.getValue(i));
         }
      }
   }
   public static final String DEFAULT_CHARSET = "UTF-8";
   public static final String START = "#START#";
   public static final String END = "#END#";
   public XMLHandler m_handler = new XMLHandler();
   public XMLVector(final File f) throws Exception {
      this(f, DEFAULT_CHARSET);
   }
   public XMLVector(final File f, final String charset) throws Exception {
      final Reader is = new BufferedReader(new InputStreamReader(new FileInputStream(f), charset));
      try {
         populate(is);
      } catch (final Exception e) {
         throw e;
      } finally {
         is.close();
      }
   }
   public XMLVector(final InputStream is) throws Exception {
      this(is, DEFAULT_CHARSET);
   }
   public XMLVector(final InputStream is, final String charset) throws Exception {
      final Reader reader = new BufferedReader(new InputStreamReader(is, charset));
      try {
         populate(reader);
      } catch (final Exception e) {
         throw e;
      } finally {
         reader.close();
      }
   }
   public XMLVector(final Reader r) throws Exception {
      populate(r);
   }
   public XMLVector(final String xml) throws Exception {
      if ((xml != null) && (xml.length() > 0)) {
         populate(xml);
      }
   }
   public XMLVector(final String paths[], final String values[]) {
      for (int i = 0; i < paths.length - 1; ++i) {
         m_handler.m_path.add(paths[i]);
         m_handler.m_value.add(values[i]);
      }
   }
   public XMLVector(final URL url) throws Exception {
      this(url, DEFAULT_CHARSET);
   }
   public XMLVector(final URL url, final String charset) throws Exception {
      final Reader is = new BufferedReader(new InputStreamReader(url.openStream(), charset));
      try {
         populate(is);
      } catch (final Exception e) {
         throw e;
      } finally {
         is.close();
      }
   }
   private void populate(final Reader reader) throws Exception {
      final SAXParser p = SAXParserFactory.newInstance().newSAXParser();
      try {
         p.parse(new InputSource(reader), m_handler);
      } finally {
         reader.close();
      }
   }
   private void populate(final String str) throws Exception {
      final SAXParser p = SAXParserFactory.newInstance().newSAXParser();
      p.parse(new InputSource(new StringReader(str)), m_handler);
   }
   public String[] getPath() {
      return m_handler.getPathArr();
   }
   public String getPath(final int i) {
      return m_handler.getPathArr()[i];
   }
   public int[] getPosition(final String key, final String value, final int start, final int end) {
      final String path[] = getPath();
      final String v[] = getValue();
      final IntVector ivec = new IntVector();
      for (int i = start; i < end; ++i) {
         if (path[i].equals(key) && v[i].equals(value)) {
            ivec.add(i);
         }
      }
      return ivec.getArray();
   }
   public int[] getPosition(final Pattern keyRe, final Pattern valueRe, final int start,
         final int end) {
      final String path[] = getPath();
      final String value[] = getValue();
      final IntVector ivec = new IntVector();
      for (int i = start; i < end; ++i) {
         if (keyRe.matcher(path[i]).matches() && valueRe.matcher(value[i]).matches()) {
            ivec.add(i);
         }
      }
      return ivec.getArray();
   }
   public int[] getPosition(final Pattern keyRe, final int start, final int end) {
      final String path[] = getPath();
      final IntVector ivec = new IntVector();
      for (int i = start; i < end; ++i) {
         if (keyRe.matcher(path[i]).matches()) {
            ivec.add(i);
         }
      }
      return ivec.getArray();
   }
   public int[] getPosition(final String key, final int start, final int end) {
      final String path[] = getPath();
      final String value[] = getValue();
      final IntVector ivec = new IntVector();
      for (int i = start; i < end; ++i) {
         if (path[i].equals(key) && !END.equals(value[i])) {
            ivec.add(i);
         }
      }
      return ivec.getArray();
   }
   public String[] getValue() {
      return m_handler.getValueArr();
   }
   public String getValue(final int i) {
      return m_handler.getValueArr()[i];
   }
   public String getValue(final Pattern keyRe) {
      return getValue(keyRe, 0, length());
   }
   public String getValue(final Pattern keyRe, final int start, final int end) {
      final String path[] = getPath();
      final String value[] = getValue();
      for (int i = start; i < end; ++i) {
         if (keyRe.matcher(path[i]).matches()) {
            return value[i];
         }
      }
      return "";
   }
   public String getValue(final String key) {
      return getValue(key, 0, length());
   }
   public String getValue(final String key, final int start, final int end) {
      final String path[] = getPath();
      final String value[] = getValue();
      for (int i = start; i < end; ++i) {
         if (path[i].equals(key)) {
            return value[i];
         }
      }
      return "";
   }
   public int length() {
      return m_handler.getPathArr().length;
   }
   public String toString() {
      final StringBuffer buf = new StringBuffer();
      final String pathArr[] = m_handler.getPathArr();
      final String valueArr[] = m_handler.getValueArr();
      for (int i = 0; i < pathArr.length; ++i) {
         buf.append(pathArr[i]).append(": ").append(valueArr[i]).append('\n');
      }
      return buf.toString();
   }
}