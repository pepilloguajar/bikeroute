package talentum.gaming;

import static java.util.concurrent.TimeUnit.*;
import ian.aws.sdb.UtilsSDB;
import ian.aws.sdb.model.Challenge;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
public class ChallengeControl {
	private UtilsSDB sdb;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public ChallengeControl(UtilsSDB sdb){
		this.sdb = sdb;
	}
	
	public void challengesForAWeek() {
		final Runnable beeper = new Runnable() {
			public void run() { 
				
				//A cada usuario se le lanza un reto
				ArrayList<String> users = sdb.UsersID();
				System.out.println("Users size: "+users.size()+" Users: "+users);
				for(int i=0; i<users.size(); i++){
					Challenge challenge = sdb.getChallenge(users.get(i), 100, 5);
					//Si no se puede lanzar ningun nuevo reto (porque el usuario no tiene mas actividades) se devuelve null y
					//no se anade a la lista de retos
					if(challenge != null){
						sdb.addNewChallenge(challenge);
					//System.out.println("yeee");
					System.out.println("Reto: "+challenge.getIdchallenge()+" Usuario: "+challenge.getIduser());
					}
				}
			}
		};
    
		scheduler.scheduleAtFixedRate(beeper, 0, 20, SECONDS);
		
			/*scheduler.schedule(new Runnable() {
				public void run() { beeperHandle.cancel(true); }
			}, 60 * 60, SECONDS);*/
	}
}
