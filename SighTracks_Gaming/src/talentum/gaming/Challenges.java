package talentum.gaming;

import ian.aws.sdb.AmazonClient;
import ian.aws.sdb.UtilsSDB;

public class Challenges {

	public static void main(String[] args) {
		//Se inicializa el Cliente de Amazon
		AmazonClient amazonclient = new AmazonClient();
		
		// Se inicializa el control de los Retos
		ChallengeControl bp = new ChallengeControl(new UtilsSDB());
		bp.challengesForAWeek(); 	 //Se disparan los retos semanalmente

	}
}
