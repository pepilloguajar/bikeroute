package talentum.gaming;

import ian.aws.sdb.AmazonClient;
import ian.aws.sdb.UtilsSDB;

public class Gaming {

	public static void main(String[] args) {
		//Se inicializa el Cliente de Amazon
		@SuppressWarnings("unused")
		AmazonClient amazonclient = new AmazonClient();
		
		// Se inicializa el control de los Retos
		GamingControl bp = new GamingControl(new UtilsSDB());
		bp.ChallengesForAnWeek(); 	 //Se disparan los retos semanalmente

	}
}
