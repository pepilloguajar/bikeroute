package talentum.gaming;

import static java.util.concurrent.TimeUnit.*;
import ian.aws.sdb.Challenges;
import ian.aws.sdb.UtilsSDB;
import ian.aws.sdb.model.Challenge;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class GamingControl {
	private UtilsSDB sdb;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public GamingControl(UtilsSDB sdb){
		this.sdb = sdb;
	}
	
	public void ChallengesForAnWeek() {
		final Runnable beeper = new Runnable() {
			public void run() { 
				
				//A cada usuario se le lanza un reto
				ArrayList<String> users = sdb.UsersID();

				Challenges challenges = new Challenges();
				
				for(int i=0; i<users.size(); i++){
					List<Challenge> challengesusers = challenges.getChallenges(users.get(i));
					if(challengesusers.size() == 10){
						challenges.delChallenge(challengesusers.get(challengesusers.size() - 1).getIdchallenge());
					}

					Challenge challenge = sdb.getChallenge(users.get(i), 100, 5);
					sdb.addNewChallenge(challenge);
					System.out.println("Reto: "+challenge.getIdchallenge()+" Usuario: "+challenge.getIduser());
				}
			}
		};
    
		scheduler.scheduleAtFixedRate(beeper, 0, 20, SECONDS);
		
			/*scheduler.schedule(new Runnable() {
				public void run() { beeperHandle.cancel(true); }
			}, 60 * 60, SECONDS);*/
	}
}
