package com.talentum.cyclistsightracks.utils;

public class ExploreSingleton {
	private static ExploreSingleton INSTANCE = null;
	
	private double min_distance;
	private double max_distance;
	private double min_distance_rel;
	private double max_distance_rel;
	private double distance;
	private int min_popularity;
	private int max_popularity;
	private int popularity;
	private boolean activate_hc;
	private boolean activate_mc;
	private boolean activate_ll;

	private ExploreSingleton(){
		this.min_distance = Double.MAX_VALUE;
		this.max_distance = Double.MIN_VALUE;
		this.min_popularity = Integer.MAX_VALUE;
		this.max_popularity = Integer.MIN_VALUE;
		this.popularity = 0;
		this.activate_hc = true;
		this.activate_mc = true;
		this.activate_ll = true;
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new ExploreSingleton();
        }
    }

    public static ExploreSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }

	public double getMin_distance() {
		return min_distance;
	}

	public double getMax_distance() {
		return max_distance;
	}

	public int getMin_popularity() {
		return min_popularity;
	}

	public int getMax_popularity() {
		return max_popularity;
	}

	public void setMin_distance(double min_distance) {
		this.min_distance = min_distance;
	}

	public void setMax_distance(double max_distance) {
		this.max_distance = max_distance;
	}

	public void setMin_popularity(int min_popularity) {
		this.min_popularity = min_popularity;
	}

	public void setMax_popularity(int max_popularity) {
		this.max_popularity = max_popularity;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public boolean isActivate_hc() {
		return activate_hc;
	}

	public boolean isActivate_mc() {
		return activate_mc;
	}

	public boolean isActivate_ll() {
		return activate_ll;
	}

	public void setActivate_hc(boolean activate_hc) {
		this.activate_hc = activate_hc;
	}

	public void setActivate_mc(boolean activate_mc) {
		this.activate_mc = activate_mc;
	}

	public void setActivate_ll(boolean activate_ll) {
		this.activate_ll = activate_ll;
	}

	public double getMin_distance_rel() {
		return min_distance_rel;
	}

	public double getMax_distance_rel() {
		return max_distance_rel;
	}

	public void setMin_distance_rel(double min_distance_rel) {
		this.min_distance_rel = min_distance_rel;
	}

	public void setMax_distance_rel(double max_distance_rel) {
		this.max_distance_rel = max_distance_rel;
	}
}

