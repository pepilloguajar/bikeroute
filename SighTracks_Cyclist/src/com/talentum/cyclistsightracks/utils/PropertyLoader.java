/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.cyclistsightracks.utils;

import java.util.Properties;

import android.util.Log;

public class PropertyLoader {
	   
    private boolean hasCredentials = false;       
    private String tokenVendingMachineURL = null;  
    private String appName = null;  
    private boolean useSSL = false;  
    private String bucketGPXteam = null;
    private String bucket3GP = null;
    
    private String bucketGPXusers = null;
//    private String bucket3GPusers = null;
    
    private String llanaTop = null;
    private String medMonTop = null;
    private String distanceMin = null;
    
    private String intervalGPS = null;
    private String intervalExplore = null;
    
    private String lineRoute = null;
    private String lineRouteNarrow = null;
    private String colorOldTrack = null;
    private String colorNewTrack = null;
    
    private String routesmovdomain;
    private String defaultPlayDistance;
    
    //Gaming
    private String points_level_begin_low;
    private String points_level_begin_top;
    private String points_level_fan_low;
    private String points_level_fan_top;
    private String points_level_cyclist_low;
    private String points_level_cyclist_top;
    private String points_level_elite_low;
    private String points_level_elite_top;
    private String points_level_elitepro_low;
    private String plain;
    private String middle_mountain;
    private String high_mountain;
    private String new_route;
    private String activity;
    private String challenge;
    
    
    
    
    private static PropertyLoader instance = null;
    
    
    
    public static PropertyLoader getInstance() {
        if ( instance == null ) {
            instance = new PropertyLoader();
        }
        
        return instance;
    }       
              
    public PropertyLoader() {
        try {
    	    Properties properties = new Properties();
    	    properties.load( this.getClass().getResourceAsStream( "AwsCredentials.properties" ) );
    	    properties.load(this.getClass().getResourceAsStream("cyclingVar.properties"));
    	    properties.load(this.getClass().getResourceAsStream("gaming.properties"));
    	    
    	    
    	    this.tokenVendingMachineURL = properties.getProperty( "tokenVendingMachineURL" );
    	    this.appName = properties.getProperty( "appName" );
    	    this.useSSL = Boolean.parseBoolean( properties.getProperty( "useSSL" ) );
    	    this.routesmovdomain = properties.getProperty("routesmovDomain");
    	    this.bucketGPXteam = properties.getProperty("bucketGPXteam");
    	    this.bucket3GP = properties.getProperty("bucket3GP");
    	    this.bucketGPXusers = properties.getProperty("bucketGPXusers");
    	    //this.bucket3GPusers = properties.getProperty("bucket3GPusers");
    	    this.llanaTop = properties.getProperty("llanaTop");
    	    this.medMonTop = properties.getProperty("medMonTop");
    	    this.distanceMin = properties.getProperty("distanceMin");
    	    this.intervalGPS = properties.getProperty("intervalGPS");
    	    this.intervalExplore = properties.getProperty("intervalExplore");
    	    this.lineRoute = properties.getProperty("lineRoute");
    	    this.lineRouteNarrow = properties.getProperty("lineRouteNarrow");
    	    this.colorOldTrack = properties.getProperty("colorOldTrack");
    	    this.colorNewTrack = properties.getProperty("colorNewTrack");
    	    this.defaultPlayDistance = properties.getProperty("defaultPlayDistance");
    	    
    	    this.points_level_begin_low = properties.getProperty("points_level_begin_low");
    	    this.points_level_begin_top = properties.getProperty("points_level_begin_top");
    	    this.points_level_fan_low = properties.getProperty("points_level_fan_low");
    	    this.points_level_fan_top = properties.getProperty("points_level_fan_top");
    	    this.points_level_cyclist_low = properties.getProperty("points_level_cyclist_low");
    	    this.points_level_cyclist_top = properties.getProperty("points_level_cyclist_top");
    	    this.points_level_elite_low = properties.getProperty("points_level_elite_low");
    	    this.points_level_elite_top = properties.getProperty("points_level_elite_top");
    	    this.points_level_elitepro_low = properties.getProperty("points_level_elitepro_low");
    	    	    	    			
    	    this.plain = properties.getProperty("plain");
    	    this.middle_mountain = properties.getProperty("middle_mountain");
    	    this.high_mountain = properties.getProperty("high_mountain");
    	    this.new_route = properties.getProperty("new_route");
    	    this.activity = properties.getProperty("activity");
    	    this.challenge = properties.getProperty("challenge");
    	    
    	    		
            if ( this.tokenVendingMachineURL == null || this.tokenVendingMachineURL.equals( "" ) || this.tokenVendingMachineURL.equals( "CHANGE ME" ) ) {
                this.tokenVendingMachineURL = null;
                this.appName = null;
                this.useSSL = false;
                this.bucketGPXteam = null;
                this.bucket3GP = null;
                this.bucketGPXusers = null;
                //this.bucket3GPusers = null;
                this.hasCredentials = false;
                this.llanaTop = null;
                this.medMonTop = null;
                this.distanceMin = null;
                this.lineRoute = null;
                this.lineRouteNarrow = null;
                this.colorOldTrack = null;
                this.colorNewTrack = null;
                this.defaultPlayDistance = null;
        	    this.points_level_begin_low = null;
        	    this.points_level_begin_top = null;
        	    this.points_level_fan_low = null;
        	    this.points_level_fan_top = null;
        	    this.points_level_cyclist_low = null;
        	    this.points_level_cyclist_top = null;
        	    this.points_level_elite_low = null;
        	    this.points_level_elite_top = null;
        	    this.points_level_elitepro_low = null;                
            }
            else {
                this.hasCredentials = true;            
            }
        }
        catch ( Exception exception ) {
            Log.e( "PropertyLoader", "Unable to read property file." );
        }
    }
    
    public boolean hasCredentials() {
        return this.hasCredentials;
    }
    
    public String getTokenVendingMachineURL() {
        return this.tokenVendingMachineURL;
    }
        
    public String getAppName() {
        return this.appName;
    }
        
    public boolean useSSL() {
        return this.useSSL;
    }

	public String getRoutesmovdomain() {
		return routesmovdomain;
	}

	public String getBucketGPXteam() {
		return bucketGPXteam;
	}

	public void setBucketGPXteam(String bucketGPXteam) {
		this.bucketGPXteam = bucketGPXteam;
	}   
	
	public String getBucket3GP() {
		return bucket3GP;
	}

	public void setBucket3GP(String bucket3gp) {
		bucket3GP = bucket3gp;
	}


	public String getBucketGPXusers() {
		return bucketGPXusers;
	}

//	public String getBucket3GPusers() {
//		return bucket3GPusers;
//	}

	public void setBucketGPXusers(String bucketGPXusers) {
		this.bucketGPXusers = bucketGPXusers;
	}

//	public void setBucket3GPusers(String bucket3gPusers) {
//		bucket3GPusers = bucket3gPusers;
//	}

	public String getLlanaTop() {
		return llanaTop;
	}
	
	public String getMedMonTop() {
		return medMonTop;
	}
	
	public String getDistanceMin() {
		return distanceMin;
	}
	
	public String getIntervalGPS(){
		return intervalGPS;
	}
	
	public void setIntervalGPS(String intervalGPS){
		this.intervalGPS = intervalGPS;
	}

	
	public String getIntervalExplore() {
		return intervalExplore;
	}

	public void setIntervalExplore(String intervalExplore) {
		this.intervalExplore = intervalExplore;
	}

	public String getLineRoute(){
		return lineRoute;
	}
	
	public String getLineRouteNarrow(){
		return lineRouteNarrow;
	}
	
	public String getColorOldTrack(){
		return colorOldTrack;
	}
	
	public String getColorNewTrack(){
		return colorNewTrack;
	}
	
	public String getDefaultPlayDistance(){
		return defaultPlayDistance;
	}

	public String getPoints_level_begin_low() {
		return points_level_begin_low;
	}

	public String getPoints_level_begin_top() {
		return points_level_begin_top;
	}

	public String getPoints_level_fan_low() {
		return points_level_fan_low;
	}

	public String getPoints_level_fan_top() {
		return points_level_fan_top;
	}

	public String getPoints_level_cyclist_low() {
		return points_level_cyclist_low;
	}

	public String getPoints_level_cyclist_top() {
		return points_level_cyclist_top;
	}

	public String getPoints_level_elite_low() {
		return points_level_elite_low;
	}

	public String getPoints_level_elite_top() {
		return points_level_elite_top;
	}

	public String getPoints_level_elitepro_low() {
		return points_level_elitepro_low;
	}

	public String getPlain() {
		return plain;
	}

	public String getMiddle_mountain() {
		return middle_mountain;
	}

	public String getHigh_mountain() {
		return high_mountain;
	}

	public String getNew_route() {
		return new_route;
	}

	public String getActivity() {
		return activity;
	}

	public String getChallenge() {
		return challenge;
	}
}
