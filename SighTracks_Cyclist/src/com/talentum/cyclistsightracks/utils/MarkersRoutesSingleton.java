package com.talentum.cyclistsightracks.utils;

import java.util.ArrayList;
import java.util.List;

import com.talentum.cyclistsightracks.model.MarkerRoute;
import com.talentum.cyclistsightracks.model.Route;


/** 
 * M??todo para disponer de las Rutas en una ??nica instancia
 * @author escabia
 *
 */
public class MarkersRoutesSingleton {
	private static MarkersRoutesSingleton INSTANCE = null;
	
	
	private List<MarkerRoute> listMarkers;
	
	private MarkersRoutesSingleton(){
		listMarkers = new ArrayList<MarkerRoute>();
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new MarkersRoutesSingleton();
        }
    }

    public static MarkersRoutesSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }

	public List<MarkerRoute> getListMarkers() {
		return listMarkers;
	}

	public void setListMarkers(List<MarkerRoute> listMarkers) {
		this.listMarkers = listMarkers;
	} 
}
