package com.talentum.cyclistsightracks.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import android.annotation.SuppressLint;
import android.util.Log;

import com.talentum.cyclistsightracks.model.Challenge;
import com.talentum.cyclistsightracks.model.Profile;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.model.UserAct;
import com.talentum.cyclistsightracks.model.UserRoute;
import com.talentum.cyclistsightracks.model.UserWarning;

/** 
 * @author escabia
 *
 */
public class Singleton {
	private static Singleton INSTANCE = null;
	
	private List<Route> listMovRoutes;		//Rutas del equipo Movistar
	private List<UserRoute> listUsersRoutes; 	//Rutas de los usuarios
	
	private List<RouteAct> myRoutes; 			//Rutas del propio usuario
	private List<RouteAct> lastRoutes;
	
	private List<UserAct> listUserActivities;
	private List<UserAct> alllistUserActivities;

	private List<Challenge> challenges;
	
	private List<UserWarning> warnings;
	
	private Profile pUser;
	
	private int totalroutes;
	private int totalpunctuation;
	private int totalmonthpunctuation;
	private Double totaldistance;
	
	private RouteAct r;
	
	private boolean isMusicServiceActive;
	
	private void getRoudebyId(String idroute, String datecreation){
		int find = -1;
		for(int i=0; i<listMovRoutes.size() && find == -1; i++)
			if(listMovRoutes.get(i).getId().equals(idroute))
				find = i;
		
		if(find == -1){
			for(int i=0; i<listUsersRoutes.size() && find == -1; i++)
				if(listUsersRoutes.get(i).getId().equals(idroute))
					find = i;
			
			if(find == -1)
				r = new RouteAct();
				//return new Route();
			else{
				r = new RouteAct(listUsersRoutes.get(find), datecreation, listUsersRoutes.get(find).getDeleteroute(),false);
			}
		}
		else{
			r = new RouteAct(listMovRoutes.get(find), datecreation, false, true);
		}
	}
	
	private double getDistanceRoude(String idroute){
		double distance = -1;
		for(int i=0; i<listMovRoutes.size() && distance == -1; i++)
			if(listMovRoutes.get(i).getId().equals(idroute))
				distance = listMovRoutes.get(i).getDistance();
		
		if(distance == -1){
			for(int i=0; i<listUsersRoutes.size() && distance == -1; i++)
				if(listUsersRoutes.get(i).getId().equals(idroute))
					distance = listUsersRoutes.get(i).getDistance();
		}
		
		return distance;
	}	
	
	public Singleton(){	
		pUser = new Profile();
		
		totalroutes = 0;
		totaldistance = 0.0;
		totalpunctuation = 0;
		totalmonthpunctuation = 0;
		
		listMovRoutes = new ArrayList<Route>();
		listUsersRoutes = new ArrayList<UserRoute>();
		listUserActivities = new ArrayList<UserAct>();
		
		myRoutes = new ArrayList<RouteAct>();
		lastRoutes = new ArrayList<RouteAct>();
		
		warnings = new ArrayList<UserWarning>();
		
		isMusicServiceActive = false;
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new Singleton();
        }
    }

    public static Singleton getInstance() {
        createInstance();
        return INSTANCE;
    }

	public List<Route> getListMovRoutes() {
		return listMovRoutes;
	}

	public List<UserRoute> getListUsersRoutes() {
		return listUsersRoutes;
	}

	public void setListMovRoutes(List<Route> listMovRoutes) {
		this.listMovRoutes = listMovRoutes;
	}

	public void setListUsersRoutes(List<UserRoute> listUsersRoutes) {
		this.listUsersRoutes = listUsersRoutes;
	}
	
	public List<UserAct> getListUserActivities() {
		return listUserActivities;
	}	

	public List<RouteAct> getMyRoutes() {
		return myRoutes;
	}
	
	public List<UserAct> getAlllistUserActivities() {
		return alllistUserActivities;
	}

	
	
	@SuppressLint("UseValueOf")
	@SuppressWarnings("deprecation")
	public void setAlllistUserActivities(List<UserAct> alllistUserActivities) {
		this.alllistUserActivities = alllistUserActivities;
		Set<String> idroutes = new HashSet<String>();
		Date today = new Date();
		for(int i=this.alllistUserActivities.size() -1; i>=0; i--){
			Date dateactivity = new Date(alllistUserActivities.get(i).getCreation());
			Long onemonth = new Long("2678400000");//1 mes en milisegundos
			
			
			if((today.getTime() - dateactivity.getTime()) < onemonth){
				totalmonthpunctuation += alllistUserActivities.get(i).getPunctuation();
			}
			totaldistance += getDistanceRoude(alllistUserActivities.get(i).getRouteid());
			totalpunctuation += alllistUserActivities.get(i).getPunctuation();
			totalroutes += 1;
			//totaldistance += r.getDistance();			
			
			if(idroutes.add(alllistUserActivities.get(i).getRouteid()))
				listUserActivities.add( alllistUserActivities.get(i) );
		}		
	}

	public void setMyRoutes(List<RouteAct> myRoutes) {
		this.myRoutes = myRoutes;
	}

	/**
	 * Carga las rutas en MyRoutes (Rutas del propio usuario) y lastRoutes (Ultimas Rutas)
	 * @param user
	 */
	public void loadRoutes(String user){
		myRoutes = new ArrayList<RouteAct>();
		lastRoutes = new ArrayList<RouteAct>();
		for(int i=0; i<listUserActivities.size(); i++){
			getRoudebyId(listUserActivities.get(i).getRouteid(), listUserActivities.get(i).getCreation());
			
			if(r.getUserid() != null && r.getUserid().equals(user))
				myRoutes.add(r);
			if(r.getUserid() != null){
				lastRoutes.add(r);
			}
		}		
		Log.i("MSGPepisLoadRoute",Integer.toString(Singleton.getInstance().getLastRoutes().size()));
	}
	
	private RouteAct findinLastRoute(String routeid){
		RouteAct r = new RouteAct();
		for(int i=0; i<lastRoutes.size(); i++){
			if(lastRoutes.get(i).getId().equals(routeid))
				return lastRoutes.get(i);
		}
		
		return r;
	}
	
	public void loadRoutesChallenges(){
		
		for(int i=0; i<this.getChallenges().size(); i++){
			this.getChallenges().get(i).setRouteact(findinLastRoute(getChallenges().get(i).getRouteid()));
		}
	}
	
	public void setNameandPublic(RouteAct route){		
		for(int i=0; i<this.myRoutes.size(); i++){
			if(this.myRoutes.get(i).getId().equals(route.getId())){
				this.myRoutes.get(i).setName(route.getName());
				this.myRoutes.get(i).setPublicroute(route.isPublicroute());
			}
		}
	}
	
	public List<RouteAct> getLastRoutes() {
		return lastRoutes;
	}

	public void setLastRoutes(List<RouteAct> lastRoutes) {
		this.lastRoutes = lastRoutes;
	}

	public Profile getpUser() {
		return pUser;
	}

	public void setListUserActivities(List<UserAct> listUserActivities) {
		this.listUserActivities = listUserActivities;
	}

	public void setpUser(Profile pUser) {
		this.pUser = pUser;
	}

	public void logout(){
		clean();
		pUser = null;
    }
	
	public void clean(){
		totalroutes = 0;
		totalpunctuation = 0;
		totaldistance = 0.0;
		totalmonthpunctuation = 0;

		listUserActivities = new ArrayList<UserAct>();
		myRoutes = new ArrayList<RouteAct>();
		listMovRoutes = new ArrayList<Route>();
		listUsersRoutes = new ArrayList<UserRoute>();
		warnings = new ArrayList<UserWarning>();
    }
	
	public void delChallenge(String challenge){
		int index = -1;
		for(int i=0; i<challenges.size() && index == -1; i++){
			if(challenges.get(i).getIdchallenge().equals(challenge))
				index = i;
		
		challenges.remove(index);
		}
	}
	
	public void cleanRoute(){
		warnings = new ArrayList<UserWarning>();
    }
	
	public int getTotalpunctuation() {
		return totalpunctuation;
	}

	public Double getTotaldistance() {
		return totaldistance;
	}
	
	public int getTotalroutes(){
		return totalroutes;
	}

	public List<Challenge> getChallenges() {
		return challenges;
	}

	public void setChallenges(List<Challenge> challenges) {
		this.challenges = challenges;
	}
	
	public boolean getMusicActive(){
    	return isMusicServiceActive;
    }
    
    public void setMusicActive(boolean serviceActive){
    	this.isMusicServiceActive = serviceActive;
    }
    
    public List<UserWarning> getWarnings() {
		return warnings;
	}
    
    public void addWarning(UserWarning warning) {
		if(warnings != null){
			warnings.add(warning);
		}
	}
    
    public void setWarnings(List<UserWarning> warnings){
		this.warnings=warnings;
	}

	public int getTotalmonthpunctuation() {
		return totalmonthpunctuation;
	}

	public void setTotalmonthpunctuation(int totalmonthpunctuation) {
		this.totalmonthpunctuation = totalmonthpunctuation;
	}
}
