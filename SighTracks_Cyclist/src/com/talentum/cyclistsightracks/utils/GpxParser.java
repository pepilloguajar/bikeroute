package com.talentum.cyclistsightracks.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.talentum.cyclistsightracks.R;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.Segment;
import com.talentum.cyclistsightracks.model.UserWarning;
import com.talentum.cyclistsightracks.utils.SegmentsSingleton;
import com.talentum.cyclistsightracks.utils.Singleton;

public class GpxParser {
	
	private ArrayList<String> names3gp;
	private File file;
	private Route route;
	private Segment currentSegment;
	private SegmentsSingleton segmentsSingleton;
	private Singleton singleton;
	private List<UserWarning> warnings;
	
	public GpxParser(File file, Route route, ArrayList<String> names3gp){
		if(file != null && names3gp != null){
			this.file = file;
			this.route = route;
			this.names3gp = names3gp;
			segmentsSingleton = SegmentsSingleton.getInstance();
			currentSegment = new Segment();
			singleton = Singleton.getInstance();
			warnings = new ArrayList<UserWarning>();
		}
	}

	
	//Metodo para parsear rutas formato GPX
		public Vector<LatLng> parseGPX() throws ParserConfigurationException, SAXException{
			Vector<Vector<LatLng>> pathFragment = null;
			Vector<LatLng> temp = new Vector<LatLng>();

			try {

				InputStream inputStream = new FileInputStream(file);
				DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = docBuilder.parse(inputStream);
				if (document == null) {
					return null;
				}
				//Primero parsea los trkpt para pintar la ruta
				NodeList listTrckptTag = document.getElementsByTagName("trkpt");
				for (int i = 0; i < listTrckptTag.getLength(); i++) {
					String latText = ((Element) listTrckptTag.item(i)).getAttribute("lat");
					String lonText = ((Element) listTrckptTag.item(i)).getAttribute("lon");
					LatLng punto = new LatLng(Double.parseDouble(latText),Double.parseDouble(lonText));
					Location pointLocation = new Location("parser");
					pointLocation.setLatitude(Double.parseDouble(latText));
					pointLocation.setLongitude(Double.parseDouble(lonText));
					currentSegment.addLocation(pointLocation);
					temp.add(punto);
					if(pathFragment != null){
						pathFragment.add(temp);
					}
				}
				ArrayList<Segment> segments = new ArrayList<Segment>();
				segments.add(currentSegment);
				segmentsSingleton.setSegments(segments);

				//Busca el nodo hijo que se llama wpt            
				NodeList children = document.getFirstChild().getChildNodes();
				boolean predef = true;
				
				Location warningLocation = null;
				int playDistance = 0;
				int warningId = 0;
				String warningDescription = "";
				for (int j = 0; j < children.getLength(); j++) {
					boolean hasVoice = false;
					Node child = children.item(j);
					if (child.getNodeName().equalsIgnoreCase("wpt")) { 
						String latText = ((Element) child).getAttribute("lat");
						String lonText = ((Element) child).getAttribute("lon");
						NodeList wptChildren = child.getChildNodes();
						for (int i = 0; i < wptChildren.getLength(); i++) {
							if(wptChildren.item(i).getNodeName().equals("name")){
								warningId = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
							}
							if(wptChildren.item(i).getNodeName().equals("cmt")){
								playDistance = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
							}
							if(wptChildren.item(i).getNodeName().equals("desc")){
								warningDescription = wptChildren.item(i).getFirstChild().getNodeValue();
							}
							warningLocation = new Location("parser");
							warningLocation.setLatitude(Double.parseDouble(latText));
							warningLocation.setLongitude(Double.parseDouble(lonText));
						}
						if(warningDescription.equals("ap")){
//							Log.i("MyMap","AvisoPropio: "+j);
							predef = false;
						}
						else{
							predef = true;
//							Log.i("MyMap","PredefinedWarning: "+j);
						}
						for(int k=0; k<names3gp.size();k++){
							if(names3gp.get(k).equals("files3gp/"+route.getId()+"/"+warnings.size()+".3gp")){
								hasVoice = true;
//								Log.i("MyMap","Warning: "+names3gp.get(k)+" hasVoice");
							}
						}
						UserWarning newWarning = new UserWarning(warningLocation, predef, warningId, warningDescription,hasVoice, playDistance);	  
						warnings.add(newWarning);						
					}
					singleton.setWarnings(warnings);
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return temp;
		}
		
		public BitmapDescriptor getWarningIcon(Context context, UserWarning warning){
				BitmapDescriptor icon_market = null;
				Log.i("DrawRouteActivity",warning.getWarningDescription());
				if(warning.hasVoice()){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_micro);
				}
				else{
					if(warning.getWarningDescription().contains("gr")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_unevenroad);
					}
					else if(warning.getWarningDescription().contains("dc")){
						if(warning.getWarningDescription().contains("le")){
							if(warning.getWarningDescription().contains("ff")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left45);
							}
							else if(warning.getWarningDescription().contains("ni")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left90);
							}
							else{
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left180);
							}
						}
						else if(warning.getWarningDescription().contains("ri")){
							if(warning.getWarningDescription().contains("ff")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right45);
							}
							else if(warning.getWarningDescription().contains("ni")){
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right90);
							}
							else{
								icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right135);
							}
						}				
					}
					else if(warning.getWarningDescription().contains("wi")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_wind);
					}
					else if(warning.getWarningDescription().contains("sb")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_baden);
					}
					else if(warning.getWarningDescription().contains("sc")){
						if(warning.getWarningDescription().contains("as")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopeup);
						}
						else if(warning.getWarningDescription().contains("de")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopedown);
						}
					}
					else{
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.ic_help);//esta linea hay que quitarla
					}
				}
			return icon_market;
		}
		
		
}
