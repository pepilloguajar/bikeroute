package com.talentum.cyclistsightracks.utils;

public interface StartFinishRouteListener {
	
	//Interfaz para que UpperFragment y UpperFragmentSmall puedan ser notificados para iniciar el chronometro y finalizar la ruta
	//cuando la actividad sobre la que se ejecutan detecte que se esta cerca del punto de inicio/fin
	public void onStartRouteListener();
	
	public void onFinishRouteListener();
}
