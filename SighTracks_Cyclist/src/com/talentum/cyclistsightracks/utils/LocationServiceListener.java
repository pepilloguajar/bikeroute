package com.talentum.cyclistsightracks.utils;

import android.location.Location;

public interface LocationServiceListener {
	public void onUpdate(Location location);
}
