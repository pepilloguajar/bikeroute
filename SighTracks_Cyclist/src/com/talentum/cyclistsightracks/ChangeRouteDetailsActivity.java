package com.talentum.cyclistsightracks;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.sdb.UsersRoutes;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ChangeRouteDetailsActivity extends ActionBarActivity {
	private boolean nameChange = false;
	private boolean publicChange = false;
	private RouteAct routes;
	private EditText name;
	
	
		@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_route_details);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		Bundle extras = getIntent().getExtras();
		routes = (RouteAct) extras.get("route");
		
		getActionBar().setTitle(routes.getName());
		
		name = (EditText)this.findViewById(R.id.name);
		name.setText(routes.getName());
		
		TextView dateTo = (TextView)this.findViewById(R.id.dateTo);
		String patron = "dd/MM/yyyy";
	    SimpleDateFormat formato = new SimpleDateFormat(patron);
	    
	    @SuppressWarnings("deprecation")
		Date dtodo = new Date(routes.getCreation());
		dateTo.setText(formato.format(dtodo));
		
		//Redondeo a dos decimales
		DecimalFormat df = new DecimalFormat("##");
		df.setRoundingMode(RoundingMode.UP);
		TextView distance =(TextView)this.findViewById(R.id.distance);
		distance.setText(df.format(routes.getDistance())+"km");
		
		TextView positiveD =(TextView)this.findViewById(R.id.DPositive);
		positiveD.setText((int)routes.getPositiveSlope()+"m");
		
		TextView negativeD =(TextView)this.findViewById(R.id.DNegative);
		negativeD.setText((int)routes.getNegativeSlope()+"m");
		
		ImageView ico =(ImageView)this.findViewById(R.id.type);
		if(routes.getCategory()!=null){
			if(routes.getCategory().compareTo("LL")==0){
	      	  ico.setImageResource(R.drawable.llana_user);
	        }else if(routes.getCategory().compareTo("MC")==0){
	      	  ico.setImageResource(R.drawable.mc_user);
	        }else{
	      	  ico.setImageResource(R.drawable.hc_user);
	        }
		}else{
			ico.setImageResource(R.drawable.llana_user);
		}
		
		
		//compruebo si es publica para desabilitar el boton
		Button shareRoute = (Button) this.findViewById(R.id.shareRoute);
		if(routes.getPublicroute()){
			shareRoute.setEnabled(false);
			shareRoute.setText(getResources().getString(R.string.route_share));
		}
		
		
		
		//Listener para ver si se modifica el nombre de la ruta
		name.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
	            nameChange = true;	        	  
	          }

	          public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

	          public void onTextChanged(CharSequence s, int start, int before, int count) {}
	       });

		
	}
	
	private void saveinBD(String name, boolean shareroute){
		Singleton s = Singleton.getInstance();
		routes.setName(name);
		routes.setPublicroute(shareroute);
		new ChangeNameAndPublic().execute(routes);
		s.setNameandPublic(routes);
	}
		
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home: // ID del boton
	    	if(nameChange==true || publicChange==true){
				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	    			@Override
	    			public void onClick(DialogInterface dialog, int which) {
	    				switch (which){
	    				case DialogInterface.BUTTON_POSITIVE:
		    				saveinBD(name.getText().toString(), publicChange);
	    					
	    					finish();
	    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.saved_changes), Toast.LENGTH_SHORT).show();
	    					startActivity(new Intent(ChangeRouteDetailsActivity.this, RoutesListActivity.class));
	    					break;
	
	    				case DialogInterface.BUTTON_NEGATIVE:
	    					finish();
	    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.discarded), Toast.LENGTH_SHORT).show();
	    					startActivity(new Intent(ChangeRouteDetailsActivity.this, RoutesListActivity.class));
	    					break;
	    				}
	    			}
	    		};
	    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    		builder.setMessage(getResources().getString(R.string.save_changes)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
	    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
			}else{
				finish();
			}
	    }
	    return true;
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(nameChange==true || publicChange==true){
				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	    			@Override
	    			public void onClick(DialogInterface dialog, int which) {
	    				switch (which){
	    				case DialogInterface.BUTTON_POSITIVE:
		    				saveinBD(name.getText().toString(), publicChange);
		    				
	    					finish();
	    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.saved_changes), Toast.LENGTH_SHORT).show();
	    					startActivity(new Intent(ChangeRouteDetailsActivity.this, RoutesListActivity.class));
	    					break;
	
	    				case DialogInterface.BUTTON_NEGATIVE:
	    					finish();
	    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.discarded), Toast.LENGTH_SHORT).show();
	    					startActivity(new Intent(ChangeRouteDetailsActivity.this, RoutesListActivity.class));
	    					break;
	    				}
	    			}
	    		};
	    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    		builder.setMessage(getResources().getString(R.string.save_changes)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
	    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
			}else{
				finish();
			}
		}
	    return true;
	}

	
	public void shareRoute(View view){
		final Button shareRoute = (Button) this.findViewById(R.id.shareRoute);
		DialogInterface.OnClickListener dialogClickListenerDiscard = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
    				saveinBD(name.getText().toString(), true);
    				
					shareRoute.setEnabled(false);
					shareRoute.setText("Ruta Compartida");
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.route_share), Toast.LENGTH_SHORT).show();
					
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					break;
				}
			}
		};
		AlertDialog.Builder builderDiscard = new AlertDialog.Builder(this);
		builderDiscard.setMessage(getResources().getString(R.string.shareMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerDiscard)
		.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerDiscard).show();
	}

	private class ChangeNameAndPublic extends AsyncTask<RouteAct, Void, Void> {
		protected Void doInBackground(RouteAct... routes) {
			UsersRoutes routeList = new UsersRoutes();
			routeList.changeAttrListRoute(routes[0]);

			return null;
		}

		protected void onPostExecute(Void result) {
			//ChangeRouteDetailsActivity.this.finish();
		}
	}	
}
