package com.talentum.cyclistsightracks;



import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.MarkerRoute;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.utils.ExploreSingleton;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.MarkersRoutesSingleton;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;


public class ExploreActivity  extends FragmentActivity implements LocationServiceListener, 
OnMyLocationButtonClickListener, ServiceConnection, OnQueryTextListener, OnActionExpandListener
{
	
	private GoogleMap mMap;	  
	private Location loc = null;
    protected PowerManager.WakeLock wakelock;
    private LocationService locationService;
    private Intent locationServiceIntent;
	
	boolean mBound = false;

	SupportMapFragment mapFragment;
	private int zoom;
	@SuppressWarnings("unused")
	private int currentOrientation;
	
	private boolean visible_popularity;
	private boolean visible_difficulty;
		
	private ArrayList<MarkerRoute> markers ;
	
	private ArrayList<String> names3gp;
	
	private void onNewMarket(BitmapDescriptor icon, LatLng l, Route route){
		Marker m = mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(route.getName() + ". " + getResources().getString(R.string.distance) + ": " + route.getDistance().intValue()+"km")
		.icon(icon));
		double pop = 100 * ((double)route.getPopularity()-(double)ExploreSingleton.getInstance().getMin_popularity())/((double)ExploreSingleton.getInstance().getMax_popularity() - (double)ExploreSingleton.getInstance().getMin_popularity());
		markers.add(new MarkerRoute(m, true, icon, l, pop, route));
	} 
	
	private Marker addMarker(LatLng l, String title, BitmapDescriptor icon){
		Marker m = mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(title)
		.icon(icon));
		return m;
	}

	
	/**
	 * Dibuja los puntos iniciales de las Rutas
	 */
	private void drawPointsIniRoutes(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = prefs.getString("username", "Default Value if not found");
		
		BitmapDescriptor icon_mov = null, icon_user = null, icon_others = null;
		icon_mov = BitmapDescriptorFactory.fromResource(R.drawable.market_mov);
		icon_user = BitmapDescriptorFactory.fromResource(R.drawable.market_user);
		icon_others = BitmapDescriptorFactory.fromResource(R.drawable.market_otherusers);
		
		//Rutas del Movistar
		for(int i=0; i<Singleton.getInstance().getListMovRoutes().size(); i++){
			//onNewMarket(icon_mov, new LatLng(Singleton.getInstance().getListMovRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListMovRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListMovRoutes().get(i).getName().toUpperCase() + "\n" + Singleton.getInstance().getListMovRoutes().get(i).getDistance().intValue()+"km. Desnivel positivo: "+Singleton.getInstance().getListMovRoutes().get(i).getPositiveSlope().intValue()+"m.", Singleton.getInstance().getListMovRoutes().get(i).getPopularity(), Singleton.getInstance().getListMovRoutes().get(i).getDistance(), Singleton.getInstance().getListMovRoutes().get(i).getCategory());
			onNewMarket(icon_mov, new LatLng(Singleton.getInstance().getListMovRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListMovRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListMovRoutes().get(i));
		}
		
		//Rutas de los Usuarios
		for(int i=0; i<Singleton.getInstance().getListUsersRoutes().size(); i++){
			if(Singleton.getInstance().getListUsersRoutes().get(i).getUserid().equals(username)){//Si es del usuario
				//onNewMarket(icon_user, new LatLng(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListUsersRoutes().get(i).getName().toUpperCase() + "\n" + Singleton.getInstance().getListUsersRoutes().get(i).getDistance().intValue()+"km. Desnivel positivo: "+Singleton.getInstance().getListUsersRoutes().get(i).getPositiveSlope().intValue()+"m.", Singleton.getInstance().getListUsersRoutes().get(i).getPopularity(), Singleton.getInstance().getListUsersRoutes().get(i).getDistance(), Singleton.getInstance().getListUsersRoutes().get(i).getCategory());
				onNewMarket(icon_user, new LatLng(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListUsersRoutes().get(i));

			}else{
				onNewMarket(icon_others, new LatLng(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListUsersRoutes().get(i));
				//onNewMarket(icon_others, new LatLng(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlat(), Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlon()), Singleton.getInstance().getListUsersRoutes().get(i).getName().toUpperCase() + "\n" + Singleton.getInstance().getListUsersRoutes().get(i).getDistance().intValue()+"km. Desnivel positivo: "+Singleton.getInstance().getListUsersRoutes().get(i).getPositiveSlope().intValue()+"m.", Singleton.getInstance().getListUsersRoutes().get(i).getPopularity(), Singleton.getInstance().getListUsersRoutes().get(i).getDistance(), Singleton.getInstance().getListUsersRoutes().get(i).getCategory());
		}	}
	}	
	
	@SuppressLint("InlinedApi")
	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PropertyLoader.getInstance().setIntervalGPS(PropertyLoader.getInstance().getIntervalExplore());
		
	    setContentView(R.layout.activity_explore);

        //evitar que la pantalla se apague
        final PowerManager pm=(PowerManager)getSystemService(this.POWER_SERVICE);
        this.wakelock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "etiqueta");
        wakelock.acquire();
		
        // Carga servicios y vista del mapa
	    locationServiceIntent = new Intent(getApplicationContext(), LocationService.class);
		bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		zoom = 12;
		markers = new ArrayList<MarkerRoute>();
		
        mapFragment = new SupportMapFragment();

		// Carga fragmento de popularidad
		PopularityFragment popFragment = new PopularityFragment();
		popFragment.setArguments(getIntent().getExtras());
        
        getSupportFragmentManager().beginTransaction()
        .add(R.id.down_fragment_container, popFragment).commit();
        getSupportFragmentManager().beginTransaction()
        .add(R.id.map_fragment_container, mapFragment).commit();		
        getSupportFragmentManager().beginTransaction().hide(popFragment).commit();
		currentOrientation = getResources().getConfiguration().orientation; 
        
        
		if(loc != null){
			LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
			mMap.animateCamera(cameraUpdate);
		}
		
        visible_difficulty = false;
        visible_popularity = true;
        
        ExploreSingleton.getInstance().setActivate_hc(true);
        ExploreSingleton.getInstance().setActivate_mc(true);
        ExploreSingleton.getInstance().setActivate_ll(true);
        
        ExploreSingleton.getInstance().setPopularity(0);
        
		//PropertyLoader.getInstance().setIntervalGPS(intervalGPS);
		names3gp = new ArrayList<String>();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.search_route, menu);
		 
		MenuItem searchItem = menu.findItem(R.id.buscar);

	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

	    
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    //Cambio el estilo del searchView del actionBar
	    SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
	    searchAutoComplete.setTextColor(Color.WHITE);
	    searchAutoComplete.setHint(R.string.search_address);
	    
	    ImageView searchIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
	    searchIcon.setImageResource(R.drawable.ic_menu_search);
	    
	    ImageView searchCloseIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
	    searchCloseIcon.setImageResource(R.drawable.discard_button);
	    
		// LISTENER PARA EL EDIT TEXT   
	    searchView.setOnQueryTextListener((OnQueryTextListener) this);
	    // LISTENER PARA LA APERTURA Y CIERRE DEL WIDGET
	    MenuItemCompat.setOnActionExpandListener(searchItem, (OnActionExpandListener) this);
     
	    return true;
	}
	
	public boolean onQueryTextChange(String arg0) {
		return false;
	}
	public boolean onQueryTextSubmit(String arg0) {
		Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
		 LatLng latLng = null;;
		try {
            List<Address> addresses =
                   geoCoder.getFromLocationName(arg0, 1);
            if (addresses.size() > 0) {
                latLng = new LatLng(addresses.get(0).getLatitude() ,addresses.get(0).getLongitude());
            }else{
            	latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
            	Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_address), Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
		

		//Cierro el teclado virtual automaticamente al hacer la b??squeda
		View v = (View) findViewById(R.id.buscar);
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

	    return true;
	}
	public boolean onMenuItemActionCollapse(MenuItem arg0) {
	    return true;
	}
	public boolean onMenuItemActionExpand(MenuItem arg0) {
	    return true;
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		LocalBinder binder = (LocalBinder) service;
		locationService = binder.getService();
		locationService.registerListener(this);
		locationService.registerListener(ExploreActivity.this);
		mBound = true;
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		locationService = null;
		mBound = false;
	}	
	
	@SuppressLint("Wakelock")
	protected void onDestroy(){
        this.wakelock.release();
        super.onDestroy();
    }
	
    @Override
    protected void onResume() {
        super.onResume();
        wakelock.acquire();
        setUpMapIfNeeded();
        CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(16);
		mMap.animateCamera(cameraUpdate);
		if(!mBound){
			getApplicationContext().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		}
	}
    
    
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if(mBound == true){
    	    loc = null;
    	    locationService.unregisterListener(this);		
    	    unbindService(this);
    	    mBound = false;
		}
    }
    
	@Override
	public boolean onMyLocationButtonClick() {
		if(loc != null){
			LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
			mMap.animateCamera(cameraUpdate);
		}
		return false;
	}

	public void updateMap(){
		for(int i=0; i<markers.size(); i++){
			//Popularidad
			if((ExploreSingleton.getInstance().getPopularity() > markers.get(i).getPopularity() && markers.get(i).isVisibility())
					||
					(!ExploreSingleton.getInstance().isActivate_ll() && markers.get(i).getRoute().getCategory().equals("LL") && markers.get(i).isVisibility())
					||
					(!ExploreSingleton.getInstance().isActivate_mc() && markers.get(i).getRoute().getCategory().equals("MC") && markers.get(i).isVisibility())
					||
					(!ExploreSingleton.getInstance().isActivate_hc() && markers.get(i).getRoute().getCategory().equals("HC") && markers.get(i).isVisibility())
					||
					(ExploreSingleton.getInstance().getMin_distance_rel() > markers.get(i).getRoute().getDistance() && markers.get(i).isVisibility())
					||
					(ExploreSingleton.getInstance().getMax_distance_rel() < markers.get(i).getRoute().getDistance() && markers.get(i).isVisibility())
					){//Si no llega a la popularidad marcada y está en el mapa
				markers.get(i).getM().remove();
				markers.get(i).setVisibility(false);
			}
			else if(ExploreSingleton.getInstance().getPopularity() < markers.get(i).getPopularity() && !markers.get(i).isVisibility()
					&&
					((ExploreSingleton.getInstance().isActivate_ll() && markers.get(i).getRoute().getCategory().equals("LL") && !markers.get(i).isVisibility())
					||
					(ExploreSingleton.getInstance().isActivate_mc() && markers.get(i).getRoute().getCategory().equals("MC") && !markers.get(i).isVisibility())
					||
					(ExploreSingleton.getInstance().isActivate_hc() && markers.get(i).getRoute().getCategory().equals("HC") && !markers.get(i).isVisibility())
					) 
					&&
					(
					ExploreSingleton.getInstance().getMin_distance_rel() < markers.get(i).getRoute().getDistance() && !markers.get(i).isVisibility()
					&&
					ExploreSingleton.getInstance().getMax_distance_rel() > markers.get(i).getRoute().getDistance()
					)){
				markers.get(i).setVisibility(true);
				markers.get(i).setM(addMarker(markers.get(i).getL(), markers.get(i).getRoute().getName() + "." + getResources().getString(R.string.distance) + markers.get(i).getRoute().getDistance() + "km", markers.get(i).getIcon()));
			}
		}
	}
	
	@Override
	public void onUpdate(Location location) {		
		if(loc == null){
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
			mMap.animateCamera(cameraUpdate);  	
		}
    	loc = new Location(location);
	}

	
    private void setUpMapIfNeeded() {
    	// Do a null check to confirm that we have not already instantiated the map.
    	if (mMap == null) {			
    		// Try to obtain the map from the SupportMapFragment.
//    		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//    				.getMap();
    		mMap = mapFragment.getMap();
    		// Check if we were successful in obtaining the map.
    		if (mMap != null) {
    			drawPointsIniRoutes();//Dibuja los puntos de la ruta
    			
    			mMap.setMyLocationEnabled(true);
    			mMap.setOnMyLocationButtonClickListener(this);
    			
    			mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
					public void onInfoWindowClick(Marker marker) {
						Location lmarket = new Location("");
						lmarket.setLatitude(marker.getPosition().latitude);
						lmarket.setLongitude(marker.getPosition().longitude);
				        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				        String username = prefs.getString("username", "Default Value if not found");
						
						//Rutas del Movistar
						boolean find = false, usuario = false;
						int p = 0;
						for(int i=0; i<Singleton.getInstance().getListMovRoutes().size() && !find; i++){
							Location l = new Location("");
							l.setLatitude(Singleton.getInstance().getListMovRoutes().get(i).getStartpointlat());
							l.setLongitude(Singleton.getInstance().getListMovRoutes().get(i).getStartpointlon());
							if( lmarket.distanceTo(l) < 1 ){
								find = true;
								p = i;
							}
						}
						if(find){//Ha encontrado ruta movistar
							Log.i("ExploreActivity", "Ruta movistar: "+Singleton.getInstance().getListMovRoutes().get(p).getName());
							new MovistarS3PutObjectTask().execute(Singleton.getInstance().getListMovRoutes().get(p));
						}
						else{
							//Rutas de los Usuarios
							for(int i=0; i<Singleton.getInstance().getListUsersRoutes().size() && !find; i++){
								Location l = new Location("");
								l.setLatitude(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlat());
								l.setLongitude(Singleton.getInstance().getListUsersRoutes().get(i).getStartpointlon());
								if( lmarket.distanceTo(l) < 1 ){
									find = true;
									p = i;
									if(Singleton.getInstance().getListUsersRoutes().get(i).getUserid().equals(username)){//Si es del usuario
										usuario = true;
									}
								}
							}
							if(find){
								if(usuario){
									Log.i("ExploreActivity", "Ruta usuario: "+Singleton.getInstance().getListUsersRoutes().get(p).getName());
									new S3PutObjectTask().execute(Singleton.getInstance().getListUsersRoutes().get(p));

								}
								else{
									Log.i("ExploreActivity", "Ruta otro usuario: "+Singleton.getInstance().getListUsersRoutes().get(p).getName());
									new S3PutObjectTask().execute(Singleton.getInstance().getListUsersRoutes().get(p));
								}
							}
						}
					}
				});
    		}
    	}
    }
    
	 public void toActivityMap(View view) {
		 //Restaura los valores mínimo y máximo
		 ExploreSingleton.getInstance().setMin_distance_rel(ExploreSingleton.getInstance().getMin_distance());
		 ExploreSingleton.getInstance().setMax_distance_rel(ExploreSingleton.getInstance().getMax_distance());
		 
		 MarkersRoutesSingleton.getInstance().setListMarkers(markers); //Guarda en el Singleton las Rutas filtradas
		 finish();
		 startActivity(new Intent(ExploreActivity.this,RoutesListMapActivity.class));
	 }   
	 
	 public void openpopularity(View view){
		// Carga fragmento de popularidad
		PopularityFragment bottomFragment = new PopularityFragment();
		bottomFragment.setArguments(getIntent().getExtras());
			
		 if(visible_popularity == false){
			getSupportFragmentManager().beginTransaction().replace(R.id.down_fragment_container, bottomFragment).commit();
	        visible_popularity = true;
	        visible_difficulty = false;
		 }
        else{
	        getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentById(R.id.down_fragment_container)).commit();
	        visible_popularity = false;
        }
	 }
	 
	 public void opendifficulty(View view){
		// Carga fragmento de popularidad
		DifficultyFragment bottomFragment = new DifficultyFragment();
		bottomFragment.setArguments(getIntent().getExtras());
		 
		if(visible_difficulty == false || (visible_popularity == true && visible_difficulty == true)){
	        getSupportFragmentManager().beginTransaction().replace(R.id.down_fragment_container, bottomFragment).commit();
	        //im_mc = (ImageButton) getSupportFragmentManager().findFragmentById(R.layout.bottom_difficulty_fragment).getView().findViewById(R.id.ib_mc);
			//im_mc.setImageDrawable(getResources().getDrawable(R.drawable.mc));
			
	        
	        visible_difficulty = true;
	        visible_popularity = false;
		}
        else{
        	Log.i("ExploreActivity", "Oculta fragment dif");
	        getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentById(R.id.down_fragment_container)).commit();
	        visible_difficulty = false;
        }
	 }	 
	 
		
		private void writefile(InputStream inputStream, String routeId, String name){
	    	OutputStream outputStream = null;
	     
	    	try {
	    		// write the inputStream to a FileOutputStream
	    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
	            if(!routeFolder.exists()){
	 	           if (!routeFolder.exists()) {
	 	               routeFolder.mkdir();
	 	           }
	            }
	    		outputStream = 
	                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
	     
	    		int read = 0;
	    		byte[] bytes = new byte[1024];
	     
	    		while ((read = inputStream.read(bytes)) != -1) {
	    			outputStream.write(bytes, 0, read);
	    		}
	          
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	} finally {
	    		if (inputStream != null) {
	    			try {
	    				inputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		}
	    		if (outputStream != null) {
	    			try {
	    				// outputStream.flush();
	    				outputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	     
	    		}
	    	}    	
	    }
		
		private class MovistarS3PutObjectTask extends AsyncTask<Route, Void, S3TaskResult> {
			ProgressDialog dialog;
			File file;
			Route route;
			String bucket;
			int currentOrientation = getResources().getConfiguration().orientation;
			
			protected void onPreExecute() {
		        super.onPreExecute();
		        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		        }
		        else {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		        }
		        
		        
		       dialog = new ProgressDialog(ExploreActivity.this);
				dialog.setMessage(ExploreActivity.this
						.getString(R.string.loading_));
				dialog.setCancelable(false);
				dialog.show();
			}
			
			protected S3TaskResult doInBackground(Route... args) {
				/** Guarda el archivo GPX (PRUEBAS) **/

				if (args == null || args.length != 1) {
					return null;
				}
				bucket = PropertyLoader.getInstance().getBucketGPXteam();
				route = args[0];

				S3TaskResult result = new S3TaskResult();	
				try{
					/************/
						 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
				            .withBucketName("sightracksteam").withPrefix("files3gp/"+route.getId()+"/");	
				   ObjectListing objectListing;
				    do {
				    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
				    	for (S3ObjectSummary objectSummary : 
				    		objectListing.getObjectSummaries()) {
				    		InputStream in = MainActivity.clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
		    		    	writefile(in, route.getId(), objectSummary.getKey());
		    		    	names3gp.add(objectSummary.getKey());
				    	}
				    } while (objectListing.isTruncated());
					/************/   
					//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
					MainActivity.clientManager.s3().getBucketAcl(bucket);
					GetObjectRequest gor = new GetObjectRequest(bucket,route.getId()+".gpx");
					S3Object object = MainActivity.clientManager.s3().getObject(gor);
					InputStream reader = new BufferedInputStream(object.getObjectContent());
					
					file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",route.getId());
					OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
					int read = -1;

					while ( ( read = reader.read() ) != -1 ) {
					    writer.write(read);
					}
					writer.flush();
					writer.close();
					reader.close();
					
				} catch (Exception exception) {
					Log.i("MyMap","Error: "+exception.getMessage());
					result.setErrorMessage(exception.getMessage());
				}


				return result;
			}
			
			protected void onPostExecute(S3TaskResult result) {
				dialog.dismiss();
				if (result.getErrorMessage() != null) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
					ExploreActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),MainActivity.class);
					startActivity(i);
				}
				else {
					ExploreActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
					i.putExtra("route", route);
					i.putExtra("filegpx", file);
					i.putStringArrayListExtra("files3gp", names3gp);
					i.putExtra("activity", "ExploreActivity");
					startActivity(i);
				}
			   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			}
		}
		
		
		private class S3PutObjectTask extends AsyncTask<Route, Void, S3TaskResult> {
			ProgressDialog dialog;
			File file;
			Route route;
			String bucket;
			int currentOrientation = getResources().getConfiguration().orientation;
			
			protected void onPreExecute() {
		        super.onPreExecute();
		        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		        }
		        else {
		             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		        }
		        
		        
		       dialog = new ProgressDialog(ExploreActivity.this);
				dialog.setMessage(ExploreActivity.this
						.getString(R.string.loading_));
				dialog.setCancelable(false);
				dialog.show();
			}
			
			protected S3TaskResult doInBackground(Route... args) {
				/** Guarda el archivo GPX (PRUEBAS) **/

				if (args == null || args.length != 1) {
					return null;
				}
				bucket = PropertyLoader.getInstance().getBucketGPXusers();
				route = args[0];

				S3TaskResult result = new S3TaskResult();	
				try{
					MainActivity.clientManager.s3().getBucketAcl(bucket);
					GetObjectRequest gor = new GetObjectRequest(bucket,route.getId()+".gpx");
					S3Object object = MainActivity.clientManager.s3().getObject(gor);
					InputStream reader = new BufferedInputStream(object.getObjectContent());
					
					file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",route.getId());
					OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
					int read = -1;

					while ( ( read = reader.read() ) != -1 ) {
					    writer.write(read);
					}
					writer.flush();
					writer.close();
					reader.close();
					
				} catch (Exception exception) {
					result.setErrorMessage(exception.getMessage());
				}

				return result;
			}
			
			protected void onPostExecute(S3TaskResult result) {
				dialog.dismiss();
				if (result.getErrorMessage() != null) {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
					ExploreActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),MainActivity.class);
					startActivity(i);
				}
				else {
					ExploreActivity.this.finish();
					Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
					i.putExtra("route", route);
					i.putExtra("filegpx", file);
					i.putStringArrayListExtra("files3gp", names3gp);
					i.putExtra("activity", "ExploreActivity");
					startActivity(i);
				}
			   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			}
		}		
		
		private class S3TaskResult {
			String errorMessage = null;

			public String getErrorMessage() {
				return errorMessage;
			}

			public void setErrorMessage(String errorMessage) {
				this.errorMessage = errorMessage;
			}
		}		
}

