package com.talentum.cyclistsightracks;

import java.text.DecimalFormat;

import com.talentum.cyclistsightracks.UpperFragmentSmall;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.StartFinishRouteListener;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;


public class UpperFragment extends Fragment implements OnClickListener, LocationServiceListener, ServiceConnection,
StartFinishRouteListener{
	
		private Chronometer chrono;
		private ImageView startButton;
		private ImageView finishButton;
		private TextView speedTextView;
		private TextView currentDistanceTextView;
		private ImageView rightArrow;
		private boolean isRecording;
		//Almacena el tiempo cuando se pauso el cronometro para reanudarlo desde esa referencia
		private long timeWhenStopped;
		
		private LocationService locationService;
		private Intent locationServiceIntent;
		private boolean mBound = false;
		private double currentDistance;
		private double totalDistance;
		private Location lastLocation;
		//Controla si el fragmento se situa sobre la actividad NewRouteActivity (true) o sobre NewUserActivity(false) 
		private boolean isNewRoute;
		private boolean hasStarted;
		private int minimunDistance = Integer.parseInt(PropertyLoader.getInstance().getDistanceMin());
		

		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.upper_fragment, container,
					false);
			startButton = (ImageView) rootView.findViewById(R.id.start_button);
			finishButton = (ImageView) rootView.findViewById(R.id.finish_button);
			chrono = (Chronometer) rootView.findViewById(R.id.chronometer1);
			speedTextView = (TextView) rootView.findViewById(R.id.speed);
			currentDistanceTextView = (TextView) rootView.findViewById(R.id.total_distance);
			rightArrow = (ImageView) rootView.findViewById(R.id.arrow_right);

			startButton.setOnClickListener(this);
			finishButton.setOnClickListener(this);
			rightArrow.setOnClickListener(this);
			locationServiceIntent = new Intent(getActivity().getApplicationContext(), LocationService.class);
			getActivity().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
			startButton.setOnClickListener(this);
			//El primer argumento indica si el fragmento se ejecuta sobre NewUserActivity o sobre newRouteActivity
			isNewRoute = getArguments().getBoolean("isNewRoute");
			totalDistance = getArguments().getDouble("totalDistance");
			if(!isNewRoute){
				((NewUserActivity) getActivity()).registerListener(this);
			}
			if(getArguments().size() > 2){
				timeWhenStopped = getArguments().getLong("stopTime");
				isRecording = getArguments().getBoolean("isRecording");
				hasStarted = getArguments().getBoolean("hasStarted");
				currentDistance = getArguments().getDouble("currentDistance");
				DecimalFormat df=new DecimalFormat("0.0");
				currentDistanceTextView.setText(df.format(currentDistance));
				if(isRecording){
					if(hasStarted){
						startChronometer();
					}
					startButton.setImageResource(R.drawable.pause_button);
				}
				if(timeWhenStopped != 0){
					chrono.setBase(SystemClock.elapsedRealtime()-timeWhenStopped);
				}
			}
			else{
				if(isNewRoute){
					hasStarted = true;
				}
				else{
					hasStarted = false;
				}
				isRecording = false;
				timeWhenStopped = 0;
				currentDistance = 0;
			}
			
			return rootView;
		}
		
		@Override
		public void onStop() {
			if(mBound){
				getActivity().unbindService(this);
				mBound = false;
				locationService.unregisterListener(this);
			}
			super.onStop();
		}
		
		//
		@Override
		public void onClick(View v) {
			if(v.getId() == R.id.start_button){
				if(isRecording){
					stopChronometer();
					isRecording = false;
					startButton.setImageResource(R.drawable.start_button);
					if(isNewRoute){
						((NewRouteActivity) getActivity()).pauseResumeRoute();
					}
					else{
						((NewUserActivity) getActivity()).pauseResumeRoute();
//						if(timeWhenStopped == 0){
//							((NewUserActivity) getActivity()).startRoute();
//						}
					}
				}
				else{
					if(isNewRoute){
						((NewRouteActivity) getActivity()).pauseResumeRoute();
					}
					else{
						((NewUserActivity) getActivity()).pauseResumeRoute();
					}
					if(hasStarted)
						startChronometer();
					isRecording = true;
					startButton.setImageResource(R.drawable.pause_button);
				}
			}
			if((v.getId() == R.id.start_button && timeWhenStopped == 0) || v.getId() == R.id.arrow_right){
				changeView();
			}
			if(v.getId() == R.id.finish_button){
				showConfirmationDialog();
			}
		}
		

	    public void startChronometer() {
	    	chrono.setBase(SystemClock.elapsedRealtime() - timeWhenStopped);
	    	chrono.start();
	    }

	    public void stopChronometer() {
	    	timeWhenStopped = SystemClock.elapsedRealtime() - chrono.getBase();
	    	chrono.stop();
	    }

	    @Override
	    public void onUpdate(Location location) {
	    	double speed = location.getSpeed();
	    	DecimalFormat df=new DecimalFormat("0.0");
	    	speedTextView.setText(df.format(speed/1000*3600));
	    	if(isRecording && hasStarted){
	    		addDistance(location);
	    		currentDistanceTextView.setText(df.format(currentDistance/1000));
	    		lastLocation = location;
	    	}
	    }

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) { 
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			locationService.registerListener(this);
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
			locationService = null;
		}
		
		private void addDistance(Location newLocation){
			if(lastLocation != null){
				currentDistance+=lastLocation.distanceTo(newLocation);

			}
		}
		
		private void changeView(){
			if(!isNewRoute){
				((NewUserActivity) getActivity()).unregisterListener(this);
			}
			UpperFragmentSmall upperFragmentSmall = new UpperFragmentSmall();
			Bundle args = new Bundle();
			if(isRecording && hasStarted)
				stopChronometer();
			args.putLong("stopTime",timeWhenStopped);
			args.putBoolean("isRecording",isRecording);
			args.putDouble("currentDistance",currentDistance);
			args.putDouble("totalDistance",totalDistance);
			args.putBoolean("isNewRoute", isNewRoute);
			args.putBoolean("hasStarted", hasStarted);
			upperFragmentSmall.setArguments(args);

			//Cambia el fragmento
			FragmentManager fragmentManager = getFragmentManager();
			android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.upper_fragment_container,upperFragmentSmall);
			fragmentTransaction.disallowAddToBackStack();
			fragmentTransaction.commit();
		}
		
		private void showConfirmationDialog(){
			DialogInterface.OnClickListener dialogClickListenerFinish = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
							finishRoute(true);
						break;

					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			AlertDialog.Builder builderDiscard = new AlertDialog.Builder(getActivity());

			//Controla si se ha superado la distancia minima para al cerrar la ruta o si se ha completado la actividad de la ruta existente
			if(isNewRoute && (currentDistance <= minimunDistance)){
				builderDiscard.setMessage(getResources().getString(R.string.noMinimumDistance)+" ("+minimunDistance/1000+"km) "+
					getResources().getString(R.string.finishWithoutSave))
					.setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerFinish)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerFinish).show();
			}
			else if(!isNewRoute){
				builderDiscard.setMessage(getResources().getString(R.string.noTotalActivityDistance)+" "+
					getResources().getString(R.string.finishWithoutSave))
					.setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerFinish)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerFinish).show();
			
			}
			else{
				//Ha pulsado el boton de terminar
				builderDiscard.setMessage(getResources().getString(R.string.finishMyRoute))
					.setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerFinish)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerFinish).show();
			}
		}
		
		//Controla si la llamada a terminar es porque el usuario ha terminado finalizar o porque se encuentra proximo al punto de fin
		private void finishRoute(boolean isFromDialog){
			Intent intent;
			//Si no se ha llegado a la distancia minima o no se ha completado la actividad, finaliza la actividad sin guardar
			if(isFromDialog && (!isNewRoute || (isNewRoute && (currentDistance <= minimunDistance)))){
				intent = new Intent(getActivity(), RoutesListActivity.class);
				getActivity().finish();
				startActivity(intent);
			}
			else if(!isFromDialog && (currentDistance > totalDistance*0.8*1000)){
				//Controla si el fragment esta sobre la actividad NewRouteActivity o NewUserActivity (para el Casting)
				stopChronometer();
				if(isNewRoute){
					((NewRouteActivity) getActivity()).finishRoute(currentDistance, timeWhenStopped);
				}
				else{
					((NewUserActivity) getActivity()).finishRoute(currentDistance, timeWhenStopped);
				}
			}
			else if(isFromDialog && (isNewRoute && (currentDistance > minimunDistance))){
				stopChronometer();
				if(isNewRoute){									
					((NewRouteActivity) getActivity()).finishRoute(currentDistance, timeWhenStopped);
				}
				else{
					((NewUserActivity) getActivity()).finishRoute(currentDistance, timeWhenStopped);
				}
			}
		}

		@Override
		public void onStartRouteListener() {
			hasStarted = true;
			startChronometer();
		}

		@Override
		public void onFinishRouteListener() {
			finishRoute(false);

		}
}
