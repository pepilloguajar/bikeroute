package com.talentum.cyclistsightracks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.utils.MarkersRoutesSingleton;
import com.talentum.cyclistsightracks.utils.PropertyLoader;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class RoutesListMapActivity extends ListActivity implements ServiceConnection, OnQueryTextListener, OnActionExpandListener {
	

	private static LocationService locationService;
	static boolean mBound = false;
	private List<Route> list = new ArrayList<Route>();
	private Location locationRoute;
	private Location location;
	private int currentOrientation;
	private ArrayList<String> names3gp;
	
	public static AmazonClientManager clientManager = null;
	 
	List<Route> orderList(List<Route> myList){
		List<Route> listOr = new ArrayList<Route>();
		List<Route> myLista = new ArrayList<Route>();
		myLista.addAll(myList);
		while(myList.size()>0){
			float min=Float.MAX_VALUE;
			int pos=0;
			float distanceTo;
			for(int i = 0; i<myList.size();i++){
				locationRoute = new Location("dummyprovider");
		    	locationRoute.setLatitude(myList.get(i).getStartpointlat());
		    	locationRoute.setLongitude(myList.get(i).getStartpointlon());
		    	distanceTo = location.distanceTo(locationRoute);
		    	if(distanceTo<min){
		    		min= distanceTo;
		    		pos = i;
		    	}
			}
			listOr.add(myList.get(pos));
			myList.remove(pos);
		}
		
		return listOr;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		for(int i=0; i<MarkersRoutesSingleton.getInstance().getListMarkers().size(); i++)
			if(MarkersRoutesSingleton.getInstance().getListMarkers().get(i).isVisibility())
				Log.i("RoutesListMapActivity", MarkersRoutesSingleton.getInstance().getListMarkers().get(i).getRoute().getName());
		
		setContentView(R.layout.activity_challenges);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);	
		
		Intent locationServiceIntent = new Intent(RoutesListMapActivity.this, LocationService.class);
		bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		
		names3gp = new ArrayList<String>();
		
		currentOrientation = getResources().getConfiguration().orientation;
		
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.search_route, menu);
		 
		MenuItem searchItem = menu.findItem(R.id.buscar);

	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    searchView.setMaxWidth(2000);
	    
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    //Cambio el estilo del searchView del actionBar
	    SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
	    searchAutoComplete.setTextColor(Color.WHITE);
	    searchAutoComplete.setHint(R.string.hint_search);
	    
	    ImageView searchIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
	    searchIcon.setImageResource(R.drawable.ic_menu_search);
	    
	    ImageView searchCloseIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
	    searchCloseIcon.setImageResource(R.drawable.discard_button);
	    
		// LISTENER PARA EL EDIT TEXT   
	    searchView.setOnQueryTextListener((OnQueryTextListener) this);
	    // LISTENER PARA LA APERTURA Y CIERRE DEL WIDGET
	    MenuItemCompat.setOnActionExpandListener(searchItem, (OnActionExpandListener) this);
     
	    return true;
	}
	
	public boolean onQueryTextChange(String arg0) {
		//Toast.makeText(this, "Buscando..."+arg0, Toast.LENGTH_LONG).show();
		List<Route> listSearch = new ArrayList<Route>();
		for(int i=0; i<list.size();i++){
			String rutaCompara = list.get(i).getName().toLowerCase();
			if(rutaCompara.contains(arg0.toLowerCase())){
				listSearch.add(list.get(i));
			}
		}
	
		//Cargo las listas que contengan la palabra que busco
		setListAdapter(new RoutesMovAdapter(this,listSearch, location));
		return false;
	}
	public boolean onQueryTextSubmit(String arg0) {
		
		List<Route> listSearch = new ArrayList<Route>();
		for(int i=0; i<list.size();i++){
			String rutaCompara = list.get(i).getName().toLowerCase();
			if(rutaCompara.contains(arg0.toLowerCase())){
				listSearch.add(list.get(i));
			}
		}
	
		//Cargo las listas que contengan la palabra que busco
		setListAdapter(new RoutesMovAdapter(this,listSearch, location));

		//Cierro el teclado virtual automaticamente al hacer la b??squeda
		View v = (View) findViewById(R.id.buscar);
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

	    return true;
	}
	public boolean onMenuItemActionCollapse(MenuItem arg0) {
	    return true;
	}
	public boolean onMenuItemActionExpand(MenuItem arg0) {
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			Intent inUp = new Intent(getApplicationContext(),ExploreActivity.class);
			startActivity(inUp);
	        return true;
	        
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
			finish();
			Intent inUp = new Intent(getApplicationContext(),ExploreActivity.class);
			startActivity(inUp);
		return super.onKeyDown(keyCode, event);
	}

	
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		LocalBinder binder = (LocalBinder) service;
		locationService = binder.getService();
		location = locationService.getLastLocation();
		mBound = true;
		list.clear();

		for(int i=0; i<MarkersRoutesSingleton.getInstance().getListMarkers().size(); i++)
			if(MarkersRoutesSingleton.getInstance().getListMarkers().get(i).isVisibility())
				list.add(MarkersRoutesSingleton.getInstance().getListMarkers().get(i).getRoute());
		list = orderList(list);
		
		RoutesMovAdapter adapter = new RoutesMovAdapter(RoutesListMapActivity.this, list, location);
		setListAdapter(adapter);
		
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id){
		super.onListItemClick(listView, view, position, id);
		new S3PutObjectTask().execute(position);
	}
	
	@Override
	public void onDestroy() {
		if(mBound == true){
		this.unbindService(this);
		}
		super.onDestroy();
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		locationService = null;
		mBound = false;
		Log.i("MyMap","ChallengesActivity onServiceDisconnected");
	}
	
	private void writefile(InputStream inputStream, String routeId, String name){
    	OutputStream outputStream = null;
     
    	try {
    		// write the inputStream to a FileOutputStream
    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
            if(!routeFolder.exists()){
 	           if (!routeFolder.exists()) {
 	               routeFolder.mkdir();
 	           }
            }
    		outputStream = 
                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
          
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (inputStream != null) {
    			try {
    				inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}    	
    }
	
	
	
	private class S3PutObjectTask extends AsyncTask<Integer, Void, S3TaskResult> {
		ProgressDialog dialog;
		File file;
		int posicion;
		String bucket;
		
		protected void onPreExecute() {
	        super.onPreExecute();
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
	        
	        
	        dialog = new ProgressDialog(RoutesListMapActivity.this);
			dialog.setMessage(RoutesListMapActivity.this
					.getString(R.string.loading_));
			dialog.setCancelable(false);
			dialog.show();
		}
		
		protected S3TaskResult doInBackground(Integer... args) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (args == null || args.length != 1) {
				return null;
			}
			posicion = args[0];

			S3TaskResult result = new S3TaskResult();	
			try{
				if(list.get(posicion).getUserid().equalsIgnoreCase("Movistar")){
					bucket = PropertyLoader.getInstance().getBucketGPXteam();
				
				/************/
					 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+list.get(posicion).getId()+"/");	
			   ObjectListing objectListing;
			    do {
			    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
			    	for (S3ObjectSummary objectSummary : 
			    		objectListing.getObjectSummaries()) {
			    		InputStream in = clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
	    		    	writefile(in, list.get(posicion).getId(), objectSummary.getKey());
	    		    	names3gp.add(objectSummary.getKey());
			    	}
			    } while (objectListing.isTruncated());
				/************/    
				}
				else{
					bucket = PropertyLoader.getInstance().getBucketGPXusers();
				}
				//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
				clientManager.s3().getBucketAcl(bucket);
				GetObjectRequest gor = new GetObjectRequest(bucket,list.get(posicion).getId()+".gpx");
				S3Object object =clientManager.s3().getObject(gor);
				InputStream reader = new BufferedInputStream(object.getObjectContent());
				
				file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",list.get(posicion).getId());
				OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				int read = -1;

				while ( ( read = reader.read() ) != -1 ) {
				    writer.write(read);
				}
				writer.flush();
				writer.close();
				reader.close();
				
			} catch (Exception exception) {
				result.setErrorMessage(exception.getMessage());
			}


			return result;
		}
		
		protected void onPostExecute(S3TaskResult result) {
			dialog.dismiss();
			if (result.getErrorMessage() != null) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
				RoutesListMapActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
			}
			else {
//				RoutesListActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
				i.putExtra("route", list.get(posicion));
				i.putExtra("filegpx", file);
				i.putStringArrayListExtra("files3gp", names3gp);
				i.putExtra("activity", "RoutesListActivity");
				startActivity(i);
				
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}


}
