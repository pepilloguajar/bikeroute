package com.talentum.cyclistsightracks;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.talentum.cyclistsightracks.model.UserWarning;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationServiceExplore extends Service implements
	LocationListener,
	GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener{
	
	private List<UserWarning> warnings;
	
	private final IBinder mBinder = new LocalBinder();
	
    private LocationManager locationManager;
    private String provider;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    
    private Location location;
    
    private final ArrayList<LocationServiceListener> mListeners
    = new ArrayList<LocationServiceListener>();
    
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(Integer.valueOf(PropertyLoader.getInstance().getIntervalExplore()))         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    
    @Override
    public void onCreate(){
    	
    	// Get the location manager
    	warnings = Singleton.getInstance().getWarnings();
	    locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
	    
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
	    Log.i("MyMaps","LocationSercive onCreate");
	    setUpLocationClientIfNeeded();
        mLocationClient.connect();
    }

	@Override
	public IBinder onBind(Intent intent) {
		//register intent aqui
		Log.i("LocationService","LocationSercive onBind");
		return mBinder;
	}
	
    public class LocalBinder extends Binder {
    	LocationServiceExplore getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocationServiceExplore.this;
        }
    }

	@Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
		Log.i("MyMap", "onConnectionFailed");
    }

	@Override
	public void onConnected(Bundle bundle) {
        //mConnectionStatus.setText(R.string.connected);
        //Log.i("MyMap", "Conectado");
        
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener

        location = mLocationClient.getLastLocation();
        
        /* Se centra la camara */
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        //mMap.animateCamera(cameraUpdate);
		
	}

	@Override
	public void onDisconnected() {
        //mConnectionStatus.setText(R.string.connected);		
	}

	//Metodo que avisa cuando la localizacion cambia. Es llamado por el sistema cuando detecta un cambio de localizacion.
	//Es el que provoca el error
	@Override
	public void onLocationChanged(Location location) {
    	String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Log.i("LocationService", msg);
        sendUpdate(location);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		return Service.START_NOT_STICKY;
		
	}
	
    public boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            return false;
        }
    }
    
    
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
    }
    
    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }
    
    public void registerListener(LocationServiceListener listener) {
    	//Log.i("MyMap","registerListener");
        mListeners.add(listener);
        //Actualiza al listener que se acaba de registrar
        if(location != null){
        mListeners.get(mListeners.size()-1).onUpdate(location);
        }
    }

    public void unregisterListener(LocationServiceListener listener) {
    	//Log.i("MyMap","unRegisterListener");
        mListeners.remove(listener);
    }

    private void sendUpdate(Location location) {
        for (int i=mListeners.size()-1; i>=0; i--) {
            mListeners.get(i).onUpdate(location);
        }
    }
    
    public Location getLastLocation(){
    	return location;
    }
    
//    public class MyBinder extends Binder {
//        LocationService getService() {
//            return LocationService.this;
//        }
//    }

}
