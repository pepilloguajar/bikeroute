package com.talentum.cyclistsightracks;

import java.text.DecimalFormat;

import com.talentum.cyclistsightracks.UpperFragmentSmall;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.utils.ExploreSingleton;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;



public class PopularityFragment extends Fragment implements OnClickListener, LocationServiceListener, ServiceConnection{
	
	
		private LocationService locationService;
		private Intent locationServiceIntent;
		boolean mBound = false;
		Location lastLocation;
		
		//Elementos visuales
		private TextView tv_popularity;
		private SeekBar sb_popularity;

		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.bottom_popularity_fragment, container,
					false);

			tv_popularity = (TextView) rootView.findViewById(R.id.tv_popularity);
			tv_popularity.setText(String.valueOf(ExploreSingleton.getInstance().getPopularity()));
			
			sb_popularity = (SeekBar) rootView.findViewById(R.id.sb_popularity);
			
			sb_popularity.setProgress(ExploreSingleton.getInstance().getPopularity());
			sb_popularity.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				int progressChanged = 0;
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					ExploreSingleton.getInstance().setPopularity(progressChanged);
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					progressChanged = progress;
					tv_popularity.setText(String.valueOf(progressChanged));	
					((ExploreActivity)getActivity()).updateMap();
				}
			});
			
			locationServiceIntent = new Intent(getActivity().getApplicationContext(), LocationService.class);
			getActivity().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
			

			return rootView;
		}
		
		@Override
		public void onStop() {
			super.onStop();
		}
		
		//
		@Override
		public void onClick(View v) {
			Log.i("PopularityFragment","click en popularity");
		}

	    @Override
	    public void onUpdate(Location location) {
    		lastLocation = location;
	    }

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) { 
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			locationService.registerListener(this);
			mBound = true;
			
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
			locationService = null;
			
		}

		
		private void changeView(){
			/*UpperFragmentSmall upperFragmentSmall = new UpperFragmentSmall();
			Bundle args = new Bundle();
			upperFragmentSmall.setArguments(args);
			//Log.i("MyMap","timeWhenStopped changeView: "+timeWhenStopped);

			FragmentManager fragmentManager = getFragmentManager();
			android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.upper_fragment_container,upperFragmentSmall);
			//fragmentTransaction.addToBackStack(null);
			fragmentTransaction.disallowAddToBackStack();
			//fragmentTransaction.hide(UpperFragment.this);
			//fragmentTransaction.disallowAddToBackStack();
			//fragmentTransaction.setTransition(fragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			fragmentTransaction.commit();

			//			FrameLayout frameLayout = (FrameLayout) getActivity().findViewById(R.id.upper_fragment_container);
			//			frameLayout.removeAllViews();
			//			frameLayout.addView(View.inflate(getActivity().getApplicationContext(),R.layout.upper_fragment_small, null));
			//			stopChronometer();
			//			chrono = (Chronometer) frameLayout.findViewById(R.id.chronometer1_small);
			//			startChronometer();*/
		}
}
