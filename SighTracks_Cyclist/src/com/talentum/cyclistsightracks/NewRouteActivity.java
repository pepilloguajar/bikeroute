package com.talentum.cyclistsightracks;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talentum.cyclistsightracks.LocationService;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.Segment;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.SegmentsSingleton;

import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;

public class NewRouteActivity extends ActionBarActivity implements LocationServiceListener, 
OnMyLocationButtonClickListener, ServiceConnection
{
	
	private GoogleMap mMap;
	private PolylineOptions polylineOptions;
	
	private SegmentsSingleton listSegments;
	private ArrayList<Segment> segments;
	private Segment currentSegment;
	private int lineRoute = Integer.parseInt(PropertyLoader.getInstance().getLineRoute());
	  
	private Location loc = null;
	private boolean recording;
    //protected PowerManager.WakeLock wakelock;
    
    private LocationService locationService;
    private Intent locationServiceIntent;
	
	private boolean mBound = false;

	private SupportMapFragment mapFragment;
	
	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //evitar que la pantalla se apague
        final PowerManager pm=(PowerManager)getSystemService(this.POWER_SERVICE);
        //this.wakelock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "etiqueta");
        //wakelock.acquire();
		getActionBar().hide();
		
		/** ---->>> Crea fichero GPS con cabeceras **/
		segments = new ArrayList<Segment>();
		
		recording = false;
		
	    locationServiceIntent = new Intent(getApplicationContext(), LocationService.class);
		bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		
		/** Empieza reconocimiento **/
	    polylineOptions = new PolylineOptions().width(lineRoute).color(0xff062344);
		Toast.makeText(getApplicationContext(), getResources().getString(R.string.recording), Toast.LENGTH_LONG).show();
	    setContentView(R.layout.activity_new_route_mov);
        if (findViewById(R.id.upper_fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            UpperFragment upperFragment = new UpperFragment();
            mapFragment = new SupportMapFragment();
            
            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            Bundle args = new Bundle();
            args.putBoolean("isNewRoute", true);
            args.putBoolean("isMovistar", false);
            upperFragment.setArguments(args);
            
            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.upper_fragment_container, upperFragment).commit();
            getSupportFragmentManager().beginTransaction()
            .add(R.id.map_fragment_container, mapFragment).commit();
        }
	}
	
	@SuppressLint("Wakelock")
	protected void onDestroy(){
        //.wakelock.release();
        super.onDestroy();
    }
	
    @Override
    protected void onResume() {
        super.onResume();
        //wakelock.acquire();
        currentSegment = new Segment();
        setUpMapIfNeeded();
        CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(16);
		mMap.animateCamera(cameraUpdate);
		if(!mBound){
			bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		}
    }
    
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        //this.wakelock.release();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if(currentSegment.getLocations().size()>1){
        	segments.add(currentSegment);
        }
        if(mBound){
    	    loc = null;
    	    locationService.unregisterListener(this);
    	    unbindService(this);
    	    mBound = false;
			
		}
    }

    private void setUpMapIfNeeded() {
    	// Do a null check to confirm that we have not already instantiated the map.
    	if (mMap == null) {
    		// Try to obtain the map from the SupportMapFragment.
    		mMap = mapFragment.getMap();
    		// Check if we were successful in obtaining the map.
    		if (mMap != null) {
    			mMap.setMyLocationEnabled(true);
    			mMap.setOnMyLocationButtonClickListener(this);
    		}
    	}
    }
    
	@Override
	public boolean onMyLocationButtonClick() {
		if(loc != null){
			LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
			mMap.animateCamera(cameraUpdate);
		}
		return false;
	}

	@Override
	public void onUpdate(Location location) {
		if(recording){
			if(loc != null){
				mMap.addPolyline(polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude())).width(lineRoute).color(0xff062344));
			}
			if(segments.isEmpty() && currentSegment.getLocations().isEmpty()){
				BitmapDescriptor icon_start = null;
				icon_start = BitmapDescriptorFactory.fromResource(R.drawable.market_user);
				onNewMarket(icon_start,new LatLng(location.getLatitude(), location.getLongitude()));
			}
			currentSegment.addLocation(location);
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
			mMap.animateCamera(cameraUpdate);
		}
        if(loc == null){
    		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng,16);
    		mMap.animateCamera(cameraUpdate);
		}
    	loc = new Location(location);

    	/** Guarda la posicion loc en el fichero GPS **/
	}
	
	private void onNewMarket(BitmapDescriptor icon, LatLng l){
		mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(getResources().getString(R.string.ini_sight))
		.icon(icon));		
	}
	
	public void pauseResumeRoute(){
		if(recording == true){//Se pausa
			Toast.makeText(this, getResources().getString(R.string.pause_sight), Toast.LENGTH_LONG).show();
			if(currentSegment.getLocations().size()>1){
	        	segments.add(currentSegment);
	        }
	        recording = false;
		}
		else{//Vuelve a grabar
			Toast.makeText(this, getResources().getString(R.string.recording), Toast.LENGTH_LONG).show();
			currentSegment = new Segment();
			recording = true;
		}
	}
	
	//Metodo llamado cuando se finaliza la ruta que guarda los segmentos en el singleton
	public void finishRoute(double currentDistance, long totalTime){		
		/** -------> Pone cabeceras finales al fichero y calcula medidas **/
		if(currentSegment.getLocations().size()>1){
			segments.add(currentSegment);
		}
		if(segments.size()>0){
			SegmentsSingleton.getInstance().setSegments(segments);
		}
		for(int i=0; i<segments.size();i++){
			for(int j=0; j<segments.get(i).getLocations().size(); j++){
			}
		}
		currentSegment=new Segment();
		Intent intent = new Intent(this, FinishRouteActivity.class);
		intent.putExtra("currentDistance", currentDistance);
		intent.putExtra("isNewRoute", true);
		intent.putExtra("isMovistar", false);
		intent.putExtra("idChallenge", "-1");
		intent.putExtra("totalTime", totalTime);
		finish();
		startActivity(intent);
	}

	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		LocalBinder binder = (LocalBinder) service;
		locationService = binder.getService();
		locationService.registerListener(this);
		mBound = true;
		
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		locationService = null;
		mBound = false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			DialogInterface.OnClickListener dialogClickListenerFinish = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						startActivity(new Intent(getApplicationContext(), RoutesListActivity.class));
						finish();
						break;

					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			AlertDialog.Builder builderDiscard = new AlertDialog.Builder(this);
			builderDiscard.setMessage(getResources().getString(R.string.finishWithoutSave))
					.setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerFinish)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerFinish).show();

		}
		return true;
	}

}



