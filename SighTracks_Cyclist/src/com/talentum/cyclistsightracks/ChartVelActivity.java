package com.talentum.cyclistsightracks;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewDataInterface;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.GraphViewStyle.GridStyle;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChartVelActivity extends ActionBarActivity  {
	
	private ArrayList<String> avgSpeed = new ArrayList<String>();
	private ArrayList<String> creationAct = new ArrayList<String>();
	private double avgmax = 0;
	private double avgmin = 999;
	
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_statistics);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        //Recoge valores del Draw
        
        String route = (String)getIntent().getExtras().get("route");
        getActionBar().setTitle(route);
        
        avgSpeed = getIntent().getStringArrayListExtra("avgSpeed");
        creationAct = getIntent().getStringArrayListExtra("creationAct");

        
        String patron = "d/M/yy";
        SimpleDateFormat formato_anio = new SimpleDateFormat(patron);
        String patron2 = "d/M";
        SimpleDateFormat formato = new SimpleDateFormat(patron2);
    	
        Date d = new Date();
    	GraphViewData gvel[];
		String label[];		
    			
        if(avgSpeed.size() == 1){
        	Date dtodo = new Date(creationAct.get(0));

    		avgmax = avgmin = Double.valueOf(avgSpeed.get(0));
            gvel = new GraphViewData[avgSpeed.size() + 1];
            label = new String[creationAct.size() + 1];
        	label[0] = "";
        	label[1] = formato_anio.format(dtodo);
        	gvel[0] = new GraphViewData((double)0, Double.valueOf(avgSpeed.get(0)));
        	gvel[1] = new GraphViewData((double)1, Double.valueOf(avgSpeed.get(0)));
        }
        else{
        	if(avgSpeed.size() < 5){
                gvel = new GraphViewData[avgSpeed.size()];
                label = new String[creationAct.size()];
        	}
        	else{
                gvel = new GraphViewData[5];
                label = new String[5];
        	}
        	
        	int j=0;
            for(int i=0; i<avgSpeed.size(); i++){
            	//max y min
            	if(Double.valueOf(avgSpeed.get(i))>avgmax)
            		avgmax = Double.valueOf(avgSpeed.get(i));
            	if(Double.valueOf(avgSpeed.get(i))<avgmin)
            		avgmin = Double.valueOf(avgSpeed.get(i));
            	
            	Date dtodo = new Date(creationAct.get(i));
            	if(i<5){
    	        	gvel[j] = new GraphViewData((double)j, Double.valueOf(avgSpeed.get(i)));
    	        	if(dtodo.getYear() < d.getYear())
    	        		label[j] = formato_anio.format(dtodo);
    	        	else
    	        		label[j] = formato.format(dtodo);
    	        	
    	        	j++;
            	}
            }
        }

        
       GraphViewSeriesStyle seriesStyle = new GraphViewSeriesStyle();
       seriesStyle.setValueDependentColor(new ValueDependentColor() {
		
		@Override
		public int get(GraphViewDataInterface arg0) {
			// the higher the more red
		    return Color.rgb(200, 50, 0);
		}
	});
          
    	
    	//GraphViewSeries velSeries = new GraphViewSeries(gvel);
    	GraphViewSeries velSeries = new GraphViewSeries("Velocidad",new GraphViewSeriesStyle(getResources().getColor(R.color.movistar_green), 3),gvel);
    	
    		GraphView graphView = new LineGraphView(
    		    this // context
    		    , getResources().getString(R.string.avg_vel) // heading
    		);
    		
    		graphView.addSeries(velSeries); // data
            graphView.setHorizontalLabels(label);
            
            //Ajuste de propiedades
            graphView.getGraphViewStyle().setGridColor(getResources().getColor(R.color.white));
            graphView.getGraphViewStyle().setHorizontalLabelsColor(getResources().getColor(R.color.white));
            graphView.getGraphViewStyle().setVerticalLabelsColor(getResources().getColor(R.color.white));
            graphView.getGraphViewStyle().setGridStyle(GridStyle.VERTICAL);
            graphView.setManualYAxisBounds(avgmax+2, avgmin-2);
            
            LinearLayout layout = (LinearLayout) findViewById(R.id.grafica);
    		layout.addView(graphView);
    		
    		
    		//cargo los datos de la actividad
    		
    		TextView avgMa =(TextView)this.findViewById(R.id.speedMax);
    		avgMa.setText(avgmax+" km/h");
    		
    		TextView avgMi =(TextView)this.findViewById(R.id.speedMin);
    		avgMi.setText(avgmin+" km/h");
    		
    		TextView nActivities =(TextView)this.findViewById(R.id.nActivities);
    		nActivities.setText(String.valueOf(avgSpeed.size()));
    }
    
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
    	switch (item.getItemId()) {
		    case android.R.id.home: // ID del boton
					finish();
	    }
	    return true;
    }
}
