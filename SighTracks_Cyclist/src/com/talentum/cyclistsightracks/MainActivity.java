/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.cyclistsightracks;


import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.File;

import com.amazonaws.tvmclient.AmazonClientManager;
import com.amazonaws.tvmclient.Response;
import com.talentum.cyclistsightracks.LocationService;
import com.talentum.cyclistsightracks.model.Profile;
import com.talentum.cyclistsightracks.sdb.Challenges;
import com.talentum.cyclistsightracks.sdb.MovRoutes;
import com.talentum.cyclistsightracks.sdb.Profiles;
import com.talentum.cyclistsightracks.sdb.UsersActivities;
import com.talentum.cyclistsightracks.sdb.UsersRoutes;
import com.talentum.cyclistsightracks.utils.ExploreSingleton;
import com.talentum.cyclistsightracks.utils.Singleton;
import com.talentum.cyclistsightracks.RoutesListActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AlertActivity {	
	private ProgressBar spinner;
	public static AmazonClientManager clientManager = null;

	Intent routesListActivityIntent;


	/**
	 * Check Test Connection Internet 
	 * @return
	 */
	private boolean checkOnlineState() {
		ConnectivityManager CManager =
				(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo NInfo = CManager.getActiveNetworkInfo();
		if (NInfo != null && NInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	protected void displayErrorConnection() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(getResources().getString(R.string.error_internet));
		confirm.setMessage(getResources().getString(R.string.error_desinternet));
		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		confirm.show().show();
	}	

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		setContentView(R.layout.main);
		spinner = (ProgressBar)findViewById(R.id.progressBar1);
		spinner.setVisibility(View.GONE);
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
		Intent locationServiceIntent = new Intent(getApplicationContext(), LocationService.class);
		startService(locationServiceIntent);
		//bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		routesListActivityIntent = new Intent(MainActivity.this, RoutesListActivity.class);	
		checkGooglePlayServices();
		if(checkOnlineState()){
			if (!MainActivity.clientManager.hasCredentials()) {
				this.displayCredentialsIssueAndExit();
				finish();
			} else if (!MainActivity.clientManager.isLoggedIn()) {
				startActivity(new Intent(MainActivity.this,LoginActivity.class));
				finish();
			} else {
//				new GetRoutes().execute();
				new LatchLoginTask().execute();
			}			
		}
		else{
			displayErrorConnection();
		}
		//		Intent newRouteMoveIntent = new Intent(getApplicationContext(), NewRouteMovActivity.class);
		//		startActivity(newRouteMoveIntent);
		//		finish();
	}

	/**
	 * Al reanudar
	 */
	/*protected void onResume() {
		super.onResume();
		if (!MainActivity.clientManager.isLoggedIn()) {
			Log.i("MainActivity", "NO esta LOGEADO RESUMMEEEEE----");
			startActivity(new Intent(MainActivity.this,LoginActivity.class));
		} else {
			Log.i("MainActivity", "CARGA RUTAS RESUMEEEE----");
			new GetRoutes().execute();
		}
	}*/
	

	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setMessage(getResources().getString(R.string.error_userpassword));
		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

	/**
	 * Muestra el error por fallo del Login
	 * @param response
	 */
	protected void displayErrorAndExit(Response response) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		if (response == null) {
			confirm.setTitle("Error Code Unkown");
			confirm.setMessage("Please review the README file.");
		} else {
			confirm.setTitle("Error Code [" + response.getResponseCode() + "]");
			confirm.setMessage(response.getResponseMessage()
					+ "\nPlease review the README file.");
		}

		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

//	private void checkLatchStatus(String userId){
//		WebView webView = (WebView) findViewById(R.id.webview);
//		webView.getSettings().setJavaScriptEnabled(true);
//		webView.loadUrl("http://latch-env.elasticbeanstalk.com/status?user="+userId);
//		//comprobamos que pagina tenemos cargada
//		webView.setWebViewClient(new WebViewClient() {
//			@Override
//		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//		        if(url.contains("statuson")){
//		        	return true;
//		        }
//		        return false;
//		    }
//		});
//	}
	private void checkLatchStatus(){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String username = prefs.getString("username", "Default Value if not found");
		WebView webView = (WebView) MainActivity.this.findViewById(R.id.webview);
		//Comprobamos que pagina tenemos cargada
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if(url.contains("statuson")){
					new GetRoutes().execute();
				}
				else{
					int duration = Toast.LENGTH_LONG;
					CharSequence text = getResources().getString(R.string.error_latch);
					Toast.makeText(getApplicationContext(), text, duration).show();
					MainActivity.clientManager.clearCredentials();	
					Singleton.getInstance().logout();
					startActivity(new Intent(MainActivity.this, LoginActivity.class));
					finish();
				}
				return false;
			}
		});
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("http://latch-env.elasticbeanstalk.com/status?user="+username);
	}
	
	/**
	 * Check Google Play Services
	 * @return
	 */
	private boolean checkGooglePlayServices() {
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS){
			return true;
		}
		else{
			try {
				GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), this, 1).show();
			} catch (Exception e) {
			}
			return false;
		}

	}
	
	/***************** TAREAS AS??NCRONAS *****************/
	/**
	 * Obtiene las Rutas y las almacena en RoutesSingleton (para poder estar accessibles desde cualquier sitio)
	 * @author escabia
	 *
	 */
	private class GetRoutes extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			spinner.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... arg0) {//Recupera todos los datos
			Singleton s = Singleton.getInstance(); 
			s.clean();

			//Se recupera el username
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			String username = prefs.getString("username", "Default Value if not found");

			//** Carga las rutas **/
			MovRoutes rmovlist = new MovRoutes();
			s.setListMovRoutes(rmovlist.getRoutes());

			UsersRoutes userlist = new UsersRoutes();
			s.setListUsersRoutes(userlist.getRoutes());
			int popmin = Integer.MAX_VALUE, popmax = Integer.MIN_VALUE;
			double dismin = Double.MAX_VALUE, dismax = Double.MIN_VALUE;

			for(int i=0; i<s.getListUsersRoutes().size(); i++){
				if(popmin > s.getListUsersRoutes().get(i).getPopularity())
					popmin = s.getListUsersRoutes().get(i).getPopularity();
				else if(popmax < s.getListUsersRoutes().get(i).getPopularity())
					popmax = s.getListUsersRoutes().get(i).getPopularity();

				if(dismin > s.getListUsersRoutes().get(i).getDistance())
					dismin = s.getListUsersRoutes().get(i).getDistance();
				else if(dismax < s.getListUsersRoutes().get(i).getDistance())
					dismax = s.getListUsersRoutes().get(i).getDistance();
			}

			for(int i=0; i<s.getListMovRoutes().size(); i++){
				if(popmin > s.getListMovRoutes().get(i).getPopularity())
					popmin = s.getListMovRoutes().get(i).getPopularity();
				else if(popmax < s.getListMovRoutes().get(i).getPopularity())
					popmax = s.getListMovRoutes().get(i).getPopularity();

				if(dismin > s.getListMovRoutes().get(i).getDistance())
					dismin = s.getListMovRoutes().get(i).getDistance();
				else if(dismax < s.getListMovRoutes().get(i).getDistance())
					dismax = s.getListMovRoutes().get(i).getDistance();
			}
			ExploreSingleton.getInstance().setMax_popularity(popmax);
			ExploreSingleton.getInstance().setMin_popularity(popmin);

			ExploreSingleton.getInstance().setMax_distance(dismax);
			ExploreSingleton.getInstance().setMin_distance(dismin);
			ExploreSingleton.getInstance().setMax_distance_rel(dismax);
			ExploreSingleton.getInstance().setMin_distance_rel(dismin);


			/** Carga actividad del usuario **/
			UsersActivities activitieslist = new UsersActivities();
			s.setAlllistUserActivities(activitieslist.getActivities(username));   

			/** Carga los retos del Usuario **/
			Challenges challengeslist = new Challenges();
			s.setChallenges(challengeslist.getChallenges(username));


			s.loadRoutes(username);
			s.loadRoutesChallenges();

			/** Carga el perfil del usuario **/
			Profiles profileslist = new Profiles();
			Profile pUser = profileslist.getProfile(username);
			s.setpUser(pUser);

//			//Comprobamos el status de la cuenta de latch, solo si el usuario esta pareado (latchId != 0)
//			if(!pUser.getLatchId().equalsIgnoreCase("0")){
//				//Si devuelve false, la cuenta esta bloqueada
//				if(!checkLatchStatus(pUser.getId())){
//					int duration = Toast.LENGTH_LONG;
//					CharSequence text = getResources().getString(R.string.error_latch);
//					Toast.makeText(getApplicationContext(), text, duration).show();
//					clientManager.clearCredentials();	
//					Singleton.getInstance().logout();
//					startActivity(new Intent(MainActivity.this, MainActivity.class));
//					finish();
//				}
//			}


			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			File folder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/");

			if(!folder.exists()){
				if (!folder.exists()) {
					folder.mkdir();
				}
			}
			File folder3gp = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/");
			if(!folder3gp.exists()){
				if (!folder3gp.exists()) {
					folder3gp.mkdir();
				}
			}
			File folderGpx = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/");
			if(!folderGpx.exists()){
				if (!folderGpx.exists()) {
					folderGpx.mkdir();
				}
			}
			//startActivity(new Intent(MainActivity.this, RoutesListActivity.class));
			if(spinner.isActivated())
				spinner.setActivated(false);
			spinner.destroyDrawingCache();
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			finish();
			startActivity(routesListActivityIntent);

			if(spinner.isActivated())
				spinner.setActivated(false);
			spinner.destroyDrawingCache();
			finish();
		}
	}
	
	   /***************** TAREAS AS??NCRONAS *****************/
//		private class LatchStatusTask extends AsyncTask<Void, Void, Void> {		
//		    @Override
//			protected Void doInBackground(Void... args) {
//				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//				String username = prefs.getString("username", "Default Value if not found");
//				WebView webView = (WebView) MainActivity.this.findViewById(R.id.webview);
//				//Comprobamos que pagina tenemos cargada
//				webView.setWebViewClient(new WebViewClient() {
//					@Override
//				    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//				        if(url.contains("statuson")){
//				        	new GetRoutes().execute();
//				        }
//				        else{
//				        	int duration = Toast.LENGTH_LONG;
//							CharSequence text = getResources().getString(R.string.error_latch);
//							Toast.makeText(getApplicationContext(), text, duration).show();
//							MainActivity.clientManager.clearCredentials();	
//							Singleton.getInstance().logout();
//							startActivity(new Intent(MainActivity.this, LoginActivity.class));
//							finish();
//				        }
//				        return false;
//				    }
//				});
//				webView.getSettings().setJavaScriptEnabled(true);
//				webView.loadUrl("http://latch-env.elasticbeanstalk.com/status?user="+username);
//				return null;
//			}
//		}
		
		private class LatchLoginTask extends AsyncTask<Void, Void, String> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				spinner.setVisibility(View.VISIBLE);
			}
			@Override
			protected String doInBackground(Void... voids) {
				//Se recupera el username
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				String username = prefs.getString("username", "Default Value if not found");
				Profiles profiles = new Profiles();
				return profiles.getLatchId(username);
			}

			@Override
			protected void onPostExecute(String latchId) {
				//Si el perfil no esta relleno, el latchid devuelto sera nulo
				if(latchId != null){
					//Si el latchid es distinto de cero, el usuario esta pareado con latch
					if(!latchId.equalsIgnoreCase("0")){
						//Comprobar el status de la cuenta
//						new LatchStatusTask().execute();
						checkLatchStatus();
					}
					//Si el usuario no esta pareado, haz la carga de rutas directamente
					else{
						new GetRoutes().execute();
					}
				}
			}
		}
}
