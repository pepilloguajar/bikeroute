package com.talentum.cyclistsightracks.sdb;

public class S3TaskResult {
	String errorMessage = null;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
