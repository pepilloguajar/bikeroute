package com.talentum.cyclistsightracks.sdb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;
import com.talentum.cyclistsightracks.MainActivity;
import com.talentum.cyclistsightracks.model.Profile;

public class Profiles {
	
	protected String nextToken;
	
	/** Atributos del dominio **/
	private static final String NAME_ATTRIBUTE = "Name";
	private static final String EMAIL_ATTRIBUTE = "Email";
	private static final String BIRHDAY_ATTRIBUTE = "Birthday";
	private static final String SEX_ATTRIBUTE = "Sex";
	private static final String WEIGHT_ATTRIBUTE = "Weight";
	private static final String HEIGHT_ATTRIBUTE = "Height";
	private static final String LATCH_ATTRIBUTE = "Accountid";
	
	
	/** Consultas sobre SimpleDB **/
	private static final String PROFILE_USER = "Select * from Profiles where itemName() = ";
	private static final String LATCH_QUERY = "Select Accountid from Profiles where itemName() = ";

	private static final String PROFILES_DOMAIN = "Profiles";

	
	
	/**
	 * Converts a item list to List of Profiles
	 * @param items
	 * @return
	 */
	protected List<Profile> convertItemListToActivityList( List<Item> items ) {
		List<Profile> profiles = new ArrayList<Profile>( items.size() );
		for ( Item item : items ) {
			profiles.add( this.convertItemToProfile( item ) );
		}
		return profiles;
	}
	
	/**
	 * Convert Item to Route
	 * @param item
	 * @return
	 */
	protected Profile convertItemToProfile( Item item ) {
		return new Profile( item.getName(), this.getNameForItem(item), this.getEmailForItem(item), this.getBirthdayForItem( item ), this.getSexForItem(item), this.getWeightForItem(item), this.getHeightForItem(item), this.getLatchIdForItem(item));
	}		

	/************ METHODS TO GET SPECIFIC ATTRIBUTES FOR ITEM ****************/
	protected String getNameForItem( Item item ) {
		return this.getStringValueForAttributeFromList( NAME_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getEmailForItem( Item item ) {
		return this.getStringValueForAttributeFromList( EMAIL_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getBirthdayForItem( Item item ) {
		return this.getStringValueForAttributeFromList( BIRHDAY_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected String getSexForItem( Item item ) {
		return this.getStringValueForAttributeFromList( SEX_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Double getWeightForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( WEIGHT_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Double getHeightForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( HEIGHT_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getLatchIdForItem( Item item ) {
		String latchId = this.getStringValueForAttributeFromList( LATCH_ATTRIBUTE, item.getAttributes() );
		//Controla que la funcion getStringValueForAttributeFromList puede devolver "" si el usuario no existe en la tabla, pero el latchId debe ser cero
		if(latchId.equalsIgnoreCase("")){
			return "0";
		}
		else{
			return latchId;
		}
	}
	
	
	
	/**
	 * Get String value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected String getStringValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return attribute.getValue();
			}
		}
		
		return "";		
	}
	
	/**
	 * Get Date value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	protected Date getSimpleDateValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		Date date = new Date();
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");	
				try {
					date = formatter.parse(attribute.getValue());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return date;		
	}	

	/**
	 * Get int value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected int getIntValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Integer.parseInt( attribute.getValue() );
			}
		}
		
		return 0;		
	}
	
	/**
	 * Get double value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected double getDoubleValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Double.parseDouble( attribute.getValue() );
			}
		}
		
		return 0;		
	}	

	/**
	 * Get boolean value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected boolean getBooleanValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				if(Integer.parseInt(attribute.getValue()) == 1)
					return true;
			}
		}
		return false;
	}	
	
	/**
	 * Get all activities
	 * @return
	 */
	public synchronized Profile getProfile(String user) {
		String query = PROFILE_USER + "'" + user + "'";
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = MainActivity.clientManager.sdb().select( selectRequest );
		this.nextToken = response.getNextToken();
		List<Profile> profiles = this.convertItemListToActivityList( response.getItems() );
		Collections.reverse(profiles);
		if(profiles.size() == 1)
			return profiles.get(0);	
		else{
			return null;
		}
	}
	
	public synchronized String getLatchId(String user) {
		String query = PROFILE_USER + "'" + user + "'";
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = MainActivity.clientManager.sdb().select( selectRequest );
		this.nextToken = response.getNextToken();
		List<Item> items = response.getItems();
		if(items.size() == 1)
			return this.getStringValueForAttributeFromList( LATCH_ATTRIBUTE, items.get(0).getAttributes() );
		else{
			return "0";
		}
	}
	
	
	public void alterProfile(Profile profile){
		ReplaceableAttribute nameAttribute = new ReplaceableAttribute( NAME_ATTRIBUTE, profile.getName(), Boolean.TRUE );
		ReplaceableAttribute emailAttribute = new ReplaceableAttribute( EMAIL_ATTRIBUTE, profile.getEmail(), Boolean.TRUE );
		ReplaceableAttribute birthdayAttribute = new ReplaceableAttribute( BIRHDAY_ATTRIBUTE, profile.getBirthday().toString(), Boolean.TRUE );
		ReplaceableAttribute sexAttribute = new ReplaceableAttribute( SEX_ATTRIBUTE, profile.getSex(), Boolean.TRUE );
		ReplaceableAttribute weightAttribute = new ReplaceableAttribute( WEIGHT_ATTRIBUTE, profile.getWeight().toString(), Boolean.TRUE );
		ReplaceableAttribute heightAttribute = new ReplaceableAttribute( HEIGHT_ATTRIBUTE, profile.getHeight().toString(), Boolean.TRUE );
		
				
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(6);
		Log.i("Profiles","Se modifica el perfil");
		attrs.add(nameAttribute);
		attrs.add(emailAttribute);
		attrs.add(birthdayAttribute);
		attrs.add( sexAttribute );
		attrs.add( weightAttribute );
		attrs.add( heightAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( PROFILES_DOMAIN, profile.getId(), attrs);		
		try {
			MainActivity.clientManager.sdb().putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
	
	
	   /*
  * Add a new route it to the Routes domain
  */
	public void addNewProfile( Profile profile ) {
		ReplaceableAttribute nameAttribute = new ReplaceableAttribute( NAME_ATTRIBUTE, profile.getName(), Boolean.FALSE );
		ReplaceableAttribute emailAttribute = new ReplaceableAttribute( EMAIL_ATTRIBUTE, profile.getEmail(), Boolean.FALSE );
		ReplaceableAttribute birthdayAttribute = new ReplaceableAttribute( BIRHDAY_ATTRIBUTE, profile.getBirthday().toString(), Boolean.FALSE );
		ReplaceableAttribute sexAttribute = new ReplaceableAttribute( SEX_ATTRIBUTE, profile.getSex(), Boolean.FALSE );
		ReplaceableAttribute weightAttribute = new ReplaceableAttribute( WEIGHT_ATTRIBUTE, profile.getWeight().toString(), Boolean.FALSE );
		ReplaceableAttribute heightAttribute = new ReplaceableAttribute( HEIGHT_ATTRIBUTE, profile.getHeight().toString(), Boolean.FALSE );
		ReplaceableAttribute latchAttribute = new ReplaceableAttribute( LATCH_ATTRIBUTE, profile.getLatchId().toString(), Boolean.FALSE );
		
				
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(6);
		attrs.add(nameAttribute);
		attrs.add(emailAttribute);
		attrs.add(birthdayAttribute);
		attrs.add( sexAttribute );
		attrs.add( weightAttribute );
		attrs.add( heightAttribute );
		attrs.add( latchAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( PROFILES_DOMAIN, profile.getId(), attrs);		
		try {
			MainActivity.clientManager.sdb().putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
	
}

