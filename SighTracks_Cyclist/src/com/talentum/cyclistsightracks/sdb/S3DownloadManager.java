package com.talentum.cyclistsightracks.sdb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.cyclistsightracks.DrawRouteActivity;
import com.talentum.cyclistsightracks.MainActivity;
import com.talentum.cyclistsightracks.R;
import com.talentum.cyclistsightracks.RoutesListActivity;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

public class S3DownloadManager extends AsyncTask<Route, Void, S3TaskResult>{
	
//	private class S3PutObjectTask extends AsyncTask<Integer, Void, S3TaskResult> {
		private ProgressDialog dialog;
		private File file;
		private int section;
		private Route route;
		private String bucket;
		private Context context;
		private int currentOrientation;
		public AmazonClientManager clientManager = null;
		private ArrayList<String> names3gp;
		
		public S3DownloadManager(Context context){
			this.context = context;
			currentOrientation = context.getResources().getConfiguration().orientation;
			clientManager = new AmazonClientManager(context.getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
			names3gp = new ArrayList<String>();
		}
		
		protected void onPreExecute() {
	        super.onPreExecute();
//	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
//	        	((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
//	        }
//	        else {
//	        	context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
//	        }
	        
	        
	        dialog = new ProgressDialog(context);
			dialog.setMessage(context
					.getString(R.string.loading_));
			dialog.setCancelable(false);
			dialog.show();
		}
		
		protected S3TaskResult doInBackground(Route... args) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (args == null || args.length != 1) {
				return null;
			}
			route = args[0];

			S3TaskResult result = new S3TaskResult();	
			try{
					bucket = PropertyLoader.getInstance().getBucketGPXteam();
				
				/************/
					 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+route.getId()+"/");	
			   ObjectListing objectListing;
			    do {
			    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
			    	for (S3ObjectSummary objectSummary : 
			    		objectListing.getObjectSummaries()) {
			    		InputStream in = clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
	    		    	writefile(in, route.getId(), objectSummary.getKey());
	    		    	names3gp.add(objectSummary.getKey());
	    		    	Log.i("MyMap","3gp encontrado: "+objectSummary.getKey());
	    		    	System.out.println(" - " + objectSummary.getKey() + "  " +
			                    "(size = " + objectSummary.getSize() + 
			    				")");
			    	}
			    } while (objectListing.isTruncated());
				/************/    
				//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
				clientManager.s3().getBucketAcl(bucket);
				GetObjectRequest gor = new GetObjectRequest(bucket,route.getId()+".gpx");
				S3Object object =clientManager.s3().getObject(gor);
				InputStream reader = new BufferedInputStream(object.getObjectContent());
				
				file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",route.getId());
				OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				int read = -1;

				while ( ( read = reader.read() ) != -1 ) {
				    writer.write(read);
				}
				writer.flush();
				writer.close();
				reader.close();
				
			} catch (Exception exception) {
				Log.i("MyMap","Error: "+exception.getMessage());
				result.setErrorMessage(exception.getMessage());
			}


			return result;
		}
		
		protected void onPostExecute(S3TaskResult result) {
			dialog.dismiss();
			if (result.getErrorMessage() != null) {
				Toast.makeText(context, context.getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
				((Activity)context).finish();
				Intent i = new Intent(context,MainActivity.class);
				context.startActivity(i);
			}
			else {
//				RoutesListActivity.this.finish();
				Intent i = new Intent(context,DrawRouteActivity.class);
				i.putExtra("route", route);
				i.putExtra("filegpx", file);
				i.putStringArrayListExtra("files3gp", names3gp);
				context.startActivity(i);
				
			}
			((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
//	}
		/**
	     * Escribe un inputstream con nombre name en la carpeta de la aplicaci??n
	     * @param inputStream
	     * @param name
	     */
	    private void writefile(InputStream inputStream, String routeId, String name){
	    	OutputStream outputStream = null;
	     
	    	try {
	    		// write the inputStream to a FileOutputStream
	    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
	            if(!routeFolder.exists()){
	 	           if (!routeFolder.exists()) {
	 	               routeFolder.mkdir();
	 	           }
	            }
	    		outputStream = 
	                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
	     
	    		int read = 0;
	    		byte[] bytes = new byte[1024];
	     
	    		while ((read = inputStream.read(bytes)) != -1) {
	    			outputStream.write(bytes, 0, read);
	    		}
	          
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	} finally {
	    		if (inputStream != null) {
	    			try {
	    				inputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	    		}
	    		if (outputStream != null) {
	    			try {
	    				// outputStream.flush();
	    				outputStream.close();
	    			} catch (IOException e) {
	    				e.printStackTrace();
	    			}
	     
	    		}
	    	}    	
	    }
}
