package com.talentum.cyclistsightracks.sdb;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.util.Log;

import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;
import com.talentum.cyclistsightracks.MainActivity;
import com.talentum.cyclistsightracks.model.UserAct;

public class UsersActivities {
	
	protected String nextToken;
	
	/** Atributos del dominio **/
	private static final String ROUTEID_ATTRIBUTE = "Routeid";
	private static final String USERID_ATTRIBUTE = "Userid";
	private static final String DURATION_ATTRIBUTE = "Duration";
	private static final String AVGSPEED_ATTRIBUTE = "AVGSpeed";
	private static final String PUNCTUATION_ATTRIBUTE = "Punctuation";
	private static final String CREATIONDATE_ATTRIBUTE = "CreationDate";
	
	/** Consultas sobre SimpleDB **/
	private static final String ACTIVITIESSUSER = "Select * from UsersActivities where Userid =  ";
	private static final String ACTIVITIES_DOMAIN = "UsersActivities";

	
	
	
	/**
	 * Converts a item list to List of Routes
	 * @param items
	 * @return
	 */
	protected List<UserAct> convertItemListToActivityList( List<Item> items ) {
		List<UserAct> activities = new ArrayList<UserAct>( items.size() );
		for ( Item item : items ) {
			UserAct ac = this.convertItemToActivity( item );
			activities.add(ac);
			Log.i("UsersActivities", ac.getCreation() + " routeid: "+ac.getRouteid());
		}
		return activities;
	}
	
	
	/**
	 * Convert Item to Route
	 * @param item
	 * @return
	 */
	protected UserAct convertItemToActivity( Item item ) {
		return new UserAct( item.getName(), this.getRouteidForItem(item), this.getUseridForItem( item ), this.getDurationForItem(item), this.getAVGSpeedForItem(item), this.getPunctuationForItem(item), this.getCreationDateForItem(item));
	}		

	/************ METHODS TO GET SPECIFIC ATTRIBUTES FOR ITEM ****************/
	protected String getRouteidForItem( Item item ) {
		return this.getStringValueForAttributeFromList( ROUTEID_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getUseridForItem( Item item ) {
		return this.getStringValueForAttributeFromList( USERID_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getDurationForItem( Item item ) {
		return this.getIntValueForAttributeFromList( DURATION_ATTRIBUTE, item.getAttributes() );
	}
		
	protected Double getAVGSpeedForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( AVGSPEED_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getPunctuationForItem( Item item ) {
		return this.getIntValueForAttributeFromList( PUNCTUATION_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getCreationDateForItem( Item item ) {
		return this.getStringValueForAttributeFromList( CREATIONDATE_ATTRIBUTE, item.getAttributes() );
	}		
		
	/**
	 * Get String value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected String getStringValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return attribute.getValue();
			}
		}
		
		return "";		
	}
	
	/**
	 * Get Date value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	protected Date getSimpleDateValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		Date date = new Date();
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");	
				try {
					date = formatter.parse(attribute.getValue());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return date;		
	}
	
	/**
	 * Get Date value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	@SuppressWarnings("deprecation")
	protected Date getDateValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		Date date = null;
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				date = new Date(attribute.getValue());
			}
		}
		return date;		
	}	

	/**
	 * Get int value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected int getIntValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Integer.parseInt( attribute.getValue() );
			}
		}
		
		return 0;		
	}
	
	/**
	 * Get double value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected double getDoubleValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Double.parseDouble( attribute.getValue() );
			}
		}
		
		return 0;		
	}	

	/**
	 * Get boolean value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected boolean getBooleanValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				if(Integer.parseInt(attribute.getValue()) == 1)
					return true;
			}
		}
		return false;
	}	
	
	/**
	 * Get all activities
	 * @return
	 */
	public synchronized List<UserAct> getActivities(String user) {
		String query = ACTIVITIESSUSER + "'" + user + "'";
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = MainActivity.clientManager.sdb().select( selectRequest );
		this.nextToken = response.getNextToken();
		List<UserAct> lactivities = this.convertItemListToActivityList( response.getItems() );
		return lactivities;	
	}
	

	@SuppressWarnings("unchecked")
	public synchronized List<UserAct> getNextPageOfScores(String user) {
		if ( this.nextToken == null ) {
			return Collections.EMPTY_LIST;
		}
		else {
			return this.getActivities(user);
		}
	}	
	
	public void addNewActivity( UserAct activity ) {
		DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
    	DecimalFormat df = new DecimalFormat("##.#", simbolos);
		
		ReplaceableAttribute routeidAttribute = new ReplaceableAttribute( ROUTEID_ATTRIBUTE, activity.getRouteid(), Boolean.FALSE );
		ReplaceableAttribute useridAttribute = new ReplaceableAttribute( USERID_ATTRIBUTE, activity.getUserid(), Boolean.FALSE );
		ReplaceableAttribute durationAttribute = new ReplaceableAttribute( DURATION_ATTRIBUTE, activity.getDuration().toString(), Boolean.FALSE );
		ReplaceableAttribute avgspeedAttribute = new ReplaceableAttribute( AVGSPEED_ATTRIBUTE, df.format(activity.getAvgspeed()), Boolean.FALSE );
		ReplaceableAttribute punctuationAttribute = new ReplaceableAttribute( PUNCTUATION_ATTRIBUTE, activity.getPunctuation().toString(), Boolean.FALSE );
		ReplaceableAttribute creationdateAttribute = new ReplaceableAttribute( CREATIONDATE_ATTRIBUTE, activity.getCreation().toString(), Boolean.FALSE );
		
				
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(12);
		attrs.add(routeidAttribute);
		attrs.add(useridAttribute);
		attrs.add( durationAttribute );
		attrs.add( avgspeedAttribute );
		attrs.add( punctuationAttribute );
		attrs.add( creationdateAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( ACTIVITIES_DOMAIN, activity.getId(), attrs);		
		try {
			MainActivity.clientManager.sdb().putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
}

