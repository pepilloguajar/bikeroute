   package com.talentum.cyclistsightracks.model;

import java.io.Serializable;
import java.util.Date;

import android.util.Log;

public class RouteAct extends Route implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String dateactivity;
	private Boolean deleteroute;
	private Boolean is_movistar;
	
	public RouteAct() {
        super();
    }
 
    public RouteAct(Route r, String dateactivity, Boolean delroute, Boolean is_movistar) {    	
        super(r.getId(), r.getUserid(), r.getName(), r.getCategory(), r.getPopularity(), r.getDistance(), r.getPositiveSlope(), 
        		r.getNegativeSlope(), r.getCreation(), r.getDifficulty(), r.getStartpointlat(), r.getStartpointlon(), 
        		r.getPublicroute(), r.getUrlfilegpx());
        this.dateactivity = dateactivity;
        this.deleteroute = delroute;
        this.is_movistar = is_movistar;
    }

	public String getDateactivity() {
		return dateactivity;
	}

	public void setDateactivity(String dateactivity) {
		this.dateactivity = dateactivity;
	}

	public Boolean getDeleteroute() {
		return deleteroute;
	}

	public void setDeleteroute(Boolean del) {
		this.deleteroute = del;
	}

	public Boolean getIs_movistar() {
		return is_movistar;
	}

	public void setIs_movistar(Boolean is_movistar) {
		this.is_movistar = is_movistar;
	}
}
