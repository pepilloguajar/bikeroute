   package com.talentum.cyclistsightracks.model;

import java.io.Serializable;

public class Profile implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;//es el userid
	private String name;
	private String email;
	private String birthday;
	private String sex;
	private Double weight;
	private Double height;
	private String latchId;
	
	public Profile() {
        super();
    }
 
    public Profile(String id, String name, String email, String birthday, String sex, Double weight, Double height, String latchId) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.birthday = birthday;
        this.sex = sex;
        this.weight = weight;
        this.height = height;
        this.latchId = latchId;
    }
    
    public Profile(String id){
    	super();
    	this.id = id;
    }
    
    public String getId(){
    	return id;
    }
    
    public void setId(String id){
    	this.id = id;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthday() {
		return birthday;
	}

	public String getSex() {
		return sex;
	}

	public Double getWeight() {
		return weight;
	}

	public Double getHeight() {
		return height;
	}
	
	public String getLatchId() {
		return latchId;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setlatchId(String latchId) {
		this.latchId = latchId;
	}
}
