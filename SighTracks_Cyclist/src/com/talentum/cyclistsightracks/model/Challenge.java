package com.talentum.cyclistsightracks.model;

import java.io.Serializable;

public class Challenge implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String idchallenge;
	private String iduser;
	private String routeid;
	private String timetodo;
	private int punctuation;
	private String lasttime;
	private RouteAct routeact;
	
	public Challenge(){
		
	}

	public Challenge(String idchallenge, String iduser, String routeid, String timetodo, int punctuation, String lasttime){
		super();
		this.idchallenge = idchallenge;
		this.iduser = iduser;
		this.routeid = routeid;
		this.timetodo = timetodo;
		this.punctuation = punctuation;
		this.lasttime = lasttime;
		routeact = new RouteAct();
	}

	
	
	public String getIdchallenge() {
		return idchallenge;
	}

	public void setIdchallenge(String idchallenge) {
		this.idchallenge = idchallenge;
	}

	/**
	 * @return the iduser
	 */
	public String getIduser() {
		return iduser;
	}

	/**
	 * @return the timetodo
	 */
	public String getTimetodo() {
		return timetodo;
	}

	/**
	 * @return the punctuation
	 */
	public int getPunctuation() {
		return punctuation;
	}

	/**
	 * @param iduser the iduser to set
	 */
	public void setIduser(String iduser) {
		this.iduser = iduser;
	}

	
	public String getRouteid() {
		return routeid;
	}

	public void setRouteid(String routeid) {
		this.routeid = routeid;
	}

	/**
	 * @param timetodo the timetodo to set
	 */
	public void setTimetodo(String timetodo) {
		this.timetodo = timetodo;
	}

	/**
	 * @param punctuation the punctuation to set
	 */
	public void setPunctuation(int punctuation) {
		this.punctuation = punctuation;
	}

	public String getLasttime() {
		return lasttime;
	}

	public void setLasttime(String lasttime) {
		this.lasttime = lasttime;
	}

	public RouteAct getRouteact() {
		return routeact;
	}

	public void setRouteact(RouteAct routeact) {
		this.routeact = routeact;
	}
}
