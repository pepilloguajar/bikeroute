package com.talentum.cyclistsightracks.model;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MarkerRoute {

	private Marker m;
	private boolean visibility;
	private BitmapDescriptor icon;
	private LatLng l;
	private double popularity;
	private Route route;
	
	public MarkerRoute(Marker m, boolean visibility, BitmapDescriptor icon, LatLng l, double popularity, Route route){
		this.m = m;
		this.visibility = visibility;
		this.icon = icon;
		this.l = l;
		this.popularity = popularity;
		this.route = route;
	}


	public Marker getM() {
		return m;
	}

	public boolean isVisibility() {
		return visibility;
	}

	public void setM(Marker m) {
		this.m = m;
	}

	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}


	public BitmapDescriptor getIcon() {
		return icon;
	}

	public LatLng getL() {
		return l;
	}

	public void setIcon(BitmapDescriptor icon) {
		this.icon = icon;
	}

	public void setL(LatLng l) {
		this.l = l;
	}

	public Route getRoute() {
		return route;
	}
	
	public double getPopularity() {
		return popularity;
	}


	public void setPopularity(double popularity) {
		this.popularity = popularity;
	}


	public void setRoute(Route route) {
		this.route = route;
	}
}

