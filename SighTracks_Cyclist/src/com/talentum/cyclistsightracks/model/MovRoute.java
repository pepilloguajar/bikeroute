   package com.talentum.cyclistsightracks.model;

import java.io.Serializable;
import java.util.Date;

import android.util.Log;

public class MovRoute extends Route implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String datetodo;
	
	public MovRoute() {
        super();
    }
 
    public MovRoute(String id, String userid,  String name, String category, Integer popularity, Double distance,
    		Integer positiveSlope, Integer negativeSlope, String creation, Integer difficulty, Double startpointlat, Double startpointlon,
    		Double endPointLat, Double endPointLon,Boolean publicroute, String urlfilegpx, String datetodo) {
    	
    	
        super(id, userid, name, category, popularity, distance, positiveSlope, negativeSlope, creation, difficulty, startpointlat, 
        		startpointlon, publicroute, urlfilegpx);
        this.datetodo = datetodo;
    }

	public String getDatetodo() {
		return datetodo;
	}

	public void setDatetodo(String datetodo) {
		this.datetodo = datetodo;
	}
	
	public Date calculateDatetodo(){
		String parts[] = this.datetodo.split("/");
		for(int i=0; i<parts.length; i++)
			Log.i("Route",parts[i]);
		
		return new Date(Integer.valueOf(parts[0]), Integer.valueOf(parts[1]), Integer.valueOf(parts[2]));
	}
}
