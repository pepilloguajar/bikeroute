   package com.talentum.cyclistsightracks.model;

import java.io.Serializable;
import java.util.Date;

public class UserRoute extends Route implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String userid;
	private Boolean deleteroute;
	
	public UserRoute() {
        super();
    }
 
    public UserRoute(String id, String userid, String name, String category, Integer popularity, Double distance,
    		Integer positiveSlope, Integer negativeSlope, String creation, Integer difficulty, Double startpointlat, Double startpointlon,
    		Boolean publicroute, String urlfilegpx, Boolean deleteroute) {
        super(id, userid, name, category, popularity, distance, positiveSlope, negativeSlope, creation, difficulty, startpointlat, 
        		startpointlon, publicroute, urlfilegpx);
        this.userid = userid;
        this.deleteroute = deleteroute;
    }
    
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Boolean getDeleteroute() {
		return deleteroute;
	}

	public void setDeleteroute(Boolean deleteroute) {
		this.deleteroute = deleteroute;
	}

}
