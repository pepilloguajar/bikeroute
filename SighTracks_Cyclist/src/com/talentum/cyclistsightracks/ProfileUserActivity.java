package com.talentum.cyclistsightracks;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.cyclistsightracks.model.Profile;
import com.talentum.cyclistsightracks.sdb.Profiles;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileUserActivity extends ActionBarActivity {

	public static AmazonClientManager clientManager = null;
	
	private Profile user;
	public EditText et_nick;
	public EditText et_email;
	public EditText et_birthday;
	
	public Spinner s_sex;
	public Spinner s_weight;
	public Spinner s_height;
	
	public TextView tv_totalroutes;
	public TextView tv_totaldistance;	
	public TextView tv_points;
	private boolean alterbirthday;

	
	@SuppressWarnings("deprecation")
	Date newbirthday = new Date();
	
	private void backprofile(){
		if(Singleton.getInstance().getpUser() != null){ //Si ya se han registrado los datos del usuario
			if(!et_nick.getText().toString().equals(Singleton.getInstance().getpUser().getName())
					|| !et_email.getText().toString().equals(Singleton.getInstance().getpUser().getEmail())
					|| alterbirthday || !getsex_pos(s_sex.getSelectedItemPosition()).equals(Singleton.getInstance().getpUser().getSex())
					|| !getweight_pos(s_weight.getSelectedItemPosition()).equals(Singleton.getInstance().getpUser().getWeight())
					|| !getheight_pos(s_height.getSelectedItemPosition()).equals(Singleton.getInstance().getpUser().getHeight())){
				
				alter_profile();
				new AlterProfile().execute(user);
			}
			else{
				Log.i("ProfileUserActivity","el usuario esta registrado y no hay cambios");
			}
		}
		else{
			Date now = new Date();
			if(et_nick.getText().toString() != "" && (now.getTime() - newbirthday.getTime()) > 31536000000L){//Si ha introducido algún nombre se sube a la BD
				add_profile();
				new AddProfile().execute(user);
			}
			else{
				Toast.makeText(this, getResources().getString(R.string.error_fillprofile), Toast.LENGTH_LONG).show();
			}
		}
	}
	
	/**
	 * Devuelve la posición del spinner dependiendo del sexo recogido
	 * @param sex
	 * @return
	 */
	private int getpos_sex(String sex){
		int pos = 0;
		
		if(sex.equals("H")){
			pos = 1;
		}
		else if(sex.equals("M")){
			pos = 2;
		}
		else if(sex.equals("O")){
			pos = 3;
		}
		else{
			pos = 0;
		}
		
		return pos;
	}
	
	/**
	 * Devuelve el sexo por la posición especificada
	 * @param pos posición
	 * @return
	 */
	private String getsex_pos(int pos){
		String sex = "";
		
		if(pos == 1){
			sex = "H";
		}
		else if(pos == 2){
			sex = "M";
		}
		else if(pos == 3){
			sex = "O";
		}
		else{
			sex = "";
		}
		
		return sex;
	}
	
	private int getpos_weitht(Double weight){
		int pos = 0;
		
		if(weight == 50){
			pos = 1;
		}
		else if(weight == 55){
			pos = 2;
		}
		else if(weight == 60){
			pos = 3;
		}
		else if(weight == 65){
			pos = 4;
		}
		else if(weight == 70){
			pos = 5;
		}
		else if(weight == 75){
			pos = 6;
		}
		else if(weight == 80){
			pos = 7;
		}
		else if(weight == 85){
			pos = 8;
		}
		else if(weight == 90){
			pos = 9;
		}
		else if(weight == 95){
			pos = 10;
		}
		else if(weight == 100){
			pos = 11;
		}
		else{
			pos = 0;
		}
		
		return pos;
	}
	
	private Double getweight_pos(int pos){
		Double weight = 0.0;
		
		if(pos == 1){
			weight = 50.0;
		}
		else if(pos == 2){
			weight = 55.0;
		}
		else if(pos == 3){
			weight = 60.0;
		}
		else if(pos == 4){
			weight = 65.0;
		}
		else if(pos == 5){
			weight = 70.0;
		}
		else if(pos == 6){
			weight = 75.0;
		}
		else if(pos == 7){
			weight = 80.0;
		}
		else if(pos == 8){
			weight = 85.0;
		}
		else if(pos == 9){
			weight = 90.0;
		}
		else if(pos == 10){
			weight = 95.0;
		}
		else if(pos == 11){
			weight = 100.0;
		}
		else{
			weight = 0.0;
		}
		
		return weight;
	}
	
	private int getpos_height(Double height){
		int pos = 0;
		
		if(height == 150){
			pos = 1;
		}
		else if(height == 155){
			pos = 2;
		}
		else if(height == 160){
			pos = 3;
		}
		else if(height == 165){
			pos = 4;
		}
		else if(height == 170){
			pos = 5;
		}
		else if(height == 175){
			pos = 6;
		}
		else if(height == 180){
			pos = 7;
		}
		else if(height == 185){
			pos = 8;
		}
		else if(height == 190){
			pos = 9;
		}
		else if(height == 195){
			pos = 10;
		}
		else if(height == 200){
			pos = 11;
		}
		else{
			pos = 0;
		}
		
		return pos;
	}
	
	private Double getheight_pos(int pos){
		Double height = 0.0;
		
		if(pos == 1){
			height = 150.0;
		}
		else if(pos == 2){
			height = 155.0;
		}
		else if(pos == 3){
			height = 160.0;
		}
		else if(pos == 4){
			height = 165.0;
		}
		else if(pos == 5){
			height = 170.0;
		}
		else if(pos == 6){
			height = 175.0;
		}
		else if(pos == 7){
			height = 180.0;
		}
		else if(pos == 8){
			height = 185.0;
		}
		else if(pos == 9){
			height = 190.0;
		}
		else if(pos == 10){
			height = 195.0;
		}
		else if(pos == 11){
			height = 200.0;
		}
		else{
			height = 0.0;
		}
		
		return height;
	}
	
	private void alter_profile(){
		//Profile a guardar
		user.setName(et_nick.getText().toString());
		user.setEmail(et_email.getText().toString());
		user.setSex(getsex_pos(s_sex.getSelectedItemPosition()));
		user.setWeight(getweight_pos(s_weight.getSelectedItemPosition()));
		user.setHeight(getheight_pos(s_height.getSelectedItemPosition()));
		
		user.setBirthday(String.valueOf(newbirthday.getTime()));
		
		//Singleton
		Singleton s = Singleton.getInstance(); 
		s.getpUser().setName(et_nick.getText().toString());
		s.getpUser().setEmail(et_email.getText().toString());		
		s.getpUser().setSex(getsex_pos(s_sex.getSelectedItemPosition()));
		s.getpUser().setWeight(getweight_pos(s_weight.getSelectedItemPosition()));
		s.getpUser().setHeight(getheight_pos(s_height.getSelectedItemPosition()));
		s.getpUser().setBirthday(String.valueOf(newbirthday.getTime()));
	}
	
	private void add_profile(){
		//Profile a guardar
		user.setName(et_nick.getText().toString());
		user.setEmail(et_email.getText().toString());
		user.setSex(getsex_pos(s_sex.getSelectedItemPosition()));
		user.setWeight(getweight_pos(s_weight.getSelectedItemPosition()));
		user.setHeight(getheight_pos(s_height.getSelectedItemPosition()));
		user.setBirthday(String.valueOf(newbirthday.getTime()));
		user.setlatchId("0");
		
		//Singleton
		Singleton s = Singleton.getInstance(); 
		s.setpUser(user);
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_user);
 	   
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
		
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = prefs.getString("username", "Default Value if not found");
		user = new Profile(username);
		alterbirthday = false;

		
		/** COMPONENTES DE LA VISTA **/
		// Carga los componentes de la Vista
		et_nick = (EditText) findViewById(R.id.et_nick);
		et_nick.setText("");
		//et_nick.setKeyListener(null);//Para que no se pueda editar
		
		
		et_email = (EditText) findViewById(R.id.et_email);
		et_email.setText("");
		
		
		// Carga Spinner
		s_sex = (Spinner) findViewById(R.id.s_sex);
		ArrayAdapter<CharSequence> adapter_sex = ArrayAdapter.createFromResource(this,
		        R.array.s_sex, R.layout.spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_sex.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		s_sex.setAdapter(adapter_sex);		
		
		s_weight = (Spinner) findViewById(R.id.s_weight);
		ArrayAdapter<CharSequence> adapter_weight = ArrayAdapter.createFromResource(this,
		        R.array.s_weight, R.layout.spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_weight.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		s_weight.setAdapter(adapter_weight);	
		
		s_height = (Spinner) findViewById(R.id.s_height);
		ArrayAdapter<CharSequence> adapter_height = ArrayAdapter.createFromResource(this,
		        R.array.s_height, R.layout.spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter_height.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		s_height.setAdapter(adapter_height);	
		
		tv_totalroutes = (TextView) findViewById(R.id.tv_totalroutes);
		tv_totaldistance = (TextView) findViewById(R.id.tv_totaldistance);
		tv_points = (TextView) findViewById(R.id.tv_points);
		
		
		/** CARGA DE DATOS **/
 	   //Se recupera el username
		ImageView maillot01 = (ImageView) findViewById(R.id.maillot01);
		ImageView maillot02 = (ImageView) findViewById(R.id.maillot02);
		ImageView maillot03 = (ImageView) findViewById(R.id.maillot03);
		ImageView maillot04 = (ImageView) findViewById(R.id.maillot04);
		ImageView maillot05 = (ImageView) findViewById(R.id.maillot05);
		
		Singleton s = Singleton.getInstance(); 
		if(s.getpUser() != null){
			
			//Nick
			et_nick.setText(s.getpUser().getName());
			
			
			//Email
			et_email.setText(s.getpUser().getEmail()); 
			
			//Fecha de nacimiento
		    String myFormat = "dd/MM/yy"; //In which you need put here
		    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
			
			et_birthday = (EditText) findViewById(R.id.et_birthday);
			newbirthday = new Date(Long.parseLong(s.getpUser().getBirthday()));
		    et_birthday.setText(sdf.format(newbirthday));	
			
			
			//Sexo
			s_sex.setSelection(getpos_sex(s.getpUser().getSex()));
			
			//Peso
			s_weight.setSelection(getpos_weitht(s.getpUser().getWeight()));
			
			//Altura
			s_height.setSelection(getpos_height(s.getpUser().getHeight()));
			
			//Rutas
			tv_totalroutes.setText(String.valueOf(s.getTotalroutes()));
			
			//Distancia
			tv_totaldistance.setText(String.valueOf(s.getTotaldistance().intValue()) + getResources().getString(R.string.km));
			
			//Categoria del ciclista
			int pun = s.getTotalmonthpunctuation();
			Log.i("monthpunctuation",String.valueOf(pun));

			if(pun <= Integer.parseInt(PropertyLoader.getInstance().getPoints_level_begin_top())){
				tv_points.setText(getResources().getString(R.string.beginners));
				maillot02.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot03.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot04.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot05.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			}else if(pun <= Integer.parseInt(PropertyLoader.getInstance().getPoints_level_fan_top())){
				tv_points.setText(getResources().getString(R.string.amateur));
				maillot03.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot04.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot05.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			}else if(pun <= Integer.parseInt(PropertyLoader.getInstance().getPoints_level_cyclist_top())){
				tv_points.setText(getResources().getString(R.string.cyclist));
				maillot04.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
				maillot05.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			}else if(pun <= Integer.parseInt(PropertyLoader.getInstance().getPoints_level_elite_top())){
				tv_points.setText(getResources().getString(R.string.elite));
				maillot05.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			}else{
				tv_points.setText(getResources().getString(R.string.elite_pro));
			}
//			tv_points.setText(String.valueOf(pun + " " + getResources().getString(R.string.pointsp));
		}
		else{
           
			//Rutas
			tv_totalroutes.setText("0");
			
			//Distancia
			tv_totaldistance.setText("0" + getResources().getString(R.string.km));
			
			//Puntos
			maillot01.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			maillot02.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			maillot03.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			maillot04.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			maillot05.setImageDrawable(getResources().getDrawable(R.drawable.maillot_gris));
			tv_points.setText("");
		}
	}
	
	Calendar myCalendar = Calendar.getInstance();
	
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
	        myCalendar.set(Calendar.YEAR, year);
	        myCalendar.set(Calendar.MONTH, monthOfYear);
	        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
	        updateLabel();			
		}
	};

	public void showDatePickerDialog(View v) {
		DatePickerDialog d;
		d = new DatePickerDialog(ProfileUserActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),myCalendar.get(Calendar.DAY_OF_MONTH));
		d.show();
	}
	
	private void updateLabel() {
		alterbirthday = true;
	    String myFormat = "dd/MM/yy"; //In which you need put here
	    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
		
		et_birthday = (EditText) findViewById(R.id.et_birthday);
		newbirthday = myCalendar.getTime();
	    et_birthday.setText(sdf.format(newbirthday));	
	}	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.profile_user, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home: // ID del boton
	    	backprofile();
	    	break;
	    	
	    case R.id.exit:
			displayLogoutSuccess();
			return true;
		
	    case R.id.pairing_latch:
//	    	Profiles profiles = new Profiles();
//	    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//	        String username = prefs.getString("username", "Default Value if not found");
//	    	String latchId = profiles.getLatchId(username);
	    	if(Singleton.getInstance().getpUser().getLatchId().equalsIgnoreCase("0")){
	    		startActivity(new Intent(ProfileUserActivity.this, PairingLatchActivity.class));
	    	}else{
	    		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	    			@Override
	    			public void onClick(DialogInterface dialog, int which) {
	    				switch (which){
	    				case DialogInterface.BUTTON_POSITIVE:
	    					startActivity(new Intent(ProfileUserActivity.this, UnpairingActivity.class));	    					
	    					break;

	    				case DialogInterface.BUTTON_NEGATIVE:
	    					break;
	    				}
	    			}
	    		};
	    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    		builder.setMessage(getResources().getString(R.string.unpairLatch)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
	    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
	    	}
	    	
			return true;
	    }
	    return super.onOptionsItemSelected(item);
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			backprofile();
	    }
	    return true;
	}
	
	private class AddProfile extends AsyncTask<Profile, Void, Void> {
		protected Void doInBackground(Profile... profiles) {
			Profiles pList = new Profiles();
			pList.addNewProfile(profiles[0]);

			return null;
		}

		protected void onPostExecute(Void result) {
			Toast.makeText(ProfileUserActivity.this, getResources().getString(R.string.profile_updated), Toast.LENGTH_LONG).show();
			ProfileUserActivity.this.finish();
		}
	}
	
	
	private class AlterProfile extends AsyncTask<Profile, Void, Void> {
		protected Void doInBackground(Profile... profiles) {
			Profiles pList = new Profiles();
			pList.alterProfile(profiles[0]);

			return null;
		}

		protected void onPostExecute(Void result) {
			ProfileUserActivity.this.finish();
		}
	}
	
	/**
	 * Dialog to confirm logout
	 */
	protected void displayLogoutSuccess() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
				case DialogInterface.BUTTON_POSITIVE:
					clientManager.clearCredentials();	
					Singleton.getInstance().logout();
					startActivity(new Intent(ProfileUserActivity.this, MainActivity.class));
					finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					break;
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.exit)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
	}
}

