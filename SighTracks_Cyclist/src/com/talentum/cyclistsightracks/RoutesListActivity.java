package com.talentum.cyclistsightracks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.cyclistsightracks.NewRouteActivity;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.sdb.UsersRoutes;
import com.talentum.cyclistsightracks.utils.Singleton;
import com.talentum.cyclistsightracks.MainActivity;
import com.talentum.cyclistsightracks.utils.PropertyLoader;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.NotificationManager;

public class RoutesListActivity extends ActionBarActivity implements
		ActionBar.TabListener {


	public static AmazonClientManager clientManager = null;
	SectionsPagerAdapter mSectionsPagerAdapter;
	

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	//RoutesSingleton listRoutes = RoutesSingleton.getInstance();
	
	private static List<RouteAct> myRoutes = new ArrayList<RouteAct>();
	private static List<RouteAct> myActivities = new ArrayList<RouteAct>();
	private ArrayList<String> names3gp;
	private int currentOrientation;

	private boolean gpsenabled(){
		boolean gpsenabled = false;
	    @SuppressWarnings("deprecation")
		String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	    if(provider.contains("gps")){ //if gps is disabled
	    	gpsenabled = true;
	    }
	    return gpsenabled;
	}
	
	/**
     * Escribe un inputstream con nombre name en la carpeta de la aplicaci??n
     * @param inputStream
     * @param name
     */
    private void writefile(InputStream inputStream, String routeId, String name){
    	OutputStream outputStream = null;
     
    	try {
    		// write the inputStream to a FileOutputStream
    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
            if(!routeFolder.exists()){
 	           if (!routeFolder.exists()) {
 	               routeFolder.mkdir();
 	           }
            }
    		outputStream = 
                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
          
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (inputStream != null) {
    			try {
    				inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}    	
    }
	

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentOrientation = getResources().getConfiguration().orientation; 
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));

		setContentView(R.layout.activity_route_list);
		getActionBar().setHomeButtonEnabled(true);
		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		//actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_background));
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
		.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		names3gp = new ArrayList<String>();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.

		getMenuInflater().inflate(R.menu.route_list, menu);
		
		int numchallenges = Singleton.getInstance().getChallenges().size();
		if(numchallenges == 0){
			menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.challenge));
		}
		else if(numchallenges <= 10){
			String draw = "challenge_"+numchallenges;
			menu.getItem(3).setIcon(getResources().getDrawable(getResources().getIdentifier(draw, "drawable", this.getPackageName())));
		}
		else{
			menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.challenge_lim));
		}
		
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		
		case android.R.id.home:
			Intent intent = new Intent(Intent.ACTION_VIEW);
	     	intent.setData(Uri.parse("https://s3-us-west-2.amazonaws.com/sightracks/help.html"));
	     	startActivity(intent);
			return true;
		case R.id.action_profile:
			startActivity(new Intent(RoutesListActivity.this, ProfileUserActivity.class));

			return true;
		case R.id.action_movistarTeam: 
			startActivity(new Intent(RoutesListActivity.this, MovistarActivity.class));

			return true;
		case R.id.action_challenges:
			startActivity(new Intent(RoutesListActivity.this, ChallengesActivity.class));

			return true;
		case R.id.action_explore:
			startActivity(new Intent(RoutesListActivity.this, ExploreActivity.class));
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).
			return PlaceholderFragment.newInstance(position + 1);

		}

		@Override
		public int getCount() {
			// Show 2 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);

			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements ServiceConnection, OnItemClickListener{
		
		RoutesAdapter adapter;
		private ListView listview;
		private Singleton listRoutes;
		private TextView isEmpty;
		private Location locationRoute;
		Location location;
		int positiondelete;

		//Datos miembro de la conexion con el servicio de localizacion
		private static LocationService locationService;
		boolean mBound = false;
		
		@Override
		public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			
		}
		
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		List<RouteAct> orderList(List<RouteAct> myLista){
			List<RouteAct> listOr = new ArrayList<RouteAct>();
			List<RouteAct> myList = new ArrayList<RouteAct>();
			myList.addAll(myLista);
			while(myList.size()>0){
				float min=Float.MAX_VALUE;
				int pos=0;
				float distanceTo;
				for(int i = 0; i<myList.size();i++){
					locationRoute = new Location("dummyprovider");
			    	locationRoute.setLatitude(myList.get(i).getStartpointlat());
			    	locationRoute.setLongitude(myList.get(i).getStartpointlon());
			    	distanceTo = location.distanceTo(locationRoute);
			    	if(distanceTo<min){
			    		min= distanceTo;
			    		pos = i;
			    	}
				}
				listOr.add(myList.get(pos));
				myList.remove(pos);
			}
			
			return listOr;
		}
		
		@SuppressWarnings("deprecation")
		List<RouteAct> orderListToDate(List<RouteAct> myLista){
			List<RouteAct> listOr = new ArrayList<RouteAct>();
			List<RouteAct> myList = new ArrayList<RouteAct>();
			myList.addAll(myLista);
			while(myList.size()>0){
				Date last = new Date(myList.get(0).getDateactivity());
//				Date last = formateador.parse(myList.get(0).getDateactivity());
				int pos=0;
				for(int i = 0; i<myList.size();i++){
					Date dtodo = new Date(myList.get(i).getDateactivity());
					if(dtodo.after(last)){
						last= new Date(myList.get(i).getDateactivity());
						pos = i;
					}
				
				}
				listOr.add(myList.get(pos));

				myList.remove(pos);
			}
			
			
			
			return listOr;
		}
		
		protected void displayDeleteRoute() {
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
			        	new DeleteRoute().execute(myRoutes.get(positiondelete));
			        	myRoutes.remove(positiondelete);
			        	adapter.notifyDataSetChanged();
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(getResources().getString(R.string.deleterec) + myRoutes.get(positiondelete).getName()+ "?").setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
		}
		
		private class DeleteRoute extends AsyncTask<RouteAct, Void, Void> {

			protected Void doInBackground(RouteAct... routes) {
				
				UsersRoutes userList = new UsersRoutes();
				userList.deleteRoute(routes[0]);

				return null;
			}

			protected void onPostExecute(Void result) {
			}
		}		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,			
				Bundle savedInstanceState) {

			View view = inflater.inflate(R.layout.fragment_list_routes, container, false);
			listview =(ListView)view.findViewById(R.id.list);
			isEmpty = (TextView)view.findViewById(R.id.isempty);
			isEmpty.setVisibility(TextView.INVISIBLE);
	    	listview.setOnItemClickListener(this);
	    	Intent locationServiceIntent = new Intent(getActivity(), LocationService.class);
			getActivity().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
	    	listview.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent,
						View view, int position, long id) {
					if(getArguments().getInt(ARG_SECTION_NUMBER) == 2){//En el caso de que sea Mis Rutas
						positiondelete = position;
						displayDeleteRoute();
					}
					return true;
				}
			});
			
			Button acceptButton = (Button) view.findViewById(R.id.buttonAceptar);
			acceptButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent newRouteMoveIntent = new Intent(getActivity().getApplicationContext(), NewRouteActivity.class);
					startActivity(newRouteMoveIntent);
					getActivity().finish();
				}
			});

			return view;
		}
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			mBound = true;
			location = locationService.getLastLocation();
			listRoutes = Singleton.getInstance();
			//Log.i("MyMap", "onServiceConnected PlaceholderFragment");
			for(int i=0; i<listRoutes.getMyRoutes().size();i++){
				if(listRoutes.getMyRoutes().get(i).getDeleteroute() == true){
					listRoutes.getMyRoutes().remove(i);
				}
			}
			
			for(int i=0; i<listRoutes.getLastRoutes().size();i++){
				if(listRoutes.getLastRoutes().get(i).getDeleteroute() == true){
					listRoutes.getLastRoutes().remove(i);
				}
			}
			
			myRoutes = Singleton.getInstance().getMyRoutes();
			myActivities = Singleton.getInstance().getLastRoutes();
			myRoutes = orderList(myRoutes);
			myActivities = orderListToDate(myActivities);
			Log.i("MSGPepis",Integer.toString(Singleton.getInstance().getLastRoutes().size()));

		

			if((getArguments().getInt(ARG_SECTION_NUMBER)) == 2){
				if(Singleton.getInstance().getMyRoutes().size() == 0){
					isEmpty.setVisibility(TextView.VISIBLE);
				}else{
					adapter = new RoutesAdapter(getActivity(), myRoutes,location);
					listview.setAdapter(adapter);
				}

			}else{
				if(Singleton.getInstance().getLastRoutes().size() == 0){
					isEmpty.setVisibility(TextView.VISIBLE);
					isEmpty.setText(getResources().getString(R.string.noActivities));
				}else{
					adapter = new RoutesAdapter(getActivity(), myActivities,location);
					listview.setAdapter(adapter);
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			locationService = null;
			mBound = false;
		}
		
		@Override
		public void onDestroyView(){
			if(mBound){
				//Log.i("MyMap","unbind RouteListActivity");
			getActivity().unbindService(this);
			mBound = false;
			}
	        super.onDestroy();
	    }
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			((RoutesListActivity)getActivity()).new S3PutObjectTask().execute(position,getArguments().getInt(ARG_SECTION_NUMBER));
		}
				
	}
		
	protected void displayGPSenabled() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            break;
		        }
		    }
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.error_gpsdis)).setPositiveButton(getResources().getString(R.string.accept), dialogClickListener)
		    .show();
	}
	
	public void loadRoute(View view){
		/** Comprobar la conexion con GPS **/
		if(gpsenabled()){
			finish();
			Intent intent = new Intent(this, NewRouteActivity.class);
			startActivity(intent);
		}
		else{
			displayGPSenabled();
			//Toast.makeText(getApplicationContext(), "Debe habilitar su GPS para realizar el reconocimeinto", Toast.LENGTH_LONG);
		}
	}
	

	
	private class S3PutObjectTask extends AsyncTask<Integer, Void, S3TaskResult> {
		ProgressDialog dialog;
		File file;
		int posicion;
		int section;
		List<RouteAct> listRoutes;
		String bucket;
		
		protected void onPreExecute() {
	        super.onPreExecute();
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
	        
	        
	        dialog = new ProgressDialog(RoutesListActivity.this);
			dialog.setMessage(RoutesListActivity.this
					.getString(R.string.loading_));
			dialog.setCancelable(false);
			dialog.show();
		}
		
		protected S3TaskResult doInBackground(Integer... args) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (args == null || args.length != 2) {
				return null;
			}
			posicion = args[0];
			section = args[1];
			
			if(section == 1){
				listRoutes = myActivities;
				
			}else{
				listRoutes = myRoutes;
				
			}

			S3TaskResult result = new S3TaskResult();	
			try{
				if(listRoutes.get(posicion).getIs_movistar()){
					bucket = PropertyLoader.getInstance().getBucketGPXteam();
				
				/************/
					 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+listRoutes.get(posicion).getId()+"/");	
			   ObjectListing objectListing;
			    do {
			    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
			    	for (S3ObjectSummary objectSummary : 
			    		objectListing.getObjectSummaries()) {
			    		InputStream in = clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
	    		    	writefile(in, listRoutes.get(posicion).getId(), objectSummary.getKey());
	    		    	names3gp.add(objectSummary.getKey());
			    	}
			    } while (objectListing.isTruncated());
				/************/    
				}
				else{
					bucket = PropertyLoader.getInstance().getBucketGPXusers();
				}
				//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
				clientManager.s3().getBucketAcl(bucket);
				GetObjectRequest gor = new GetObjectRequest(bucket,listRoutes.get(posicion).getId()+".gpx");
				S3Object object =clientManager.s3().getObject(gor);
				InputStream reader = new BufferedInputStream(object.getObjectContent());
				
				file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",listRoutes.get(posicion).getId());
				OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				int read = -1;

				while ( ( read = reader.read() ) != -1 ) {
				    writer.write(read);
				}
				writer.flush();
				writer.close();
				reader.close();
				
			} catch (Exception exception) {
				result.setErrorMessage(exception.getMessage());
			}


			return result;
		}
		
		protected void onPostExecute(S3TaskResult result) {
			dialog.dismiss();
			if (result.getErrorMessage() != null) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
				RoutesListActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
			}
			else {
//				RoutesListActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
				i.putExtra("route", listRoutes.get(posicion));
				i.putExtra("filegpx", file);
				i.putStringArrayListExtra("files3gp", names3gp);
				i.putExtra("activity", "RoutesListActivity");
				startActivity(i);
				
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}
}
