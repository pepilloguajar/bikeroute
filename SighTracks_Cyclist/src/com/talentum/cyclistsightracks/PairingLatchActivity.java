package com.talentum.cyclistsightracks;


import com.talentum.cyclistsightracks.utils.Singleton;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class PairingLatchActivity extends AlertActivity {
	private WebView webView;
	private String urlActual = "http://latch-env.elasticbeanstalk.com/pair.jsp";
	private boolean isLoad = false;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_latch);
 
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("http://latch-env.elasticbeanstalk.com/pair.jsp");
		webView.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			        isLoad = true;
			    }
			});

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	if(isLoad){
		    	urlActual = webView.getUrl();
		    	if(urlActual.equalsIgnoreCase("http://latch-env.elasticbeanstalk.com/pairlatch"))
		    		Singleton.getInstance().getpUser().setlatchId("123");
	    	}
	    	finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home: // ID del boton
	    	if(isLoad){
		    	urlActual = webView.getUrl();
		    	if(urlActual.equalsIgnoreCase("http://latch-env.elasticbeanstalk.com/pairlatch"))
		    		Singleton.getInstance().getpUser().setlatchId("123");
	    	}
	    	finish();
	    	break;
	    }
	    return super.onOptionsItemSelected(item);
	}
	

}
