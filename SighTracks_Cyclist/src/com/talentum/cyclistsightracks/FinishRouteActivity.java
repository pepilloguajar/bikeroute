package com.talentum.cyclistsightracks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.talentum.cyclistsightracks.io.file.exporter.FileSegmentExporter;
import com.talentum.cyclistsightracks.io.file.exporter.GpxSegmentWriter;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.UserAct;
import com.talentum.cyclistsightracks.model.UserRoute;
import com.talentum.cyclistsightracks.sdb.Challenges;
import com.talentum.cyclistsightracks.sdb.MovRoutes;
import com.talentum.cyclistsightracks.sdb.UsersActivities;
import com.talentum.cyclistsightracks.sdb.UsersRoutes;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.RoutesSingleton;
import com.talentum.cyclistsightracks.utils.SegmentsSingleton;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FinishRouteActivity extends ActionBarActivity implements OnClickListener {
	
	public static AmazonClientManager clientManager = null;
	
	private UserRoute userRoute;
	private Route route;
	private UserAct userActivity;
	
	private String idRoute;
	private String idActivity;
	private String idChallenge;
	private String userId;
	private Double totalDistance;
	private Date fecha;
	private double avgSpeed;
	private Double currentDistance;
	//Tiempo total de la ruta en milisegundos

	private long totalTime, totalTimeChallenge;
	private boolean isNewRoute = true;
	private boolean isChallenge = true;
	private boolean isMovistar = true;
	private String userName;
	private float distanceTotal=0;
	
    File file = new File(Environment.getExternalStorageDirectory(), "Android/data/com.talentum.cyclistsightracks/temp.gpx");
    
	/** 
	 * @brief Carga los datos estadisticos de descripcion de la ruta
	 */
	private void loadViewData(){		
		//Lineas de c??digo que calculan la distancia de la ruta y el desnivel positivo y negativo
		boolean firstIteration=true, changeSegment=false;
		DecimalFormat df = new DecimalFormat("##.#");
		df.setRoundingMode(RoundingMode.UP);
		
		Double dNeg=0.0, dPos=0.0;
		
		
		if(isNewRoute){
			Location loc_a=null, loc_b=null;
			SegmentsSingleton segments = SegmentsSingleton.getInstance();
			if(segments.getSegments() != null){
				for(int i=0; i<segments.getSegments().size(); i++){
					for(int j=0; j<segments.getSegments().get(i).getLocations().size() - 1; j++){
						if(!(i==0 && j==0)){// Este if est?? proque el primer tramo de puntos toma un valor an??malo
							if(firstIteration){
								if(changeSegment){
									loc_a = loc_b;
									loc_b = segments.getSegments().get(i).getLocations().get(j);
									distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
									changeSegment=false;
								}else{
									loc_a = segments.getSegments().get(i).getLocations().get(j);
									loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
									distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
								}
								firstIteration=false;
							}else{
								loc_a = loc_b;
								loc_b = segments.getSegments().get(i).getLocations().get(j + 1);
								distanceTotal = distanceTotal + loc_a.distanceTo(loc_b);
							}
							if(loc_a.getAltitude() != 0.0 && loc_b.getAltitude() != 0.0 ){
								if((loc_a.getAltitude() - loc_b.getAltitude())>0){
									dNeg = dNeg + (loc_a.getAltitude() - loc_b.getAltitude());
								}else{
									dPos = dPos + ((loc_a.getAltitude() - loc_b.getAltitude())*-1);
								}
							}
						}

						if(j==segments.getSegments().get(i).getLocations().size() - 2){
							changeSegment=true;
						}
					}
				}
			}
			Log.i("totalDistanceMetros", String.valueOf(distanceTotal));
			distanceTotal= distanceTotal/1000; 
			//Redondeo a dos decimales
		}
		else{
			distanceTotal = route.getDistance().floatValue();
			dNeg = route.getNegativeSlope().doubleValue(); 
			dPos = route.getPositiveSlope().doubleValue();
		}
		
		//Detecta las rutas muy cortas
//		if(distanceTotal <  Integer.parseInt(PropertyLoader.getInstance().getDistanceMin())){
////			startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
//			finish();
//			Toast.makeText(this, getResources().getString(R.string.toleast)+" "+PropertyLoader.getInstance().getDistanceMin()+" "+getResources().getString(R.string.meters), Toast.LENGTH_LONG).show();
//		}else if(SegmentsSingleton.getInstance().getSegments().size() > 0 ){
		if(SegmentsSingleton.getInstance().getSegments().size() > 0 ){
			Button discardroute = (Button) this.findViewById(R.id.discardroute);
			Button saveroute = (Button) this.findViewById(R.id.saveroute);
	   		discardroute.setOnClickListener(this);
	   		saveroute.setOnClickListener(this);
			
			EditText name = (EditText)this.findViewById(R.id.name);
			name.setText(userRoute.getName());
			
			
			TextView distance =(TextView)this.findViewById(R.id.distance);
			distance.setText(df.format(distanceTotal)+"km");
			userRoute.setDistance((double) distanceTotal);
			
			userRoute.setStartpointlat(SegmentsSingleton.getInstance().getSegments().get(0).getLocations().get(0).getLatitude());
			userRoute.setStartpointlon(SegmentsSingleton.getInstance().getSegments().get(0).getLocations().get(0).getLongitude());
			
			TextView positiveD =(TextView)this.findViewById(R.id.DPositive);
			positiveD.setText(dPos.intValue()+"m");
			userRoute.setPositiveSlope(dPos.intValue());
			
			TextView negativeD =(TextView)this.findViewById(R.id.DNegative);
			negativeD.setText(dNeg.intValue()+"m");
			userRoute.setNegativeSlope(dNeg.intValue());
			
			if(!isNewRoute){
				name.setEnabled(false);
			}
			//Coeficiente de dificultad de la ruta
			if((int)distanceTotal == 0){
				userRoute.setCategory("LL");
				//ico.setImageResource(R.drawable.llana_user);
			}else{
				double coefType = dPos/(int)distanceTotal;
				if(coefType<=Integer.parseInt(PropertyLoader.getInstance().getLlanaTop())){
		      	  	//ico.setImageResource(R.drawable.llana_user);
		      	  	userRoute.setCategory("LL");
		        }else if(coefType<=Integer.parseInt(PropertyLoader.getInstance().getMedMonTop())){
		      	  	//ico.setImageResource(R.drawable.mc_user);
					userRoute.setCategory("MC");
		        }else{
					userRoute.setCategory("HC");
		      	  	//ico.setImageResource(R.drawable.hc_user);
		        }
			}
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String username = prefs.getString("username", "Default Value if not found");
	        
			ImageView ico =(ImageView)this.findViewById(R.id.type);
	    	if(userRoute.getCategory().compareTo("LL")==0){
	    		if(userRoute.getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.llana_user);
	    		}else if(userRoute.getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.llana_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.llana_otherusers);
	    		}

	    	}else if(userRoute.getCategory().compareTo("MC")==0){
	    		if(userRoute.getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.mc_user);
	    		}else if(userRoute.getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.mc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.mc_otherusers);
	    		}
	    	}else{
	    		if(userRoute.getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.hc_user);
	    		}else if(userRoute.getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.hc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.hc_otherusers);
	    		}
	    	}
			
			
			//carga tiempo de la actividad
			long hor, min, seg;
			String minutos="", segundos="";
			hor = TimeUnit.MILLISECONDS.toHours(totalTime);
			min = TimeUnit.MILLISECONDS.toMinutes(totalTime) - (hor*60);
			seg = TimeUnit.MILLISECONDS.toSeconds(totalTime) - (hor*60*60) - (min*60);
			if(min<10){
				minutos = "0"+min;
			}else{
				minutos = Long.toString(min);
			}
			if(seg<10){
				segundos = "0"+seg;
			}else{
				segundos = Long.toString(seg);
			}
			

			TextView time = (TextView)this.findViewById(R.id.timeActivity);
			if(hor==0){
				time.setText(minutos+":"+segundos);
			}else{
				time.setText(hor+":"+minutos+":"+segundos);
			}
			
			avgSpeed = ((currentDistance/1000)/(totalTime/1000)) *3600;
			Log.i("totalDistance", String.valueOf(distanceTotal));
			Log.i("totalTime", String.valueOf(totalTime));
			Log.i("avgSpedd", String.valueOf(avgSpeed));
			

			//carga velocidad media
			TextView speedAvg = (TextView)this.findViewById(R.id.speedAvg);
			speedAvg.setText(df.format(avgSpeed)+"km/h");
		}
	}
	
    
	/**
	 * Borra un directorio 
	 * @param path
	 * @return
	 */
    public static boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          if (files == null) {
              return true;
          }
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return true;
      }
    
    /**
     * Calcula los puntos dependiendo de la distancia y el tipo de ruta
     * @return puntos
     */
    private double getPointsTypeDistance(){
    	double points = 0.0;
    	
    	if(userRoute.getCategory().equals("LL")){
        	points += totalDistance * Double.valueOf(PropertyLoader.getInstance().getPlain());
    	}
    	else if(userRoute.getCategory().equals("MC")){
        	points += totalDistance * Double.valueOf(PropertyLoader.getInstance().getMiddle_mountain());
    	}
    	else{
        	points += totalDistance * Double.valueOf(PropertyLoader.getInstance().getHigh_mountain());
    	}
    	Log.i("FinishRouteActivity","Points: " + points+"");
    	return points;
    }
    
    /**
     * Puntos de un reto
     * @return
     */
    private int pointsChallenge(){
    	double factor = Double.valueOf(PropertyLoader.getInstance().getChallenge()) * getPointsTypeDistance();
    	return (int)factor;
    }
    
    /**
     * Puntos de una actividad
     * @return
     */
    private int pointsActivity(){
    	double factor = Double.valueOf(PropertyLoader.getInstance().getActivity()) * getPointsTypeDistance();
    	return (int)factor;
    }
    
    /**
     * Puntos de una ruta
     * @return
     */
    private int pointsRoute(){
    	double factor = Double.valueOf(PropertyLoader.getInstance().getNew_route());
    	Log.i("FinishRouteActivity","Factor: " + factor+"");
    	factor *= getPointsTypeDistance();
    	
    	return (int)factor;
    }
    
    
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_finish);
		
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE)); //Carga cliente de amazón
		
		//Parámetros que recoge de la ruta
		totalDistance = getIntent().getExtras().getDouble("totalDistance");
		totalTime = getIntent().getExtras().getLong("totalTime");
		currentDistance = getIntent().getExtras().getDouble("currentDistance");
		
		
		isNewRoute = getIntent().getExtras().getBoolean("isNewRoute");
		isMovistar = getIntent().getExtras().getBoolean("isMovistar");
		
		idChallenge = getIntent().getExtras().getString("idChallenge");
		
		if(idChallenge.equals("-1")){
			isChallenge = false;
			totalTimeChallenge = 0;
		}
		else{
			isChallenge = true;
			totalTimeChallenge = getIntent().getExtras().getLong("totalTimeChallenge");
		}
		
		
		// Otros parámetros
		fecha = new Date();
		idActivity = "Activity"+String.valueOf(fecha.getTime());
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userId = prefs.getString("username", "Default Value if not found");
		
		
		int punctuation = 0; //De momento es cero
		
		// Si es nueva ruta
		if(isNewRoute){ 
			idRoute = "Route"+String.valueOf(fecha.getTime());
			userRoute = new UserRoute(idRoute, userId, "", "", 1, totalDistance, 0, 0, fecha.toGMTString(), 0, null, null, false, "",false);
			loadViewData();
			punctuation += pointsRoute();
		}
		else{
			route = (Route) getIntent().getExtras().get("route");
			idRoute = route.getId();
			
			userRoute = new UserRoute(idRoute, route.getUserid(), route.getName(), route.getCategory(),1, totalDistance, 0, 0, fecha.toGMTString(), 0, null, null, false, "",false);
			loadViewData();
			
			
			if(isChallenge && totalTime < totalTimeChallenge ){
				punctuation += pointsChallenge();
			}
			else if(isChallenge){
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.challenge_nopass), Toast.LENGTH_SHORT).show();
			}
			else{
				punctuation += pointsActivity();
			}
		}
		
		TextView points =(TextView)this.findViewById(R.id.tv_scoreAdd);
		points.setText(String.valueOf(punctuation));
		
		DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
		simbolos.setDecimalSeparator('.');
		DecimalFormat formateador = new DecimalFormat("##.#",simbolos);
				
		
		//Siempre se almacena la actividad asociada (Sea nueva ruta, ruta existente o ruta movistar)
		userActivity = new UserAct(idActivity, idRoute, userId, (int)totalTime, Double.parseDouble(formateador.format(avgSpeed)), punctuation, fecha.toGMTString());
		getActionBar().hide(); //desactiva actionBar
	}    
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
    			@Override
    			public void onClick(DialogInterface dialog, int which) {
    				switch (which){
    				case DialogInterface.BUTTON_POSITIVE:
    					finish();
    					Toast.makeText(getApplicationContext(), getResources().getString(R.string.route_des), Toast.LENGTH_SHORT).show();
//    					startActivity(new Intent(FinishRouteActivity.this, RoutesListActivity.class));
    					break;

    				case DialogInterface.BUTTON_NEGATIVE:
    					break;
    				}
    			}
    		};
    		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setMessage(getResources().getString(R.string.cancelMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
    		.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
	    }
	    return true;
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.discardroute:
			DialogInterface.OnClickListener dialogClickListenerDiscard = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						finish();
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.route_des), Toast.LENGTH_SHORT).show();
						startActivity(new Intent(FinishRouteActivity.this, MainActivity.class));
						break;

					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			AlertDialog.Builder builderDiscard = new AlertDialog.Builder(this);
			builderDiscard.setMessage(getResources().getString(R.string.cancelMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerDiscard)
			.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerDiscard).show();

			break;
			
		case R.id.saveroute:
			final EditText name = (EditText) this.findViewById(R.id.name);
			if(!name.getText().toString().isEmpty()){
				if(isNewRoute){ //Si es nueva ruta
					DialogInterface.OnClickListener dialogClickListenerBis = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which){
								case DialogInterface.BUTTON_POSITIVE:
									userRoute.setPublicroute(true);
									userRoute.setName(name.getText().toString());
									new S3PutObjectTask().execute(idRoute);
									
									break;
								case DialogInterface.BUTTON_NEGATIVE:
									userRoute.setPublicroute(false);
									userRoute.setName(name.getText().toString());
									
									new S3PutObjectTask().execute(idRoute);	
									break;
							}
						}
					};
					AlertDialog.Builder builderBis = new AlertDialog.Builder(this);
					builderBis.setMessage(getResources().getString(R.string.shareMyRoute)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerBis)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerBis).show();				
				}
				else{
					new S3PutObjectTask().execute(idRoute);	
				}
			}else{
				Toast.makeText(getApplicationContext(), "Debes indicar un nombre de ruta", Toast.LENGTH_SHORT).show();
			}
			break;
		}
	}
	
	@Override
	protected void onDestroy(){
		SegmentsSingleton.getInstance().clear();
		Singleton.getInstance().cleanRoute();
        super.onDestroy();
    }

	
	/** Nueva Ruta **/
	private class AddRoute extends AsyncTask<UserRoute, Void, Void> {
		protected Void doInBackground(UserRoute... routes) {
	        /** Recupera url para guardarla en la bd **/
			UsersRoutes routeList = new UsersRoutes();
			routeList.addNewRoute(routes[0]);
			RoutesSingleton listRoutes = RoutesSingleton.getInstance();
			listRoutes.addRoute(routes[0]);
			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
	
	/** Añade popularidad a ruta de usuario **/
	private class AddPopularity extends AsyncTask<UserRoute, Void, Void> {
		protected Void doInBackground(UserRoute... routes) {
	        /** Recupera url para guardarla en la bd **/
			UsersRoutes routeList = new UsersRoutes();
			routeList.addPopularity(routes[0]);
			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}	
	
	/** Añade popularidad a ruta de Movistar **/
	private class AddPopularityMovistar extends AsyncTask<Route, Void, Void> {
		protected Void doInBackground(Route... routes) {
	        /** Recupera url para guardarla en la bd **/
			MovRoutes routeList = new MovRoutes();
			routeList.addPopularity(routes[0].getId(), routes[0].getPopularity());
			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
	
	/** Nueva Actividad **/
	private class AddActivity extends AsyncTask<UserAct, Void, Void> {
		protected Void doInBackground(UserAct... activities) {
	        /** Recupera url para guardarla en la bd **/
			UsersActivities activityList = new UsersActivities();
			activityList.addNewActivity(activities[0]);			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
	
	
	private class DelChallenge extends AsyncTask<String, Void, Void>{
		protected Void doInBackground(String... challenges) {
	        /** Recupera url para guardarla en la bd **/
			Challenges listchallenges = new Challenges();
			listchallenges.deleteChallenge(challenges[0]);	
			Singleton s = Singleton.getInstance();
			s.delChallenge(challenges[0]);
			
			
			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
	
	
	// Display an Alert message for an error or failure.
	protected void displayAlert(String title, String message) {

		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(
				FinishRouteActivity.this.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
					}
				});
		confirm.show().show();
	}

	protected void displayErrorAlert(String title, String message) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(title);
		confirm.setMessage(message);

		confirm.setNegativeButton(
				FinishRouteActivity.this.getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						FinishRouteActivity.this.finish();
					}
				});

		confirm.show();
	}
	
	
	
	/** 
	 * 
	 * Se guarda la RUTA con el fichero asociado GPX
	 * 
	 */
	private class S3PutObjectTask extends AsyncTask<String, Void, S3TaskResult> {
		ProgressDialog dialog;

		protected void onPreExecute() {
			dialog = new ProgressDialog(FinishRouteActivity.this);
			dialog.setMessage(FinishRouteActivity.this
					.getString(R.string.saving));
			dialog.setCancelable(false);
			dialog.show();
		}

		protected S3TaskResult doInBackground(String... strings) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (strings == null || strings.length != 1) {
				return null;
			}
			S3TaskResult result = new S3TaskResult();
			if(isNewRoute){
				// Put the image data into S3.
				try {       
					FileOutputStream fileOutputStream = null;
			        FileSegmentExporter segmentExporter = new FileSegmentExporter(SegmentsSingleton.getInstance().getSegments(), 
			        		new GpxSegmentWriter(), userRoute.getName());

			        try {
			        	fileOutputStream = new FileOutputStream(file);
						segmentExporter.writeSegment(fileOutputStream);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
					
			        /** Fichero de la ruta **/
					PutObjectRequest filegpx = new PutObjectRequest(PropertyLoader.getInstance().getBucketGPXusers(), strings[0]+".gpx", file);
					clientManager.s3().putObject(filegpx);
					
					GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(
							PropertyLoader.getInstance().getBucketGPXusers(), "/"+strings[0]+".gpx");

					URL url = clientManager.s3().generatePresignedUrl(urlRequest);
					userRoute.setUrlfilegpx(url.toString());//Guarda la url que se ha asignado al fichero gpx
					deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/"));
				} catch (Exception exception) {

					result.setErrorMessage(exception.getMessage());
				}
			}

			return result;
		}

		protected void onPostExecute(S3TaskResult result) {
			
			Toast.makeText(getApplicationContext(), "Ruta guardada correctamente", Toast.LENGTH_LONG).show();
			dialog.dismiss();
			
			//Si es nueva ruta
			if(isNewRoute){
				new AddRoute().execute(userRoute);
			}
			else{//Se aumenta en 1 la popularidad de la ruta
				if(isMovistar){
					new AddPopularityMovistar().execute(route);
				}
				else{
					new AddPopularity().execute(userRoute);
				}
				
				//Retos
				if(isChallenge && totalTime < totalTimeChallenge ){ //Si es reto y se ha superado, se elimina
					new DelChallenge().execute(idChallenge);
				}
				
			}
			new AddActivity().execute(userActivity);
			
			
			
			
			if (result.getErrorMessage() != null) {
				displayErrorAlert(
						FinishRouteActivity.this
								.getString(R.string.loading_),
						result.getErrorMessage());
			}
			FinishRouteActivity.this.finish();
			startActivity(new Intent(FinishRouteActivity.this, MainActivity.class));
		}
	}
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}
}
