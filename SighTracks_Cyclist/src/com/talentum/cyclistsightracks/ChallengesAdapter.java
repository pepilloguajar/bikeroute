package com.talentum.cyclistsightracks;



import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Location;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.talentum.cyclistsightracks.model.Challenge;


import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.utils.Singleton;



public class ChallengesAdapter extends BaseAdapter

{
    private final Activity actividad;
    private final List<Challenge> lista;
    private final Location myLocation;
    
    private Location locationRoute;
//	private Singleton singleton;

    
    public ChallengesAdapter(Activity actividad, List<Challenge> lista,  Location myLocation) {
          super();
          this.actividad = actividad;
          this.lista = lista;
          this.myLocation = myLocation;
    }

    public View getView(int position, View convertView, 
                                     ViewGroup parent) {
    	
    	
	    	//Redondeo a dos decimales
	    	DecimalFormat df = new DecimalFormat("##");
	    	df.setRoundingMode(RoundingMode.UP);
	    	//con un decimal
	    	DecimalFormat df1 = new DecimalFormat("##.#");
	    	df1.setRoundingMode(RoundingMode.UP);
	    	
	    	LayoutInflater inflater = actividad.getLayoutInflater();
	    	View view = inflater.inflate(R.layout.elemento_lista, parent, false);
	                                                                                                                        
	    	TextView nameRoute =(TextView)view.findViewById(R.id.titulo);
	    	nameRoute.setText((lista.get(position).getRouteact().getName()));
	    	
	    	//Se recupera el username
	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(parent.getContext());
	        String username = prefs.getString("username", "Default Value if not found");
	        
	    	ImageView ico =(ImageView)view.findViewById(R.id.type);
	    	if(lista.get(position).getRouteact().getCategory().compareTo("LL")==0){
	    		if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.llana_user);
	    		}else if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.llana_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.llana_otherusers);
	    		}
	
	    	}else if(lista.get(position).getRouteact().getCategory().compareTo("MC")==0){
	    		if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.mc_user);
	    		}else if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.mc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.mc_otherusers);
	    		}
	    	}else{
	    		if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.hc_user);
	    		}else if(lista.get(position).getRouteact().getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.hc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.hc_otherusers);
	    		}
	    	}

	    		TextView kmAndPoints =(TextView)view.findViewById(R.id.km);
	    		kmAndPoints.setText(df.format(lista.get(position).getRouteact().getDistance()).toString()+"km - "+lista.get(position).getPunctuation()+" Puntos");

	    	locationRoute = new Location("dummyprovider");
	    	locationRoute.setLatitude(lista.get(position).getRouteact().getStartpointlat());
	    	locationRoute.setLongitude(lista.get(position).getRouteact().getStartpointlon());
	    	
	    	Log.i("pepilloMSG", lista.get(position).getRouteact().getStartpointlat().toString()+" a "+ lista.get(position).getRouteact().getStartpointlon().toString());
	    	
	    	float distTo = myLocation.distanceTo(locationRoute);
	    	Log.i("pepilloMSG", Float.toString(distTo));
	    	
	    	TextView distanceto =(TextView)view.findViewById(R.id.dateDetails);
	    	if(distTo/1000<10){
	    		distanceto.setText("A "+(df1.format(myLocation.distanceTo(locationRoute) / 1000))+"km");
	    	}else{
	    		distanceto.setText("A "+(df.format(myLocation.distanceTo(locationRoute) / 1000))+"km");
	    	}
	    	
    	
    	
    	return view;
    }

    public int getCount() {
          return lista.size();
    }

    public Object getItem(int arg0) {
          return lista.size();
    }

    public long getItemId(int position) {
          return position;
    }

       
    
}
