package com.talentum.cyclistsightracks;

import java.io.IOException;

import android.media.MediaPlayer;
import android.os.Environment;

public class RecorderManager extends MediaPlayer{
	    private final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();

	    private MediaPlayer   mPlayer = null;
	    
	    public RecorderManager(){
	    	mPlayer = new MediaPlayer();
	    	mPlayer.setOnCompletionListener(new OnCompletionListener() {
		        @Override
		        public void onCompletion(MediaPlayer mp) {
		        	mPlayer.stop();
		        	mPlayer.reset();
		        }
		        	
		        });
	    }

	    public void startPlaying(String mFileName) {
        	mPlayer.reset();
            try {
				mPlayer.setDataSource(mFileName);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            try {
				mPlayer.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            mPlayer.start();
	    }
}
