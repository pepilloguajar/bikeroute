package com.talentum.cyclistsightracks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.Challenge;
import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.sdb.Challenges;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class ChallengesActivity extends ListActivity implements ServiceConnection {
	
	public int positiontodelete;
	private Singleton singleton;
	private static LocationService locationService;
	private static boolean mBound = false;
	private Location locationRoute;
	private Location location;
	private ArrayList<String> names3gp;
	private List <Challenge> challenges;
	ChallengesAdapter adapter;	
	
	protected void displayDeleteRoute() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
					new DeleteRoute().execute(challenges.get(positiontodelete).getIdchallenge());
					
					challenges.remove(positiontodelete);
					Singleton.getInstance().setChallenges(challenges);
					adapter.notifyDataSetChanged();
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            break;
		        }
		    }
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.deletecha) + "?").setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
		    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenges);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Intent locationServiceIntent = new Intent(ChallengesActivity.this, LocationService.class);
		ChallengesActivity.this.bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		names3gp = new ArrayList<String>();
		singleton = Singleton.getInstance();
		challenges = singleton.getChallenges();
		
		
		
   		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent,
					View view, int position, long id) {
				//view.setAlpha(0);
				positiontodelete = position;
				Log.i("Posición ********", position+"");
				displayDeleteRoute();
				return true;
			}
		});
	}

	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		LocalBinder binder = (LocalBinder) service;
		locationService = binder.getService();
		location = locationService.getLastLocation();
		mBound = true;
		challenges = orderList(challenges);
		adapter = new ChallengesAdapter(ChallengesActivity.this, challenges, location);
		setListAdapter(adapter);
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		locationService = null;
		mBound = false;
		Log.i("MyMap","ChallengesActivity onServiceDisconnected");
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id){
		super.onListItemClick(listView, view, position, id);
		challenges = orderList(challenges);
		new S3PutObjectTask().execute(challenges.get(position).getRouteact());
	}
	
	private void writefile(InputStream inputStream, String routeId, String name){
    	OutputStream outputStream = null;
     
    	try {
    		// write the inputStream to a FileOutputStream
    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
            if(!routeFolder.exists()){
 	           if (!routeFolder.exists()) {
 	               routeFolder.mkdir();
 	           }
            }
    		outputStream = 
                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
          
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (inputStream != null) {
    			try {
    				inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
     
    		}
    	}    	
    }
	
	private List<Challenge> orderList(List<Challenge> myLista){
		List<Challenge> listOr = new ArrayList<Challenge>();
		List<Challenge> myList = new ArrayList<Challenge>();
		myList.addAll(myLista);
		while(myList.size()>0){
			float min=Float.MAX_VALUE;
			int pos=0;
			float distanceTo;
			for(int i = 0; i<myList.size();i++){
				locationRoute = new Location("dummyprovider");
				Log.i("MSGPepillo", myList.get(i).getRouteact().getStartpointlat().toString());
		    	locationRoute.setLatitude(myList.get(i).getRouteact().getStartpointlat());
		    	locationRoute.setLongitude(myList.get(i).getRouteact().getStartpointlon());
		    	distanceTo = location.distanceTo(locationRoute);
		    	if(distanceTo<min){
		    		min= distanceTo;
		    		pos = i;
		    	}
			}
			listOr.add(myList.get(pos));
			myList.remove(pos);
		}
		
		return listOr;
	}
	
	private class S3PutObjectTask extends AsyncTask<RouteAct, Void, S3TaskResult> {
		ProgressDialog dialog;
		File file;
		RouteAct route;
		String bucket;
		int currentOrientation = getResources().getConfiguration().orientation;
		
		protected void onPreExecute() {
	        super.onPreExecute();
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
	        
	        
	        dialog = new ProgressDialog(ChallengesActivity.this);
			dialog.setMessage(ChallengesActivity.this
					.getString(R.string.loading_));
			dialog.setCancelable(false);
			dialog.show();
		}
		
		protected S3TaskResult doInBackground(RouteAct... args) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (args == null || args.length != 1) {
				return null;
			}
			route = args[0];

			S3TaskResult result = new S3TaskResult();	
			try{
				if(route.getIs_movistar()){
					bucket = PropertyLoader.getInstance().getBucketGPXteam();
				/************/
					 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+route.getId()+"/");	
			   ObjectListing objectListing;
			    do {
			    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
			    	for (S3ObjectSummary objectSummary : 
			    		objectListing.getObjectSummaries()) {
			    		InputStream in = MainActivity.clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
	    		    	writefile(in, route.getId(), objectSummary.getKey());
	    		    	names3gp.add(objectSummary.getKey());
	    		    	System.out.println(" - " + objectSummary.getKey() + "  " +
			                    "(size = " + objectSummary.getSize() + 
			    				")");
			    	}
			    } while (objectListing.isTruncated());
				}
				else{
					bucket = PropertyLoader.getInstance().getBucketGPXusers();
				}					
				/************/   
				//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
				MainActivity.clientManager.s3().getBucketAcl(bucket);
				GetObjectRequest gor = new GetObjectRequest(bucket,route.getId()+".gpx");
				S3Object object = MainActivity.clientManager.s3().getObject(gor);
				InputStream reader = new BufferedInputStream(object.getObjectContent());
				
				file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",route.getId());
				OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				int read = -1;

				while ( ( read = reader.read() ) != -1 ) {
				    writer.write(read);
				}
				writer.flush();
				writer.close();
				reader.close();
				
				
			} catch (Exception exception) {
				Log.i("MyMap","Error: "+exception.getMessage());
				result.setErrorMessage(exception.getMessage());
			}


			return result;
		}
		
		protected void onPostExecute(S3TaskResult result) {
			dialog.dismiss();
			if (result.getErrorMessage() != null) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
				ChallengesActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
			}
			else {
				Intent i = new Intent(getApplicationContext(),DrawChallengeActivity.class);
				i.putExtra("route", route);
				i.putExtra("filegpx", file);
				i.putStringArrayListExtra("files3gp", names3gp);
				i.putExtra("activity", "MovistarActivity");
				startActivity(i);
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
	
	private class DeleteRoute extends AsyncTask<String, Void, Void> {

		protected Void doInBackground(String... challenges) {
			
			Challenges challengeslist = new Challenges();
			challengeslist.deleteChallenge(challenges[0]);

			return null;
		}

		protected void onPostExecute(Void result) {
		}
	}
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}

}
