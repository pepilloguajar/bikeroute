package com.talentum.cyclistsightracks;


import java.io.IOException;
import java.util.ArrayList;

import com.talentum.cyclistsightracks.utils.Singleton;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;

public class MusicService extends Service{
	

//	private MediaPlayer avisoRandom;
	//MediaPlayer avisoVoz;
    private Intent intent;
    
    private MediaPlayer mPlayer;
    private ArrayList<String> playerQeue;
    
    private final IBinder mBinder = new MusicBinder();
    
    private String routeId;

    @Override
    public void onCreate() {
    	mPlayer = new MediaPlayer();
    	mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
    	playerQeue = new ArrayList<String>();
    	mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
    		@Override
    		public void onPrepared(MediaPlayer mp) {
//    			Log.i("MyMap","MusicService start");
    			mPlayer.start();
    		}
    	});
    	mPlayer.setOnCompletionListener(new OnCompletionListener() {
    		@Override
    		public void onCompletion(MediaPlayer mp) {
//    			Log.i("MyMap","onCompletion");
    			playerQeue.remove(0);
    			mPlayer.stop();
    			mPlayer.reset();
    			if(!playerQeue.isEmpty()){
//    				Log.i("MyMap","playQeue: "+playerQeue.get(0));
    				play(playerQeue.get(0));
    			}
    			
    		}

    	});
    	mPlayer.reset();
    }
    
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		
		routeId = intent.getExtras().getString("routeId");
		return Service.START_NOT_STICKY;
		
	}
    public void addToQeue(String avisoVoz, boolean hasVoice) {
//    	Log.i("MyMap","addToQeue: "+avisoVoz);
    	Singleton singleton =  Singleton.getInstance();
    	singleton.setMusicActive(true);
    	String path;
//    	String avisoPre = intent.getExtras().get("avisoPre").toString();
    	//final String avisoVoz = intent.getExtras().get("avisoVoz").toString();
    	int random = (int) (Math.random()*3);


    	//Compruebo que hay aviso predefinido
    	if(hasVoice){
    		//idRoute = intent.getExtras().get("idroute").toString();
    		path = Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId+"/"+avisoVoz+".3gp";
    	}else{ //Si hay predefinido buscamos el aviso a reproducir y comprobamos si hay tambi??????n de voz
    		if( random >= 0 && random < 1 ){
    			playerQeue.add("android.resource://com.talentum.cyclistsightracks/raw/attention");
        	}else if(random >= 1 && random < 2){
        		playerQeue.add("android.resource://com.talentum.cyclistsightracks/raw/attentive");
        	}else{
        		playerQeue.add("android.resource://com.talentum.cyclistsightracks/raw/care");
        	}
    		path = "android.resource://com.talentum.cyclistsightracks/raw/"+avisoVoz;
    	}
    	playerQeue.add(path);
    	if(playerQeue.size() == 1 || (playerQeue.size() == 2 && !hasVoice)){
//    		Log.i("MyMap","playDirect: "+path);
    		play(path);
    	}
    }
    
    private void play(String path){
    		try {
    			if(path.contains("raw"))
    				mPlayer.setDataSource(this,Uri.parse(path));
    			else{
    				mPlayer.setDataSource(path);
    			}
    		} catch (IllegalArgumentException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (SecurityException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IllegalStateException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		try {
    			mPlayer.prepare();
    		} catch (IllegalStateException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    }

    @Override
    public void onDestroy() {
          mPlayer.release();
    }

    @Override
    public IBinder onBind(Intent intencion) {
    	return mBinder;
    }
    
    public class MusicBinder extends Binder {
    	MusicService getService() {
    		// Return this instance of LocalService so clients can call public methods
    		return MusicService.this;
    	}
    }

}
