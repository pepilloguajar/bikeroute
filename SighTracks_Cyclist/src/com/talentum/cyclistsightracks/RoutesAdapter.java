package com.talentum.cyclistsightracks;



import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Location;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.talentum.cyclistsightracks.model.Challenge;


import com.talentum.cyclistsightracks.model.RouteAct;
import com.talentum.cyclistsightracks.utils.Singleton;



public class RoutesAdapter extends BaseAdapter

{
    private final Activity actividad;
    private final List<RouteAct> lista;
    private final Location myLocation;
    
    private Location locationRoute;

    
    public RoutesAdapter(Activity actividad, List<RouteAct> lista,  Location myLocation) {
          super();
          this.actividad = actividad;
          this.lista = lista;
//          this.context = context;
          this.myLocation = myLocation;
    }


    public View getView(int position, View convertView, 
                                     ViewGroup parent) {
    	
    	
	    	//Redondeo a dos decimales
	    	DecimalFormat df = new DecimalFormat("##");
	    	df.setRoundingMode(RoundingMode.UP);
	    	//con un decimal
	    	DecimalFormat df1 = new DecimalFormat("##.#");
	    	df1.setRoundingMode(RoundingMode.UP);
	    	
	    	LayoutInflater inflater = actividad.getLayoutInflater();
	    	View view = inflater.inflate(R.layout.elemento_lista, parent, false);
	                                                                                                                        
	    	TextView nameRoute =(TextView)view.findViewById(R.id.titulo);
	    	nameRoute.setText((lista.get(position).getName()));
	    	
	    	//Se recupera el username
	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(parent.getContext());
	        String username = prefs.getString("username", "Default Value if not found");
	        
	    	ImageView ico =(ImageView)view.findViewById(R.id.type);
	    	if(lista.get(position).getCategory().compareTo("LL")==0){
	    		if(lista.get(position).getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.llana_user);
	    		}else if(lista.get(position).getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.llana_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.llana_otherusers);
	    		}
	
	    	}else if(lista.get(position).getCategory().compareTo("MC")==0){
	    		if(lista.get(position).getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.mc_user);
	    		}else if(lista.get(position).getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.mc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.mc_otherusers);
	    		}
	    	}else{
	    		if(lista.get(position).getUserid().equalsIgnoreCase(username)){
	    			ico.setImageResource(R.drawable.hc_user);
	    		}else if(lista.get(position).getUserid().equalsIgnoreCase("Movistar")){
	    			ico.setImageResource(R.drawable.hc_movistar);
	    		}else{
	    			ico.setImageResource(R.drawable.hc_otherusers);
	    		}
	    	}
	    	String patron = "dd/MM/yyyy";
	    	SimpleDateFormat formato = new SimpleDateFormat(patron);
	    	@SuppressWarnings("deprecation")
	    	Date dtodo = new Date(lista.get(position).getDateactivity());

	    	TextView dateAndKm =(TextView)view.findViewById(R.id.km);
	    	Log.i("RoutesAdapter", "----> "+lista.get(position).getDistance()+ " fecha: "+formato.format(dtodo).toString());
	    	dateAndKm.setText(formato.format(dtodo).toString()+" - "+df.format(lista.get(position).getDistance()).toString()+"km");
	    	locationRoute = new Location("dummyprovider");
	    	locationRoute.setLatitude(lista.get(position).getStartpointlat());
	    	locationRoute.setLongitude(lista.get(position).getStartpointlon());

	    	Log.i("pepilloMSG", lista.get(position).getStartpointlat().toString()+" a "+ lista.get(position).getStartpointlon().toString());

	    	float distTo = myLocation.distanceTo(locationRoute);
	    	Log.i("pepilloMSG", Float.toString(distTo));

	    	TextView distanceto =(TextView)view.findViewById(R.id.dateDetails);
	    	if(distTo/1000<10){
	    		distanceto.setText("A "+(df1.format(myLocation.distanceTo(locationRoute) / 1000))+"km");
	    	}else{
	    		distanceto.setText("A "+(df.format(myLocation.distanceTo(locationRoute) / 1000))+"km");
	    	}

    	
    	
    	return view;
    }

    public int getCount() {
          return lista.size();
    }

    public Object getItem(int arg0) {
          return lista.size();
    }

    public long getItemId(int position) {
          return position;
    }

       
    
}
