/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.cyclistsightracks;

import com.amazonaws.tvmclient.Response;
import com.talentum.cyclistsightracks.sdb.Profiles;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AlertActivity {

	protected Button okButton;
	protected EditText username;
	protected EditText password;
	protected TextView introText;
	protected TextView usernameHeader;
	protected TextView passwordHeader;
	protected ProgressDialog pDialog;
	
	private void saveUserinPreferences(String user){
	      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	      Editor ed = prefs.edit();
	      ed.putString("username", user);
	      ed.commit();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_menu);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
		username = (EditText) findViewById(R.id.login_username_input_field);
		password = (EditText) findViewById(R.id.login_password_input_field);

		okButton = (Button) findViewById(R.id.login_main_ok_button);

		wireButtons();
	}

	public void wireButtons() {
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Context context = getApplicationContext();
				int duration = Toast.LENGTH_SHORT;
				if(username.getText().toString().equals("")){
					CharSequence text = getResources().getString(R.string.usernameclear);
					Toast toast = Toast.makeText(context, text, duration);
					InputMethodManager imm = (InputMethodManager)getSystemService(
							Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
					toast.show();
				}
				else if(password.getText().toString().equals("")){
					CharSequence text = getResources().getString(R.string.passclear);
					Toast toast = Toast.makeText(context, text, duration);
					InputMethodManager imm = (InputMethodManager)getSystemService(
							Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(username.getWindowToken(), 0);
					toast.show();
				}
				else{
					new TVMLoginTask().execute();
				}
			}
		});

	}

	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(getResources().getString(R.string.error_credentials));
		confirm.setMessage(getResources().getString(R.string.error_descredentials));
		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//finish();
			}
		});
		confirm.show().show();
	}

	/**
	 * 
	 * @param response
	 */
	protected void displayErrorAndExit(Response response) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		if (response == null) {
			confirm.setTitle("Error Code Unkown");
			confirm.setMessage("Please review the README file.");
		} else {
			confirm.setTitle(getResources().getString(R.string.error_enter));
			confirm.setMessage(getResources().getString(R.string.error_desenter));
		}

		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//finish();
			}
		});
		confirm.show().show();
	}
	
	 public void gohelp(View view) {
     	Intent intent = new Intent(Intent.ACTION_VIEW);
     	intent.setData(Uri.parse("https://s3-us-west-2.amazonaws.com/sightracks/help.html"));
     	startActivity(intent);
	 }
	 
	 public void gotalentum(View view) {
     	Intent intent = new Intent(Intent.ACTION_VIEW);
     	intent.setData(Uri.parse("https://talentum.telefonica.com"));
     	startActivity(intent);
	 }	
	 
	 public void goregister(View view) {
     	Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
     	//intent.setData(Uri.parse("http://sightracks.elasticbeanstalk.com/register.jsp"));
     	startActivity(intent);
	 }	
	 
	 
	 private boolean checkLatchStatus(String latchId){
		 WebView webView = (WebView) findViewById(R.id.webview);
		 webView.getSettings().setJavaScriptEnabled(true);
		 webView.loadUrl("http://sightracks.elasticbeanstalk.com/checkLatchStatus/"+latchId);
		 //comprobamos que pagina tenemos cargada
		 return shouldOverrideUrlLoading(webView, webView.getUrl());
	 }
	 
	@Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if(url.contains("statuson")){
        	return true;
        }
        return false;
    }
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    // Check if the key event was the Back button and if there's history
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) {
	    	finish();
	        return true;
	    }
	    // If it wasn't the Back key or there's no web page history, bubble up to the default
	    // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	}
	 
   /***************** TAREAS AS??NCRONAS *****************/
	private class TVMLoginTask extends AsyncTask<Void, Void, Response> {
	
	    @Override
	    protected void onPreExecute() {
	        // TODO Auto-generated method stub
	        super.onPreExecute();
	     // TODO Auto-generated method stub
	        pDialog = new ProgressDialog(LoginActivity.this);
	        pDialog.setCancelable(true);
	        pDialog.show();
	    }
	    
		protected Response doInBackground(Void... voids) {
			try {
				return MainActivity.clientManager.login(username
						.getText().toString(), password.getText().toString());
				
			} catch (Throwable e) {
				setStackAndPost(e);
			}
			return null;
		}

		protected void onPostExecute(Response response) {
			if (response != null && response.getResponseCode() == 404) {
				pDialog.dismiss();
				LoginActivity.this.displayCredentialsIssueAndExit();
			} else if (response != null && response.getResponseCode() != 200) {
				pDialog.dismiss();
				LoginActivity.this.displayCredentialsIssueAndExit();
			} else {
				saveUserinPreferences(username.getText().toString());
				startActivity(new Intent(LoginActivity.this, MainActivity.class));
				finish();
				//Tarea asincrona para comprobar el status de la cuenta latch
//				new LatchLoginTask().execute();
			}
		}
	}
	
//	private class LatchLoginTask extends AsyncTask<Void, Void, Void> {
//		protected Void doInBackground(Void... voids) {
//			Profiles profiles = new Profiles();
//			String latchId = profiles.getLatchId(username.getText().toString());
//			//Si el perfil no esta relleno, el latchid devuelto sera nulo
//			if(latchId != null){
//				//Si el latchid es distinto de cero, el usuario esta pareado con latch
//				if(!latchId.equalsIgnoreCase("0")){
//					//Pasa al MainActivity, ahi se comprobara el status de la cuenta
//					saveUserinPreferences(username.getText().toString());
//					startActivity(new Intent(LoginActivity.this, MainActivity.class));
//					finish();
//				}
//			}
//			//Si el latchid es igual a cero, el usuario no esta pareado con latch. Hacer loggin directamente
//			return null;
//		}

//		protected void onPostExecute(Boolean isUnblocked) {
//			pDialog.dismiss();
//			if(isUnblocked){
//				saveUserinPreferences(username.getText().toString());
//				startActivity(new Intent(LoginActivity.this, MainActivity.class));
//				finish();
//			}
//			//Si la cuenta esta bloqueada, avisa al usuario y haz logout
//			else{
//				int duration = Toast.LENGTH_LONG;
//				CharSequence text = getResources().getString(R.string.error_latch);
//				Toast.makeText(getApplicationContext(), text, duration).show();
//				MainActivity.clientManager.clearCredentials();	
//				Singleton.getInstance().logout();
//				startActivity(new Intent(LoginActivity.this, LoginActivity.class));
//				finish();
//			}
//		}
//	}
	
//	private class LatchStatusTask extends AsyncTask<WebView, Void, Void> {
//	    @Override
//		protected Void doInBackground(WebView... args) {
//			String userId = username.getText().toString();
//			WebView webView = (WebView) findViewById(R.id.webview);
//			webView.getSettings().setJavaScriptEnabled(true);
//			webView.loadUrl("http://latch-env.elasticbeanstalk.com/status?user="+userId);
//			//Comprobamos que pagina tenemos cargada
//			webView.setWebViewClient(new WebViewClient() {
//				@Override
//			    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//			        if(url.contains("statuson")){
//			        	saveUserinPreferences(username.getText().toString());
//						startActivity(new Intent(LoginActivity.this, MainActivity.class));
//						finish();
//			        }
//			        else{
//			        	int duration = Toast.LENGTH_LONG;
//						CharSequence text = getResources().getString(R.string.error_latch);
//						Toast.makeText(getApplicationContext(), text, duration).show();
//						MainActivity.clientManager.clearCredentials();	
//						Singleton.getInstance().logout();
//						startActivity(new Intent(LoginActivity.this, LoginActivity.class));
//						finish();
//			        }
//			        return false;
//			    }
//			});
//			return null;
//		}
//	}
}
