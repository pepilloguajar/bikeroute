package com.talentum.cyclistsightracks;

import java.text.DecimalFormat;

import com.talentum.cyclistsightracks.UpperFragment;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.StartFinishRouteListener;
import com.talentum.cyclistsightracks.R;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;


public class UpperFragmentSmall extends Fragment implements OnClickListener, LocationServiceListener, ServiceConnection,
StartFinishRouteListener{
	
		private Chronometer chrono;
		//private Button startButton;
		private TextView speedTextView;
		private TextView currentDistanceTextView;
		private ImageView leftArrow;
		private boolean isRecording;
		private boolean hasStarted;
		//Almacena el tiempo cuando se pauso el cronometro para reanudarlo desde esa referencia
		private long timeWhenStopped;
		private LocationService locationService;
		private Intent locationServiceIntent;
		private boolean mBound = false;
		private double currentDistance;
		private double totalDistance;
		private Location lastLocation;
		
		private boolean isNewRoute;

		public UpperFragmentSmall() {
		}

		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.upper_fragment_small, container,
					false);
			timeWhenStopped = getArguments().getLong("stopTime");
			isRecording = getArguments().getBoolean("isRecording");
			hasStarted = getArguments().getBoolean("hasStarted");
			currentDistance = getArguments().getDouble("currentDistance");
			totalDistance = getArguments().getDouble("totalDistance");
			isNewRoute = getArguments().getBoolean("isNewRoute");
			if(!isNewRoute){
				((NewUserActivity) getActivity()).registerListener(this);
			}
			chrono = (Chronometer) rootView.findViewById(R.id.chronometer1_small);
			speedTextView = (TextView) rootView.findViewById(R.id.speed_small);
			currentDistanceTextView = (TextView) rootView.findViewById(R.id.total_distance_small);
			DecimalFormat df=new DecimalFormat("0.0");
			currentDistanceTextView.setText(df.format(currentDistance));
			leftArrow = (ImageView) rootView.findViewById(R.id.arrow_left);
			rootView.setOnClickListener(this);
			leftArrow.setOnClickListener(this);

			locationServiceIntent = new Intent(getActivity().getApplicationContext(), LocationService.class);
			getActivity().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
			if(timeWhenStopped != 0){
				chrono.setBase(SystemClock.elapsedRealtime() - timeWhenStopped);
			}
			if(isRecording && hasStarted){
				startChronometer();
			}
			
			
			return rootView;
		}
		
		@Override
		public void onStop() {
			if(mBound == true){
			getActivity().unbindService(this);
			locationService.unregisterListener(this);
			mBound = false;
			}
			super.onStop();
		}

		
		//
		@Override
		public void onClick(View v) {
				changeView();
		}
		

	    public void startChronometer() {
	    	chrono.setBase(SystemClock.elapsedRealtime() - timeWhenStopped);
	    	chrono.start();
	    }

	    public void stopChronometer() {
	    	timeWhenStopped =  SystemClock.elapsedRealtime() - chrono.getBase();
	    	chrono.stop();
	    }

	    @Override
	    public void onUpdate(Location location) {
	    	double speed = location.getSpeed();
	    	DecimalFormat df=new DecimalFormat("0.0");
	    	speedTextView.setText(df.format(speed/1000*3600));
	    	if(isRecording && hasStarted){
	    		addDistance(location);
	    		currentDistanceTextView.setText(df.format(currentDistance/1000));
	    		lastLocation = location;
	    	}
	    }

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) { 
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			locationService.registerListener(this);
			mBound = true;
			
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
			locationService = null;
			
		}
		
		private void addDistance(Location newLocation){
			if(lastLocation != null){
				currentDistance+=lastLocation.distanceTo(newLocation);

			}
		}
		
		private void changeView(){
			UpperFragment upperFragment = new UpperFragment();
			Bundle args = new Bundle();
			if(isRecording && hasStarted)
				stopChronometer();
			args.putLong("stopTime",timeWhenStopped);
			args.putBoolean("isRecording",isRecording);
			args.putBoolean("hasStarted",hasStarted);
			args.putDouble("currentDistance",currentDistance);
			args.putDouble("totalDistance",totalDistance);
			args.putBoolean("isNewRoute", isNewRoute);
			upperFragment.setArguments(args);
			if(!isNewRoute){
				((NewUserActivity) getActivity()).unregisterListener(this);
			}	

			FragmentManager fragmentManager = getFragmentManager();
			android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.upper_fragment_container,upperFragment);
			fragmentTransaction.disallowAddToBackStack();
			fragmentTransaction.commit();
		}
		
		private void finishRouteFragment(){
			if(!isNewRoute){
			//Desde el UpperFragmentSmall solo se puede finalizar si se llega al final de la ruta. Se controla si se ha hecho toda la distancia
			if(currentDistance > (totalDistance*0.8*1000)){
				stopChronometer();
				((NewUserActivity) getActivity()).finishRoute(currentDistance, timeWhenStopped);
				}
			}
		}

		@Override
		public void onStartRouteListener() {
			hasStarted = true;
			startChronometer();
		}

		@Override
		public void onFinishRouteListener() {
			finishRouteFragment();
		}
		
		@Override
		public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		}
}
