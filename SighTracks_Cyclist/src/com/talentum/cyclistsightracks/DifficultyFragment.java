package com.talentum.cyclistsightracks;

import com.talentum.cyclistsightracks.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.utils.ExploreSingleton;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;



public class DifficultyFragment extends Fragment implements OnClickListener, LocationServiceListener, ServiceConnection{
	
		
		private LocationService locationService;
		private Intent locationServiceIntent;
		boolean mBound = false;
		Location lastLocation;
		
		// Tipo de Ruta
		private ImageButton im_hc;
		private ImageButton im_mc;
		private ImageButton im_ll;
		
		private TextView tv_mindistance;
		private TextView tv_maxdistance;
		
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.bottom_difficulty_fragment, container,
					false);

	        	        
			locationServiceIntent = new Intent(getActivity().getApplicationContext(), LocationService.class);
			getActivity().bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
			
			im_ll = (ImageButton)rootView.findViewById(R.id.ib_ll);
			
			//Inicia logo
			if(ExploreSingleton.getInstance().isActivate_ll())
				 im_ll.setImageDrawable(getResources().getDrawable(R.drawable.ll_activate));
			else
				 im_ll.setImageDrawable(getResources().getDrawable(R.drawable.ll));
				
			im_ll.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					 if(ExploreSingleton.getInstance().isActivate_ll()){
						 ExploreSingleton.getInstance().setActivate_ll(false);
						 im_ll.setImageDrawable(getResources().getDrawable(R.drawable.ll));
						 ((ExploreActivity)getActivity()).updateMap();
					 }
					 else{
						 ExploreSingleton.getInstance().setActivate_ll(true);
						 im_ll.setImageDrawable(getResources().getDrawable(R.drawable.ll_activate));
						 ((ExploreActivity)getActivity()).updateMap();
					 }	
				}
			});			
			
			im_mc = (ImageButton)rootView.findViewById(R.id.ib_mc);
			//Inicia logo
			if(ExploreSingleton.getInstance().isActivate_mc())
				 im_mc.setImageDrawable(getResources().getDrawable(R.drawable.mc_activate));
			else
				 im_mc.setImageDrawable(getResources().getDrawable(R.drawable.mc));
			
			im_mc.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					 if(ExploreSingleton.getInstance().isActivate_mc()){
						 ExploreSingleton.getInstance().setActivate_mc(false);
						 im_mc.setImageDrawable(getResources().getDrawable(R.drawable.mc));
						 ((ExploreActivity)getActivity()).updateMap();
					 }
					 else{
						 ExploreSingleton.getInstance().setActivate_mc(true);
						 im_mc.setImageDrawable(getResources().getDrawable(R.drawable.mc_activate));
						 ((ExploreActivity)getActivity()).updateMap();
					 }	
				}
			});
			
			im_hc = (ImageButton)rootView.findViewById(R.id.ib_hc);
			//Inicia logo
			if(ExploreSingleton.getInstance().isActivate_hc())
				 im_hc.setImageDrawable(getResources().getDrawable(R.drawable.hc_activate));
			else
				 im_hc.setImageDrawable(getResources().getDrawable(R.drawable.hc));
			
			im_hc.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					 if(ExploreSingleton.getInstance().isActivate_hc()){
						 ExploreSingleton.getInstance().setActivate_hc(false);
						 im_hc.setImageDrawable(getResources().getDrawable(R.drawable.hc));
						 ((ExploreActivity)getActivity()).updateMap();
					 }
					 else{
						 ExploreSingleton.getInstance().setActivate_hc(true);
						 im_hc.setImageDrawable(getResources().getDrawable(R.drawable.hc_activate));
						 ((ExploreActivity)getActivity()).updateMap();

					 }	
				}
			});		
			
			tv_mindistance = (TextView) rootView.findViewById(R.id.tv_mindistance);
			tv_maxdistance = (TextView) rootView.findViewById(R.id.tv_maxdistance);
			
			tv_mindistance.setText(String.valueOf((int)ExploreSingleton.getInstance().getMin_distance_rel()) + "km");
			tv_maxdistance.setText(String.valueOf((int)ExploreSingleton.getInstance().getMax_distance_rel() + 1) + "km");
			
			//Distancia
			// create RangeSeekBar as Integer range between 20 and 75
			RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(Integer.valueOf((int)ExploreSingleton.getInstance().getMin_distance()), Integer.valueOf((int)ExploreSingleton.getInstance().getMax_distance()), rootView.getContext());
			
			seekBar.setSelectedMinValue(Integer.valueOf((int)ExploreSingleton.getInstance().getMin_distance_rel()));
			seekBar.setSelectedMaxValue(Integer.valueOf((int)ExploreSingleton.getInstance().getMax_distance_rel()));
			
			seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
		        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
	                // handle changed range values
	        		ExploreSingleton.getInstance().setMin_distance_rel(minValue);
	        		ExploreSingleton.getInstance().setMax_distance_rel(maxValue);
	    			tv_mindistance.setText(String.valueOf(minValue) + "km");
	    			tv_maxdistance.setText(String.valueOf(maxValue) + "km");
					((ExploreActivity)getActivity()).updateMap();
		        }
			});
			
			
			

			// add RangeSeekBar to pre-defined layout
			ViewGroup layout = (ViewGroup) rootView.findViewById(R.id.seekdouble);
			layout.addView(seekBar);

			return rootView;
		}
		
		@Override
		public void onStop() {
			super.onStop();
		}
		
		//
		@Override
		public void onClick(View v) {
			Log.i("DifficultyFragment","onclick en Difficult");

		}

	    @Override
	    public void onUpdate(Location location) {
    		lastLocation = location;
	    }

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) { 
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			locationService.registerListener(this);
			mBound = true;
			
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBound = false;
			locationService = null;
			
		}

				
 		
}
