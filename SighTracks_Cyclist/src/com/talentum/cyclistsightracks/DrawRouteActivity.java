package com.talentum.cyclistsightracks;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.tvmclient.AmazonClientManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.UserAct;
import com.talentum.cyclistsightracks.utils.ExploreSingleton;
import com.talentum.cyclistsightracks.utils.GpxParser;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
//import com.talentum.cyclistsightracks.utils.SegmentsSingleton;
import com.talentum.cyclistsightracks.utils.Singleton;


public class DrawRouteActivity extends ActionBarActivity {

	private GoogleMap mMap;
	private int lineRoute;

	private Route route;
	private ArrayList<String> files3gp;
	private File fileGpx;
	private List<UserAct> activitiesThisRoute = new ArrayList<UserAct>();
	private ArrayList<String> avgSpeed = new ArrayList<String>();
	private ArrayList<String> creationAct = new ArrayList<String>();
	SupportMapFragment mapFragment;
	
	private GpxParser gpxParser;


	public static AmazonClientManager clientManager = null;

	@SuppressWarnings({ "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		fileGpx = (File) getIntent().getSerializableExtra("filegpx");
		route = (Route) getIntent().getExtras().get("route");
		files3gp = getIntent().getStringArrayListExtra("files3gp");
		
		/** ---->>> Crea fichero GPS con cabeceras **/
		
		gpxParser = new GpxParser(fileGpx, route, files3gp);
		
		lineRoute = Integer.parseInt(PropertyLoader.getInstance().getLineRouteNarrow());

		//Fija el layout, que es el mismo que el de NewRouteMovActivity
		setContentView(R.layout.activity_draw_route);
              
	}
	
	public void loadDataRoute(){
		//Se recupera el username
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = prefs.getString("username", "Default Value if not found");
        
		//carga de datos de la ruta
		getActionBar().setTitle(route.getName());
		//Redondeo a dos decimales
		DecimalFormat df = new DecimalFormat("##");
		df.setRoundingMode(RoundingMode.UP);
		TextView distance =(TextView)this.findViewById(R.id.distance);
		distance.setText(df.format(route.getDistance())+"km");
		
		TextView positiveD =(TextView)this.findViewById(R.id.DPositive);
		positiveD.setText((int)route.getPositiveSlope()+"m");
		
		TextView negativeD =(TextView)this.findViewById(R.id.DNegative);
		negativeD.setText((int)route.getNegativeSlope()+"m");
		
		TextView popularity =(TextView)this.findViewById(R.id.tv_popularity);
		Double pop = 100 * ((double)route.getPopularity()-(double)ExploreSingleton.getInstance().getMin_popularity())/((double)ExploreSingleton.getInstance().getMax_popularity() - (double)ExploreSingleton.getInstance().getMin_popularity());
		popularity.setText(String.valueOf(pop.intValue()) + "/100");

		
		ImageView ico =(ImageView)this.findViewById(R.id.type);
		if(route.getCategory().compareTo("LL")==0){
    		if(route.getUserid().equalsIgnoreCase(username)){
    			ico.setImageResource(R.drawable.llana_user);
    		}else if(route.getUserid().equalsIgnoreCase("Movistar")){
    			ico.setImageResource(R.drawable.llana_movistar);
    		}else{
    			ico.setImageResource(R.drawable.llana_otherusers);
    		}

    	}else if(route.getCategory().compareTo("MC")==0){
    		if(route.getUserid().equalsIgnoreCase(username)){
    			ico.setImageResource(R.drawable.mc_user);
    		}else if(route.getUserid().equalsIgnoreCase("Movistar")){
    			ico.setImageResource(R.drawable.mc_movistar);
    		}else{
    			ico.setImageResource(R.drawable.mc_otherusers);
    		}
    	}else{
    		if(route.getUserid().equalsIgnoreCase(username)){
    			ico.setImageResource(R.drawable.hc_user);
    		}else if(route.getUserid().equalsIgnoreCase("Movistar")){
    			ico.setImageResource(R.drawable.hc_movistar);
    		}else{
    			ico.setImageResource(R.drawable.hc_otherusers);
    		}
    	}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		int j=0;
		List<UserAct> activities = Singleton.getInstance().getAlllistUserActivities();
		for(int i=0; i<activities.size();i++){
			Log.i("DrawRouteActivity", activities.get(i).getRouteid() + " Fecha: "+activities.get(i).getCreation());
			if(route.getId().equalsIgnoreCase(activities.get(i).getRouteid())){
				activitiesThisRoute.add(activities.get(i));
				avgSpeed.add(String.valueOf(activities.get(i).getAvgspeed()));
	        	Log.i("DrawRouteActivity", avgSpeed.get(j));
				creationAct.add(activities.get(i).getCreation());
				j++;
				
			}
		}
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = prefs.getString("username", "Default Value if not found");

		if(username.equalsIgnoreCase(route.getUserid())){
			getMenuInflater().inflate(R.menu.draw_menu, menu);
		}else{
			if(activitiesThisRoute.size()>0){
				getMenuInflater().inflate(R.menu.no_edit_route, menu);
			}
		}
		loadDataRoute();
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			if(getIntent().getExtras().get("activity").toString().equalsIgnoreCase("ExploreActivity")){
				finish();
				Intent inUp = new Intent(getApplicationContext(),ExploreActivity.class);
				startActivity(inUp);
			}else{
				finish();
			}
	        return true;
	        		
		case R.id.editRoute:
			Intent i = new Intent(DrawRouteActivity.this, ChangeRouteDetailsActivity.class);
			i.putExtra("route", route);
			startActivity(i);
			return true;
			
		case R.id.details_activities:
			Intent in = new Intent(DrawRouteActivity.this, ChartVelActivity.class);
			in.putExtra("route", route.getName());
			in.putStringArrayListExtra("avgSpeed", avgSpeed);
			in.putStringArrayListExtra("creationAct", creationAct);
			startActivity(in);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(getIntent().getExtras().get("activity").toString().equalsIgnoreCase("ExploreActivity")){
			finish();
			Intent inUp = new Intent(getApplicationContext(),ExploreActivity.class);
			startActivity(inUp);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onResume() {
		//Instancia el singleton de segments y warnings
		super.onResume();
		try {
			setUpMapIfNeeded();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onSaveInstanceState(Bundle icicle) {
		super.onSaveInstanceState(icicle);
	}
	
	
	private void setUpMapIfNeeded() throws ParserConfigurationException, SAXException {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				drawPath();
			}
		}
	}
	
	public void startNewUserActivity(View v){
		Intent i = new Intent(getApplicationContext(),NewUserActivity.class);
		i.putExtra("route", route);
		i.putExtra("filegpx", fileGpx);
		i.putStringArrayListExtra("files3gp",files3gp);
		i.putExtra("idChallenge", "-1");
		startActivity(i);
		finish();
	}
	
	//Metodo para dibujar rutas sobre el mapa con Polylines. Se le pasa como argumento la ruta al archivo .GPX o .KML y el mapa
	//sobre el que se va a pintar la ruta
	private void drawPath() throws ParserConfigurationException, SAXException {
		try {
			final Vector<LatLng> list = gpxParser.parseGPX();

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	        String username = prefs.getString("username", "Default Value if not found");
			BitmapDescriptor iconStart;
			if(route.getUserid().equalsIgnoreCase("Movistar")){
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_mov);
			}
			else if(route.getUserid().equalsIgnoreCase(username)){
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_user);
			}
			else{
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_otherusers);
			}
			mMap.addMarker(new MarkerOptions()
			.position(list.get(0))
			.title(getResources().getString(R.string.ini_sight))
			.icon(iconStart));
			BitmapDescriptor iconFinish = BitmapDescriptorFactory.fromResource(R.drawable.market_finish_small);
			mMap.addMarker(new MarkerOptions()
			.position(list.get(list.size()-1))
			.title(getResources().getString(R.string.end_sight))
			.icon(iconFinish));
			
			mMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {
				
				@Override
				public void onMapLoaded() {
					LatLngBounds.Builder builder = new LatLngBounds.Builder();
					for (LatLng marker : list) {
					    builder.include(marker);
					}
					LatLngBounds bounds = builder.build();
					int padding = 40; // offset from edges of the map in pixels
					CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
					mMap.animateCamera(cu);		
				}
			});
			
			PolylineOptions polylineOptions = new PolylineOptions()
			.width(Integer.parseInt(PropertyLoader.getInstance().getLineRouteNarrow()))
			.color(0xff062344);
			mMap.addPolyline(polylineOptions.addAll(list));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
	
}
