package com.talentum.cyclistsightracks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.widget.Toast;

import com.amazonaws.tvmclient.AmazonClientManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.talentum.cyclistsightracks.RecorderManager;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.MusicService.MusicBinder;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.Segment;
import com.talentum.cyclistsightracks.model.UserWarning;
import com.talentum.cyclistsightracks.utils.GpxParser;
import com.talentum.cyclistsightracks.utils.LocationServiceListener;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.SegmentsSingleton;
import com.talentum.cyclistsightracks.utils.Singleton;
import com.talentum.cyclistsightracks.utils.StartFinishRouteListener;
import com.talentum.cyclistsightracks.MusicService;


public class NewUserActivity extends ActionBarActivity implements LocationServiceListener, 
OnMyLocationButtonClickListener, ServiceConnection{

	private GoogleMap mMap;
	private int lineRoute;

	private List<UserWarning> warnings;
	//Lista de warnings para reproducir automaticamente (se eliminan de la lista al reproducirse)
	private ArrayList<UserWarning> automaticWarnings;
	private Segment currentSegment;
	private ArrayList<Segment> segments;

	private Route loadedRoute;
	private Vector<LatLng> pointsListLoadedRoute;
	private Location loc = null;
	private RecorderManager recorderManager;
	private boolean recording;
	private boolean hasStarted;
	private ArrayList<String> loaded3gp;
	private File fileGpx;
	
	private LocationService locationService;
	private MusicService musicService;
    private Intent locationServiceIntent;
    private Intent musicServiceIntent;
	boolean isBoundLocationService = false;
	boolean isBoundMusicService = false;
	
	private long colorOldTrack;
	private long colorNewTrack;
	
	private SupportMapFragment mapFragment;
	
	private GpxParser gpxParser;
	
	private PowerManager.WakeLock wakelock;
	
	private Marker startMarker;
	private Marker endMarker;
	
	private ArrayList<StartFinishRouteListener> mListeners;

	public static AmazonClientManager clientManager = null;
	private PolylineOptions polylineOptions;
	
	private String idChallenge;
	private long totalTimeChallenge;
	private String userId;

	@SuppressWarnings({ "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//evitar que la pantalla se apague
        final PowerManager pm=(PowerManager)getSystemService(this.POWER_SERVICE);
		this.wakelock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "etiqueta");
        wakelock.acquire();
        getActionBar().hide();
        
		fileGpx = (File) getIntent().getSerializableExtra("filegpx");
		loadedRoute = (Route) getIntent().getExtras().get("route");
		loaded3gp = getIntent().getStringArrayListExtra("files3gp");
		idChallenge = getIntent().getExtras().getString("idChallenge");
		if(!idChallenge.equals("-1")){
			totalTimeChallenge = getIntent().getExtras().getLong("totalTimeChallenge");
		}
		locationServiceIntent = new Intent(getApplicationContext(), LocationService.class);
		bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		
		recorderManager = new RecorderManager();
		
		/** ---->>> Crea fichero GPS con cabeceras **/
		segments = new ArrayList<Segment>();
		
		gpxParser = new GpxParser(fileGpx, loadedRoute, loaded3gp);
		
		hasStarted = false;
		
		mListeners = new ArrayList<StartFinishRouteListener>();
		
		lineRoute = Integer.parseInt(PropertyLoader.getInstance().getLineRoute());
		colorOldTrack=Long.parseLong(PropertyLoader.getInstance().getColorOldTrack(),16);
		colorNewTrack=Long.parseLong(PropertyLoader.getInstance().getColorNewTrack(),16);
		
		polylineOptions = new PolylineOptions()
		.width(Integer.parseInt(PropertyLoader.getInstance().getLineRoute()))
		.color((int)colorNewTrack);

		//Fija el layout, que es el mismo que el de NewRouteMovActivity
		setContentView(R.layout.activity_new_user_activity);
        if (findViewById(R.id.upper_fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            UpperFragment upperFragment = new UpperFragment();
            mapFragment = new SupportMapFragment();
            
            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            Bundle args = new Bundle();
            args.putBoolean("isNewRoute", false);
            args.putBoolean("isMovistar", loadedRoute.getUserid().equalsIgnoreCase("Movistar"));
            args.putDouble("totalDistance", loadedRoute.getDistance());
            upperFragment.setArguments(args);
            
            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.upper_fragment_container, upperFragment).commit();
            getSupportFragmentManager().beginTransaction()
            .add(R.id.map_fragment_container, mapFragment).commit();
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userId = prefs.getString("username", "Default Value if not found");
	}

	@Override
	protected void onResume() {
		//Instancia el singleton de segments y warnings
		super.onResume();
		wakelock.acquire();
		currentSegment = new Segment();
		polylineOptions = new PolylineOptions()
		.width(Integer.parseInt(PropertyLoader.getInstance().getLineRoute()))
		.color((int)colorNewTrack);
		try {
			if(mapFragment == null){
				mapFragment = new SupportMapFragment();
				getSupportFragmentManager().beginTransaction()
				.add(R.id.map_fragment_container, mapFragment).commit();
			}
			setUpMapIfNeeded();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!isBoundLocationService){
			bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		}
	}

	public void onSaveInstanceState(Bundle icicle) {
		super.onSaveInstanceState(icicle);
		this.wakelock.release();
	}

	@Override
	public void onPause() {
		super.onPause();
		if(currentSegment.getLocations().size()>1){
        	segments.add(currentSegment);
        }
		if(isBoundLocationService == true){
			locationService.unregisterListener(this);
			unbindService(this);
			isBoundLocationService = false;
		}
	}
	
	@SuppressLint("Wakelock")
	protected void onDestroy(){
        this.wakelock.release();
        if(isBoundLocationService == true){
			locationService.unregisterListener(this);
			unbindService(this);
			isBoundLocationService = false;
		}
        if(musicService != null){
        	stopService(musicServiceIntent);
        }
        super.onDestroy();
    }
	
	//Metodo llamado cuando se finaliza la ruta que guarda los segmentos en el singleton
	public void finishRoute(double currentDistance, long totalTime){		
		/** -------> Pone cabeceras finales al fichero y calcula medidas **/
		if(currentSegment.getLocations().size()>1){
        	segments.add(currentSegment);
        }
		if(segments.size()>0){
			SegmentsSingleton.getInstance().setSegments(segments);
		}
		Intent intent = new Intent(this, FinishRouteActivity.class);
		intent.putExtra("currentDistance", currentDistance);
		intent.putExtra("isNewRoute", false);
		intent.putExtra("isMovistar", loadedRoute.getUserid().equalsIgnoreCase("Movistar"));
		intent.putExtra("route", loadedRoute);
		intent.putExtra("totalTime", totalTime);
		intent.putExtra("idChallenge", idChallenge);
		if(!idChallenge.equals("-1")){
			intent.putExtra("totalTimeChallenge", totalTimeChallenge);
		}
		finish();
		startActivity(intent);
	}
	
	public void pauseResumeRoute(){
		if(recording){
			Toast.makeText(this, getResources().getString(R.string.pause_sight), Toast.LENGTH_LONG).show();
			if(currentSegment.getLocations().size()>0){
	        	segments.add(currentSegment);
	        }
	        recording = false;
		}
		else{
			Toast.makeText(this, getResources().getString(R.string.recording), Toast.LENGTH_LONG).show();
			currentSegment = new Segment();
			recording = true;
		}
	}

	@Override
	public void onUpdate(Location location) {
		//Controla si el usuario le ha dado al boton de comenzar
		if(recording){
			loc = new Location(location);
			//Al detectarse proximidad al punto de origen de la ruta, la ruta ha comenzado y hay que empezar a pintar
			if(hasStarted){
					mMap.addPolyline(polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude())));
					//Comprueba si hay avisos que reproducir en la nueva localizacion recibida
					checkWarnings(location);
				
				if(segments.isEmpty() && currentSegment.getLocations().isEmpty()){
					BitmapDescriptor icon_start = null;
					icon_start = BitmapDescriptorFactory.fromResource(R.drawable.market_start);
				}
				currentSegment.addLocation(location);
				if(checkIfClose(pointsListLoadedRoute.get(pointsListLoadedRoute.size()-1))){
					notifyRouteEnd();
				}
				

			}else{
				if(!checkIfClose(pointsListLoadedRoute.get(0)))
					Toast.makeText(this, getResources().getString(R.string.go_closer), Toast.LENGTH_SHORT).show();
				else{
					//Se anade tambien el icono propio del usuario
					BitmapDescriptor iconStartUser = BitmapDescriptorFactory.fromResource(R.drawable.market_user);
					mMap.addMarker(new MarkerOptions()
					.position(new LatLng(location.getLatitude(),location.getLongitude()))
					.title(getResources().getString(R.string.ini_sight))
					.icon(iconStartUser));
					notifyRouteStart();
					hasStarted = true;
				}
			}
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
			mMap.animateCamera(cameraUpdate);
		}
		/** Guarda la posicion loc en el fichero GPS **/
	}
	
	private void setUpMapIfNeeded() throws ParserConfigurationException, SAXException {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = mapFragment.getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				drawPath();
				drawWarnings();
				mMap.setMyLocationEnabled(true);
				mMap.setOnMyLocationButtonClickListener(this);
				mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
					@Override
					public boolean onMarkerClick(Marker marker) {
						marker.showInfoWindow();
						for(int i=0; i<warnings.size(); i++){
							Location l = new Location("");
							l.setLatitude(marker.getPosition().latitude);
							l.setLongitude(marker.getPosition().longitude);
							if( l.distanceTo(warnings.get(i).getLocation()) < 1 ){
								if(warnings.get(i).hasVoice()){
									recorderManager.startPlaying(
											Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+loadedRoute.getId()+"/"+warnings.get(i).getWarningId()+".3gp");
								} 
							}
						}
						return false;
					}
				});
			}
		}
	}
	
	public boolean checkIfClose(LatLng location){
		Location locationToCheck = new Location("toCheck");
		locationToCheck.setLatitude(location.latitude);
		locationToCheck.setLongitude(location.longitude);
		//Si esta a menos de 50m, devuelve positivo
		if(locationToCheck.distanceTo(loc) <= 50)
			return true;
		else
			return false;
	}

	@Override
	public boolean onMyLocationButtonClick() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean hasStarted(){
		return hasStarted;
	}

	//Metodo para dibujar rutas sobre el mapa con Polylines. Se le pasa como argumento la ruta al archivo .GPX o .KML y el mapa
	//sobre el que se va a pintar la ruta
	private void drawPath() throws ParserConfigurationException, SAXException {
		try {
			pointsListLoadedRoute = gpxParser.parseGPX();
			BitmapDescriptor iconStart;
			if(loadedRoute.getUserid().equalsIgnoreCase("Movistar")){
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_start_gray);
			}
			else if(loadedRoute.getUserid().equalsIgnoreCase(userId)){
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_user_gray);
			}
			else{
				iconStart = BitmapDescriptorFactory.fromResource(R.drawable.market_otherusers_gray);
			}
		    
			startMarker = mMap.addMarker(new MarkerOptions()
			.position(pointsListLoadedRoute.get(0))
			.title(getResources().getString(R.string.ini_sight))
			.icon(iconStart));
			BitmapDescriptor iconFinish = BitmapDescriptorFactory.fromResource(R.drawable.market_finish_gray);
			endMarker = mMap.addMarker(new MarkerOptions()
			.position(pointsListLoadedRoute.get(pointsListLoadedRoute.size()-1))
			.title(getResources().getString(R.string.end_sight))
			.icon(iconFinish));

			mMap.setOnMapLoadedCallback(new OnMapLoadedCallback() {

				@Override
				public void onMapLoaded() {
					LatLngBounds.Builder builder = new LatLngBounds.Builder();
					for (LatLng marker : pointsListLoadedRoute) {
						builder.include(marker);
					}
					LatLngBounds bounds = builder.build();
					int padding = 40; // offset from edges of the map in pixels
					CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
					mMap.animateCamera(cu);
				}
			});
			PolylineOptions polylineOptions = new PolylineOptions()
			.width(Integer.parseInt(PropertyLoader.getInstance().getLineRoute()))
			.color((int) colorOldTrack);
			mMap.addPolyline(polylineOptions.addAll(pointsListLoadedRoute));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
	
	private void drawWarnings(){
		warnings = Singleton.getInstance().getWarnings();
		if(!warnings.isEmpty()){
			musicServiceIntent = new Intent(this, MusicService.class);
			musicServiceIntent.putExtra("routeId",loadedRoute.getId());
			startService(musicServiceIntent);
			bindService(musicServiceIntent, this, Context.BIND_IMPORTANT);
		}
		automaticWarnings = new ArrayList<UserWarning>();
		automaticWarnings.addAll(warnings);
		
		for(int i=0; i < warnings.size(); i++){
			BitmapDescriptor icon_market = null;
			if(warnings.get(i) == null){
			}
			String title = getResources().getString(getResources().getIdentifier(warnings.get(i).getWarningDescription(),"string",this.getPackageName()));
			if(warnings.get(i).hasVoice()){
				icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_micro);
				
			}
			else{
				if(warnings.get(i).getWarningDescription().contains("gr")){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_unevenroad);
				}
				else if(warnings.get(i).getWarningDescription().contains("dc")){
					if(warnings.get(i).getWarningDescription().contains("le")){
						if(warnings.get(i).getWarningDescription().contains("ff")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left45);
						}
						else if(warnings.get(i).getWarningDescription().contains("ni")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left90);
						}
						else{
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_left180);
						}
					}
					else if(warnings.get(i).getWarningDescription().contains("ri")){
						if(warnings.get(i).getWarningDescription().contains("ff")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right45);
						}
						else if(warnings.get(i).getWarningDescription().contains("ni")){
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right90);
						}
						else{
							icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_right135);
						}
					}				
				}
				else if(warnings.get(i).getWarningDescription().contains("wi")){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_wind);
				}
				else if(warnings.get(i).getWarningDescription().contains("sb")){
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_baden);
				}
				else if(warnings.get(i).getWarningDescription().contains("sc")){
					if(warnings.get(i).getWarningDescription().contains("as")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopeup);
					}
					else if(warnings.get(i).getWarningDescription().contains("de")){
						icon_market = BitmapDescriptorFactory.fromResource(R.drawable.market_slopedown);
					}
				}
				else{
					icon_market = BitmapDescriptorFactory.fromResource(R.drawable.ic_help);//esta linea hay que quitarla
				}
			}
			
			mMap.addMarker(new MarkerOptions()
			.position(new LatLng(warnings.get(i).getLocation().getLatitude(),warnings.get(i).getLocation().getLongitude()))
			.title(title)
			.icon(icon_market));
		}
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		if(name.getClassName().equals(LocationService.class.getName())){
			LocalBinder binder = (LocalBinder) service;
			currentSegment = new Segment();
			locationService = binder.getService();
			locationService.registerListener(this);
			isBoundLocationService = true;
		}
		if(name.getClassName().equals(MusicService.class.getName())){
			MusicBinder binder = (MusicBinder) service;
			musicService = binder.getService();
		}
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		if(name.getClassName().equals(LocationService.class)){
			locationService = null;
			isBoundLocationService = false;
		}
	}

    public void registerListener(StartFinishRouteListener listener) {
        mListeners.add(listener);
        //Actualiza al listener que se acaba de registrar
    }

    public void unregisterListener(StartFinishRouteListener listener) {
        mListeners.remove(listener);
    }
    
    public void notifyRouteStart() {
    	for (int i=mListeners.size()-1; i>=0; i--) {
            mListeners.get(i).onStartRouteListener();
        }
    }
    
    public void notifyRouteEnd() {
        mListeners.get(mListeners.size()-1).onFinishRouteListener();
    }
    
    
    private void checkWarnings(Location location){
    	//Clona la lista de Warnings para no eliminarlos del dato miembro de la clase
    	
    	for(int i=0; i< automaticWarnings.size() && i < 4;i++){
    		if(automaticWarnings.get(i).getLocation().distanceTo(location) <= automaticWarnings.get(i).getPlayDistance()){
    			if(!automaticWarnings.get(i).hasVoice()){
    				musicService.addToQeue(automaticWarnings.get(i).getWarningDescription(), false);
    			}else{
    				musicService.addToQeue(String.valueOf(automaticWarnings.get(i).getWarningId()),true);
    			}
   				automaticWarnings.remove(i);
   				i--;
   				if(i > 0){
   					automaticWarnings.remove(i);
   					i--;
   					if(i > 0){
   						automaticWarnings.remove(i);
   						i--;
   					}
   				}
   				break;
    		}
    		
    	}
    }
    
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			DialogInterface.OnClickListener dialogClickListenerFinish = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						startActivity(new Intent(getApplicationContext(), RoutesListActivity.class));
						finish();
						break;

					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			AlertDialog.Builder builderDiscard = new AlertDialog.Builder(this);
			builderDiscard.setMessage(getResources().getString(R.string.finishWithoutSave))
					.setPositiveButton(getResources().getString(R.string.yes), dialogClickListenerFinish)
					.setNegativeButton(getResources().getString(R.string.no), dialogClickListenerFinish).show();

		}
		return true;
	}

}
