package com.talentum.cyclistsightracks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.talentum.cyclistsightracks.LocationService.LocalBinder;
import com.talentum.cyclistsightracks.model.Route;
import com.talentum.cyclistsightracks.model.Segment;
import com.talentum.cyclistsightracks.utils.PropertyLoader;
import com.talentum.cyclistsightracks.utils.Singleton;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


public class MovistarActivity extends ListActivity implements ServiceConnection, OnQueryTextListener, OnActionExpandListener  {

	private Singleton listRoutes;
	private static LocationService locationService;
	static boolean mBound = false;
	private Location locationRoute;
	Location location;
	private List<Route> list = new ArrayList<Route>();
	private ArrayList<String> names3gp;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenges);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Intent locationServiceIntent = new Intent(MovistarActivity.this, LocationService.class);
		bindService(locationServiceIntent, this, Context.BIND_ADJUST_WITH_ACTIVITY);
		names3gp = new ArrayList<String>();
		
		//Inicializa locatio en Madrid, para evitar que la aplicacion de exception si el movil no toma ninguna localizacion
		
		location = new Location("dummyprovider");
		location.setLatitude(40.416947);
		location.setLongitude(-3.703528);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(mBound == true){
			unbindService(this);
			mBound = false;
		}
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.search_route, menu);
		 
		MenuItem searchItem = menu.findItem(R.id.buscar);

	    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
	    searchView.setMaxWidth(2000);
	    
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
	    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
	    //Cambio el estilo del searchView del actionBar
	    SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete)searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
	    searchAutoComplete.setTextColor(Color.WHITE);
	    searchAutoComplete.setHint(R.string.hint_search);
	    
	    ImageView searchIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
	    searchIcon.setImageResource(R.drawable.ic_menu_search);
	    
	    ImageView searchCloseIcon = (ImageView)searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
	    searchCloseIcon.setImageResource(R.drawable.discard_button);
	    
		// LISTENER PARA EL EDIT TEXT   
	    searchView.setOnQueryTextListener((OnQueryTextListener) this);
	    // LISTENER PARA LA APERTURA Y CIERRE DEL WIDGET
	    MenuItemCompat.setOnActionExpandListener(searchItem, (OnActionExpandListener) this);
     
	    return true;
	}
	
	public boolean onQueryTextChange(String arg0) {
		//Toast.makeText(this, "Buscando..."+arg0, Toast.LENGTH_LONG).show();
		List<Route> listSearch = new ArrayList<Route>();
		for(int i=0; i<list.size();i++){
			String rutaCompara = list.get(i).getName().toLowerCase();
			if(rutaCompara.contains(arg0.toLowerCase())){
				listSearch.add(list.get(i));
			}
		}
	
		//Cargo las listas que contengan la palabra que busco
		setListAdapter(new RoutesMovAdapter(this,listSearch, location));
		return false;
	}
	public boolean onQueryTextSubmit(String arg0) {
		
		List<Route> listSearch = new ArrayList<Route>();
		for(int i=0; i<list.size();i++){
			String rutaCompara = list.get(i).getName().toLowerCase();
			if(rutaCompara.contains(arg0.toLowerCase())){
				listSearch.add(list.get(i));
			}
		}
	
		//Cargo las listas que contengan la palabra que busco
		setListAdapter(new RoutesMovAdapter(this,listSearch, location));

		//Cierro el teclado virtual automaticamente al hacer la b??squeda
		View v = (View) findViewById(R.id.buscar);
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

	    return true;
	}
	public boolean onMenuItemActionCollapse(MenuItem arg0) {
	    return true;
	}
	public boolean onMenuItemActionExpand(MenuItem arg0) {
	    return true;
	}
	
	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id){
		super.onListItemClick(listView, view, position, id);
		new S3PutObjectTask().execute(list.get(position));
	}
	
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		LocalBinder binder = (LocalBinder) service;
		locationService = binder.getService();
		Location myLocation = locationService.getLastLocation();
		if(myLocation != null){
			location = myLocation;
		}
		mBound = true;
		listRoutes = Singleton.getInstance();
		
		list = orderList(listRoutes.getListMovRoutes());
		
		RoutesMovAdapter adapter = new RoutesMovAdapter(MovistarActivity.this, list, location);
		setListAdapter(adapter);
		
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		locationService = null;
		mBound = false;
		//Log.i("MyMap","MovistarActivity onServiceDisconnected");
	}
	
	private void writefile(InputStream inputStream, String routeId, String name){
    	OutputStream outputStream = null;
     
    	try {
    		// write the inputStream to a FileOutputStream
    		File routeFolder = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/files3gp/"+routeId);
            if(!routeFolder.exists()){
 	           if (!routeFolder.exists()) {
 	               routeFolder.mkdir();
 	           }
            }
    		outputStream = 
                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/", name));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
          
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (inputStream != null) {
    			try {
    				inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
     
    		}
    	}    	
    }
	
	private List<Route> orderList(List<Route> myLista){
		List<Route> listOr = new ArrayList<Route>();
		List<Route> myList = new ArrayList<Route>();
		myList.addAll(myLista);
		while(myList.size()>0){
			float min=999999999;
			int pos=0;
			float distanceTo;
			for(int i = 0; i<myList.size();i++){
				locationRoute = new Location("dummyprovider");
		    	locationRoute.setLatitude(myList.get(i).getStartpointlat());
		    	locationRoute.setLongitude(myList.get(i).getStartpointlon());
		    	distanceTo = location.distanceTo(locationRoute);
		    	if(distanceTo<min){
		    		min= distanceTo;
		    		pos = i;
		    	}
			}
			listOr.add(myList.get(pos));
			myList.remove(pos);
		}
		
		return listOr;
	}
	
	private class S3PutObjectTask extends AsyncTask<Route, Void, S3TaskResult> {
		ProgressDialog dialog;
		File file;
		Route route;
		String bucket;
		int currentOrientation = getResources().getConfiguration().orientation;
		
		protected void onPreExecute() {
	        super.onPreExecute();
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
	        
	        
	        dialog = new ProgressDialog(MovistarActivity.this);
			dialog.setMessage(MovistarActivity.this
					.getString(R.string.loading_));
			dialog.setCancelable(false);
			dialog.show();
		}
		
		protected S3TaskResult doInBackground(Route... args) {
			/** Guarda el archivo GPX (PRUEBAS) **/

			if (args == null || args.length != 1) {
				return null;
			}
			bucket = PropertyLoader.getInstance().getBucketGPXteam();
			route = args[0];

			S3TaskResult result = new S3TaskResult();	
			try{
				
				/************/
					 ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+route.getId()+"/");	
			   ObjectListing objectListing;
			    do {
			    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
			    	for (S3ObjectSummary objectSummary : 
			    		objectListing.getObjectSummaries()) {
			    		InputStream in = MainActivity.clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
	    		    	writefile(in, route.getId(), objectSummary.getKey());
	    		    	names3gp.add(objectSummary.getKey());
	    		    	System.out.println(" - " + objectSummary.getKey() + "  " +
			                    "(size = " + objectSummary.getSize() + 
			    				")");
			    	}
			    } while (objectListing.isTruncated());
				/************/   
				//Log.i("MyMap","bucketGPX: "+bucket+listRoutes.get(posicion).getId()+"/");
				MainActivity.clientManager.s3().getBucketAcl(bucket);
				GetObjectRequest gor = new GetObjectRequest(bucket,route.getId()+".gpx");
				S3Object object = MainActivity.clientManager.s3().getObject(gor);
				InputStream reader = new BufferedInputStream(object.getObjectContent());
				
				file = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.cyclistsightracks/filesGPX/",route.getId());
				OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
				int read = -1;

				while ( ( read = reader.read() ) != -1 ) {
				    writer.write(read);
				}
				writer.flush();
				writer.close();
				reader.close();
				
			} catch (Exception exception) {
				Log.i("MyMap","Error: "+exception.getMessage());
				result.setErrorMessage(exception.getMessage());
			}


			return result;
		}
		
		protected void onPostExecute(S3TaskResult result) {
			dialog.dismiss();
			if (result.getErrorMessage() != null) {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_routedel), Toast.LENGTH_SHORT).show();
				MovistarActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
			}
			else {
//				RoutesListActivity.this.finish();
				Intent i = new Intent(getApplicationContext(),DrawRouteActivity.class);
				i.putExtra("route", route);
				i.putExtra("filegpx", file);
				i.putStringArrayListExtra("files3gp", names3gp);
				i.putExtra("activity", "MovistarActivity");
				finish();
				startActivity(i);
			}
		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
		}
	}
	
	private class S3TaskResult {
		String errorMessage = null;

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}
}
