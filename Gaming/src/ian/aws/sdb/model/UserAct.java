package ian.aws.sdb.model;

import java.io.Serializable;


public class UserAct implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String routeid;
	private String userid;
	private Integer duration;
	private Double avgspeed;
	private Integer punctuation;
	private String creation;
	
	public UserAct() {
        super();
    }
 
    public UserAct(String id, String routeid, String userid, Integer duration, Double avgspeed, Integer punctuation, String creation) {
        super();
        this.id = id;
        this.routeid = routeid;
        this.userid = userid;
        this.duration = duration;
        this.avgspeed = avgspeed;
        this.punctuation = punctuation;
        this.creation = creation;
    }
    
    
    public String getId(){
    	return id;
    }
    
    public void setId(String id){
    	this.id = id;
    }

	public String getRouteid() {
		return routeid;
	}

	public String getUserid() {
		return userid;
	}

	public Integer getDuration() {
		return duration;
	}

	public Double getAvgspeed() {
		return avgspeed;
	}

	public Integer getPunctuation() {
		return punctuation;
	}

	public String getCreation() {
		return creation;
	}

	public void setRouteid(String routeid) {
		this.routeid = routeid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setAvgspeed(Double avgspeed) {
		this.avgspeed = avgspeed;
	}

	public void setPunctuation(Integer punctuation) {
		this.punctuation = punctuation;
	}

	public void setCreation(String creation) {
		this.creation = creation;
	}
}
