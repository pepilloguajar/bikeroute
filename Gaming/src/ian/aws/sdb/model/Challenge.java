package ian.aws.sdb.model;

import java.io.Serializable;

public class Challenge implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idroute;
	private String iduser;
	private String timetodo;
	private int punctuation;
	
	public Challenge(){
		
	}

	public Challenge(String idroute, String iduser, String timetodo, int punctuation){
		this.idroute = idroute;
		this.iduser = iduser;
		this.timetodo = timetodo;
		this.punctuation = punctuation;
	}

	/**
	 * @return the idroute
	 */
	public String getIdroute() {
		return idroute;
	}

	/**
	 * @return the iduser
	 */
	public String getIduser() {
		return iduser;
	}

	/**
	 * @return the timetodo
	 */
	public String getTimetodo() {
		return timetodo;
	}

	/**
	 * @return the punctuation
	 */
	public int getPunctuation() {
		return punctuation;
	}

	/**
	 * @param idroute the idroute to set
	 */
	public void setIdroute(String idroute) {
		this.idroute = idroute;
	}

	/**
	 * @param iduser the iduser to set
	 */
	public void setIduser(String iduser) {
		this.iduser = iduser;
	}

	/**
	 * @param timetodo the timetodo to set
	 */
	public void setTimetodo(String timetodo) {
		this.timetodo = timetodo;
	}

	/**
	 * @param punctuation the punctuation to set
	 */
	public void setPunctuation(int punctuation) {
		this.punctuation = punctuation;
	}
}
