package ian.aws.sdb;

import ian.util.Arr;
import java.util.TreeMap;

public class SDBTest {
   public static void main(String[] args) throws Exception {
      SDB sdb = new SDB("YOUR KEY HERE", "YOUR SECRET HERE");
      sdb.createDomain("test");
      System.out.println(Arr.toString(sdb.listDomains()));
      TreeMap<String, String> map = new TreeMap<String, String>();
      map.put("id", "item1");
      map.put("a", "av");
      map.put("b", "bv");
      sdb.putAttributes("test", map);
      System.out.println(sdb.getAttributes("test", "item1"));
      System.out.println(Arr.toString(sdb.select("select * from test")));
      map = new TreeMap<String, String>();
      map.put("id", "item1");
      sdb.deleteAttributes("test", map);
      System.out.println(sdb.getAttributes("test", "item1"));
      sdb.deleteDomain("test");
      System.out.println(Arr.toString(sdb.listDomains()));
   }
}
