package ian.aws.sdb;

import ian.xml.Embed;
import ian.xml.XMap;

@Embed
interface KeyValuePair {
   @XMap(path = "/SelectResponse/SelectResult/Item/Attribute/Name/#text")
   String[] key();
   @XMap(path = "/SelectResponse/SelectResult/Item/Name/#text")
   String name();
   @XMap(path = "/SelectResponse/SelectResult/Item/Attribute/Value/#text")
   String[] value();
}