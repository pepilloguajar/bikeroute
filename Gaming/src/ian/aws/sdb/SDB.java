package ian.aws.sdb;

import ian.io.IO;
import ian.util.Base64;
import ian.util.Binary;
import ian.xml.ProxyMap;
import ian.xml.XMap;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class SDB {
   interface AttributeGetKeyValuePair {
      @XMap(path = "/GetAttributesResponse/GetAttributesResult/Attribute/Name/#text")
      String[] key();
      @XMap(path = "/GetAttributesResponse/GetAttributesResult/Attribute/Value/#text")
      String[] value();
   }
   interface Domains {
      @XMap(path = "/ListDomainsResponse/ListDomainsResult/DomainName/#text")
      String[] domains();
   }
   interface SelectResult {
      @XMap(path = "/SelectResponse/SelectResult/Item/")
      KeyValuePair[] items();
      @XMap(path = "/SelectResponse/SelectResult/NextToken/#text", defaultValue = "")
      String nextToken();
   }
   static Mac mac;
   static Random r = new Random();
   static {
      try {
         mac = Mac.getInstance("HmacSHA256");
      } catch (final Exception e) {
         e.printStackTrace();
      }
   }
   public static String base64(final byte b[]) {
      return Base64.encodeBytes(b);
   }
   public static String httpDate() {
      final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      df.setTimeZone(TimeZone.getTimeZone("UTC"));
      return df.format(new Date());
   }
   public static int toInt(final String inumb) {
      final byte[] data = new byte[4];
      Binary.hexStrToBytes(inumb, data, 0, 0, 4);
      return Binary.byteArrayToInt(data, 0);
   }
   public static long toLong(final String lnumb) {
      final byte[] data = new byte[8];
      Binary.hexStrToBytes(lnumb, data, 0, 0, 8);
      return Binary.byteArrayToLong(data, 0);
   }
   public static String toString(final int i) {
      final byte b[] = new byte[4];
      Binary.intToByteArray(i, b, 0);
      return Binary.bytesToHexStr(b);
   }
   public static String toString(final long l) {
      final byte b[] = new byte[8];
      Binary.longToByteArray(l, b, 0);
      return Binary.bytesToHexStr(b);
   }
   private final String id;
   private final SecretKeySpec secret;
   public SDB(final String id, final String key) {
      this.id = id;
      secret = new SecretKeySpec(key.getBytes(), "HmacSHA256");
   }
   public String createDomain(final String name) throws Exception {
      final String time = encodeUrl(httpDate());
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + "AWSAccessKeyId=" + id
            + "&Action=CreateDomain" + "&DomainName=" + name + "&SignatureMethod=HmacSHA256"
            + "&SignatureVersion=2" + "&Timestamp=" + time + "&Version=2009-04-15";
      final String endpoint = "https://sdb.amazonaws.com/" + "?" + "AWSAccessKeyId=" + id
            + "&Action=CreateDomain" + "&DomainName=" + name + "&SignatureMethod=HmacSHA256"
            + "&SignatureVersion=2" + "&Timestamp=" + time + "&Version=2009-04-15" + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      return IO.getString(con.getInputStream(), "utf-8");
   }
   // tuple map MUST contain an 'id' key, otherwise nothing will be deleted
   public void deleteAttributes(final String domain, final Map<String, String> tuple)
         throws Exception {
      if (!tuple.containsKey("id")) {
         System.err.println("Warning: tuple does not contain 'id' key, nothing will be deleted");
         return;
      }
      final String time = encodeUrl(httpDate());
      final StringBuffer queryPart = new StringBuffer();
      int count = 1;
      for (final Map.Entry<String, String> x : tuple.entrySet()) {
         if (!x.getKey().equals("id")) {
            queryPart.append("&Attribute.").append(count).append(".Name").append("=").append(
                  encodeUrl(x.getKey()));
            queryPart.append("&Attribute.").append(count).append(".Value").append("=").append(
                  encodeUrl(x.getValue()));
            ++count;
         }
      }
      final String com = "AWSAccessKeyId=" + id + "&Action=DeleteAttributes" + queryPart
            + "&DomainName=" + domain + "&ItemName=" + tuple.get("id")
            + "&SignatureMethod=HmacSHA256" + "&SignatureVersion=2&Timestamp=" + time
            + "&Version=2009-04-15";
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
      final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      // consume result
      IO.getString(con.getInputStream(), "utf-8");
   }
   public void deleteDomain(final String name) throws Exception {
      final String time = encodeUrl(httpDate());
      final String com = "AWSAccessKeyId=" + id + "&Action=DeleteDomain" + "&DomainName=" + name
            + "&SignatureMethod=HmacSHA256" + "&SignatureVersion=2&Timestamp=" + time
            + "&Version=2009-04-15";
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
      final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      // consume result
      IO.getString(con.getInputStream(), "utf-8");
   }
   String encodeUrl(String value) {
      try {
         return URLEncoder.encode(value, "utf-8").replace("+", "%20").replace("*", "%2A").replace(
               "%7E", "~");
      } catch (final UnsupportedEncodingException ex) {
         throw new RuntimeException(ex);
      }
   }
   public Map<String, String> getAttributes(final String domain, final String item)
         throws Exception {
      final String time = encodeUrl(httpDate());
      final String com = "AWSAccessKeyId=" + id + "&Action=GetAttributes" + "&DomainName=" + domain
            + "&ItemName=" + item + "&SignatureMethod=HmacSHA256" + "&SignatureVersion=2"
            + "&Timestamp=" + time + "&Version=2009-04-15";
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
      final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      final String xml = IO.getString(con.getInputStream(), "utf-8");
      final AttributeGetKeyValuePair x = ProxyMap.getObject(AttributeGetKeyValuePair.class, xml,
            null);
      final TreeMap<String, String> map = new TreeMap<String, String>();
      map.put("id", item);
      final String key[] = x.key();
      final String value[] = x.value();
      for (int j = 0; j < key.length; ++j) {
         map.put(key[j], value[j]);
      }
      return map;
   }
   byte[] hmacSha256(final String str) {
      try {
         mac.init(secret);
      } catch (final Exception e) {
         throw new RuntimeException(e);
      }
      return mac.doFinal(str.getBytes());
   }
   public String[] listDomains() throws Exception {
      final String time = encodeUrl(httpDate());
      final String com = "AWSAccessKeyId=" + id + "&Action=ListDomains"
            + "&SignatureMethod=HmacSHA256" + "&SignatureVersion=2" + "&Timestamp=" + time
            + "&Version=2009-04-15";
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
      final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      final String xml = IO.getString(con.getInputStream(), "utf-8");
      final Domains x = ProxyMap.getObject(Domains.class, xml, null);
      return x.domains();
   }
   // if tuple does not contain an 'id' key a unique id will be generated
   public void putAttributes(final String domain, Map<String, String> tuple) throws Exception {
      if (!tuple.containsKey("id")) {
         tuple.put("id", Integer
               .toHexString((int) (r.nextLong() << 4 | System.currentTimeMillis())));
      }
      final String time = encodeUrl(httpDate());
      final StringBuffer queryPart = new StringBuffer();
      int count = 1;
      for (final Map.Entry<String, String> me : tuple.entrySet()) {
         if (!me.getKey().equals("id")) {
            queryPart.append("&Attribute.").append(count).append(".Name").append("=").append(
                  encodeUrl(me.getKey()));
            queryPart.append("&Attribute.").append(count).append(".Replace").append("=").append(
                  "true");
            queryPart.append("&Attribute.").append(count).append(".Value").append("=").append(
                  encodeUrl(me.getValue()));
            ++count;
         }
      }
      final String com = "AWSAccessKeyId=" + id + "&Action=PutAttributes" + queryPart
            + "&DomainName=" + domain + "&ItemName=" + tuple.get("id")
            + "&SignatureMethod=HmacSHA256" + "&SignatureVersion=2&Timestamp=" + time
            + "&Version=2009-04-15";
      final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
      final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
            + encodeUrl(base64(hmacSha256(toSign)));
      final URL url = new URL(endpoint);
      final HttpURLConnection con = (HttpURLConnection) url.openConnection();
      if (con.getResponseCode() >= 300) {
         throw new Exception(con.getResponseMessage());
      }
      // consume result
      IO.getString(con.getInputStream(), "utf-8");
   }
   @SuppressWarnings("unchecked")
   public Map<String, String>[] select(final String q) throws Exception {
      final ArrayList<Map<String, String>> list = new ArrayList<Map<String, String>>();
      String tok = null;
      do {
         final String time = encodeUrl(httpDate());
         String tokstr = "";
         if (tok != null) {
            tokstr = "&NextToken=" + encodeUrl(tok);
         }
         final String com = "AWSAccessKeyId=" + id + "&Action=Select" + tokstr
               + "&SelectExpression=" + encodeUrl(q) + "&SignatureMethod=HmacSHA256"
               + "&SignatureVersion=2" + "&Timestamp=" + time + "&Version=2009-04-15";
         final String toSign = "GET\n" + "sdb.amazonaws.com\n" + "/\n" + com;
         final String endpoint = "https://sdb.amazonaws.com/?" + com + "&Signature="
               + encodeUrl(base64(hmacSha256(toSign)));
         final URL url = new URL(endpoint);
         final HttpURLConnection con = (HttpURLConnection) url.openConnection();
         if (con.getResponseCode() >= 300) {
            throw new Exception(con.getResponseMessage());
         }
         final String xml = IO.getString(con.getInputStream(), "utf-8");
         final SelectResult x = ProxyMap.getObject(SelectResult.class, xml, null);
         tok = x.nextToken().replace("\n", "");
         final KeyValuePair item[] = x.items();
         for (int i = 0; i < item.length; ++i) {
            final String name = item[i].name();
            final TreeMap<String, String> map = new TreeMap<String, String>();
            map.put("id", name);
            final String key[] = item[i].key();
            final String value[] = item[i].value();
            for (int j = 0; j < key.length; ++j) {
               map.put(key[j], value[j]);
            }
            list.add(map);
         }
      } while (tok != null && tok.length() > 0);
      return list.toArray(new Map[0]);
   }
}
