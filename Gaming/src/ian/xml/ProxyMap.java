package ian.xml;

import ian.util.Arr;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ProxyMap implements InvocationHandler {
   // private static final DateFormat DATEFORMAT_XSD_ZULU = new
   // SimpleDateFormat(
   // "yyyy-MM-dd'T'HH:mm:ss'Z'");
   // static {
   // DATEFORMAT_XSD_ZULU.setTimeZone(TimeZone.getTimeZone("UTC"));
   // DATEFORMAT_XSD_ZULU.setLenient(false);
   // }
   public static Object convert(final Class<?> c, final String str) throws Exception {
      if (c.getName().equals("java.lang.String")) {
         return str;
      } else if (c.getName().equals("int") || c.equals(Integer.class)) {
         return Integer.parseInt(str);
      } else if (c.getName().equals("boolean") || c.equals(Boolean.class)) {
         return Boolean.parseBoolean(str);
      }
      // else if (c.equals(Date.class)) {
      // return DATEFORMAT_XSD_ZULU.parse(str);
      // }
      else if (c.getName().equals("long") || c.equals(Long.class)) {
         return Long.parseLong(str);
      } else if (c.getName().equals("double") || c.equals(Double.class)) {
         return Double.parseDouble(str);
      } else if (c.getName().equals("float") || c.equals(Float.class)) {
         return Float.parseFloat(str);
      } else if (c.equals(URL.class)) {
         return new URL(str);
      }
      final Constructor<?> con = c.getConstructor(new Class<?>[] { String.class });
      return con.newInstance(new Object[] { str });
   }
   public static <T> T getObject(final Class<T> c, final String str, final Map<String, ?> meta)
         throws Exception {
      return getObject(c, new XMLVector(str), meta);
   }
   public static <T> T getObject(final Class<T> c, final XMLVector vec, final Map<String, ?> meta) {
      return getObject(c, vec, meta, 0, vec.length());
   }
   @SuppressWarnings("unchecked")
   public static <T> T getObject(final Class<T> c, final XMLVector vec, final Map<String, ?> meta,
         final int start, final int end) {
      return (T) Proxy.newProxyInstance(c.getClassLoader(), new Class[] { c }, new ProxyMap(c
            .getClassLoader(), vec, meta, start, end));
   }
   Map<String, Object> map = new HashMap<String, Object>();
   Map<String, ?> meta = new HashMap<String, Object>();
   XMLVector vec;
   int start, end;
   private final ClassLoader cl;
   public ProxyMap(final ClassLoader cl, final XMLVector vec, final Map<String, ?> meta,
         final int start, final int end) {
      this.cl = cl;
      this.vec = vec;
      this.start = start;
      this.end = end;
      this.meta = meta;
      if (meta != null) {
         map.putAll(meta);
      }
   }
   @SuppressWarnings("unchecked")
   public Object invoke(final Object proxy, final Method m, final Object[] args) throws Throwable {
      String name = m.getName().toLowerCase();
      if (name.startsWith("get")) {
         name = name.substring(3);
      }
      final XMap xmap = m.getAnnotation(XMap.class);
      if (xmap != null) {
         if (!xmap.custom().equals(Object.class)) {
            final Method cm = xmap.custom().getMethod(name, m.getParameterTypes());
            final Constructor con = xmap.custom().getConstructor(
                  new Class[] { XMLVector.class, Integer.class, Integer.class, Map.class });
            final Object obj = cm.invoke(con.newInstance(new Object[] { vec, start, end, meta }),
                  args);
            map.put(name, obj);
            return obj;
         }
         final Class<?> r = m.getReturnType();
         Embed em = r.getAnnotation(Embed.class);
         Class<?> ac = r;
         if (r.isArray()) {
            final String cn = r.getCanonicalName();
            ac = cl.loadClass(cn.substring(0, cn.length() - 2));
            em = ac.getAnnotation(Embed.class);
            int pos[] = null;
            if (xmap.path().length() > 0) {
               pos = vec.getPosition(xmap.path(), start, end);
            } else if (xmap.rePath().length() > 0) {
               pos = vec.getPosition(Pattern.compile(xmap.rePath()), start, end);
            }
            final Object arr[] = (Object[]) Array.newInstance(ac, pos.length);
            pos = Arr.cat(pos, new int[] { end });
            if (em != null) {
               for (int i = 0; i < pos.length - 1; ++i) {
                  arr[i] = getObject(ac, vec, meta, pos[i], pos[i + 1]);
               }
            } else {
               final Constructor<?> con = ac.getConstructor(new Class<?>[] { String.class });
               for (int i = 0; i < pos.length - 1; ++i) {
                  arr[i] = con.newInstance(vec.getValue(pos[i]));
               }
            }
            if (r.isArray()) {
               map.put(name, arr);
               return arr;
            }
            if (arr.length > 0) {
               map.put(name, arr[0]);
               return arr[0];
            }
            return null;
         } else if (em != null) {
            int pos[] = null;
            if (xmap.path().length() > 0) {
               pos = vec.getPosition(xmap.path(), start, end);
            } else if (xmap.rePath().length() > 0) {
               pos = vec.getPosition(Pattern.compile(xmap.rePath()), start, end);
            }
            pos = Arr.cat(pos, new int[] { end });
            return getObject(r, vec, meta, pos[0], pos[1]);
         } else {
            String val = null;
            if (xmap.path().length() > 0) {
               val = vec.getValue(xmap.path(), start, end);
            } else if (xmap.rePath().length() > 0) {
               val = vec.getValue(Pattern.compile(xmap.rePath()), start, end);
            }
            if (((val == null) || (val.length() == 0))) {
               val = xmap.defaultValue();
            }
            try {
               final Object obj = convert(r, val);
               map.put(name, obj);
               return obj;
            } catch (final Exception e) {
               System.err.println("Error converting " + val + " to type " + r + " for method " + m);
            }
         }
      }
      if (map.containsKey(name)) {
         return map.get(name);
      }
      if (name.equals("tostring")) {
         return map.toString();
      }
      System.err.println("ProxyMap did not find entry for given method: " + name);
      return null;
   }
}
