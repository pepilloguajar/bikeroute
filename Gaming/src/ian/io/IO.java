package ian.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

public class IO {
   public static byte[] getByteArray(final InputStream is) {
      try {
         final ByteArrayOutputStream out = new ByteArrayOutputStream();
         final byte ba[] = new byte[8192];
         int read = is.read(ba);
         while (read > -1) {
            out.write(ba, 0, read);
            read = is.read(ba);
         }
         return out.toByteArray();
      } catch (final Exception e) {
         e.printStackTrace();
         return null;
      } finally {
         try {
            if (is != null) {
               is.close();
            }
         } catch (final Exception e) {
            e.printStackTrace();
         }
      }
   }
   public static byte[] getByteArray(final URL u, int timeout) {
      try {
         final URLConnection con = u.openConnection();
         con.setReadTimeout(timeout);
         return getByteArray(u.openStream());
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return new byte[] {};
   }
   public static String getString(final File f) {
      try {
         return getString(new FileInputStream(f));
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return "";
   }
   public static String getString(final File f, final String charset) {
      try {
         return getString(new FileInputStream(f), charset);
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return "";
   }
   public static String getString(final InputStream is) {
      return getString(is, "iso8859-1");
   }
   public static String getString(final InputStream is, final String charEncoding) {
      try {
         final ByteArrayOutputStream out = new ByteArrayOutputStream();
         final byte ba[] = new byte[8192];
         int read = is.read(ba);
         while (read > -1) {
            out.write(ba, 0, read);
            read = is.read(ba);
         }
         return out.toString(charEncoding);
      } catch (final Exception e) {
         e.printStackTrace();
         return null;
      } finally {
         try {
            if (is != null) {
               is.close();
            }
         } catch (final Exception e) {
            e.printStackTrace();
         }
      }
   }
   public static String getString(final URL url, int timeout) {
      try {
         final URLConnection con = url.openConnection();
         con.setReadTimeout(timeout);
         return getString(con.getInputStream());
      } catch (final Exception e) {
         e.printStackTrace();
      }
      return "";
   }
   public static byte[] getUTF8Bytes(final String str) {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      try {
         final Writer writer = new OutputStreamWriter(baos, "UTF-8");
         writer.write(str);
         writer.close();
         return baos.toByteArray();
      } catch (final Exception e) {}
      return str.getBytes();
   }
   public static Map<String, Object> getValues(final Object obj) {
      final Map<String, Object> result = new LinkedHashMap<String, Object>();
      final Class<?> currentClass = obj.getClass();
      final Method[] meth = currentClass.getMethods();
      final int count = meth.length;
      for (int i = 0; i < count; i++) {
         final Method m = meth[i];
         m.setAccessible(true);
         final String name = m.getName();
         if (name.startsWith("get") && (m.getReturnType() != null)
               && (m.getParameterTypes().length == 0)) {
            try {
               final Object v = m.invoke(obj, new Object[0]);
               result.put(name, v);
            } catch (final Exception e) {
               // e.printStackTrace();
            }
         }
      }
      return result;
   }
}
