package ian.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
   public static byte[] md5(final byte b[]) {
      MessageDigest md5;
      try {
         md5 = MessageDigest.getInstance("MD5");
         md5.update(b);
         return md5.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
   public static byte[] md5(final InputStream is) throws NoSuchAlgorithmException, IOException {
      final MessageDigest md5 = MessageDigest.getInstance("MD5");
      final byte b[] = new byte[8192];
      try {
         int read = is.read(b);
         while (read > -1) {
            md5.update(b, 0, read);
            read = is.read(b);
         }
      } finally {
         is.close();
      }
      return md5.digest();
   }
   public static byte[] md5(final String str) {
      MessageDigest md5;
      try {
         md5 = MessageDigest.getInstance("MD5");
         md5.update(str.getBytes());
         return md5.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
   public static byte[] sha160(final byte b[]) {
      MessageDigest sha160;
      try {
         sha160 = MessageDigest.getInstance("SHA-1");
         sha160.update(b);
         return sha160.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
   public static byte[] sha160(final String str) throws NoSuchAlgorithmException {
      final MessageDigest sha160 = MessageDigest.getInstance("SHA-1");
      sha160.update(str.getBytes());
      return sha160.digest();
   }
   public static byte[] sha256(final byte b[]) {
      MessageDigest sha256;
      try {
         sha256 = MessageDigest.getInstance("SHA-256");
         sha256.update(b);
         return sha256.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
   public static byte[] sha256(final String str) throws NoSuchAlgorithmException {
      final MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
      sha256.update(str.getBytes());
      return sha256.digest();
   }
   public static byte[] sha512(final byte b[]) {
      MessageDigest sha512;
      try {
         sha512 = MessageDigest.getInstance("SHA-512");
         sha512.update(b);
         return sha512.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
   public static byte[] sha512(final String str) {
      try {
         final MessageDigest sha512 = MessageDigest.getInstance("SHA-512");
         sha512.update(str.getBytes());
         return sha512.digest();
      } catch (final NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
      return null;
   }
}
