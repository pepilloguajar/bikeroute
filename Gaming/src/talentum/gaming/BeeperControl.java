package talentum.gaming;

import static java.util.concurrent.TimeUnit.*;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
public class BeeperControl {
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public void ChallengesForAnWeek() {
		final Runnable beeper = new Runnable() {
			public void run() { 
				// Enviar Retos
				System.out.println("Envia Reto"); 
			}
		};
    
		scheduler.scheduleAtFixedRate(beeper, 5, 20, SECONDS);
		
			/*scheduler.schedule(new Runnable() {
				public void run() { beeperHandle.cancel(true); }
			}, 60 * 60, SECONDS);*/
	}
}
