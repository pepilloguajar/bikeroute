   package com.talentum.sightracks.model;

import java.io.Serializable;
import java.util.Date;

public class Route implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String category;
	private Integer popularity;
	private Double distance;
	private Integer positiveSlope;
	private Integer negativeSlope;
	private String creation;
	private Integer difficulty;
	private Double startpointlat;
	private Double startpointlon;
	private Boolean publicroute;
	private String urlfilegpx;
	private String datetodo;
	
	public Route() {
        super();
    }
 
    public Route(String id, String name, String category, Integer popularity, Double distance,
    		Integer positiveSlope, Integer negativeSlope, String creation, Integer difficulty, Double startpointlat, Double startpointlon,
    		Boolean publicroute, String urlfilegpx, String datetodo) {
        super();
        this.id = id;
        this.name = name;
        this.category = category;
        this.popularity = popularity;
        this.distance = distance;
        this.positiveSlope = positiveSlope;
        this.negativeSlope = negativeSlope;
        this.creation = creation;
        this.difficulty = difficulty;
        this.publicroute = publicroute;
        this.startpointlat = startpointlat;
        this.startpointlon = startpointlon;
        this.urlfilegpx = urlfilegpx;
        this.datetodo = datetodo;
    }
    
    public String getId(){
    	return id;
    }
    
    public void setId(String id){
    	this.id = id;
    }

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getPopularity() {
		return popularity;
	}
	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Integer getPositiveSlope() {
		return positiveSlope;
	}
	public void setPositiveSlope(Integer positiveSlope) {
		this.positiveSlope = positiveSlope;
	}
	public Integer getNegativeSlope() {
		return negativeSlope;
	}
	public void setNegativeSlope(Integer negativeSlope) {
		this.negativeSlope = negativeSlope;
	}
	public String getCreation() {
		return creation;
	}
	public void setCreation(String creation) {
		this.creation = creation;
	}
	public Integer getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(Integer difficulty) {
		this.difficulty = difficulty;
	}
	public boolean isPublicroute() {
		return publicroute;
	}
	public void setPublicroute(boolean publicroute) {
		this.publicroute = publicroute;
	}

	public Double getStartpointlat() {
		return startpointlat;
	}

	public Double getStartpointlon() {
		return startpointlon;
	}

	public void setStartpointlat(Double startpointlat) {
		this.startpointlat = startpointlat;
	}

	public void setStartpointlon(Double startpointlon) {
		this.startpointlon = startpointlon;
	}

	public String getUrlfilegpx() {
		return urlfilegpx;
	}

	public void setUrlfilegpx(String urlfilegpx) {
		this.urlfilegpx = urlfilegpx;
	}

	public String getDatetodo() {
		return datetodo;
	}

	public void setDatetodo(String datetodo) {
		this.datetodo = datetodo;
	}
	
	@SuppressWarnings("deprecation")
	public Date calculateDatetodo(){
		String parts[] = this.datetodo.split("/");
		return new Date(Integer.valueOf(parts[0]), Integer.valueOf(parts[2]), Integer.valueOf(parts[1]));
	}
}
