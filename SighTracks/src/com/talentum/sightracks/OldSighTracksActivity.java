package com.talentum.sightracks;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.talentum.sightracks.RecorderManager;
import com.talentum.sightracks.model.UserWarning;
import com.talentum.sightracks.utils.PropertyLoader;
import com.talentum.sightracks.utils.RoutesSingleton;
import com.talentum.sightracks.utils.WarningsSingleton;

import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;

public class OldSighTracksActivity extends ActionBarActivity implements 
ConnectionCallbacks,
OnConnectionFailedListener,
LocationListener,
OnMyLocationButtonClickListener
{
	
	private GoogleMap mMap;
	private List<UserWarning> warnings;
	private LocationClient mLocationClient;
	private LocationManager locationManager;
	private String provider;
	private ArrayList<String> names3gp;
	
	private Location loc = null;
	
	//Metodo para parsear rutas formato GPX
	private boolean getSightFile(File file, String idroute) throws ParserConfigurationException, SAXException{
		try {
			InputStream inputStream = new FileInputStream(file);
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = docBuilder.parse(inputStream);
			if (document == null) {
				return false;
			}

			//Busca el nodo hijo que se llama wpt            
			NodeList children = document.getFirstChild().getChildNodes();
			boolean predef = true;
			boolean hasVoice = false;
			Location warningLocation = null;
			int playDistance = 0;
			int warningId = 0;
			String warningDescription = "";
			for (int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);
				if (child.getNodeName().equalsIgnoreCase("wpt")) { 
					String latText = ((Element) child).getAttribute("lat");
					String lonText = ((Element) child).getAttribute("lon");
					NodeList wptChildren = child.getChildNodes();
					for (int i = 0; i < wptChildren.getLength(); i++) {
						if(wptChildren.item(i).getNodeName().equals("name")){
							warningId = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("cmt")){
							playDistance = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("desc")){
							warningDescription = wptChildren.item(i).getFirstChild().getNodeValue();
						}
						warningLocation = new Location("parser");
						warningLocation.setLatitude(Double.parseDouble(latText));
						warningLocation.setLongitude(Double.parseDouble(lonText));
						/** Recorrer el directorio local de la ruta y ver qu?? avisos tienen voz propia **/
					}
					
					if(warningDescription.contains("AvisoPropio"))
						predef = false;
					else
						predef = true;
					//Comprueba si el id del aviso coincide con un archivo de voz
					hasVoice = false;
					for(int k=0; k<names3gp.size() && !hasVoice;k++){
						if(names3gp.get(k).equals("files3gp/"+idroute+"/"+warnings.size()+".3gp"))
							hasVoice = true;
					}
					
					UserWarning newWarning = new UserWarning(warningLocation, predef, warningId, warningDescription,hasVoice, playDistance);	  
					warnings.add(newWarning);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	
    // These settings are the same as the settings for the map. They will in fact give you updates
    // at the maximal rates currently possible.
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(Integer.valueOf(PropertyLoader.getInstance().getIntervalGPS()))         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

	
	@SuppressWarnings({ "static-access" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //evitar que la pantalla se apague
		getActionBar().hide();
		warnings = WarningsSingleton.getInstance().getWarnings();

	    // Get the location manager
	    locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
	    
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
	    
	    Location location = locationManager.getLastKnownLocation(provider);
		names3gp = getIntent().getStringArrayListExtra("files3gp");

	    new RecorderManager();

	    // Initialize the location fields
	    if (location != null) {
	      System.out.println("Provider " + provider + " has been selected.");
	    } 		

	    setContentView(R.layout.activity_new_route_mov);
	}

	
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        setUpLocationClientIfNeeded();
        mLocationClient.connect();
    }
    
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        if (mLocationClient != null) {
    	    loc = null;
            mLocationClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
    	// Do a null check to confirm that we have not already instantiated the map.
    	if (mMap == null) {
    		// Try to obtain the map from the SupportMapFragment.
    		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
    				.getMap();
    		// Check if we were successful in obtaining the map.
    		if (mMap != null) {
    			mMap.setMyLocationEnabled(true);
    			mMap.setOnMyLocationButtonClickListener(this);

    			mMap.setOnMarkerClickListener(new OnMarkerClickListener() {			
    				@Override
    				public boolean onMarkerClick(Marker marker) {
    					return false;
    				}
    			});
    		}
    	}
    }

    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }
    
    @Override
	public boolean onMyLocationButtonClick() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
    	loc = new Location(location);    	
    	
    	/***
    	 * Para cada aviso
    	 * preguntar si se encuentra dentro del radio de cada uno de ellos. Y si lo esta ejecutar servicio de musica
    	 */
    	
    	for(int i=0; i< warnings.size();i++){
    		if(warnings.get(i).getLocation().distanceTo(loc) <= warnings.get(i).getPlayDistance()){
    			Intent extras = new Intent(this, MusicService.class);
    			//stopService(extras);
    			
    			if(warnings.get(i).isPredefinedWarning()){
    				extras.putExtra("avisoPre", warnings.get(i).getWarningDescription());
    			}else{
    				extras.putExtra("avisoPre", 0);
    			}
    			
    			if(warnings.get(i).hasVoice()){
    				extras.putExtra("idroute", RoutesSingleton.getInstance().getListRoutes().get(0).getId());
    				extras.putExtra("avisoVoz", warnings.get(i).getWarningId());
    			}else{
    				extras.putExtra("avisoVoz", -1);
    			}
   				startService(extras);
   				warnings.remove(i);
   				break;
    		}
    		
    	}
    	
    	
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnected(Bundle connectionHint) {
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener

        Location location = mLocationClient.getLastLocation();
        loc = location;
        
        /*********
         * 
         * Para todas las rutas, coger la mas cercana a loc
         * De esa ruta, coger toda las informaci??n de avisos pertenecientes a ella y guardarlos en WarningSingleton
         * 
         * 
         */

        if(RoutesSingleton.getInstance().getListRoutes() == null || RoutesSingleton.getInstance().getListRoutes().size() == 0){
        	Toast.makeText(getApplicationContext(), "No se encuentra ninguna ruta para hoy", Toast.LENGTH_LONG);
        	finish();
        }
        else if(RoutesSingleton.getInstance().getListRoutes().size() == 1){
        	try {
				getSightFile(new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/",RoutesSingleton.getInstance().getListRoutes().get(0).getId()+".gpx"),RoutesSingleton.getInstance().getListRoutes().get(0).getId());

			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        else{
        	double lat,lon;
        	lat = RoutesSingleton.getInstance().getListRoutes().get(0).getStartpointlat();
        	lon = RoutesSingleton.getInstance().getListRoutes().get(0).getStartpointlon();        			
        			
        	double min = Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon);
        	int pmin = 0;
        	for(int i=1; i<RoutesSingleton.getInstance().getListRoutes().size(); i++){
            	lat = RoutesSingleton.getInstance().getListRoutes().get(i).getStartpointlat();
            	lon = RoutesSingleton.getInstance().getListRoutes().get(i).getStartpointlon();        			
        		if(Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon) < min){
        			min = Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon);
        			pmin = i;
        		}
        	}
        	try {
				getSightFile(new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/",RoutesSingleton.getInstance().getListRoutes().get(pmin).getId()+".gpx"),RoutesSingleton.getInstance().getListRoutes().get(pmin).getId());

			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }
        
        /* Se centra la camara */
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        mMap.animateCamera(cameraUpdate);  
	}



	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
	
	public void onNewMarket(BitmapDescriptor icon, LatLng l){
		mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(getResources().getString(R.string.ini_sight))
		.icon(icon));		
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
						finish();

			            //break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            break;
			        }
			    }
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.exit)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			    .setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();
			
		}

	    return super.onKeyDown(keyCode, event);
	}	
	
	public Location getLocation(){
		return loc;
	}
}
