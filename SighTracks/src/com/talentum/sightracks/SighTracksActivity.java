package com.talentum.sightracks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.talentum.sightracks.RecorderManager;
import com.talentum.sightracks.LocationService.LocalBinder;
import com.talentum.sightracks.model.UserWarning;
import com.talentum.sightracks.utils.LocationServiceListener;
import com.talentum.sightracks.utils.RoutesSingleton;
import com.talentum.sightracks.utils.WarningsSingleton;

import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.widget.Toast;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources.NotFoundException;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;

public class SighTracksActivity extends ActionBarActivity implements OnMyLocationButtonClickListener, LocationServiceListener {
	public static Notification notification;
	
	private GoogleMap mMap;

	private Location loc = null;
	private List<UserWarning> warnings;
	private ArrayList<String> names3gp;

	private LocationService locationService;
	private Intent locationServiceIntent;
	boolean mBound = false;
	private MusicService music;
	//MusicServiceSingleton serviceActive = MusicServiceSingleton.getInstance();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//evitar que la pantalla se apague
		getActionBar().hide();

		new RecorderManager();
		setContentView(R.layout.activity_new_route_mov);
		
		//Conecta con el servicio de localizacion
		//Log.i("MyMaps","bindService");
		//locationServiceIntent = (Intent) getIntent().getSerializableExtra("locationService");
		//locationServiceIntent=new Intent("com.talentum.bikeroute.LocationService");
		names3gp = getIntent().getStringArrayListExtra("files3gp");
		
		warnings = WarningsSingleton.getInstance().getWarnings();
		locationServiceIntent = new Intent(getApplicationContext(), LocationService.class);
		startService(locationServiceIntent);

		Intent i = new Intent(Intent.ACTION_MAIN);
		i.addCategory(Intent.CATEGORY_HOME);
		startActivity(i);
		
		//Pon icono de la aplicacion en la barra de notificaciones
		notification = new Notification.Builder(this)
	    .setContentTitle(getResources().getString(R.string.app_name))
	    .setContentText(getResources().getString(R.string.notification_bar_message))
	    .setSmallIcon(R.drawable.ic_launcher)
	    .build();
		notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
		NotificationManager notifier = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
		notifier.notify(1, notification);
		
	}


	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(16);
		mMap.animateCamera(cameraUpdate);
		if(!mBound){
		getApplicationContext().bindService(locationServiceIntent, mConnection, Context.BIND_ADJUST_WITH_ACTIVITY);
		}
	}

	public void onSaveInstanceState(Bundle icicle) {
		super.onSaveInstanceState(icicle);
	}

	@Override
	public void onPause() {
		super.onPause();
		if(mBound == true){
			getApplicationContext().unbindService(mConnection);
			locationService.unregisterListener(SighTracksActivity.this);
			mBound = false;
		}
	}
	
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		//Elimina el icono de la aplicacion de la barra de notificaciones
		((NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(1);
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();
			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				mMap.setMyLocationEnabled(true);
				mMap.setOnMyLocationButtonClickListener(this);

				mMap.setOnMarkerClickListener(new OnMarkerClickListener() {			
					@Override
					public boolean onMarkerClick(Marker marker) {
						return false;
					}
				});
			}
		}
	}


	@Override
	public boolean onMyLocationButtonClick() {
		if(loc != null){
			LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
			mMap.animateCamera(cameraUpdate);
		}
		return false;
	}

	public void onNewMarket(BitmapDescriptor icon, LatLng l){
		mMap.addMarker(new MarkerOptions()
		.position(l)
		.title(getResources().getString(R.string.ini_sight))
		.icon(icon));		
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){
			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which){
					case DialogInterface.BUTTON_POSITIVE:
						finish();

						//break;

					case DialogInterface.BUTTON_NEGATIVE:
						break;
					}
				}
			};
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(getResources().getString(R.string.exit)).setPositiveButton(getResources().getString(R.string.yes), dialogClickListener)
			.setNegativeButton(getResources().getString(R.string.no), dialogClickListener).show();

		}

		return super.onKeyDown(keyCode, event);
	}	

	/** Defines callbacks for service binding, passed to bindService() */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			// We've bound to LocalService, cast the IBinder and get LocalService instance
			LocalBinder binder = (LocalBinder) service;
			locationService = binder.getService();
			mBound = true;
			locationService.registerListener(SighTracksActivity.this);
			
			//getClosestRoute();
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			locationService.unregisterListener(SighTracksActivity.this);
			mBound = false;
		}
	};


	@Override
	public void onUpdate(Location location) {

		LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
		mMap.animateCamera(cameraUpdate);
		
		//Si es el primer punto recibido, ejecuta el metodo para detectar la ruta mas proxima
		if(loc == null){
			loc = new Location(location);
			getClosestRoute();
		}
		else{
			loc = new Location(location);
		}
    	
    	/***
    	 * Para cada aviso
    	 * preguntar si se encuentra dentro del radio de cada uno de ellos. Y si lo esta ejecutar servicio de musica
    	 */
    	
    	
//    	for(int i=0; i< warnings.size();i++){
//    		if(warnings.get(i).getLocation().distanceTo(loc) <= warnings.get(i).getPlayDistance()){
//    			Intent extras = new Intent(this, MusicService.class);
//    			//stopService(extras);
//    			Log.i("es predefinido??", Boolean.toString(warnings.get(i).isPredefinedWarning()));
//    			
//    			if(warnings.get(i).isPredefinedWarning()){
//    				extras.putExtra("avisoPre", warnings.get(i).getWarningDescription());
//    			}else{
//    				extras.putExtra("avisoPre", 0);
//    			}
//    			Log.i("es voz??", Boolean.toString(warnings.get(i).hasVoice()));
//    			
//    			if(warnings.get(i).hasVoice()){
//    				extras.putExtra("idroute", RoutesSingleton.getInstance().getListRoutes().get(0).getId());
//    				extras.putExtra("avisoVoz", warnings.get(i).getWarningId());
//    			}else{
//    				extras.putExtra("avisoVoz", -1);
//    			}
//    			MusicServiceSingleton serviceActive = MusicServiceSingleton.getInstance();
//    			while(serviceActive.getMusicActive()){
//    				Log.i("bucle", "aki entra");
//    			}
//    			
//   				startService(extras);
//   				warnings.remove(i);
//   				break;
//    		}
//    		
//    	}
        
	}
	
	private boolean getSightFile(File file, String idroute) throws ParserConfigurationException, SAXException{
		try {
			InputStream inputStream = new FileInputStream(file);
			DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = docBuilder.parse(inputStream);
			if (document == null) {
				return false;
			}

			//Busca el nodo hijo que se llama wpt            
			NodeList children = document.getFirstChild().getChildNodes();
			boolean predef = true;
			boolean hasVoice = false;
			Location warningLocation = null;
			int playDistance = 0;
			int warningId = 0;
			String warningDescription = "";
			for (int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);
				if (child.getNodeName().equalsIgnoreCase("wpt")) { 
					String latText = ((Element) child).getAttribute("lat");
					String lonText = ((Element) child).getAttribute("lon");
					NodeList wptChildren = child.getChildNodes();
					for (int i = 0; i < wptChildren.getLength(); i++) {
						if(wptChildren.item(i).getNodeName().equals("name")){
							warningId = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("cmt")){
							playDistance = Integer.parseInt(wptChildren.item(i).getFirstChild().getNodeValue());
						}
						if(wptChildren.item(i).getNodeName().equals("desc")){
							warningDescription = wptChildren.item(i).getFirstChild().getNodeValue();
						}
						warningLocation = new Location("parser");
						warningLocation.setLatitude(Double.parseDouble(latText));
						warningLocation.setLongitude(Double.parseDouble(lonText));
						/** Recorrer el directorio local de la ruta y ver qu?? avisos tienen voz propia **/
					}
					
					if(warningDescription.contains("ap"))
						predef = false;
					else
						predef = true;
					//Comprueba si el id del aviso coincide con un archivo de voz
					hasVoice = false;
					for(int k=0; k<names3gp.size();k++){
						if(names3gp.get(k).equals("files3gp/"+idroute+"/"+warnings.size()+".3gp"))
							hasVoice = true;
					}
					
					UserWarning newWarning = new UserWarning(warningLocation, predef, warningId, warningDescription,hasVoice, playDistance);	  
					warnings.add(newWarning);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private void getClosestRoute(){
        /*********
         * 
         * Para todas las rutas, coger la mas cercana a loc
         * De esa ruta, coger toda las informaci??n de avisos pertenecientes a ella y guardarlos en WarningSingleton
         * 
         * 
         */

        if(RoutesSingleton.getInstance().getListRoutes() == null || RoutesSingleton.getInstance().getListRoutes().size() == 0){
        	Toast.makeText(getApplicationContext(), getResources().getString(R.string.noroutetoday), Toast.LENGTH_LONG).show();
        	finish();
        	System.runFinalizersOnExit(true);
        
        }
        else if(RoutesSingleton.getInstance().getListRoutes().size() == 1){
        	Toast.makeText(getApplicationContext(), getResources().getString(R.string.route)+RoutesSingleton.getInstance().getListRoutes().get(0).getName()+getResources().getString(R.string.ejecute), Toast.LENGTH_LONG).show();
        	try {
				getSightFile(new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/",RoutesSingleton.getInstance().getListRoutes().get(0).getId()+".gpx"),RoutesSingleton.getInstance().getListRoutes().get(0).getId());

			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        else{
        	double lat,lon;
        	lat = RoutesSingleton.getInstance().getListRoutes().get(0).getStartpointlat();
        	lon = RoutesSingleton.getInstance().getListRoutes().get(0).getStartpointlon();        			
        			
        	double min = Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon);
        	int pmin = 0;
        	for(int i=1; i<RoutesSingleton.getInstance().getListRoutes().size(); i++){
            	lat = RoutesSingleton.getInstance().getListRoutes().get(i).getStartpointlat();
            	lon = RoutesSingleton.getInstance().getListRoutes().get(i).getStartpointlon();        			
        		if(Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon) < min){
        			min = Math.hypot(loc.getLatitude()-lat, loc.getLongitude()-lon);
        			pmin = i;
        		}
        	}
        	try {
            	Toast.makeText(getApplicationContext(), getResources().getString(R.string.route)+RoutesSingleton.getInstance().getListRoutes().get(pmin).getName()+getResources().getString(R.string.ejecute), Toast.LENGTH_LONG).show();
				getSightFile(new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/",RoutesSingleton.getInstance().getListRoutes().get(pmin).getId()+".gpx"),RoutesSingleton.getInstance().getListRoutes().get(pmin).getId());

			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}        	
        }
	}


}

