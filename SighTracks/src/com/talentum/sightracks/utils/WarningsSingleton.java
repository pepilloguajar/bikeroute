package com.talentum.sightracks.utils;

import java.util.ArrayList;
import java.util.List;

import com.talentum.sightracks.model.UserWarning;

public class WarningsSingleton {
	private static WarningsSingleton INSTANCE = null;
	
	
	private List<UserWarning> warnings;
	
	private WarningsSingleton(){
		warnings = new ArrayList<UserWarning>();
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new WarningsSingleton();
        }
    }

    public static WarningsSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    
    public List<UserWarning> getWarnings() {
		return warnings;
	}

	public void addWarning(UserWarning warning) {
		if(warnings != null){
			warnings.add(warning);
		}
	}

	public void clear(){
		warnings = null;
    }
    
	public int getSize(){
		return warnings.size();
	}
}
