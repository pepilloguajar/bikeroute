package com.talentum.sightracks.utils;



public class MusicServiceSingleton {
private static MusicServiceSingleton INSTANCE = null;
	
	
	private boolean serviceActive;
	
	private MusicServiceSingleton(){
		serviceActive = false;
	}
	
    private synchronized static void createInstance() {
        if (INSTANCE == null) { 
            INSTANCE = new MusicServiceSingleton();
        }
    }

    public static MusicServiceSingleton getInstance() {
        createInstance();
        return INSTANCE;
    }
    
    public boolean getMusicActive(){
    	return serviceActive;
    }
    
    public void setMusicAvtive(boolean serviceActive){
    	this.serviceActive = serviceActive;
    }
}
