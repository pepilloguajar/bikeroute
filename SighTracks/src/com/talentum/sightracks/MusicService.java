package com.talentum.sightracks;


import com.talentum.sightracks.utils.MusicServiceSingleton;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

public class MusicService extends Service{
	
	MediaPlayer aviso;
	MediaPlayer avisoRandom;
	//MediaPlayer avisoVoz;
    RecorderManager recorderManager;

    @Override
    public void onCreate() {
          recorderManager = new RecorderManager();
    }

    @Override
    public int onStartCommand(Intent intenc, int flags, int idArranque) {
    	
    	
    	MusicServiceSingleton serviceActive =  MusicServiceSingleton.getInstance();
    	serviceActive.setMusicAvtive(true);
    	
    	String idRoute;
        String avisoPre = intenc.getExtras().get("avisoPre").toString();
        String avisoVoz = intenc.getExtras().get("avisoVoz").toString();
        int random = (int) (Math.random()*3);

        
        //Compruebo que hay aviso predefinido
        if(avisoPre.equalsIgnoreCase("0")){
        	idRoute = intenc.getExtras().get("idroute").toString();
        	recorderManager.startPlaying(
					Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.sightracks/files3gp/"+idRoute+"/"+avisoVoz+".3gp");
		}else{ //Si hay predefinido buscamos el aviso a reproducir y comprobamos si hay tambi??????n de voz
			aviso = MediaPlayer.create(this, Uri.parse( "android.resource://com.talentum.sightracks/raw/"+avisoPre));
    		if( random >= 0 && random < 1 ){
    			avisoRandom = MediaPlayer.create(this, R.raw.attention);
    		}else if(random >= 1 && random < 2){
    			avisoRandom = MediaPlayer.create(this, R.raw.attentive);
    		}else{
    			avisoRandom = MediaPlayer.create(this, R.raw.care);
    		}
    		Log.i("MusicService","Antes de start");
			avisoRandom.start();
			try {
      			Thread.sleep(avisoRandom.getDuration());
      		} catch(InterruptedException e) {}
			avisoRandom.stop();
			aviso.start();
    		try {
      			Thread.sleep(aviso.getDuration() + 2000);
      		} catch(InterruptedException e) {}
    		if(!avisoVoz.equalsIgnoreCase("-1")){ // Reproduzco aviso de voz
	      		idRoute = intenc.getExtras().get("idroute").toString();
	      		recorderManager.startPlaying(
						Environment.getExternalStorageDirectory() + "/Android/data/com.talentum.sightracks/files3gp/"+idRoute+"/"+avisoVoz+".3gp");
    		}
    		aviso.stop();
	     }
        serviceActive.setMusicAvtive(false);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
          aviso.stop();
    }

    @Override
    public IBinder onBind(Intent intencion) {
          return null;
    }

}
