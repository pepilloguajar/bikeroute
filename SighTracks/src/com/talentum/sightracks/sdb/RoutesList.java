package com.talentum.sightracks.sdb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.util.Log;

import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.PutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;
import com.talentum.sightracks.MainActivity;
import com.talentum.sightracks.model.Route;
import com.talentum.sightracks.utils.PropertyLoader;

public class RoutesList {
	
	protected String nextToken;
	
	/** Atributos del dominio **/
	private static final String NAME_ATTRIBUTE = "Name";
	private static final String CATEGORY_ATTRIBUTE = "Category";
	private static final String POPULARITY_ATTRIBUTE = "Popularity";
	private static final String DISTANCE_ATTRIBUTE = "Distance";
	private static final String POSITIVESLOPE_ATTRIBUTE = "PositiveSlope";
	private static final String NEGATIVESLOPE_ATTRIBUTE = "NegativeSlope";
	private static final String CREATIONDATE_ATTRIBUTE = "CreationDate";
	private static final String DIFFICULTY_ATTRIBUTE = "Difficulty";
	private static final String STARTPOINTLAT_ATTRIBUTE = "StartPointLat";
	private static final String STARTPOINTLON_ATTRIBUTE = "StartPointLon";
	private static final String PUBLICROUTE_ATTRIBUTE = "PublicRoute";
	private static final String URLFILEGPX_ATTRIBUTE = "UrlFileGPX";
	private static final String TODODATE_ATTRIBUTE = "TodoDate";
	
	/** Consultas sobre SimpleDB **/
	private static final String ROUTES = "Select * from BR_MovistarRoutes";
	private static final String TODAYROUTES = "Select * from BR_MovistarRoutes where TodoDate = ";
	
	private static final String ROUTES_DOMAIN = "BR_MovistarRoutes";

	
	
	/**
	 * Converts a item list to List of Routes
	 * @param items
	 * @return
	 */
	protected List<Route> convertItemListToRouteList( List<Item> items ) {
		List<Route> scores = new ArrayList<Route>( items.size() );
		for ( Item item : items ) {
			scores.add( this.convertItemToRoute( item ) );
		}
		return scores;
	}
	
	/**
	 * Convert Item to Route
	 * @param item
	 * @return
	 */
	protected Route convertItemToRoute( Item item ) {
		return new Route( item.getName(),this.getNameForItem( item ), this.getCategoryForItem(item), this.getPopularityForItem(item), this.getDistanceForItem(item), this.getPositiveSlopeForItem(item), this.getNegativeSlopeForItem(item), this.getCreationDateForItem(item), this.getDifficultyForItem(item), this.getStartPointLatForItem(item), this.getStartPointLonForItem(item), this.getPublicRouteForItem(item), this.getFileGPXRouteForItem(item), this.getTodoDateForItem(item));
	}		

	/************ METHODS TO GET SPECIFIC ATTRIBUTES FOR ITEM ****************/
	protected String getNameForItem( Item item ) {
		return this.getStringValueForAttributeFromList( NAME_ATTRIBUTE, item.getAttributes() );
	}
	
	protected String getCategoryForItem( Item item ) {
		return this.getStringValueForAttributeFromList( CATEGORY_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getPopularityForItem( Item item ) {
		return this.getIntValueForAttributeFromList( POPULARITY_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Double getDistanceForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( DISTANCE_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected Integer getPositiveSlopeForItem( Item item ) {
		return this.getIntValueForAttributeFromList( POSITIVESLOPE_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Integer getNegativeSlopeForItem( Item item ) {
		return this.getIntValueForAttributeFromList( NEGATIVESLOPE_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected String getCreationDateForItem( Item item ) {
		return this.getStringValueForAttributeFromList( CREATIONDATE_ATTRIBUTE, item.getAttributes() );
	}		
	
	protected Integer getDifficultyForItem( Item item ) {
		return this.getIntValueForAttributeFromList( DIFFICULTY_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected Double getStartPointLatForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( STARTPOINTLAT_ATTRIBUTE, item.getAttributes() );
	}
	
	protected Double getStartPointLonForItem( Item item ) {
		return this.getDoubleValueForAttributeFromList( STARTPOINTLON_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected Boolean getPublicRouteForItem( Item item ) {
		return this.getBooleanValueForAttributeFromList( PUBLICROUTE_ATTRIBUTE, item.getAttributes() );
	}	
	
	protected String getFileGPXRouteForItem( Item item ){
		return this.getStringValueForAttributeFromList(URLFILEGPX_ATTRIBUTE, item.getAttributes());
	}
	
	protected String getTodoDateForItem( Item item ) {
		return this.getStringValueForAttributeFromList( TODODATE_ATTRIBUTE, item.getAttributes() );
	}		
	
	/**
	 * Get String value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected String getStringValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return attribute.getValue();
			}
		}
		
		return "";		
	}
	
	/**
	 * Get Date value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 *
	protected Date getDateValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		Date date = new Date();
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");	
				try {
					date = formatter.parse(attribute.getValue());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return date;		
	}	*/

	/**
	 * Get int value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected int getIntValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Integer.parseInt( attribute.getValue() );
			}
		}
		
		return 0;		
	}
	
	/**
	 * Get double value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected double getDoubleValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				return Double.parseDouble( attribute.getValue() );
			}
		}
		
		return 0;		
	}	

	/**
	 * Get boolean value for attribute from list
	 * @param attributeName
	 * @param attributes
	 * @return
	 */
	protected boolean getBooleanValueForAttributeFromList( String attributeName, List<Attribute> attributes ) {
		for ( Attribute attribute : attributes ) {
			if ( attribute.getName().equals( attributeName ) ) {
				if(Integer.parseInt(attribute.getValue()) == 1)
					return true;
			}
		}
		return false;
	}	
	
	/**
	 * Get all routes
	 * @return
	 */
	public synchronized List<Route> getRoutes() {
		String query = ROUTES;
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = MainActivity.clientManager.sdb().select( selectRequest );
		this.nextToken = response.getNextToken();
		List<Route> lroute = this.convertItemListToRouteList( response.getItems() );
		Collections.reverse(lroute);
		return lroute;	
	}
	
	/**
	 * Get today routes
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public synchronized List<Route> getTodayRoutes() {
		Date date = new Date();

		
		String query = TODAYROUTES + "'" + String.valueOf(date.getYear()+"/"+date.getMonth()+"/"+date.getDate())+"'";
		
		SelectRequest selectRequest = new SelectRequest( query ).withConsistentRead( true );
		selectRequest.setNextToken( this.nextToken );
		
		SelectResult response = MainActivity.clientManager.sdb().select( selectRequest );
		this.nextToken = response.getNextToken();
		List<Route> lroute = this.convertItemListToRouteList( response.getItems() );
		Collections.reverse(lroute);
		return lroute;	
	}

	@SuppressWarnings("unchecked")
	public synchronized List<Route> getNextPageOfScores() {
		if ( this.nextToken == null ) {
			return Collections.EMPTY_LIST;
		}
		else {
			return this.getRoutes();
		}
	}
	
	   /*
  * Change name and publicroute attributes it to the Routes domain
  */
	public void deleteRoute( Route route ) {
		DeleteAttributesRequest par = new DeleteAttributesRequest(ROUTES_DOMAIN, route.getId());		
		try {
			MainActivity.clientManager.sdb().deleteAttributes(par);
			MainActivity.clientManager.s3().deleteObject(PropertyLoader.getInstance().getBucketGPX(), route.getId()+".gpx");
		    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
            .withBucketName("sightracksteam").withPrefix("files3gp/"+route.getId()+"/");	
		    List<S3ObjectSummary> list = MainActivity.clientManager.s3().listObjects(listObjectsRequest).getObjectSummaries();
		    
		    if(list != null){
			    for(int i=0; i<list.size(); i++){
    		    	Log.i("DrawRouteActivity", list.get(i).getKey());
			    	MainActivity.clientManager.s3().deleteObject("sightracksteam", list.get(i).getKey());
			    }
		    }
			MainActivity.clientManager.s3().deleteBucket(PropertyLoader.getInstance().getBucket3GP()+"/"+route.getId());
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}	
	
	
	   /*
     * Change name and publicroute attributes it to the Routes domain
     */
	public void changeAttrListRoute( Route route ) {
		String ispublic;
		if(route.isPublicroute()){
			ispublic = "1";
		}else{
			ispublic = "0";
		}
		
		ReplaceableAttribute nameAttribute = new ReplaceableAttribute( NAME_ATTRIBUTE, route.getName(), Boolean.TRUE );
		ReplaceableAttribute publicAttribute = new ReplaceableAttribute( PUBLICROUTE_ATTRIBUTE, ispublic, Boolean.TRUE );
		
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(2);
		attrs.add( nameAttribute );
		attrs.add( publicAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( ROUTES_DOMAIN, route.getId(), attrs);		
		try {
			MainActivity.clientManager.sdb().putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
	
	
	
	   /*
  * Add a new route it to the Routes domain
  */
	public void addNewRoute( Route route ) {
		String ispublic;
		if(route.isPublicroute()){
			ispublic = "1";
		}else{
			ispublic = "0";
		}
		
		ReplaceableAttribute nameAttribute = new ReplaceableAttribute( NAME_ATTRIBUTE, route.getName(), Boolean.FALSE );
		ReplaceableAttribute categoryAttribute = new ReplaceableAttribute( CATEGORY_ATTRIBUTE, route.getCategory(), Boolean.FALSE );
		ReplaceableAttribute popularityAttribute = new ReplaceableAttribute( POPULARITY_ATTRIBUTE, route.getPopularity().toString(), Boolean.FALSE );
		ReplaceableAttribute distanceAttribute = new ReplaceableAttribute( DISTANCE_ATTRIBUTE, route.getDistance().toString(), Boolean.FALSE );
		ReplaceableAttribute positiveslopeAttribute = new ReplaceableAttribute( POSITIVESLOPE_ATTRIBUTE, route.getPositiveSlope().toString(), Boolean.FALSE );
		ReplaceableAttribute negativeslopeAttribute = new ReplaceableAttribute( NEGATIVESLOPE_ATTRIBUTE, route.getNegativeSlope().toString(), Boolean.FALSE );
		ReplaceableAttribute creationdateAttribute = new ReplaceableAttribute( CREATIONDATE_ATTRIBUTE, route.getCreation().toString(), Boolean.FALSE );
		ReplaceableAttribute difficultyAttribute = new ReplaceableAttribute( DIFFICULTY_ATTRIBUTE, route.getDifficulty().toString(), Boolean.FALSE );
		ReplaceableAttribute startpointlatAttribute = new ReplaceableAttribute( STARTPOINTLAT_ATTRIBUTE, route.getStartpointlat().toString(), Boolean.FALSE );
		ReplaceableAttribute startpointlonAttribute = new ReplaceableAttribute( STARTPOINTLON_ATTRIBUTE, route.getStartpointlon().toString(), Boolean.FALSE );
		ReplaceableAttribute publicAttribute = new ReplaceableAttribute( PUBLICROUTE_ATTRIBUTE, ispublic, Boolean.FALSE );
		ReplaceableAttribute urlfileGPXAttribute = new ReplaceableAttribute( URLFILEGPX_ATTRIBUTE, route.getUrlfilegpx(), Boolean.FALSE );
		ReplaceableAttribute TodoDateAttribute = new ReplaceableAttribute( TODODATE_ATTRIBUTE, route.getDatetodo().toString(), Boolean.FALSE );
		
		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(12);
		attrs.add( nameAttribute );
		attrs.add( categoryAttribute );
		attrs.add( popularityAttribute );
		attrs.add( distanceAttribute );
		attrs.add( positiveslopeAttribute );
		attrs.add( negativeslopeAttribute );
		attrs.add( creationdateAttribute );
		attrs.add( difficultyAttribute );
		attrs.add( startpointlatAttribute );
		attrs.add( startpointlonAttribute );
		attrs.add( publicAttribute );
		attrs.add( urlfileGPXAttribute );
		attrs.add( TodoDateAttribute );
		
		PutAttributesRequest par = new PutAttributesRequest( ROUTES_DOMAIN, route.getId(), attrs);		
		try {
			MainActivity.clientManager.sdb().putAttributes(par);
		}
		catch ( Exception exception ) {
			System.out.println( "EXCEPTION = " + exception );
		}
	}
	
	
	
}




