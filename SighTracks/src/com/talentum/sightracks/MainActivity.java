/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.talentum.sightracks;



import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.tvmclient.AmazonClientManager;
import com.amazonaws.tvmclient.Response;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.talentum.sightracks.sdb.RoutesList;
import com.talentum.sightracks.utils.PropertyLoader;
import com.talentum.sightracks.utils.RoutesSingleton;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;

public class MainActivity extends Activity {	
	public static AmazonClientManager clientManager = null;
	ArrayList<String> names3gp;

	private boolean gpsenabled(){
		boolean gpsenabled = false;
	    String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
	    if(provider.contains("gps")){ //if gps is disabled
	    	gpsenabled = true;
	    }
	    return gpsenabled;
	}
	
    /**
     * Escribe un inputstream con nombre name en la carpeta de la aplicaci??n
     * @param inputStream
     * @param name
     */
    private void writefilesound(InputStream inputStream, String name){
    	OutputStream outputStream = null;
     
    	try {
    		// write the inputStream to a FileOutputStream
    		outputStream = 
                        new FileOutputStream(new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder), name));
     
    		int read = 0;
    		byte[] bytes = new byte[1024];
     
    		while ((read = inputStream.read(bytes)) != -1) {
    			outputStream.write(bytes, 0, read);
    		}
          
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (inputStream != null) {
    			try {
    				inputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    		if (outputStream != null) {
    			try {
    				// outputStream.flush();
    				outputStream.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}     
    		}
    	}    	
    }


	
	/**
	 * Check Test Connection Internet 
	 * @return
	 */
	private boolean checkOnlineState() {
	    ConnectivityManager CManager =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo NInfo = CManager.getActiveNetworkInfo();
	    if (NInfo != null && NInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	
	/**
	 * Check Google Play Services
	 * @return
	 */
	private boolean checkGooglePlayServices() {
		if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == ConnectionResult.SUCCESS){
			return true;
		}
		else{
			try {
				GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), this, 1).show();
			} catch (Exception e) {
			}
			return false;
		}

	}

	protected void displayErrorConnection() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setTitle(getResources().getString(R.string.error_internet));
		confirm.setMessage(getResources().getString(R.string.error_desinternet));
		confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		confirm.show().show();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		names3gp = new ArrayList<String>();
		clientManager = new AmazonClientManager(getSharedPreferences("com.talentum.bikeroute.AWSDemo", Context.MODE_PRIVATE));
		//Comprueba si los servicios de google y la conectividad estan disponibles
		checkGooglePlayServices();
		if(checkOnlineState()){
			if (!MainActivity.clientManager.hasCredentials()) {
				this.displayCredentialsIssueAndExit();
				finish();
			} else if (!MainActivity.clientManager.isLoggedIn()) {
				startActivity(new Intent(MainActivity.this,LoginActivity.class));
				finish();
			} else {
				new GetRoutes().execute();
			}			
		}
		else{
			displayErrorConnection();
		}
	}

	/**
	 * Al reanudar
	 */
	/*protected void onResume() {
		super.onResume();
		if (!MainActivity.clientManager.isLoggedIn()) {
			Log.i("MainActivity", "NO esta LOGEADO RESUMMEEEEE----");
			startActivity(new Intent(MainActivity.this,LoginActivity.class));
		} else {
			Log.i("MainActivity", "CARGA RUTAS RESUMEEEE----");
			new GetRoutes().execute();
		}
	}*/

	protected void displayCredentialsIssueAndExit() {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		confirm.setMessage(getResources().getString(R.string.error_userpassword));
		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

	/**
	 * Muestra el error por fallo del Login
	 * @param response
	 */
	protected void displayErrorAndExit(Response response) {
		AlertDialog.Builder confirm = new AlertDialog.Builder(this);
		if (response == null) {
			confirm.setTitle("Error Code Unkown");
			confirm.setMessage("Please review the README file.");
		} else {
			confirm.setTitle("Error Code [" + response.getResponseCode() + "]");
			confirm.setMessage(response.getResponseMessage()
					+ "\nPlease review the README file.");
		}

		confirm.setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		confirm.show().show();
	}

	
	/***************** TAREAS AS??NCRONAS *****************/
	/**
	 * Obtiene las Rutas y las almacena en RoutesSingleton (para poder estar accessibles desde cualquier sitio)
	 * @author escabia
	 *
	 */
	private class GetRoutes extends AsyncTask<Void, Void, Void> {
       @Override
       protected void onPreExecute() {
	        super.onPreExecute();
	        int currentOrientation = getResources().getConfiguration().orientation; 
	        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	        }
	        else {
	             setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
	        }
       }

       @Override
       protected Void doInBackground(Void... arg0) {
    	   RoutesSingleton listRoutes = RoutesSingleton.getInstance();
           RoutesList rlist = new RoutesList();
           listRoutes.setListRoutes(rlist.getTodayRoutes());
		   File file;
           
           if(listRoutes.getListRoutes().size() > 0){
               File mainfolder = new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder));
               if(!mainfolder.exists()){
               boolean success = true;
	               if (!mainfolder.exists()) {
	                   success = mainfolder.mkdir();
	               }
               }
        	   
               File foldergpx = new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/");
               if(!foldergpx.exists()){
               boolean success = true;
	               if (!foldergpx.exists()) {
	                   success = foldergpx.mkdir();
	               }
               }
               File folderaudio = new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"files3gp/");
               if(!folderaudio.exists()){
	                boolean success = true;
                   success = folderaudio.mkdir();
               }
               
               for(int i=0; i<listRoutes.getListRoutes().size(); i++){
                   File segfolder = new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"files3gp/"+String.valueOf(listRoutes.getListRoutes().get(i).getId())+"/");
                   if(!segfolder.exists()){
    	                boolean success = true;
                       success = segfolder.mkdir();
                   }
            	   
	   				try{
						/**********/
					    ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
			            .withBucketName("sightracksteam").withPrefix("files3gp/"+listRoutes.getListRoutes().get(i).getId()+"/");	
					   
					    
					    ObjectListing objectListing;

					    do {
					    	objectListing = MainActivity.clientManager.s3().listObjects(listObjectsRequest);
					    	for (S3ObjectSummary objectSummary : 
					    		objectListing.getObjectSummaries()) {
			    		    	InputStream in = clientManager.s3().getObject("sightracksteam", objectSummary.getKey()).getObjectContent();
			    		    	writefilesound(in, objectSummary.getKey());
			    		    	names3gp.add(objectSummary.getKey());
					    	}
					    	listObjectsRequest.setMarker(objectListing.getNextMarker());
					    } while (objectListing.isTruncated());  					
						/************/					    
					    
						clientManager.s3().getBucketAcl(PropertyLoader.getInstance().getBucketGPX());
						GetObjectRequest gor = new GetObjectRequest(PropertyLoader.getInstance().getBucketGPX(),listRoutes.getListRoutes().get(i).getId()+".gpx");
						S3Object object =clientManager.s3().getObject(gor);
						InputStream reader = new BufferedInputStream(object.getObjectContent());
						
						file = new File(Environment.getExternalStorageDirectory() + getResources().getString(R.string.localfolder) +"filesgpx/",listRoutes.getListRoutes().get(i).getId()+".gpx");
						OutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
						int read = -1;
	
						while ( ( read = reader.read() ) != -1 ) {
						    writer.write(read);
						}
						writer.flush();
						writer.close();
						reader.close();
					} catch (Exception exception) {
					}        		   
        	   }
           }
           
           
           return null;
       }


       @Override
       protected void onPostExecute(Void result) {
    	   super.onPostExecute(result);         

    	   if(gpsenabled()){
    		   Intent i = new Intent(MainActivity.this, SighTracksActivity.class);
    		   i.putStringArrayListExtra("files3gp", names3gp);
    		   startActivity(i);
    		   finish();
    		   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    	   }
    	   else{
    		   //        	   Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_gpsdis), Toast.LENGTH_LONG).show();
    		   AlertDialog.Builder confirm = new AlertDialog.Builder(MainActivity.this);
    		   confirm.setTitle(getResources().getString(R.string.error_enter));
    		   confirm.setMessage(getResources().getString(R.string.error_gpsdis));
    		   confirm.setNegativeButton(getResources().getString(R.string.accept), new DialogInterface.OnClickListener() {
    			   public void onClick(DialogInterface dialog, int which) {
    				   System.exit(0);
    			   }
    		   });
    		   confirm.show().show();
    	   }
       }
	}	   
}
