package com.talentum.sightracks;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.talentum.sightracks.model.UserWarning;
import com.talentum.sightracks.utils.LocationServiceListener;
import com.talentum.sightracks.utils.MusicServiceSingleton;
import com.talentum.sightracks.utils.PropertyLoader;
import com.talentum.sightracks.utils.RoutesSingleton;
import com.talentum.sightracks.utils.WarningsSingleton;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.Toast;

public class LocationService extends Service implements
	LocationListener,
	GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener{
	
	private List<UserWarning> warnings;
	
	private final IBinder mBinder = new LocalBinder();
	
    private LocationManager locationManager;
    private String provider;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    
    private Location location;
    
    private final ArrayList<LocationServiceListener> mListeners
    = new ArrayList<LocationServiceListener>();
    
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setInterval(Integer.valueOf(PropertyLoader.getInstance().getIntervalGPS()))         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    
    @Override
    public void onCreate(){
    	
    	// Get the location manager
    	warnings = WarningsSingleton.getInstance().getWarnings();
	    locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);
	    
	    // Define the criteria how to select the locatioin provider -> use
	    // default
	    Criteria criteria = new Criteria();
	    provider = locationManager.getBestProvider(criteria, false);
	    setUpLocationClientIfNeeded();
        mLocationClient.connect();
    }

	@Override
	public IBinder onBind(Intent intent) {
		//register intent aqui
		return mBinder;
	}
	
    public class LocalBinder extends Binder {
    	LocationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return LocationService.this;
        }
    }

	@Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
    }

	@Override
	public void onConnected(Bundle bundle) {
        //mConnectionStatus.setText(R.string.connected);
        
        mLocationClient.requestLocationUpdates(
                REQUEST,
                this);  // LocationListener

        Location location = mLocationClient.getLastLocation();
        
        /* Se centra la camara */
        //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        //CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        //mMap.animateCamera(cameraUpdate);
		
	}

	@Override
	public void onDisconnected() {
        //mConnectionStatus.setText(R.string.connected);		
	}

	//Metodo que avisa cuando la localizacion cambia. Es llamado por el sistema cuando detecta un cambio de localizacion.
	@SuppressWarnings("deprecation")
	//Es el que provoca el error
	@Override
	public void onLocationChanged(Location location) {
    	String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        sendUpdate(location);
        
        // aqui en el i<3 es la ventana para buscar en los 3 siguiente avisos. Si se tiene seguridad de que no se va a perder la cobertura podría ponerse i<1 ó i<2
    	for(int i=0; i< warnings.size() && i < 3 ;i++){
    		if(warnings.get(i).getLocation().distanceTo(location) <= warnings.get(i).getPlayDistance()){
    			Intent extras = new Intent(this, MusicService.class);
    			//stopService(extras);
    			
    			if(warnings.get(i).isPredefinedWarning()){
    				extras.putExtra("avisoPre", warnings.get(i).getWarningDescription());
    			}else{
    				extras.putExtra("avisoPre", 0);
    			}
    			
    			if(warnings.get(i).hasVoice()){
    				extras.putExtra("idroute", RoutesSingleton.getInstance().getListRoutes().get(0).getId());
    				extras.putExtra("avisoVoz", warnings.get(i).getWarningId());
    			}else{
    				extras.putExtra("avisoVoz", -1);
    			}
    			MusicServiceSingleton serviceActive = MusicServiceSingleton.getInstance();
    			while(serviceActive.getMusicActive()){
    			}
    			
   				startService(extras);
   				warnings.remove(i);
   				if(i > 0){
   					i--;
   					warnings.remove(i);
   					if(i > 0){
   						i--;
   						warnings.remove(i);
   					}
   				}
   				if(warnings.size()==0){
   					//Toast.makeText(getApplicationContext(), getResources().getString(R.string.end_route), Toast.LENGTH_LONG).show();
   					SighTracksActivity.notification.setLatestEventInfo(getApplicationContext(), getResources().getString(R.string.app_name), getResources().getString(R.string.notification_bar_message_end), null);
   					
   					((NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(1);
   					SighTracksActivity.notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
   					NotificationManager notifier = (NotificationManager)this.getSystemService(Context.NOTIFICATION_SERVICE);
   					notifier.notify(1, SighTracksActivity.notification);
   					//System.exit(0);
   				}
   				break;
    		}
    		
    	}

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		return Service.START_NOT_STICKY;
		
	}
	
    public boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            return false;
        }
    }
    
    
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
    }
    
    private void setUpLocationClientIfNeeded() {
        if (mLocationClient == null) {
            mLocationClient = new LocationClient(
                    getApplicationContext(),
                    this,  // ConnectionCallbacks
                    this); // OnConnectionFailedListener
        }
    }
    
    public void registerListener(LocationServiceListener listener) {
        mListeners.add(listener);
        //Actualiza al listener que se acaba de registrar
        if(location != null){
        mListeners.get(mListeners.size()-1).onUpdate(location);
        }
    }

    public void unregisterListener(LocationServiceListener listener) {
        mListeners.remove(listener);
    }

    private void sendUpdate(Location location) {
        for (int i=mListeners.size()-1; i>=0; i--) {
            mListeners.get(i).onUpdate(location);
        }
    }
    
//    public class MyBinder extends Binder {
//        LocationService getService() {
//            return LocationService.this;
//        }
//    }

}
