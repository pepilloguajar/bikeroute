package com.talentum.sightracks;

import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;

public class RecorderManager extends MediaPlayer{


	private static final String LOG_TAG = "AudioRecordTest";
	//private final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath();

	private MediaPlayer   mPlayer = null;

	public RecorderManager(){
		mPlayer = new MediaPlayer();
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
	        @Override
	        public void onCompletion(MediaPlayer mp) {
	        	mPlayer.stop();
	        	mPlayer.reset();
	        }
	        	
	        });
	}


	public void startPlaying(String mFileName) {
		if(!mPlayer.isPlaying()){
			try {
				mPlayer.setDataSource(mFileName);
				mPlayer.prepare();
				mPlayer.start();
			} catch (IOException e) {
			}
		}
	}
	
}
